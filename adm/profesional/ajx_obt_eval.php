<?php
/**
 * Complemento ajax para obtener los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/admtbl_evaluaciones.class.php';
    $objEval = new AdmtblEvaluaciones();
    
    $objEval->select($_GET["id"]);
    if ($objEval->id_evaluacion > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["id_tipo_eval"] = $objEval->id_tipo_eval;
        $ajx_datos["evaluacion"] = utf8_encode($objEval->evaluacion);
        $ajx_datos["id_result_eval"] = $objEval->id_result_eval;
        $ajx_datos["fecha_ini"] = date("d/m/Y", strtotime($objEval->fecha_ini));
        $ajx_datos["fecha_fin"] = date("d/m/Y", strtotime($objEval->fecha_fin));
        $ajx_datos["duracion"] = $objEval->duracion;
        $ajx_datos["importancia"] = $objEval->importancia;
        $ajx_datos["observa"] = utf8_encode($objEval->observaciones);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;        
        $ajx_datos['error'] = $objEval->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>