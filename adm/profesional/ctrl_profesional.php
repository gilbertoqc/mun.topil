<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_adscripcion.class.php';
include 'includes/class/admtbl_doctos_ident.class.php';
include 'includes/class/admtbl_cursos.class.php';
include 'includes/class/admtbl_evaluaciones.class.php';
include 'includes/class/admtbl_estimulos.class.php';
include 'includes/class/admtbl_sanciones.class.php';
$objAdscrip = new AdmtblAdscripcion();
$objDocIdent = new AdmtblDoctosIdent();
$objCurso = new AdmtblCursos();
$objEval = new AdmtblEvaluaciones();
$objEstim = new AdmtblEstimulos();
$objSancion = new AdmtblSanciones();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Profesionalizaci�n',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/profesional/_js/ctrlprofesional.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <style type="text/css">
    .tdTituloTab{ color: #666362; font-style: italic; font-size: 10pt; font-weight: bold; padding-left: 5px; text-align: left; width: 70%; }
    </style>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <?php                    
                    $urlCancel = 'index.php?m=' . $_SESSION['xIdMenu'];
                    ?>
                    <!-- Botones de opci�n... -->                    
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="" title="Cancelar la alta del nuevo elemento...">
                        <img src="includes/css/imgs/icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <?php
    $_SESSION["xCurp"] = ( isset($_GET["id"]) ) ? $objSys->decrypt($_GET["id"]) : $_SESSION["xCurp"];
    $objAdscrip->select($_SESSION["xCurp"]);
    // Foto
    $objDocIdent->select($_SESSION['xCurp']);
    $foto_fte = ( !empty($objDocIdent->foto_frente) ) ? 'adm/expediente/fotos/' . $objDocIdent->foto_frente : '';
    if( empty($foto_fte) )
        $foto_fte = ( $objDatPer->genero == 1 ) ? 'adm/expediente/sin_foto_h.jpg' : 'adm/expediente/sin_foto_m.jpg';
    ?>    
    <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 1000px;">
        <span class="dvForm-Data-pTitle"><img src="includes/css/imgs/icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Seguimiento de Profesionalizaci�n</span>
        
        <fieldset class="fsetForm-Data" style="width: 900px;">
            <legend>Datos Generales</legend>   
            <table class="tbForm-Data" style="table-layout: fixed;">
                <tr>
                    <td rowspan="11" style="text-align: center; width: 290px;">
                        <div class="dvFoto-Normal" style="background-image: url(<?php echo $foto_fte;?>);vertical-align: bottom;"></div>
                    </td>
                </tr>
                <tr>                        
                    <td style="width: 145px;"><label for="txtCurp">C.U.R.P.:</label></td>
                    <td class="control-group" style="width: 450px;">
                        <input type="text" name="txtCurp" id="txtCurp" value="<?php echo $objAdscrip->AdmtblDatosPersonales->curp;?>" title="..." readonly="true" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td><label for="txtCuip">C.U.I.P.:</label></td>
                    <td class="control-group" style="width: 450px;">
                        <input type="text" name="txtCuip" id="txtCuip" value="<?php echo $objAdscrip->AdmtblDatosPersonales->cuip;?>" title="..." readonly="true" style="width: 200px;" />
                    </td>
                </tr>
                <tr>
                    <td><label for="txtNombre">Nombre Completo:</label></td>
                    <td class="control-group">
                        <?php
                        $nombrePersona = $objAdscrip->AdmtblDatosPersonales->nombre . " " . $objAdscrip->AdmtblDatosPersonales->a_paterno . " " . $objAdscrip->AdmtblDatosPersonales->a_materno;
                        ?>
                        <input type="text" name="txtNombre" id="txtNombre" value="<?php echo $nombrePersona;?>" title="..." readonly="true" style="width: 350px;" />                                    
                    </td>
                </tr>                    
                <tr>
                    <td><label for="txtCategoria">Categor�a:</label></td>
                    <td class="control-group">                                        
                        <input type="text" name="txtCategoria" id="txtCategoria" value="<?php echo $objAdscrip->AdmcatCategorias->categoria;?>" title="..." readonly="true" style="width: 350px;" />                                    
                    </td>
                </tr>   
                <tr>
                    <td><label for="txtNivelMando">Nivel de mando</label>:</label></td>
                    <td class="control-group">
                        <input type="text" name="txtNivelMando" id="txtNivelMando" value="<?php echo $objAdscrip->AdmcatNivelMando->nivel_mando;?>" title="..." readonly="true" style="width: 350px;" />
                    </td>
                </tr>
                <tr>
                    <td><label for="txtArea">�rea de adscripci�n:</label></td>
                    <td class="control-group">
                        <input type="text" name="txtArea" id="txtArea" value="<?php echo $objAdscrip->AdmcatAreas->area;?>" title="..." readonly="true" style="width: 350px;" />
                    </td>
                </tr>
            </table>
        </fieldset>
                    
        <div id="tabs" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
            <ul>
                <li><a href="#tabs-1" style="font-size: 11pt; min-width: 150px; text-align: center;">Formaci�n Continua</a></li>
                <li><a href="#tabs-2" style="font-size: 11pt; min-width: 150px; text-align: center;">Evaluaciones</a></li>                
                <li><a href="#tabs-3" style="font-size: 11pt; min-width: 150px; text-align: center;">Est�mulos</a></li>
                <li><a href="#tabs-4" style="font-size: 11pt; min-width: 150px; text-align: center;">Sanciones</a></li>
            </ul>         
            <!-- Cursos -->
            <div id="tabs-1">    
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Formaci�n Continua :: Cursos y Capacitaciones</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddCurso" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Agregar un nuevo curso...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>                 
                <div id="dvGrid-Cursos" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">&nbsp;</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 25%;">CURSO</th>                     
                                <th class="xGrid-tbCols-ColSortable" style="width: 17%;">TIPO</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">EFIC. TERMINAL</th>                   
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">INICIO</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">FIN</th>
                                <th style="width: 8%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    $datos = $objCurso->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.fecha_ini Asc");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-Cursos-tbBody">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-Cursos-' . $dato["id_curso"] . '">';
                                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                                $html .= '<td style="text-align: left; width: 25%; ' . $sty_color . '">' . $dato["curso"] . '</td>';
                                $html .= '<td style="text-align: center; width: 17%; ' . $sty_color . '">' . $dato["tipo_curso"] . '</td>';
                                if( $dato["eficiencia_term"] == 1 )
                                    $ef_term = "COMPLETADO";                            
                                else if( $dato["eficiencia_term"] == 2 )
                                    $ef_term = "CURSANDO";
                                else if( $dato["eficiencia_term"] == 3 )
                                    $ef_term = "INCOMPLETO";
                                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . $ef_term . '</td>';
                                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_ini"])) . '</td>';            
                                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_fin"])) . '</td>';
                                $html .= '<td style="text-align: center; width: 8%; ' . $sty_color . '">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_curso"] . '" title="Moficar este curso..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }                   
                    ?>    
                    </div>
                </div>
                
                <div id="dvFormCurso" title="">
                    <form id="frmCurso" method="post" action="#" enctype="multipart/form-data">
                    <table class="tbForm-Data">
                        <tr>
                            <td><label for="cbxTipoCurso">Tipo de Curso:</label></td>
                            <td class="validation">
                                <select name="cbxTipoCurso" id="cbxTipoCurso" style="max-width: 300px;"> 
                                    <option value="0"></option>                               
                                    <?php
                                    echo $objCurso->AdmcatTipoCurso->shwTipoCurso();
                                    ?>
                                </select>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtNombreCurso">Nombre del Curso:</label></td>
                            <td class="validation">
                                <input type="text" name="txtNombreCurso" id="txtNombreCurso" value="" maxlength="200" title="..." style="width: 350px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtInstitucion">Instituci�n o Academia:</label></td>
                            <td class="validation">
                                <input type="text" name="txtInstitucion" id="txtInstitucion" value="" maxlength="120" title="..." style="width: 350px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtFechaIniCurso">Fecha de Inicio:</label></td>
                            <td class="validation">
                                <input type="text" name="txtFechaIniCurso" id="txtFechaIniCurso" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>                        
                        <tr>
                            <td><label for="txtFechaFinCurso">Fecha de Terminaci�n:</label></td>
                            <td class="validation">
                                <input type="text" name="txtFechaFinCurso" id="txtFechaFinCurso" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>                        
                        <tr>
                            <td><label for="txtDuracionCurso">Duraci�n en Horas:</label></td>
                            <td class="validation">
                                <input type="text" name="txtDuracionCurso" id="txtDuracionCurso" value="" maxlength="25" title="..." style="width: 80px;" /> Hrs.
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="">Eficiencia Terminal:</label></td>
                            <td class="validation">
                                <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerm" id="rbnEfiTerm1" value="1" checked="true" />Completado</label>
                                <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnEfiTerm" id="rbnEfiTerm2" value="2" />En proceso</label>
                                <label class="label-Radio" ><input type="radio" name="rbnEfiTerm" id="rbnEfiTerm3" value="3" />Incompleto</label>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtObservaCurso">Observaciones:</label></td>
                            <td class="validation">
                                <textarea name="txtObservaCurso" id="txtObservaCurso" style="height: 60px; width: 410px;" ></textarea>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="hdnTypeOperCurso" id="hdnTypeOperCurso" value="0" />
                    <input type="hidden" name="hdnIdCurso" id="hdnIdCurso" value="0" />
                    </form>
                </div>
                <input type="hidden" id="hdnUrlSaveCurso" value="<?php echo $objSys->encrypt('adm/profesional/ajx_reg_curso.php');?>" />
                <input type="hidden" id="hdnUrlGetCurso" value="<?php echo $objSys->encrypt('adm/profesional/ajx_obt_curso.php');?>" />                
            </div>
            
            <!-- Evaluaciones -->
            <div id="tabs-2">
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Evaluaciones :: Desempe�o, Destrezas, Control y Confianza</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddEvaluacion" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Agregar una nueva evaluaci�n...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>
                <div id="dvGrid-Evaluaciones" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">&nbsp;</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 25%;">EVALUACI�N</th>                     
                                <th class="xGrid-tbCols-ColSortable" style="width: 17%;">TIPO EVAL.</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">INICIO</th>                   
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">FIN</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">RESULTADO</th>
                                <th style="width: 8%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    $datos = $objEval->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.fecha_ini Asc");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-Evaluaciones-tbBody">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-Evaluaciones-' . $dato["id_evaluacion"] . '">';
                                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                                $html .= '<td style="text-align: left; width: 25%; ' . $sty_color . '">' . $dato["evaluacion"] . '</td>';
                                $html .= '<td style="text-align: center; width: 17%; ' . $sty_color . '">' . $dato["tipo_eval"] . '</td>';
                                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_ini"])) . '</td>';            
                                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha_fin"])) . '</td>';
                                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . $dato["resultado"] . '</td>';
                                $html .= '<td style="text-align: center; width: 8%; ' . $sty_color . '">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_evaluacion"] . '" title="Moficar este curso..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }                   
                    ?>                         
                    </div>
                </div>
                
                <div id="dvFormEvaluacion" title="">
                    <form id="frmEvaluacion" method="post" action="#" enctype="multipart/form-data">
                    <table class="tbForm-Data">
                        <tr>
                            <td><label for="cbxTipoEval">Tipo de Evaluaci�n:</label></td>
                            <td class="validation">
                                <select name="cbxTipoEval" id="cbxTipoEval" style="max-width: 300px;"> 
                                    <option value="0"></option>                               
                                    <?php
                                    echo $objEval->AdmcatTipoEval->shwTipoEval();
                                    ?>
                                </select>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtNombreEval">Evaluaci�n:</label></td>
                            <td class="validation">
                                <input type="text" name="txtNombreEval" id="txtNombreEval" value="" maxlength="200" title="..." style="width: 350px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>                        
                        <tr>
                            <td><label for="txtFechaIniEval">Fecha de Inicio:</label></td>
                            <td class="validation">
                                <input type="text" name="txtFechaIniEval" id="txtFechaIniEval" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>                        
                        <tr>
                            <td><label for="txtFechaFinEval">Fecha de Terminaci�n:</label></td>
                            <td class="validation">
                                <input type="text" name="txtFechaFinEval" id="txtFechaFinEval" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>                        
                        <tr>
                            <td><label for="txtDuracionEval">Duraci�n en Horas:</label></td>
                            <td class="validation">
                                <input type="text" name="txtDuracionEval" id="txtDuracionEval" value="" maxlength="25" title="..." style="width: 80px;" /> Hrs.
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="">Importancia:</label></td>
                            <td class="validation">
                                <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnImportEval" id="rbnImportEval1" value="1" checked="true" />Obligatorio</label>
                                <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnImportEval" id="rbnImportEval2" value="2" />Opcional</label>                                
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="cbxResultEval">Resultado:</label></td>
                            <td class="validation">
                                <select name="cbxResultEval" id="cbxResultEval" style="max-width: 300px;"> 
                                    <option value="0"></option>                               
                                    <?php
                                    echo $objEval->AdmcatResultEval->shwResultEval();
                                    ?>
                                </select>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtObservaEval">Observaciones:</label></td>
                            <td class="validation">
                                <textarea name="txtObservaEval" id="txtObservaEval" style="height: 60px; width: 410px;" ></textarea>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="hdnTypeOperEval" id="hdnTypeOperEval" value="0" />
                    <input type="hidden" name="hdnIdEval" id="hdnIdEval" value="0" />
                    </form>
                </div>
                <input type="hidden" id="hdnUrlSaveEval" value="<?php echo $objSys->encrypt('adm/profesional/ajx_reg_eval.php');?>" />
                <input type="hidden" id="hdnUrlGetEval" value="<?php echo $objSys->encrypt('adm/profesional/ajx_obt_eval.php');?>" />                
            </div>
            
            <!-- Est�mulos -->
            <div id="tabs-3">
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Est�mulos</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddEstimulo" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Agregar un nuevo est�mulo...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>
                <div id="dvGrid-Estimulos" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">&nbsp;</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 60%;">EST�MULO</th>                     
                                <th class="xGrid-tbCols-ColSortable" style="width: 20%;">FECHA</th>                                
                                <th style="width: 15%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    $datos = $objEstim->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.fecha Asc");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-Estimulos-tbBody">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-Estimulos-' . $dato["id_estimulo"] . '">';
                                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                                $html .= '<td style="text-align: left; width: 60%; ' . $sty_color . '">' . $dato["descripcion"] . '</td>';
                                $html .= '<td style="text-align: center; width: 20%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha"])) . '</td>';
                                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_estimulo"] . '" title="Moficar este curso..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }                   
                    ?>
                    </div>
                </div>
                <div id="dvFormEstimulo" title="">
                    <form id="frmEstimulo" method="post" action="#" enctype="multipart/form-data">
                    <table class="tbForm-Data">
                        <tr>
                            <td><label for="txtEstimulo">Est�mulo(descripci�n):</label></td>
                            <td class="validation">
                                <input type="text" name="txtEstimulo" id="txtEstimulo" value="" maxlength="200" title="..." style="width: 400px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtFechaEstim">Fecha de Entrega:</label></td>
                            <td class="validation">
                                <input type="text" name="txtFechaEstim" id="txtFechaEstim" value="" readonly="true" title="..." style="text-align: center; width: 120px;" />
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="hdnTypeOperEstim" id="hdnTypeOperEstim" value="0" />
                    <input type="hidden" name="hdnIdEstim" id="hdnIdEstim" value="0" />
                    </form>
                </div>
                <input type="hidden" id="hdnUrlSaveEstim" value="<?php echo $objSys->encrypt('adm/profesional/ajx_reg_estim.php');?>" />
                <input type="hidden" id="hdnUrlGetEstim" value="<?php echo $objSys->encrypt('adm/profesional/ajx_obt_estim.php');?>" />
            </div>
            
            <!-- Sanciones -->
            <div id="tabs-4">
                <div class="dvBar-Normal" style="">
                    <table style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <tr>
                            <td class="tdTituloTab">Sanciones</td>
                            <td style="padding: 3px; text-align: right; width: 30%;">
                                <a href="#" id="btnAddSancion" class="Tool-Bar-Btn gradient" style="height: 30px; min-width: 60px;" title="Agregar una nueva sanci�n...">
                                    <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" />
                                </a>
                            </td>                                
                        </tr>
                    </table>
                </div>
                <div id="dvGrid-Sanciones" style="border: none; margin-top: 5px;  width: 100%;">
                    <div class="xGrid-dvHeader gradient">
                        <table class="xGrid-tbSearch">
                            <tr>
                                <td style="padding: 5px; text-align: center;">&nbsp;</td>
                            </tr>
                        </table>
                        <table class="xGrid-tbCols">
                            <tr>
                                <th style="width: 5%;">&nbsp;</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 45%;">MOTIVO</th>                     
                                <th class="xGrid-tbCols-ColSortable" style="width: 25%;">TIPO</th>
                                <th class="xGrid-tbCols-ColSortable" style="width: 15%;">DURACI�N</th>
                                <th style="width: 10%;">&nbsp;</th>
                            </tr>
                        </table>
                    </div>
                    <div class="xGrid-dvBody" style="border-bottom: 2px double #488ac7; min-height: 200px; overflow-y: hidden;">
                    <?php
                    $datos = $objSancion->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.fecha_reg Asc");
                    if( count($datos) > 0 ){
                        $html = '<table class="xGrid-tbBody" id="dvGrid-Sanciones-tbBody">';
                        foreach( $datos As $reg => $dato ){
                            $html .= '<tr id="dvGrid-Sanciones-' . $dato["id_sancion"] . '">';
                                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                                $html .= '<td style="text-align: left; width: 45%; ' . $sty_color . '">' . $dato["motivo_sancion"] . '</td>';
                                $html .= '<td style="text-align: center; width: 25%; ' . $sty_color . '">' . $dato["tipo_sancion"] . '</td>';
                                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">' . $dato["duracion"] . '</td>';
                                $html .= '<td style="text-align: center; width: 10%; ' . $sty_color . '">';
                                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_sancion"] . '" title="Moficar esta sancion..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                                $html .= '</td>';
                            $html .= '</tr>';
                        }
                        $html .= '</table>';
                        echo $html;
                    }                   
                    ?> 
                    </div>
                </div>
                
                <div id="dvFormSancion" title="">
                    <form id="frmSancion" method="post" action="#" enctype="multipart/form-data">
                    <table class="tbForm-Data">
                        <tr>
                            <td><label for="cbxTipoSancion">Tipo de Sanci�n:</label></td>
                            <td class="validation">
                                <select name="cbxTipoSancion" id="cbxTipoSancion" style="max-width: 300px;"> 
                                    <option value="0"></option>                               
                                    <?php
                                    echo $objSancion->AdmcatTipoSancion->shwTipoSancion();
                                    ?>
                                </select>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="cbxMotivoSancion">Motivo de la Sanci�n:</label></td>
                            <td class="validation">
                                <select name="cbxMotivoSancion" id="cbxMotivoSancion" style="max-width: 300px;"> 
                                    <option value="0"></option>                               
                                    <?php
                                    echo $objSancion->AdmcatMotivoSancion->shwMotivoSancion();
                                    ?>
                                </select>
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtDuracionSancion">Duraci�n en Horas:</label></td>
                            <td class="validation">
                                <input type="text" name="txtDuracionSancion" id="txtDuracionSancion" value="" maxlength="25" title="..." style="width: 80px;" /> Hrs.
                                <span class="pRequerido">*</span>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="txtObservaSancion">Observaciones:</label></td>
                            <td class="validation">
                                <textarea name="txtObservaSancion" id="txtObservaSancion" style="height: 60px; width: 410px;" ></textarea>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="hdnTypeOperSancion" id="hdnTypeOperSancion" value="0" />
                    <input type="hidden" name="hdnIdSancion" id="hdnIdSancion" value="0" />
                    </form>
                </div>
                <input type="hidden" id="hdnUrlSaveSancion" value="<?php echo $objSys->encrypt('adm/profesional/ajx_reg_sancion.php');?>" />
                <input type="hidden" id="hdnUrlGetSancion" value="<?php echo $objSys->encrypt('adm/profesional/ajx_obt_sancion.php');?>" />
            </div>
        </div>
    </div>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>