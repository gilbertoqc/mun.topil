<?php
/**
 * Complemento ajax para obtener los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/admtbl_estimulos.class.php';
    $objEstim = new AdmtblEstimulos();
    
    $objEstim->select($_GET["id"]);
    if ($objEstim->id_estimulo > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["descripcion"] = utf8_encode($objEstim->descripcion);
        $ajx_datos["fecha"] = date("d/m/Y", strtotime($objEstim->fecha));
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;        
        $ajx_datos['error'] = $objEstim->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>