<?php
/**
 * Complemento ajax para guardar los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_estimulos.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objEstim = new AdmtblEstimulos();    
    
    $idEstim = $_POST["hdnIdEstim"];
    $objEstim->id_estimulo = $idEstim;
    $objEstim->curp = $_SESSION["xCurp"];
    $objEstim->descripcion = $_POST["txtEstimulo"];
    $objEstim->fecha = $objSys->convertirFecha($_POST["txtFechaEstim"], "yyyy-mm-dd");
                
    $result = ($_POST["hdnTypeOperEstim"] == 1) ? $objEstim->insert() : $objEstim->update();
    if ($result) {
        $ajx_datos['rslt']  = true;
        if( $_POST["hdnTypeOperEstim"] == 1 ){
            $oper = "Ins";
            $id_reg = $result;    
        } else {
            $oper = "Edt";
            $id_reg = $idEstim;
        }        
        $objSys->registroLog($objUsr->idUsr, "admtbl_estimulos", $id_reg, $oper);
        // Se obtienen todos los estimulos de la persona y se genera la lista
        $datos = $objEstim->selectAll("a.curp='" . $_SESSION['xCurp'] . "'", "a.fecha Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Estimulos-tbBody">';
        foreach( $datos As $reg => $dato ){
            if( $result === true )
                $sty_color = ( $idEstim == $dato["id_estimulo"] ) ? 'color: #2b60de;' : '';
            else
                $sty_color = ( $result == $dato["id_estimulo"] ) ? 'color: #2b60de;' : '';
            $html .= '<tr id="dvGrid-Estimulos-' . $dato["id_estimulo"] . '">';
                $html .= '<td style="text-align: center; width: 5%;">' . ($reg+1) . '</td>';                
                $html .= '<td style="text-align: left; width: 60%; ' . $sty_color . '">' . $dato["descripcion"] . '</td>';
                $html .= '<td style="text-align: center; width: 20%; ' . $sty_color . '">' . date("d/m/Y", strtotime($dato["fecha"])) . '</td>';
                $html .= '<td style="text-align: center; width: 15%; ' . $sty_color . '">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid" rel="' . $dato["id_estimulo"] . '" title="Moficar este est�mulo..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                $html .= '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objEstim->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>