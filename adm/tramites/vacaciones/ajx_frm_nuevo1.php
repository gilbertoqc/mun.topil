<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    //include $path . 'includes/class/logtbl_arm_armamento.class.php';
    //include $path . 'includes/class/logtbl_arm_asignacion.class.php';       
    $objSys = new System();
    //$objArm             = new LogtblArmArmamento();
    //$objAsigna          = new LogtblArmAsignacion();
    
    
    //se incluye archivo javascript para funcionalidad necesario
    $html .= '<script type="text/javascript" src="adm/_js/tramites/vacaciones/ajx_frm_asigna.js"></script>';
    $html .= '<script type="text/javascript" src="includes/js/xgrid.js"></script>';
    
    
    //variables recibidas
    $curp = $_POST["curp"]; 
    
    //se seleccionan los datos de la adscripcion
    //$datosAds = $objAds->selectAll( "a.curp='" . $curp . "'", '', '' );      

    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
    $html .= ' 
         
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 850px;">
            <!--<span class="dvForm-Data-pTitle"><img src="'.PATH_IMAGES.'icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Nuevo</span>-->                                
            <div id="tabs" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">
                <ul>
                    <li><a href="#tab-1" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">1</span>Datos Generales</a></li>
                    <li><a href="#tab-2" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">2</span>Datos del Oficio</a></li>
                  
                </ul>         
                <!-- Datos Generales -->
                <div id="tab-1">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                            <tr>
                                <td><label for="txtNoFolio">No. Folio:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoFolio" id="txtNoFolio" value="" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                    
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="txtAnioPer">A�o de Periodo:</label></td>
                                <td class="validation">
                                   <select name="cbxAnioPer" id="cbxAnioPer" style="max-width: 200px;">                                
                                        <option value="2010" >2010</option>
                                        <option value="2011" >2011</option>
                                        <option value="2012" >2012</option>
                                        <option value="2013" >2013</option>
                                        <option selected value="2014" >2014</option>
                                        <option value="2015" >2015</option>
                                    </select>
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtPeriodo">Periodo:</label></td>
                                <td class="validation">
                                   <select name="cbxPeriodo" id="cbxPeriodo" style="max-width: 200px;">                                
                                        <option value="1" >PRIMERO</option>
                                        <option value="2" >SEGUNDO</option>
                                    </select>
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtFechaInicio">Fecha de Inicio </label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaInicio" id="txtFechaInicio" value="" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtFechaFin">Fecha de Finalizaci�n </label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaFin" id="txtFechaFin" value="" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>     
                               
                            <tr>
                                <td><label for="txtDias">Numero de Dias:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtDias" id="txtDias" value="" maxlength="35" title="..." style="width: 200px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                            
                            
                        </table>
                    </fieldset>
                </div>
                
                
                <div id="tab-2">
                    <fieldset class="fsetForm-Data" style="width: auto;">                                
                        <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                            <tr>
                                <td><label for="txtNoOficio">No. Oficio:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoOficio" id="txtNoOficio" value="" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>              
                            <tr>
                                <td><label for="txtFechaOficio">Fecha Oficio:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaOficio" id="txtFechaOficio" readonly="true" value="" maxlength="35" title="..." style="width: 120px;" />
                                    <span class="pRequerido">*</span>
                                </td>
                            </tr>
                             <tr>
                                <td><label for="txtNoSoporte">No. de Soporte:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtNoSoporte" id="txtNoSoporte" value="" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                                </td>
                            </tr>  
                            <tr>
                                <td><label for="txtFechaSoporte">Fecha de Soporte:</label></td>
                                <td class="validation">
                                    <input type="text" name="txtFechaSoporte" id="txtFechaSoporte" readonly="true" value="" maxlength="35" title="..." style="width: 120px;" />
                                    
                                </td>
                            </tr>
                            <tr>
                                <td><label for="txtFirmante">Firmante del Soporte:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtFirmante" id="txtFirmante" value="" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 350px;" />
                                   
                                </td>
                            </tr>   
                            <tr>
                                <td><label for="txtCargoFirmante">Cargo del Firmante:</label></td>
                                <td class="validation" style="width: 400px;">
                                    <input type="text" name="txtCargoFirmante" id="txtCargoFirmante" value="" maxlength="18" title="..." style="font-size: 12pt; padding: 5px; width: 350px;" />
                                    
                                </td>
                            </tr>     
                            
                        </table>
                    </fieldset>
                </div>
                <script> 
                    $("#tabs").tabs ();                    
                    $("#txtFechaInicio").datepicker({ yearRange: "1920:2002", }); 
                    $("#txtFechaFin").datepicker({ yearRange: "1920:2002", });
                    $("#txtFechaOficio").datepicker({ yearRange: "1920:2002", });
                    $("#txtFechaSoporte").datepicker({ yearRange: "1920:2002",defaultDate: "+1w", });
                </script>
            </div>
            
        </div>
    </form>
    ';    
                  
   /*$html .='<form id="frmAlta" method="post" enctype="multipart/form-data">';    
        $html .='<div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">';                         
        $html .='<ul>
                    <li><a href="#tab-1" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">1</span>Datos Personales</a></li>
                    <li><a href="#tab-2" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">2</span>Domicilio</a></li>
                    <li><a href="#tab-3" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">3</span>Nivel de Estudios</a></li>
                    <li><a href="#tab-4" style="width: 150px;"><span style="border: 2px solid gray; border-radius: 50%; margin-right: 7px; padding: 3px 7px;">4</span>Adscripci�n</a></li>
                </ul>';   
                //registro de armamento
                $html .='<table id="tbForm-Alta" class="tbForm-Data" style="width: 600px;">'; 
                    $html .='<tr>';
                        $html .='<td><label for="txtMatricula">Matricula:</label></td>';
                        $html .='<td class="validation" style="width: 400px;">';
                            $html .='<input type="text" name="txtMatricula" id="txtMatricula" value="" maxlength="12" title="..." style="padding: 5px; width: 210px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                                 
                    
                   
                $html .='</table>';                                                  
         $html .='</div>';
            $html .='<p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>';                        
        $html .= '<input type="hidden" id="dtTypeOper" name="dtTypeOper" value="' . $oper . '" />';
    $html .='</form>';*/
  
    
    //se inyecta la informacion en los contenedores
    $ajx_datos["html"]  = utf8_encode($html);      
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>