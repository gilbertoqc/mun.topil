<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_vacaciones.class.php';
    include $path . 'includes/class/admtbl_adscripcion.class.php';
        
    
    $objSys = new System();        
    $objAsigna= new AdmtblVacaciones();  
    
    
    //    
    $sql_where  = ' ( curp = ? ) ';
    $sql_values = array( $_GET["curp"] );     

    //--------------------- Recepci�n de par�metros --------------------------//
    $datos = $objAsigna->selectListTramite($sql_where, $sql_values);
    //se contabiliza el numero de registro obtenidos.
    $totalReg = count( $datos );
    
    $nombreGrid = $_GET["IdGrid"];   
        
    //$xDat = $objAsigna->LogtblArmArmamento->LogcatArmMarca;
    $html = '';
    $i=0;
    if ( $totalReg > 0 ) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';        
        foreach ($datos As $reg => $dato) {          
            $id = $dato["curp"].'-'.$dato["anio"].'-'.$dato["periodo"];
           
            $html .= '<tr id="' . $nombreGrid . '-' . $id . '">';              
                $html.='<td style="width:  4%; text-align : center;">' . ++$i .'</td>';
                $html.='<td style="width: 15%; text-align : center;">' . $dato["fecha_ini"] . '</td>';  
                $html.='<td style="width: 15%; text-align : center;">' . $dato["fecha_fin"] . '</td>';  ;
                //$html.= $xDat->LogcatArmClase->LogcatArmTipo->getTipo( $id_marca ) . '</td>';
                $html.='<td style="width: 10%; text-align : center;">' . $dato["periodo"] . '</td>';  ;
                //$html.= $xDat->getMarca( $id_marca ) . '</td>';
                $html.='<td style="width: 17%; text-align : center;">' . $dato["num_dias"] . '</td>';  ;
                //$html.= $xDat->LogcatArmModelo->getModelo( $id_marca ) . '</td>';
                $html.='<td style="width: 15%; text-align : center;">' . $dato["no_oficio"] . '</td>';  ;
                //$html.= $xDat->LogcatArmClase->getClase( $id_marca ) . '</td>';                                                        
                $html.='<td style="width: 10%; text-align : center;">' . $dato["fecha_oficio"] . '</td>';  ;
                //$html.= $xDat->LogcatArmModelo->LogcatArmCalibre->getCalibre( $id_marca ) . '</td>';
                
                $html.='<td style="width:  10%; text-align: center;">';
                
                    $html .= '  <a href="#" class="lnkBtnOpcionGrid classModificar" id="' . $id .  '" title="Actualizar los datos..." >
                                    <img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" />
                                </a>';
                            
                    $html.='<a href="#" rel="Eli-' . $id . '" class="lnkBtnOpcionGrid classDescarga" title="Eliminar ..." >';
                    $html.='<img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="Eliminar." />';
                    $html.='</a>';
                $html.='</td>';                                                           
            $html.='</tr>';              		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';       		
            $html .= 'No hay registros que mostrar ...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    //   
    $ajx_datos["total"] = $totalReg;  
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>