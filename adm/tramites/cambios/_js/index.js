var xGrid;
var xGrid2;
var xGrid3;
$(document).ready(function(){
   //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Personal').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal datos del elemento...
    xGrid = $('#dvGrid-Personal').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });
    
    // Definici�n del control grid la muestra datos de los tramites    
    xGrid2 = $('#dvGrid-Tramites').xGrid({                
        xOnlySearch: true,                           
        xUrlData: xDcrypt($('#hdnUrlObtListTram').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });  
    /*xGrid3 = $('#dvBuscarCont').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xTypeSearch: 2,
        xOnlySearch: true,                    
        xUrlData: xDcrypt($('#hdnUrlObtArmas').val()),
        xLoadDataStart: 0,
        xTypeDataAjax: 'json'
    });   */ 
    
    //-- Definici�n del dialog como formulario para portacion de armas....
    $('#dvCambios').dialog({
        show: {
            effect: "fade",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 950,
        height: 520,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
     $('#dvTramites').dialog({
        
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 940,
        height: 600,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
     $('#dvNuevo').dialog({
        show: {
            effect: "fade",
            duration: 500
        },
        hide: {
            effect: "drop",
            duration: 500
        },
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 950,
        height: 550,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Guardar --': function(){
                //funcion guardar los datos del cambio                
                GuardarFormulario();                
            },
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });     
    
    $(".classNuevo, .classModificar").live("click", function(e){
        var oper;
        var titulo;
        var id;
        //se cambia el flujo del script segu sea insercion o actualizacion. 
        if( $(this).attr('accion') == 'insertar' ){
            var curp = $(this).attr("id").substring(4);            
            oper=0;            
            titulo='Registro';                                         
        }else{
            var id_cambio= $(this).attr('id').substring(4);
            oper=1;
            titulo='Actualizaci�n';                    
        }        
        $.ajax({
            url: xDcrypt( $('#hdnUrlNuevo').val() ),            
            dataType: 'json',
            type: 'POST',
            data: { 'oper' : oper , 'curp' : curp, 'id_cambio' : id_cambio },
            async: true,
            cache: false,            
            success: function (xdata) {  
                    $('#dvNuevo').html(xdata.html);                                        
                    $('#dvNuevo').dialog('option','title',titulo);
                    $('#dvNuevo').dialog('open');
                     
                                                                                                  
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }); 
    
    
    //-- Controla el bot�n para visualizar el formulario de nuevo cambio...
   /* $(".classNuevo").live("click", function(e){
        //var ID = $("#hdnUrlCurp").val();
        
        $.ajax({
            url: xDcrypt( $('#hdnUrlNuevo').val() ),  
            data: { 'curp': ID.substring(4) },          
            dataType: 'json',
            type: 'POST',
            async: true,
            cache: false,            
            success: function (xdata) {     
                      
                    $('#dvNuevo').html(xdata.html);                                        
                    $('#dvNuevo').dialog('open');                    
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });  */                        
    
    /*-- Controla el bot�n para visualizar el formulario de portacion de armas...
    $(".classAsigna").live("click", function(e){
        var ID = $(this).attr("id");
        $.ajax({
            url: xDcrypt( $('#hdnUrlAsigna').val() ),  
            data: { 'curp': ID.substring(4) },          
            dataType: 'json',
            type: 'POST',
            async: true,
            cache: false,            
            success: function (xdata) {  
                     
                    $('#dvCambiosHead').html(xdata.html);                    
                    $('#dvCambios').dialog('option','title',xdata.titulo);
                    $('#dvCambios').dialog('open');                    
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });*/
    
    $(".classAsigna").live("click", function(e){        
        $.ajax({
            url: xDcrypt( $('#hdnUrlObtDatosPer').val() ),  
            data: { 'curp': $(this).attr("id").substring(4) },          
            dataType: 'json',
            type: 'post',
            async: true,
            cache: false,            
            success: function (xdata) { 
                $("#hdnUrlCurp").val( xdata.curp ) 
                xGrid2.resetAjaxParam();
                xGrid2.addParamAjax( "curp="+xdata.curp );  
                xGrid2.refreshGrid();                                     
                $('#dvDatos').html(xdata.html_dat);                                                             
                $('#dvTramites').dialog('option','title',xdata.titulo);
                $('#dvTramites').dialog('open');                    
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    });    

    $('#xReportes').click(function(){
        $('#dvForm-Registro').dialog('open'); 
    });
    
    
    
    

});

function obtenerMunicipios(id){    
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_region': id, 'filtro': 2},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#cbxMpioAdscripcion').html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxMpioAdscripcion').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}
    //-- Controla el bot�n para visualizar el formulario de alta o actualizacion de cambios...
    function GuardarFormulario(){    
        var valida = $('#frmRegistro').validate().form();
        var mensaje='';  
        var oper = $("#dtTypeOper").val();  
                  
        if (valida) {
            if( oper == '1' ){
                var id_cambio = $("#id_cambio").val();
                mensaje= 'Confirme la actualizaci�n de los datos.';
            }else{
                mensaje='Confirme el alta en el sistema.';
            }
            var frmPreg = $(getHTMLMensaje(mensaje, 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 350,
                buttons:{
                    'Aceptar': function(){
                        $.ajax({
                            url: xDcrypt( $('#hdnUrlAddRg').val() ),   
                            data: $( "#frmRegistro" ).serialize(),         
                            dataType: 'json',
                            type: 'POST',
                            async: true,
                            cache: false,   
                            //encoding: 'UTF-8', 
                            //contentType: 'text/json; charset=UTF-8',       
                            success: function ( xdata ) {  
                                //alert(xdata.error)
                                if( xdata.rslt == 1 ){  
                                    if( oper == '1' ){                                   
                                        $("#dvNuevo").dialog('close');
                                        
                                        xGrid2.refreshGrid(); 
                                              
                                    }else{                                        
                                                                                                                
                                        //$("#txtNoFolio").focus();
                                        //$("#xGridBody").html( xdata.html );
                                        
                                        $("#dvNuevo").dialog('close');
                                        
                                        xGrid.refreshGrid();                                        
                                        xGrid2.refreshGrid();    
                                                                            
                                    }                                    
                                                                                                      
                                }else{
                                    $(this).dialog('close');
                                    alert("No se guardaron los datos. " + xdata.rslt);                                                                        
                                }                                                                                               
                            },
                            error: function(objeto, detalle, otroobj){
                                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                            }
                        });  
                        $(this).dialog('close');                                              
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });                        
        }

    }
