
$(document).ready(function(){ 
   
  
    $('#frmRegistro').validate({
        rules:{
            //cbxTipoLicencia:{
            //    min: 1,
            //},
            // Cambios de Adscripcion
            txtFechaInicio: 'required',
            txtFechaFin: 'required',
            txtDias:{
                digits: true,
                required: true,
            },
            txtDiagnostico: 'required',
            //rbnDefinida: 'required',
            
            
    	},
    	messages:{
           //cbxTipoLicencia:{
           //     min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
           //},
    	   // Cambios de Adscripcion
           txtFechaInicio: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
           txtFechaFin: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
           txtDias:{
                digits: '<span class="ValidateError" title="Solo se aceptan digitos"></span>',
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
           },
           txtDiagnostico: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            
            
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    });
    


    
    
    
    
    
    
    
    
    
    
    
          

});

