
<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_datos_personales.class.php';

$objDatPer = new AdmtblDatosPersonales();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Tr�mites de Personal - Lactancia',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="adm/_js/tramites/incidencias/lactancias/index.js"></script>'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 50%;">
                    <!-- Botones de opci�n... -->
                    <!--<a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('persona_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Registrar un nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Registrar
                    </a>
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('form_ficha_crim');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                    -->
                    <a href="#" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <!-- GRID PRINCIPAL DATOS DEL ELEMENTO -->
    <div id="dvGrid-Personal" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ESPECIALIDAD</th>
                    <th style="width: 17%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">ADSCRIPCI�N</th>
                    <th style="width: 15%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <!-- CONTENEDOR PARA LA EL FORMULARIO DE TRAMITES ------------- -->
    <div id="dvTramites" style="display: none;"  >    
        
        <!-- FICHA DEL ELEMENTO CON FOTOGRAFIA MINIATURA -->
        <div id="dvDatos" style="margin: auto auto; margin-top: 10px; min-height: 100px; width: auto;"></div>
            
                    
        <!-- LISTA DE TRAMITES DEL ELEMENTO EN CUESTION -->
        <div id="dvGrid-Tramites" style="border: none; height: 240px; margin: auto auto; margin-top: 10px; width: auto;">
        
            <div class="xGrid-dvHeader gradient">
                <table class="xGrid-tbCols">
                    <tr>
                        <th style="width:  4%; text-align: center;" >No.</th>
                        <th style="width: 10%;" >FOLIO</th>
                        <th style="width: 10%;" >FECHA REGISTRO</th>
                        <th style="width: 10%;" >FECHA LACTANCIA</th>
                        <th style="width: 15%;" >STATUS</th>                                                        
                        <th style="width: 35%;" >OBSERVACION</th>
                        <th style="width:  10%;" >&nbsp;</th>                                                         
                    </tr>
                </table>
                
            </div>
            
            <div class="xGrid-dvBody">
            <!-- aqui van datos de las armas -->
            </div>
        </div>                                 
            
        <!-- CONTENEDOR PARA EL AGREGAR NUEVO TRAMITES ------------- -->
        <div id="dvNuevo" style="display: none;" title="Nuevo Lactancia" >      </div>
                     
        <!-- datos necesarios para pasarlos por formulario                                                    
        $html.='<input type="hidden" id="hdnUrlCurp"        value="' . $objSys->encrypt( $curp ) . '" /> -->
        <input type="hidden" id="hdnUrlCurp" />
        <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('adm/tramites/incidencias/lactancias/ajx_obt_datos_grid.php');?>" />
        <input type="hidden" id="hdnUrlObtDatosPer" value="<?php echo $objSys->encrypt('adm/tramites/incidencias/lactancias/ajx_obt_datos_per.php');?>" />
        <input type="hidden" id="hdnUrlObtListTram"  value="<?php echo $objSys->encrypt('adm/tramites/incidencias/lactancias/ajx_obt_tram_asig.php'); ?>" />
        <input type="hidden" id="hdnUrlNuevo"  value="<?php echo $objSys->encrypt('adm/tramites/incidencias/lactancias/ajx_frm_nuevo.php'); ?>" />
        <input type="hidden" id="hdnUrlAddRg"  value="<?php echo $objSys->encrypt('adm/tramites/incidencias/lactancias/ajx_reg_frm.php'); ?>" />
        <input type="hidden" id="hdnUrlElimTram"  value="<?php echo $objSys->encrypt('adm/tramites/incidencias/lactancias/ajx_elim_tram.php'); ?>" />
        <input type="hidden" id="hdnUrlMpio"  value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php'); ?>" />
        
                         
    </div>          
     
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>