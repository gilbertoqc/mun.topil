<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_adscripcion.class.php';
    $objSys = new System();
    $objAds             = new AdmtblAdscripcion();
    
    //se incluye archivo javascript para funcionalidad necesario
    $html .= '<script type="text/javascript" src="adm/_js/tramites/cambios/ajx_frm_asigna.js"></script>';
    $html .='<script type="text/javascript" src="includes/js/xgrid.js"></script>';
    
    //variables recibidas
    $curp = $_POST["curp"]; 
    //se seleccionan los datos personales
    $objAds->AdmtblDatosPersonales->select( $curp );
    //se seleccionan los datos de la adscripcion
    $datosAds = $objAds->selectAll( "a.curp='" . $curp . "'", '', '' );
    $objPer = $objAds->AdmtblDatosPersonales;        
    //$matricula  =   $objAsigna->select('',$curp);

    //se elabora el encabezado de la ventana
    $titulo = 'Cambio de Adscripci�n [ ' . $objPer->nombre . " " . $objPer->a_paterno . " " . $objPer->a_materno . ' ] ';
    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
                
    $html .='<form id="frmAsigna" method="post" enctype="multipart/form-data">'; 
               
            //FICHA DEL ELEMENTO CON FOTOGRAFIA MINIATURA
            $html .='<div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 100px; width: auto;">';                                
                //registro de armamento
                $html .='<table id="tbForm-Ficha" class="tbForm-Data" style="width: 750px;" border=0>'; 
                    $html .='<colgroup>';
                        $html.='<col style="width : 90px;">';
                        $html.='<col style="width : 110px;">';
                        $html.='<col style="width : 650px;">';
                    $html.='</colgroup>';     
                    $html .='<tr>';
                        $html .='<td rowspan="4"> <img src="imgs/sin_foto_h.jpg" title="imgs/sin_foto_h.jpg" /> </td>';                        
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td>Categoria: </td>';
                        $html .='<td>' . $datosAds[0]['admcat_categorias_categoria'] . ' </td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td>Corporaci&oacute;n: </td>';
                        $html .='<td>' . $datosAds[0]["admcat_corporaciones_corporacion"] . ' </td>';
                    $html .='</tr>';  
                    $html .='<tr>';                        
                        $html .='<td>Municipio: </td>';
                        $html .='<td>' . $datosAds[0]["admcat_municipios_municipio"] . ' </td>';
                    $html .='<tr>';                                  
                $html .='</table>';                                                  
            $html .='</div>';
            
            
            /*$html .= ' <div id="dvTool-Bar" class="dvTool-Bar">';
                $html .= '<table style="width: 100%;">';
                    $html .= '<tr> ';
                    $html .= '<td style="text-align: left; width: 50%;">';
                    $html .= '</td> ';
                    $html .= '<td style="text-align: right; width: 50%;"> ';
                    $url_new .= "index.php?m=" .$_SESSION["xIdMenu"]. "&mod=" . $objSys->encrypt("nuevo_cambio");
                        $html .= '<a href="'.$url_new.'" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Registrar un nuevo elemento...">';    
                            $html = '<img src="'.PATH_IMAGES.'icons/add.png" alt="" style="border: none;" /><br />Registrar';                    
                        $html .= '</a>';
                    $html .= '</td>';
                    $html .= '</tr>';
                $html .= '</table>';
            $html .= '</div>';*/
            
            
            //div para el boton de asignaciones
            $html.='<div id="dvAgregar" style="margin: auto auto; margin-top: 10px; min-height: 15px; width: auto; position: relative;">';
                $html.='<table style="width: 100%; border: 0px;">';
                    $html.='<tr>';
                    $html.='<td style="width: 95%;">&nbsp;</td>';
                    $html.='<td style="text-align: center; width:  10%;">';  
                    $html.='<a href="#" id="curp' . $curp . '" class="Tool-Bar-Btn classNuevo"'; 
                    $html.='title="Nuevo cambio..." ><img src="' . PATH_IMAGES . 'icons/add.png" alt="Nuevo Cambio." style="border: none;" /><br />Nuevo</a>';
                    $html.='</td>';
                    $html.='</tr>';
                $html.='</table>';
            $html.='</div>';
            
            $html.='';                        
            //$datosAsg = $objAsigna->selectAll( "a.curp='" . $curp ."'" ); 
            
            //PORTACION DE ARMAMENTO DEL ELEMENTO EN CUESTION.                                                                        
            $html.='<div id="dvGrid-Port" style="border: none; height: 150px; margin: auto auto; margin-top: 10px; width: auto;">';
                $html.='<div class="xGrid-dvHeader gradient">';
                    $html.='<table class="xGrid-tbCols">';
                        $html.='<tr>';
                            $html.='<th style="width: 5%; text-align: center;">No.</th>';
                            $html.='<th style="width: 10%;">FECHA</th>';
                            $html.='<th style="width: 6%;">TIPO</th>';  
                            $html.='<th style="width: 15%;">ADSCRIPCION</th>';
                            $html.='<th style="width: 20%;">NO OFICIO</th>';
                            $html.='<th style="width: 19%;">FECHA NOTIFICACION</th>';                                                        
                            $html.='<th style="width: 5%; text-align: center;">&nbsp;</th>';                            
                        $html.='</tr>';
                    $html.='</table>';
                $html.='</div>';
                $html.='<div class="xGrid-dvBody" id="divAsig" style="overflow-y: hidden;">';                                       
                    $html.= '<table class="xGrid-tbBody" >'; 
                    /*foreach($datosAsg as $reg => $valor){   
                        $id_marca = $valor["logtbl_arm_armamento_id_marca"];
                        $html.='<tr>';
                            $html.='<td style="width: 5%; text-align: center;">' . ++$i .'</td>';
                            $html.='<td style="width: 10%; text-align : center;">';
                            $html.= $valor["matricula"] . '</td>';  
                            $html.='<td style="width: 6%; text-align : center;">';
                            $html.= $objArm->LogcatArmMarca->LogcatArmClase->LogcatArmTipo->getTipo( $id_marca ) . '</td>';
                            $html.='<td style="width: 15%; text-align : center;">';
                            $html.= $objArm->LogcatArmMarca->getMarca( $id_marca ) . '</td>';
                            $html.='<td style="width: 20%; text-align : center;">';
                            $html.= $objArm->LogcatArmMarca->LogcatArmModelo->getModelo( $id_marca ) . '</td>';
                            $html.='<td style="width: 19%; text-align : center;">';
                            $html.= $objArm->LogcatArmMarca->LogcatArmClase->getClase( $id_marca ) . '</td>';                                                        
                            $html.='<td style="width: 20%; text-align : center;">';
                            $html.= $objArm->LogcatArmMarca->LogcatArmModelo->LogcatArmCalibre->getCalibre( $id_marca ) . '</td>';
                            $html.='<td style="width: 5%; text-align: left;">';
                                $html.='<a href="#" id="mat-' . $valor["matricula"] . '" class="lnkBtnOpcionGrid classDescarga" title="Descargar el armamento..." ><img src="' . PATH_IMAGES . 'icons/eliminar24.png" alt="Descargar el armamento." /></a>';
                            $html.='</td>';                            
                        $html.='</tr>';
                    }*/
                    $html .= '</table>';
                $html.='</div>';        
            $html.='</div>'; 
            
            // CONTENEDOR PARA EL FORMULARIO DE ALTA DE CAMBIO ------------- 
            $html.='<div id="dvNuevo" style="display: none;" title="Nuevo cambio de Adscripcion" >';    
                //$html.='<div id="dvNuevoCont" class="dvForm-Data" style="border-bottom: 2px double #488ac7; min-height: 280px; min-width: 650px; overflow: hidden;"></div>';        
            $html.='</div>';
                     
            //datos necesarios para pasarlos por formulario                                                      
              $html.='<input type="hidden" id="hdnUrlCurp" value="' . $objSys->encrypt( $curp ) . '" />';
              $html.='<input type="hidden" id="hdnUrlNuevo"    value="' . $objSys->encrypt('adm/tramites/cambios/ajx_frm_nuevo.php') . '" />';
           
    $html .='</form>';

    // Formatea los datos y los envia al grid...
    $ajx_datos["titulo"] = utf8_encode($titulo);
    //se inyecta la informacion en los contenedores
    $ajx_datos["html"]  = utf8_encode($html);      
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>