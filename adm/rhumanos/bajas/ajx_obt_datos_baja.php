<?php
/**
 * Complemento ajax para obtener los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';    
    include $path . 'includes/class/admtbl_bajas.class.php';
    include $path . 'includes/class/admtbl_adscripcion.class.php';
    $objBaja = new AdmtblBajas();
    $objAdscrip = new AdmtblAdscripcion();
    
    $objBaja->select($_GET["id"]);
    if ($objBaja->id_baja > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["id_motivo"] = $objBaja->id_motivo;
        $ajx_datos["fecha_baja"] = date("d/m/Y", strtotime($objBaja->fecha_baja));
        $ajx_datos["num_oficio"] = utf8_encode($objBaja->num_oficio);
        $ajx_datos["fecha_oficio"] = date("d/m/Y", strtotime($objBaja->fecha_oficio));
        $ajx_datos["observaciones"] = utf8_encode($objBaja->observaciones);
        $ajx_datos["curp"] = $objBaja->curp;
        $ajx_datos["nombre"] = $objBaja->AdmtblDatosPersonales->nombre . " " . $objBaja->AdmtblDatosPersonales->a_paterno . " " . $objBaja->AdmtblDatosPersonales->a_materno;
        $objAdscrip->select($objBaja->curp);
        $ajx_datos["categoria"] = $objAdscrip->AdmcatCategorias->categoria;
        $ajx_datos["area"] = $objAdscrip->AdmcatAreas->area;
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;        
        $ajx_datos['error'] = $objBaja->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>