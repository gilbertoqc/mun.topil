<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/archivo/_js/index.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 80%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 20%;">
                    <!-- Botones de opci�n... -->
                    <!--
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('persona_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Registrar un nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/personal_add24.png" alt="" style="border: none;" /><br />Registrar
                    </a>                    
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('ctrl_aspi');?>" id="xAspirantes" class="Tool-Bar-Btn" style="width: 100px;" title="M�dulo de gesti�n de aspirantes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/aspi24.png" alt="" style="border: none;" /><br />Reclutamiento
                    </a>
                    -->
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('form_ficha_crim');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvGrid-Personal" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 5%; text-align: center;">&nbsp;</th>
                    <th style="width: 17%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                    <th style="width: 14%;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                    <th style="width: 9%;" class="xGrid-tbCols-ColSortable">SEXO</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ESPECIALIDAD</th>
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                    <th style="width: 20%;" class="xGrid-tbCols-ColSortable">ADSCRIPCI�N</th>
                    <th style="width: 10%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <div id="dvForm-Ficha" title="SISP :: Ficha de informaci�n">
        
    </div>
    
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_obt_datos_grid.php');?>" />
    <input type="hidden" id="hdnUrlFicha" value="<?php echo $objSys->encrypt('adm/rhumanos/archivo/ajx_obt_ficha_inf.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>