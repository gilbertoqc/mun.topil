<?php
/**
 * Complemento del llamado ajax mostrar las im�genes de una subcarpeta del expediente de una persona.
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/admtbl_archivo_exped.class.php';
    $objExped = new AdmtblArchivoExped();
    
    $objExped->AdmtblArchivo->select($_SESSION["xCurp"]);    
    $path_exped = 'adm/expediente/doctos/';
    
    $html = '<div class="dvDoctos">';
    $total = $objExped->selectCount($objExped->AdmtblArchivo->id_archivo, $_POST["id_docto"]);    
    if( $total > 0 ){
        $files_docto = $objExped->selectAll("a.id_archivo=" . $objExped->AdmtblArchivo->id_archivo . " AND a.id_documento=" . $_POST["id_docto"]);
        foreach( $files_docto As $rfd => $fd ){
            $name_img = $fd["curp"] . '/' . $fd["curp"] . '_' . $fd["id_archivo"] . '_' . $fd["id_documento"] . '_' . $fd["id_registro"];
            $img_mini = $name_img . '-thb' . $fd["ext"];
            $html .= '<div class="dvDocto">';
                $rel = $fd["id_archivo"] . "-" . $fd["id_documento"] . "-" . $fd["id_registro"] . '-' . $fd["ext"];
                $html .= '<a href="#" class="lnkImgDoc" rel="' . $rel . '" title="' . $fd["descripcion"] . '">';
                    $html .= '<img src="' . $path_exped . $img_mini . '" alt="' . $fd["descripcion"] . '" />';
                $html .= '</a>';
                $html .= '<a href="#" class="lnkDocDlt" rel="' . $rel . '" title="Eliminar este documento...">Eliminar</a>';
            $html .= '</div>';
        }
    }
    $html .= '</div>';
        
    $ajx_datos["doctos"] = utf8_encode($html);
    $ajx_datos["total"] = (int)$total;
    
    echo json_encode($ajx_datos);
}
?>