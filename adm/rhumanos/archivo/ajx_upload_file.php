<?php
/**
 * Complemento del llamado ajax para cargar un archivo en una carpeta temporal del servidor.
 * Lista de par�metros recibidos por POST 
 * @param FILE uploadfile, contiene el archivo para cargar en el servidor.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    $path = "../../../";
    include $path . "includes/lib/thumbnails.lib.php";
    
    if( !$_FILES['userfile']['error'] ){
        $curp = $_SESSION["xCurp"];
        $fx = mt_rand();
        $uploaddir = '../../_uploadfiles/';	        	
        //-- Se obtien la extensi�n del archivo original...
        $ext = pathinfo($_FILES['userfile']['name'], PATHINFO_EXTENSION);        	
        $ext = '.' . strtolower($ext);
        //-- Se genera el nombre del archivo para cargar...
        $fileName = $curp . '_' . $fx;
        $destinationfile = $uploaddir . $fileName . $ext;
        
        if( move_uploaded_file($_FILES['userfile']['tmp_name'], $destinationfile) ){            
            $fileMini = $fileName . "-thb" . $ext;
            //-- Se genera la imagen en miniatura...
            $thumb = New thumbnail($destinationfile);
            $thumb->size_auto(150);
            $thumb->jpeg_quality(100);
            $thumb->save($uploaddir . $fileMini);
            
            $ajx_result['rslt'] = true;
            $ajx_result['file'] = $fileName . $ext;
            $ajx_result['file_thb'] = $fileMini;	
        } else {
            $ajx_result['rslt'] = false;
            $ajx_result['error'] = 'No se pudo cargar el archivo a la carpeta temporal';
        }
    } else {
        $ajx_result['rslt'] = false;
        switch($_FILES['userfile']['error']){
            case UPLOAD_ERR_INI_SIZE://Code 1
                $ajx_result['error'] = utf8_encode('El tama�o del archivo supera el tama�o m�ximo permitito por el servidor');
            break;
            case UPLOAD_ERR_FORM_SIZE://Code 2
                $ajx_result['error'] = utf8_encode('El tama�o del archivo supera el tama�o m�ximo permitito en este formulario');
            break;
            case UPLOAD_ERR_PARTIAL://Code 3
                $ajx_result['error'] = utf8_encode('El tama�o del archivo no se carg� completamente');
            break;
            case UPLOAD_ERR_CANT_WRITE://Code 7
                $ajx_result['error'] = utf8_encode('No se pudo guardar el archivo en el directorio especificado, es posible que no tenga permisos');
            break;
            default:
                $ajx_result['error'] = utf8_encode('Error desconocido !!!');
            break;
        }
    }
    
    echo json_encode($ajx_result);
}
?>