<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla de aspirantes en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_aspirantes.class.php';
    $objSys = new System();
    $objAspi = new AdmtblAspirantes();

    if( $objAspi->select($_GET["curp"]) ){        
        $ajx_datos["rslt"] = true;    
        $ajx_datos['curp'] = $objAspi->curp;
        $ajx_datos['nombre'] = utf8_encode($objAspi->nombre);
        $ajx_datos['a_paterno'] = utf8_encode($objAspi->a_paterno);
        $ajx_datos['a_materno'] = utf8_encode($objAspi->a_materno);
        $ajx_datos['genero'] = $objAspi->genero;
        $ajx_datos['fecha_nac'] = date("d/m/Y", strtotime($objAspi->fecha_nac));
        $ajx_datos['id_estado_civil'] = $objAspi->id_estado_civil;
        $ajx_datos['id_area'] = $objAspi->id_area;
        $ajx_datos['id_categoria'] = $objAspi->id_categoria;
        $ajx_datos['cargo'] = utf8_encode($objAspi->cargo);
        $ajx_datos['id_especialidad'] = $objAspi->id_especialidad;
        $ajx_datos['id_tipo_funcion'] = $objAspi->id_tipo_funcion;
        $ajx_datos['id_region_adscrip'] = $objAspi->id_region_adscrip;
        $ajx_datos['id_nivel_estudios'] = $objAspi->id_nivel_estudios;
        $ajx_datos['calle'] = utf8_encode($objAspi->calle);
        $ajx_datos['num_ext'] = utf8_encode($objAspi->num_ext);
        $ajx_datos['colonia'] = utf8_encode($objAspi->colonia);
        $ajx_datos['ciudad'] = utf8_encode($objAspi->ciudad);
        $ajx_datos['tel_fijo'] = $objAspi->tel_fijo;
        $ajx_datos['tel_movil'] = $objAspi->tel_movil;
        $ajx_datos['id_municipio_domi'] = $objAspi->id_municipio_domi;
        $objAspi->AdmcatMunicipios->select($objAspi->id_municipio_domi);
        $ajx_datos['id_entidad_domi'] = $objAspi->AdmcatMunicipios->id_entidad;
    } else {
        $ajx_datos["rslt"] = false;
        $ajx_datos["error"] = $objAspi->msjError;
    }    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>