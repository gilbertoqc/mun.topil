<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/ctrl_aspi.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Reclutamiento');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <?php
    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 50%;">
                    <!-- Botones de opci�n... -->
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('aspi_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Registrar un nuevo recluta...">
                        <img src="<?php echo PATH_IMAGES;?>icons/aspi_add24.png" alt="" style="border: none;" /><br />Registrar
                    </a>                    
                    <a href="#" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'];?>" id="xRegresar" class="Tool-Bar-Btn" style="" title="Regresar al m�dulo principal...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <div id="dvGrid-Aspi" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: 95%;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                    <td style="text-align: right; width: 40%;">
                        <select id="cbxStatus">
                            <option value="1">ACTIVOS</option>
                            <option value="2">PER. ACTIVO</option>
                        </select>
                    </td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 17%;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                    <th style="width: 15%;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                    <th style="width: 8%;" class="xGrid-tbCols-ColSortable">STATUS</th>
                    <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ESPECIALIDAD</th>
                    <th style="width: 13%;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                    <th style="width: 14%;" class="xGrid-tbCols-ColSortable">�REA</th>
                    <th style="width: 12%;" class="xGrid-tbCols-ColSortable">REGI�N</th>
                    <th style="width: 8%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_datos_aspi_grid.php');?>" />
    <input type="hidden" id="hdnUrlDltAspi" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_dlt_aspi.php');?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>