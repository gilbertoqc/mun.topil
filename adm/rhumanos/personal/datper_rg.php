<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
$path = '';
include_once $path . 'includes/class/admtbl_datos_personales.class.php';
$objDatPer = new AdmtblDatosPersonales();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/rhumanos/personal/_js/datosrg.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Actualización');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    $curp = $_SESSION['xCurp'];
    // Datos Personales
    $objDatPer->curp = $curp;
    $objDatPer->nombre = $_POST["txtNombre"];
    $objDatPer->a_paterno = $_POST["txtAPaterno"];
    $objDatPer->a_materno = $_POST["txtAMaterno"];
    $objDatPer->fecha_nac = $objSys->convertirFecha($_POST["txtFechaNac"], 'yyyy-mm-dd');
    $objDatPer->genero = $_POST["rbnSexo"];
    $objDatPer->rfc = $_POST["txtRfc"];
    $objDatPer->cuip = $_POST["txtCuip"];
    $objDatPer->folio_ife = $_POST["txtFolioIfe"];
    $objDatPer->mat_cartilla = $_POST["txtCartilla"];
    $objDatPer->licencia_conducir = $_POST["txtLicencia"];
    $objDatPer->pasaporte = $_POST["txtPasaporte"];
    $objDatPer->id_estado_civil = $_POST["cbxEdoCivil"];
    $objDatPer->id_tipo_sangre = ($_POST["cbxTipoSangre"] != 0) ? $_POST["cbxTipoSangre"] : null;
    $objDatPer->id_nacionalidad = 1;
    $objDatPer->id_entidad = ($_POST["cbxEntNacimiento"] != 0) ? $_POST["cbxEntNacimiento"] : null;
    $objDatPer->id_municipio = ($_POST["cbxMpioNacimiento"] != 0) ? $_POST["cbxMpioNacimiento"] : null;
    $objDatPer->lugar_nac = $_POST["txtLugarNacimiento"];
    $objDatPer->id_status = 1;
           
    //-------------------------------------------------------------------//            
    if ($objDatPer->update()) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_datos_personales', $curp, "Edt");
    } else {        
        $error = (!empty($objDatPer->msjError)) ? $objDatPer->msjError : 'Error al guardar los Datos Personales';        
    }
    
    $mod = $objSys->encrypt("datper_edit");//(empty($error)) ? $objSys->encrypt("persona_menu") : $objSys->encrypt("datper_edit"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>