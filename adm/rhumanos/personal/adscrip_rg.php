<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusión de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_adscripcion.class.php';
$objAdscrip = new AdmtblAdscripcion();

//-----------------------------------------------------------------//
//-- Bloque de definición de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="adm/_js/rhumanos/personal/datosrg.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Actualización');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido dinámico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    $curp = $_SESSION['xCurp'];
    // Adscripción
    $objAdscrip->curp = $curp;
    $objAdscrip->id_corporacion = 1;
    $objAdscrip->id_area = $_POST["cbxArea"];
    $objAdscrip->id_tipo_funcion = $_POST["cbxTipoFuncion"];
    $objAdscrip->id_especialidad = $_POST["cbxEspecialidad"];
    $objAdscrip->id_categoria = $_POST["cbxCategoria"];
    $objAdscrip->cargo = $_POST["txtCargo"];
    $objAdscrip->id_nivel_mando = $_POST["cbxNivelMando"];
    $objAdscrip->fecha_ing = $objSys->convertirFecha($_POST["txtFechaIng"], 'yyyy-mm-dd');
    $objAdscrip->id_horario = $_POST["cbxHorario"];
    $objAdscrip->id_municipio = $_POST["cbxMpioAdscripcion"];
    $objAdscrip->id_ubicacion = $_POST["cbxUbicacion"];
    $objAdscrip->id_entidad = 12;//Guerrero
    //$objAdscrip->id_plaza = ?;
           
    //-------------------------------------------------------------------//            
    if ($objAdscrip->update()) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_datos_personales', $curp, "Upd");
    } else {        
        $error = (!empty($objAdscrip->msjError)) ? $objAdscrip->msjError : 'Error al guardar los Datos Personales';        
    }
    
    $mod = $objSys->encrypt("adscrip_edit");//(empty($error)) ? $objSys->encrypt("persona_menu") : $objSys->encrypt("adscrip_edit"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>