<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admcat_municipios.class.php';
include 'includes/class/admcat_estado_civil.class.php';
include 'includes/class/admcat_tipo_sangre.class.php';
include 'includes/class/admtbl_datos_personales.class.php';

$objMunicipio = new AdmcatMunicipios();
$objEdoCivil = new AdmcatEstadoCivil();
$objTipoSangre = new AdmcatTipoSangre();

$objDatPer = new AdmtblDatosPersonales();
$objDatPer->select($_SESSION['xCurp']);

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Recursos Humanos',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/curp.js"></script>',
                                   '<script type="text/javascript" src="adm/rhumanos/personal/_js/datper.js?v=1.0"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"] . '&mod=' . $objSys->encrypt('persona_menu');
    $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('datper_rg');
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <?php
                    if( $objDatPer->id_status != 2 ){// diferente de BAJA
                    ?>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los cambios realizados...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <?php
                    }
                    ?>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la modificaci�n de datos...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>
    
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; text-align: left; width: 750px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Actualizaci�n : Datos Personales</span>
            <fieldset class="fsetForm-Data" style="width: auto;">                                
                <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;"> 
                    <tr>
                        <td><label for="txtCurp">C.U.R.P.:</label></td>
                        <td class="validation" style="width: 400px;">
                            <input type="text" name="txtCurp" id="txtCurp" value="<?php echo $objDatPer->curp;?>" maxlength="18" readonly="true" title="..." style="font-size: 12pt; padding: 5px; width: 210px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>              
                    <tr>
                        <td><label for="txtNombre">Nombre:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNombre" id="txtNombre" value="<?php echo $objDatPer->nombre;?>" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtAPaterno">Apellido Paterno:</label></td>
                        <td class="validation">
                            <input type="text" name="txtAPaterno" id="txtAPaterno" value="<?php echo $objDatPer->a_paterno;?>" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtAMaterno">Apellido Materno:</label></td>
                        <td class="validation">
                            <input type="text" name="txtAMaterno" id="txtAMaterno" value="<?php echo $objDatPer->a_materno;?>" maxlength="35" title="..." style="width: 200px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaNac">Fecha de Nacimiento:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFechaNac" id="txtFechaNac" value="<?php echo date('d/m/Y', strtotime($objDatPer->fecha_nac));?>" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label>Sexo:</label></td>
                        <td class="validation">
                            <?php
                            if ($objDatPer->genero == 1) {
                                $rbnSexo1 = 'checked="true"';
                                $rbnSexo2 = '';
                            } else {
                                $rbnSexo1 = '';
                                $rbnSexo2 = 'checked="true"';
                            } 
                            ?>
                            <label class="label-Radio" style="margin-right: 10px;"><input type="radio" name="rbnSexo" id="rbnSexo1" value="1" <?php echo $rbnSexo1;?> />Masculino</label>
                            <label class="label-Radio"><input type="radio" name="rbnSexo" id="rbnSexo2" value="2" <?php echo $rbnSexo2;?> />Femenino</label>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtRfc">R.F.C.:</label></td>
                        <td class="validation">
                            <input type="text" name="txtRfc" id="txtRfc" value="<?php echo $objDatPer->rfc;?>" maxlength="14" title="..." style="width: 140px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCuip">C.U.I.P.:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCuip" id="txtCuip" value="<?php echo $objDatPer->cuip;?>" maxlength="20" title="..." style="width: 200px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFolioIfe">Folio IFE:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFolioIfe" id="txtFolioIfe" value="<?php echo $objDatPer->folio_ife;?>" maxlength="20" title="..." style="width: 150px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtCartilla">Matr�cula del S.M.N.:</label></td>
                        <td class="validation">
                            <input type="text" name="txtCartilla" id="txtCartilla" value="<?php echo $objDatPer->mat_cartilla;?>" maxlength="10" title="..." style="width: 100px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtLicencia">Licencia de Conducir:</label></td>
                        <td class="validation">
                            <input type="text" name="txtLicencia" id="txtLicencia" value="<?php echo $objDatPer->licencia_conducir;?>" maxlength="11" title="..." style="width: 100px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtPasaporte">Pasaporte:</label></td>
                        <td class="validation">
                            <input type="text" name="txtPasaporte" id="txtPasaporte" value="<?php echo $objDatPer->pasaporte;?>" maxlength="10" title="..." style="width: 100px;" />
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxEdoCivil">Estado Civil:</label></td>
                        <td class="validation">
                            <select name="cbxEdoCivil" id="cbxEdoCivil" title="">                                
                                <?php
                                echo $objEdoCivil->shwEstadoCivil($objDatPer->id_estado_civil);
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxTipoSangre">Grupo Sanguineo:</label></td>
                        <td class="validation">
                            <select name="cbxTipoSangre" id="cbxTipoSangre">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objTipoSangre->shwTipoSangre($objDatPer->id_tipo_sangre);
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left;">
                            <fieldset class="fsetForm-Data" style="border: 1px solid #c1c1bf; width: 590px;">
                                <legend style="border: 1px solid #c1c1bf; font-size: 9pt;">Lugar de Nacimiento</legend>        
                                <table class="tbForm-Data">
                                    <tr>
                                        <td><label for="cbxEntNacimiento">Entidad Federativa:</label></td>
                                        <td class="validation">
                                            <select name="cbxEntNacimiento" id="cbxEntNacimiento">
                                                <option value="0">&nbsp;</option>
                                                <?php
                                                echo $objMunicipio->AdmcatEntidades->shwEntidades($objDatPer->id_entidad, 1);// Pa�s: 1=M�xico
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="cbxMpioNacimiento">Municipio:</label></td>
                                        <td class="validation">
                                            <select name="cbxMpioNacimiento" id="cbxMpioNacimiento" style="max-width: 500px;">
                                                <option value="0"></option>
                                                <?php
                                                if ($objDatPer->id_entidad != 0) {
                                                    echo $objMunicipio->shwMunicipios($objDatPer->id_municipio, $objDatPer->id_entidad);
                                                }
                                                ?>
                                            </select>                                                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="txtLugarNacimiento">Ciudad o Localidad:</label></td>
                                        <td class="validation">
                                            <input type="text" name="txtLugarNacimiento" id="txtLugarNacimiento" value="<?php echo $objDatPer->lugar_nac;?>" maxlength="200" title="..." style="width: 350px;" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </fieldset>           
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
        <input type="hidden" name="dtTypeOper" value="2" />
        <input type="hidden" id="hdnUrlMpio" value="<?php echo $objSys->encrypt('adm/rhumanos/personal/ajx_obt_municipios.php');?>" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>