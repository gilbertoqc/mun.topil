var xGrid;
$(document).ready(function(){
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Aspi').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Aspi').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xAjaxParam: 'stat=' + $('#cbxStatus').val(),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });
    
    //-- Definici�n del dialog como formulario de registro general...
    $('#dvForm-Registro').dialog({
        autoOpen: false,
        width: 600,
        modal: true,
        resizable: false,
        buttons:{
            'Aceptar': function(){                
                if( setRegistro() ){
                    $(this).dialog('close');
                    //-- Mensaje...
                    var msj = $(getHTMLMensaje('En hora buena !!! el proceso se realiz� correctamente.', 1));
                    msj.dialog({
                        autoOpen: true,                        
                        minWidth: 350,
                        resizable: false,
                        modal: true,
                        buttons:{
                            'Aceptar': function(){
                                $(this).dialog('close');
                                location.reload();
                            }
                        }
                    });
                }
            },
            'Cancelar': function(){                
                $(this).dialog('close');
            }
        },
        close: function(evt){
            clearFormRegistro();
        }
    });
    
    //-- Funcionalidad de los botones de la barra de herramientas...
    $('#xRegistrar').click(function(){
        //$('#dvForm-Registro').dialog('open'); 
    });
    $('#xReportes').click(function(){
        $('#dvForm-Registro').dialog('open'); 
    });
    
    $('#dvGrid-Aspi a.lnkBtnOpcionGrid').live('click', function(){
        var prm = $(this).attr("rel").split('-');
        if( prm[0] == 'dlt' ){
            bajaAspi(prm[1]);
        }
    });    
    
    $('#cbxStatus').change(function(){
        xGrid.resetAjaxParam();
        xGrid.addParamAjax('stat=' + $(this).val());
        xGrid.refreshGrid();
    });
});

function bajaAspi(id){
    //-- Confirmaci�n...
    var msj = $(getHTMLMensaje('�Est� seguro que desea dar debaja a este recluta?.', 2));
    msj.dialog({
        autoOpen: true,                        
        minWidth: 450,
        resizable: false,
        modal: true,
        buttons:{
            'S� estoy seguro': function(){
                $.ajax({
                    url: xDcrypt($('#hdnUrlDltAspi').val()),
                    data: {'id': id},
                    dataType: 'json',
                    async: true,
                    cache: false,
                    beforeSend: function(){
                        msj.html('<div class="xGrid-dvWait-LoadData" style="margin-top: 10px; padding-top: 55px;">Procesando, por favor espere...</div>');
                        msj.dialog({
                            title: 'Procesando...',
                            buttons:{}
                        });
                    },
                    success: function(xdata){
                        msj.dialog('close');
                        if( xdata.rslt ){
                            xGrid.refreshGrid();                            
                        } else {
                            shwError(xdata.error, 450);
                        }
                    },
                    error: function(objeto, detalle, otroobj){
                        shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                    }              
                });
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });
}