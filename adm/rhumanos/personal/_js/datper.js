$(document).ready(function(){    
    // Inicializaci�n de los componentes Fecha
    $('#txtFechaNac').datepicker({
        yearRange: '1920:2002',
    });    
        
    // Convierte a may�sculas el contenido de los textbox y textarea
    $('#txtNombre').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAPaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtAMaterno').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtRfc').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCuip').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtCartilla').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtLicencia').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtPasaporte').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });    
    $('#txtLugarNacimiento').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });    
    // Control especial del campo CURP.
    $('#txtCurp').blur(function(){
        $(this).val(xTrim( $(this).val().toUpperCase() ));
        if (msjErrorCURP != '') {
            shwError(msjErrorCURP);
        }
        else if ($(this).val() != '') {
            // Obtiene la fecha de nacimiento
            var fecha = $(this).val().substr(4, 6);
            $('#txtFechaNac').val(fecha.substr(4, 2) + '/' + fecha.substr(2, 2) + '/19' + fecha.substr(0, 2) );
            // Asigna el rfc autom�tico
            $("#txtRfc").val( $(this).val().substr(0, 10) );
            // Asigna el g�nero autom�tico
            var genero = $(this).val().substr(10, 1);  
            if (genero == "H")
                $("#rbnSexo1").attr('checked', 'checked');
            else if (genero == "M" )
                $("#rbnSexo2").attr('checked', 'checked');
        }
    });
    
    // Control para la carga din�mica de municipios
    $('#cbxEntNacimiento').change(function(){        
        obtenerMunicipios($(this).val());  
    });
    
    // Aplicaci�n de las reglas de validaci�n de los campos del formulario
    jQuery.validator.addMethod("valCURP", function(value, element) {
        return validaCURP(value);
    });
    $('#frmRegistro').validate({
        rules:{
            // Datos Personales
            txtCurp:{
                required: true,
                minlength: 18,
                valCURP: true,    
            },
            txtNombre: 'required',            
            txtAPaterno: 'required',  
            txtAMaterno: 'required',
            rbnSexo: 'required',
            txtFechaNac: 'required',
            txtRfc: 'required',
            txtCuip:{
                minlength: 16,
            },
            txtFolioIfe: 'digits',
            cbxEdoCivil:{
                required: true,
                min: 1,
            }
    	},
    	messages:{
    	   txtCurp:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                minlength: '<span class="ValidateError" title="El tama�o de la CURP debe de ser de 18 caracteres"></span>',
                valCURP: '<span class="ValidateError" title="La CURP especificada no es v�lida"></span>',    
            },
    		txtNombre: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAPaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtAMaterno: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            rbnSexo: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtFechaNac: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtRfc: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            txtCuip:{ 
                minlength: '<span class="ValidateError" title="El tama�o de la CUIP debe de ser de al menos 16 caracteres"></span>',
            },
            txtFolioIfe: '<span class="ValidateError" title="Este campo s�lo acepta d�gitos"></span>',
            cbxEdoCivil:{
                required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
                min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            }
    	},
    	errorClass: "help-inline",
    	errorElement: "span",
    	highlight:function(element, errorClass, validClass) {
            $(element).parents('.validation').removeClass('success').addClass('error');
    	},
    	unhighlight: function(element, errorClass, validClass){
    		$(element).parents('.validation').removeClass('error').addClass('success');
    	}
    }); 
    
   
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida) {
            var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los cambios realizados a los datos del elemento?', 2));
            frmPreg.dialog({
                autoOpen: true,
                modal: true,
                resizable: false,
                width: 380,
                buttons:{
                    'Aceptar': function(){
                        $('#frmRegistro').submit();                        
                    },
                    'Cancelar': function(){
                        $(this).dialog('close');
                    }
                }
            });
        }           
    });  
    
    /*--------------------------------------------------------------------//
     * Controla el scroll para seguir mostrando el header y el toolbar...
     *-------------------------------------------------------------------*/
    var toolbar = $('#dvTool-Bar');        
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        //alert(toolbar.offset().top);
        if ($(this).scrollTop() > toolbar.offset().top) {
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        } else {
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');            
        }
    });    
});

function obtenerMunicipios(id){    
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_entidad': id, 'filtro': 1},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#cbxMpioNacimiento').html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxMpioNacimiento').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}