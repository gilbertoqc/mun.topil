<?php
/**
 * Complemento ajax para guardar los datos de una referencia. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/admtbl_datos_nomina.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    $objDatPlaza = new AdmtblDatosNomina();

    // Se asignan los valores a los campos
    $curp = $_SESSION['xCurp'];
    // Domicilio
    $objDatPlaza->curp = $curp;
    $objDatPlaza->id_plaza = $_POST["hdnIdPlaza"];
    $objDatPlaza->id_tipo_plaza = $_POST["cbxTipoPlaza"];
    $objDatPlaza->num_empleado = $_POST["txtNumEmpleado"];
    $objDatPlaza->fecha_alta = date('Y-m-d');
    $objDatPlaza->nueva_creacion = ( isset($_POST["chkNuevaCreacion"]) ) ? 1 : 0;
    $objDatPlaza->clave_categoria = null;
    $objDatPlaza->sueldo = ( !empty($_POST["txtSueldo"]) ) ? $_POST["txtSueldo"] : '0.00';
    $objDatPlaza->id_banco = ( !empty($_POST["cbxBanco"]) ) ? $_POST["cbxBanco"] : null;
    $objDatPlaza->cuenta = $_POST["txtNumCuenta"];
    $objDatPlaza->oficina_pagadora = $_POST["txtOficinaPag"];
        
    if( $_POST["dtTypeOper"] == 1 ){
        $result = $objDatPlaza->insert();
        // Se marca la plaza como asignada...
        $objDatPlaza->AdmtblPlantillaPlazas->select($objDatPlaza->id_plaza);
        $objDatPlaza->AdmtblPlantillaPlazas->status = 2;
        $objDatPlaza->AdmtblPlantillaPlazas->update();
        $oper = 'Ins';
    } else {
        $result = $objDatPlaza->update();
        $oper = 'Edt';
    }         
    //-------------------------------------------------------------------//            
    if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'admtbl_datos_nomina', $curp, $oper);
        $ajx_datos['rslt']  = true;
        $ajx_datos['msj'] = utf8_encode('Los datos se guardaron satisfactoriamente');
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = (!empty($objDatPlaza->msjError)) ? utf8_encode($objDatPlaza->msjError) : utf8_encode('Error al guardar los datos de la N�mina');
    } 
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>