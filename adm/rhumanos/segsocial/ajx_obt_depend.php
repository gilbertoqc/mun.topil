<?php
/**
 * Complemento ajax para obtener los datos de un dependiente econ�mico. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/admtbl_dependientes.class.php';
    $objDepend = new AdmtblDependientes();
    
    $objDepend->select($_GET["id"]);
    if ($objDepend->id_dependiente > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["id_dependiente"] = $objDepend->id_dependiente;
        $ajx_datos["id_parentesco"] = $objDepend->id_parentesco;
        $ajx_datos["nombre"] = utf8_encode($objDepend->nombre);        
        $ajx_datos["a_paterno"] = utf8_encode($objDepend->a_paterno);
        $ajx_datos["a_materno"] = utf8_encode($objDepend->a_materno);
        $ajx_datos["fecha_nac"] = ( !empty($objDepend->fecha_nac) ) ? date("d/m/Y", strtotime($objDepend->fecha_nac)) : '';
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;        
        $ajx_datos['error'] = $objDepend->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>