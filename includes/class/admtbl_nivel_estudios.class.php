<?php
/**
 *
 */
class AdmtblNivelEstudios
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_nivel_estudios; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $eficencia_term; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $institucion; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $carrera; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $especialidad; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cct; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $documento; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $folio; /** @Tipo: varchar(25), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $n_cedula; /** @Tipo: varchar(10), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $promedio; /** @Tipo: double(4,2) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatNivelEstudios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_nivel_estudios.class.php';
        require_once 'admtbl_datos_personales.class.php';
        $this->AdmcatNivelEstudios = new AdmcatNivelEstudios();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, id_nivel_estudios, eficencia_term, institucion, carrera, especialidad, cct, documento, folio, n_cedula, promedio
                FROM admtbl_nivel_estudios
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->id_nivel_estudios = $data['id_nivel_estudios'];
            $this->eficencia_term = $data['eficencia_term'];
            $this->institucion = $data['institucion'];
            $this->carrera = $data['carrera'];
            $this->especialidad = $data['especialidad'];
            $this->cct = $data['cct'];
            $this->documento = $data['documento'];
            $this->folio = $data['folio'];
            $this->n_cedula = $data['n_cedula'];
            $this->promedio = $data['promedio'];

            $this->AdmcatNivelEstudios->select($this->id_nivel_estudios);
            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.id_nivel_estudios, a.eficencia_term, a.institucion, a.carrera, a.especialidad, a.cct, a.documento, a.folio, a.n_cedula, a.promedio,
                  b.id_nivel_estudios, b.nivel_estudios,
                  c.curp, c.nombre, c.a_paterno, c.a_materno, c.fecha_nac, c.genero, c.rfc, c.cuip, c.folio_ife, c.mat_cartilla, c.licencia_conducir, c.pasaporte, c.id_estado_civil, c.id_tipo_sangre, c.id_nacionalidad, c.id_entidad, c.id_municipio, c.lugar_nac, c.id_status
                FROM admtbl_nivel_estudios a 
                 LEFT JOIN admcat_nivel_estudios b ON a.id_nivel_estudios=b.id_nivel_estudios
                 LEFT JOIN admtbl_datos_personales c ON a.curp=c.curp";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'id_nivel_estudios' => $data['id_nivel_estudios'],
                               'eficencia_term' => $data['eficencia_term'],
                               'institucion' => $data['institucion'],
                               'carrera' => $data['carrera'],
                               'especialidad' => $data['especialidad'],
                               'cct' => $data['cct'],
                               'documento' => $data['documento'],
                               'folio' => $data['folio'],
                               'n_cedula' => $data['n_cedula'],
                               'promedio' => $data['promedio'],
                               'admcat_nivel_estudios_nivel_estudios' => $data['nivel_estudios'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_nivel_estudios(curp, id_nivel_estudios, eficencia_term, institucion, carrera, especialidad, cct, documento, folio, n_cedula, promedio)
                VALUES(:curp, :id_nivel_estudios, :eficencia_term, :institucion, :carrera, :especialidad, :cct, :documento, :folio, :n_cedula, :promedio);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":id_nivel_estudios" => $this->id_nivel_estudios, ":eficencia_term" => $this->eficencia_term, ":institucion" => $this->institucion, ":carrera" => $this->carrera, ":especialidad" => $this->especialidad, ":cct" => $this->cct, ":documento" => $this->documento, ":folio" => $this->folio, ":n_cedula" => $this->n_cedula, ":promedio" => $this->promedio));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_nivel_estudios
                   SET id_nivel_estudios=:id_nivel_estudios, eficencia_term=:eficencia_term, institucion=:institucion, carrera=:carrera, especialidad=:especialidad, cct=:cct, documento=:documento, folio=:folio, n_cedula=:n_cedula, promedio=:promedio
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":id_nivel_estudios" => $this->id_nivel_estudios, ":eficencia_term" => $this->eficencia_term, ":institucion" => $this->institucion, ":carrera" => $this->carrera, ":especialidad" => $this->especialidad, ":cct" => $this->cct, ":documento" => $this->documento, ":folio" => $this->folio, ":n_cedula" => $this->n_cedula, ":promedio" => $this->promedio));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>