<?php
/**
 *
 */
class OpecatMidMunicipiosCuarteles
{
    public $id_municipio; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_cuartel; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidCuarteles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_municipios.class.php';
        require_once 'opecat_mid_cuarteles.class.php';
        $this->OpecatMidMunicipios = new OpecatMidMunicipios();
        $this->OpecatMidCuarteles = new OpecatMidCuarteles();
    }

     /**
     * Funci�n para mostrar la lista de --- dentro de un combobox.
     * @param int $id, id .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Municipios_Cuarteles($id=0){
        $aryDatos = $this->selectAll('id_municipio Asc','');
        $html = '<option value="0"></option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_municipio"] )
                $html .= '<option value="'.$datos["id_municipio"].'" selected>'.$datos["id_cuartel"].'</option>';
            else
                $html .= '<option value="'.$datos["id_municipio"].'" >'.$datos["id_cuartel"].'</option>';
        }
        return $html;
    }


    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_municipio, $id_cuartel)
    {
        $sql = "SELECT id_municipio, id_cuartel
                FROM opecat_mid_municipios_cuarteles
                WHERE id_municipio=:id_municipio AND id_cuartel=:id_cuartel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_municipio' => $id_municipio, ':id_cuartel' => $id_cuartel));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_municipio = $data['id_municipio'];
            $this->id_cuartel = $data['id_cuartel'];

            $this->OpecatMidMunicipios->select($this->id_municipio);
            $this->OpecatMidCuarteles->select($this->id_cuartel);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_municipio, a.id_cuartel,
                  b.id_municipio, b.id_estado, b.municipio, b.alcalde,
                  c.id_cuartel, c.id_region, c.id_cuartel_tipo, c.cuartel, c.oficial
                FROM opecat_mid_municipios_cuarteles a
                 LEFT JOIN opecat_mid_municipios b ON a.id_municipio=b.id_municipio
                 LEFT JOIN opecat_mid_cuarteles c ON a.id_cuartel=c.id_cuartel";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_municipio' => $data['id_municipio'],
                               'id_cuartel' => $data['id_cuartel'],
                               'opecat_mid_municipios_id_estado' => $data['id_estado'],
                               'opecat_mid_municipios_municipio' => $data['municipio'],
                               'opecat_mid_municipios_alcalde' => $data['alcalde'],
                               'opecat_mid_cuarteles_id_region' => $data['id_region'],
                               'opecat_mid_cuarteles_id_cuartel_tipo' => $data['id_cuartel_tipo'],
                               'opecat_mid_cuarteles_cuartel' => $data['cuartel'],
                               'opecat_mid_cuarteles_oficial' => $data['oficial'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_municipios_cuarteles(id_municipio, id_cuartel)
                VALUES(:id_municipio, :id_cuartel);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_municipio" => $this->id_municipio, ":id_cuartel" => $this->id_cuartel));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_municipios_cuarteles
                   SET
                WHERE id_municipio=:id_municipio AND id_cuartel=:id_cuartel;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_municipio" => $this->id_municipio, ":id_cuartel" => $this->id_cuartel));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>