<?php
/**
 *
 */
class OpetblMpcPerfilesCriminales
{
    public $id_perfil_criminal; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_situacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $nombre; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $apellido_paterno; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $apellido_materno; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $alias; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $sexo; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_nacimineto; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_nacionalidad; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $lugar_origen; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_estado_civil; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $complexion; /** @Tipo: varchar(45), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ocupacion; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $domicilio; /** @Tipo: varchar(200), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $estatura; /** @Tipo: decimal(4,2), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $Peso; /** @Tipo: decimal(6,2), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cabello; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $colorPiel; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ColorOjos; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fotografia_frente;
    public $edad;

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMpcEstadosCiviles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMpcNacionalidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMpcSituaciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mpc_estados_civiles.class.php';
        require_once 'opecat_mpc_nacionalidades.class.php';
        require_once 'opecat_mpc_situaciones.class.php';
        $this->OpecatMpcEstadosCiviles = new OpecatMpcEstadosCiviles();
        $this->OpecatMpcNacionalidades = new OpecatMpcNacionalidades();
        $this->OpecatMpcSituaciones = new OpecatMpcSituaciones();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_perfil_criminal)
    {
        $sql = "SELECT id_perfil_criminal, id_situacion, nombre, apellido_paterno, apellido_materno, alias, sexo, fecha_nacimineto, id_nacionalidad, lugar_origen, id_estado_civil, complexion, ocupacion, domicilio, estatura, Peso, cabello, colorPiel, ColorOjos,fotografia_frente,edad
                FROM opetbl_mpc_perfiles_criminales
                WHERE id_perfil_criminal=:id_perfil_criminal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_perfil_criminal' => $id_perfil_criminal));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_perfil_criminal = $data['id_perfil_criminal'];
            $this->id_situacion = $data['id_situacion'];
            $this->nombre = $data['nombre'];
            $this->apellido_paterno = $data['apellido_paterno'];
            $this->apellido_materno = $data['apellido_materno'];
            $this->alias = $data['alias'];
            $this->sexo = $data['sexo'];
            $this->fecha_nacimineto = $data['fecha_nacimineto'];
            $this->id_nacionalidad = $data['id_nacionalidad'];
            $this->lugar_origen = $data['lugar_origen'];
            $this->id_estado_civil = $data['id_estado_civil'];
            $this->complexion = $data['complexion'];
            $this->ocupacion = $data['ocupacion'];
            $this->domicilio = $data['domicilio'];
            $this->estatura = $data['estatura'];
            $this->Peso = $data['Peso'];
            $this->cabello = $data['cabello'];
            $this->colorPiel = $data['colorPiel'];
            $this->ColorOjos = $data['ColorOjos'];
            $this->fotografia_frente = $data['fotografia_frente'];
            $this->edad = $data['edad'];

            $this->OpecatMpcEstadosCiviles->select($this->id_estado_civil);
            $this->OpecatMpcNacionalidades->select($this->id_nacionalidad);
            $this->OpecatMpcSituaciones->select($this->id_situacion);
            
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_perfil_criminal, a.id_situacion, a.nombre, a.apellido_paterno, a.apellido_materno, a.alias, a.sexo, a.fecha_nacimineto, a.id_nacionalidad, a.lugar_origen, a.id_estado_civil, a.complexion, a.ocupacion, a.domicilio, a.estatura, a.Peso, a.cabello, a.colorPiel, a.ColorOjos,
                  b.id_estado_civil, b.estado_civil,
                  c.id_nacionalidad, c.nacionalidad,
                  d.id_situacion, d.situacion,a.fotografia_frente,a.edad
                FROM opetbl_mpc_perfiles_criminales a
                 LEFT JOIN opecat_mpc_estados_civiles b ON a.id_estado_civil=b.id_estado_civil
                 LEFT JOIN opecat_mpc_nacionalidades c ON a.id_nacionalidad=c.id_nacionalidad
                 LEFT JOIN opecat_mpc_situaciones d ON a.id_situacion=d.id_situacion";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_perfil_criminal' => $data['id_perfil_criminal'],
                               'id_situacion' => $data['id_situacion'],
                               'nombre' => $data['nombre'],
                               'apellido_paterno' => $data['apellido_paterno'],
                               'apellido_materno' => $data['apellido_materno'],
                               'alias' => $data['alias'],
                               'sexo' => $data['sexo'],
                               'fecha_nacimineto' => $data['fecha_nacimineto'],
                               'id_nacionalidad' => $data['id_nacionalidad'],
                               'lugar_origen' => $data['lugar_origen'],
                               'id_estado_civil' => $data['id_estado_civil'],
                               'complexion' => $data['complexion'],
                               'ocupacion' => $data['ocupacion'],
                               'domicilio' => $data['domicilio'],
                               'estatura' => $data['estatura'],
                               'Peso' => $data['Peso'],
                               'cabello' => $data['cabello'],
                               'colorPiel' => $data['colorPiel'],
                               'ColorOjos' => $data['ColorOjos'],
                               'fotografia_frente' => $data['fotografia_frente'],
                               'edad' => $data['edad'],
                               'opecat_mpc_estados_civiles_estado_civil' => $data['estado_civil'],
                               'opecat_mpc_nacionalidades_nacionalidad' => $data['nacionalidad'],
                               'opecat_mpc_situaciones_situacion' => $data['situacion'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }



    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT pc.id_perfil_criminal,sit.situacion,pc.nombre,pc.apellido_paterno,pc.apellido_materno,pc.alias,pc.sexo,
                 pc.fecha_nacimineto, nal.nacionalidad ,gd.grupo_delictivo,dc.cargo_ocupa, pc.complexion,pc.fotografia_frente,pc.edad,pc.estatura,
                 (concat('ESTATURA: ',pc.estatura, ' m., PESO: ',pc.Peso,' kg., CABELLO: ',pc.cabello,', PIEL: ',pc.colorPiel))  as aspecto,
                 dc.zona_operacion,dc.modus_operandis
                 FROM opetbl_mpc_perfiles_criminales as pc
                 LEFT JOIN opecat_mpc_nacionalidades as nal ON pc.id_nacionalidad=nal.id_nacionalidad
                 LEFT JOIN opecat_mpc_situaciones as sit ON pc.id_situacion=sit.id_situacion
                 left join opetbl_mpc_datos_criminales as dc ON pc.id_perfil_criminal=dc.id_perfil_criminal
                 LEFT JOIN opecat_mpc_grupos_delictivos as gd ON dc.id_grupo_delictivo=gd.id_grupo_delictivo";

        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_perfil_criminal' => $data['id_perfil_criminal'],
                                'situacion' =>  $data['situacion'],
                                'nombre' =>  $data['nombre'],
                                'apellido_paterno' =>  $data['apellido_paterno'],
                                'apellido_materno' =>  $data['apellido_materno'],
                                'alias' =>  $data['alias'],
                                'sexo' =>  $data['sexo'],
                                'fecha_nacimineto' =>  $data['fecha_nacimineto'],
                                'nacionalidad' =>  $data['nacionalidad'],
                                'grupo_delictivo' =>  $data['grupo_delictivo'],
                                'cargo_ocupa' =>  $data['cargo_ocupa'],
                                'complexion' => $data['complexion'],
                                'aspecto' => $data['aspecto'],
                                'estatura' => $data['estatura'],
                                'fotografia_frente' => $data['fotografia_frente'],
                                'edad' => $data['edad'],
                                'zona_operacion' => $data['zona_operacion'],
                                'modus_operandis' => $data['modus_operandis']
                                );
                                
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
                 FROM opetbl_mpc_perfiles_criminales as pc
                 LEFT JOIN opecat_mpc_nacionalidades as nal ON pc.id_nacionalidad=nal.id_nacionalidad
                 LEFT JOIN opecat_mpc_situaciones as sit ON pc.id_situacion=sit.id_situacion
                 left join opetbl_mpc_datos_criminales as dc ON pc.id_perfil_criminal=dc.id_perfil_criminal
                 LEFT JOIN opecat_mpc_grupos_delictivos as gd ON dc.id_grupo_delictivo=gd.id_grupo_delictivo";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }


    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mpc_perfiles_criminales(id_perfil_criminal, id_situacion, nombre, apellido_paterno, apellido_materno, alias, sexo, fecha_nacimineto, id_nacionalidad, lugar_origen, id_estado_civil, complexion, ocupacion, domicilio, estatura, Peso, cabello, colorPiel, ColorOjos,fotografia_frente,edad)
                VALUES(:id_perfil_criminal, :id_situacion, :nombre, :apellido_paterno, :apellido_materno, :alias, :sexo, :fecha_nacimineto, :id_nacionalidad, :lugar_origen, :id_estado_civil, :complexion, :ocupacion, :domicilio, :estatura, :Peso, :cabello, :colorPiel, :ColorOjos, :fotografia_frente, :edad);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_perfil_criminal" => $this->id_perfil_criminal, ":id_situacion" => $this->id_situacion, ":nombre" => $this->nombre, ":apellido_paterno" => $this->apellido_paterno, ":apellido_materno" => $this->apellido_materno, ":alias" => $this->alias, ":sexo" => $this->sexo, ":fecha_nacimineto" => $this->fecha_nacimineto, ":id_nacionalidad" => $this->id_nacionalidad, ":lugar_origen" => $this->lugar_origen, ":id_estado_civil" => $this->id_estado_civil, ":complexion" => $this->complexion, ":ocupacion" => $this->ocupacion, ":domicilio" => $this->domicilio, ":estatura" => $this->estatura, ":Peso" => $this->Peso, ":cabello" => $this->cabello, ":colorPiel" => $this->colorPiel, ":ColorOjos" => $this->ColorOjos,":fotografia_frente" => $this->fotografia_frente,":edad" => $this->edad));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mpc_perfiles_criminales
                SET id_situacion=:id_situacion, nombre=:nombre, apellido_paterno=:apellido_paterno, apellido_materno=:apellido_materno, alias=:alias, sexo=:sexo, fecha_nacimineto=:fecha_nacimineto, id_nacionalidad=:id_nacionalidad, lugar_origen=:lugar_origen, id_estado_civil=:id_estado_civil, complexion=:complexion, ocupacion=:ocupacion, domicilio=:domicilio, estatura=:estatura, Peso=:Peso, cabello=:cabello, colorPiel=:colorPiel, ColorOjos=:ColorOjos,fotografia_frente=:fotografia_frente, edad=:edad
                WHERE id_perfil_criminal=:id_perfil_criminal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_perfil_criminal" => $this->id_perfil_criminal, ":id_situacion" => $this->id_situacion, ":nombre" => $this->nombre, ":apellido_paterno" => $this->apellido_paterno, ":apellido_materno" => $this->apellido_materno, ":alias" => $this->alias, ":sexo" => $this->sexo, ":fecha_nacimineto" => $this->fecha_nacimineto, ":id_nacionalidad" => $this->id_nacionalidad, ":lugar_origen" => $this->lugar_origen, ":id_estado_civil" => $this->id_estado_civil, ":complexion" => $this->complexion, ":ocupacion" => $this->ocupacion, ":domicilio" => $this->domicilio, ":estatura" => $this->estatura, ":Peso" => $this->Peso, ":cabello" => $this->cabello, ":colorPiel" => $this->colorPiel, ":ColorOjos" => $this->ColorOjos, ":fotografia_frente" => $this->fotografia_frente, ":edad" => $this->edad));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
    
  
    
    /**
     * Funci�n para obtener un el total de registros pertenecientes a un ID
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
     
    public function TotalDatosCriminales($id_perfil){
       $sql = "SELECT count(*) AS total_registros
                FROM opetbl_mpc_datos_criminales
                WHERE id_perfil_criminal=$id_perfil;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["total_registros"];            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return x;
        }
    }
    public function TotalAntecedentesCriminales($id_perfil){       
        $sql = "SELECT count(*) AS total_registros
                FROM opetbl_mpc_antecedentes_criminales
                WHERE id_perfil_criminal=$id_perfil;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["total_registros"];            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return x;
        }
    }
    
    public function TotalAnalisisCriminales($id_perfil){
         $sql = "SELECT count(*) AS total_registros
                FROM opetbl_mpc_analisis_criminal
                WHERE id_perfil_criminal=$id_perfil;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["total_registros"];            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return x;
        }
    }
    
    
}


?>