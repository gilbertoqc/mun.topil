<?php
/**
 *
 */
class LogtblArmHistorialAsignaDescarga
{
    public $id_registro; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $matricula; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_operacion; /** @Tipo: tinyint(2), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $oficio; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha; /** @Tipo: timestamp, @Acepta Nulos: NO, @Llave: --, @Default: CURRENT_TIMESTAMP */
    public $usuario; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblArmArmamento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatArmOperacion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_arm_armamento.class.php';
        require_once 'logcat_arm_operacion.class.php';
        $this->LogtblArmArmamento = new LogtblArmArmamento();
        $this->LogcatArmOperacion = new LogcatArmOperacion();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_registro)
    {
        $sql = "SELECT id_registro, matricula, curp, id_operacion, oficio, fecha, usuario
                FROM logtbl_arm_historial_asigna_descarga
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_registro' => $id_registro));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_registro = $data['id_registro'];
            $this->matricula = $data['matricula'];
            $this->curp = $data['curp'];
            $this->id_operacion = $data['id_operacion'];
            $this->oficio = $data['oficio'];
            $this->fecha = $data['fecha'];
            $this->usuario = $data['usuario'];

            $this->LogtblArmArmamento->select($this->matricula);
            $this->LogcatArmOperacion->select($this->id_operacion);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_registro, a.matricula, a.curp, a.id_operacion, a.oficio, a.fecha, a.usuario,
                  b.matricula, b.serie, b.id_loc, b.id_foliod, b.id_situacion, b.id_propietario, b.id_estado, b.id_estatus, b.id_motivo_movimiento, b.id_marca,
                  c.id_operacion, c.descripcion
                FROM logtbl_arm_historial_asigna_descarga a 
                 LEFT JOIN logtbl_arm_armamento b ON a.matricula=b.matricula
                 LEFT JOIN logcat_arm_operacion c ON a.id_operacion=c.id_operacion";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_registro' => $data['id_registro'],
                               'matricula' => $data['matricula'],
                               'curp' => $data['curp'],
                               'id_operacion' => $data['id_operacion'],
                               'oficio' => $data['oficio'],
                               'fecha' => $data['fecha'],
                               'usuario' => $data['usuario'],
                               'logtbl_arm_armamento_serie' => $data['serie'],
                               'logtbl_arm_armamento_id_loc' => $data['id_loc'],
                               'logtbl_arm_armamento_id_foliod' => $data['id_foliod'],
                               'logtbl_arm_armamento_id_situacion' => $data['id_situacion'],
                               'logtbl_arm_armamento_id_propietario' => $data['id_propietario'],
                               'logtbl_arm_armamento_id_estado' => $data['id_estado'],
                               'logtbl_arm_armamento_id_estatus' => $data['id_estatus'],
                               'logtbl_arm_armamento_id_motivo_movimiento' => $data['id_motivo_movimiento'],
                               'logtbl_arm_armamento_id_marca' => $data['id_marca'],
                               'logcat_arm_operacion_descripcion' => $data['descripcion'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_arm_historial_asigna_descarga(matricula, curp, id_operacion, usuario)
                VALUES(:matricula, :curp, :id_operacion, :usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":matricula" => $this->matricula, ":curp" => $this->curp, ":id_operacion" => $this->id_operacion, ":usuario" => $this->usuario));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_arm_historial_asigna_descarga
                   SET matricula=:matricula, curp=:curp, id_operacion=:id_operacion, oficio=:oficio, fecha=:fecha, usuario=:usuario
                WHERE id_registro=:id_registro;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_registro" => $this->id_registro, ":matricula" => $this->matricula, ":curp" => $this->curp, ":id_operacion" => $this->id_operacion, ":oficio" => $this->oficio, ":fecha" => $this->fecha, ":usuario" => $this->usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>