<?php

/**
 * @author SisA
 * @copyright 2013
 * 
 * La clase Usuario. Para la gesti�n de la o las cuentas de usuario que accesan al sistema.
 * Puede recibir el Id de un usuario para tener acceso a todas sus funciones.
 * 
 */
class Usuario 
{
    public $idUsr;
    public $nombreUsr;
    public $passw;
    public $nombre;
    public $perfil;
    public $fechaAlta;
    public $fechaEdit;    
    public $insert;
    public $edit;
    public $delete;
    public $ctrlUsr;
    public $reportes;
    public $msjError;
    
    private $_objBd;
    private $objSys;    
        
    public function __construct( $id = 0 )
    {
        require_once('mysql.class.php');
        $this->_objBd = new MySqlPdo();        
        $this->_objSys = new System();
         
        $idUsr = ( $id != 0 ) ? $id : $_SESSION['xlogin_id_sisp'];            			            
        $qSql = $this->_objBd->prepare("SELECT * 
                                        FROM xtblusuarios 
                                        WHERE id_usuario=:id_usuario;");
        
        if ($qSql->execute( array(':id_usuario' => $idUsr) )) {
            $data = $qSql->fetch(PDO::FETCH_ASSOC);
            
            $this->idUsr    = $data['id_usuario'];
            $this->nombreUsr= $data['nom_usr'];
            $this->nombre   = $data['nombre'];
            $this->fechaAlta= $data['fecha_reg'];
            $this->fechaEdit= $data['fecha_edit'];
            $this->perfil   = ( !empty($data['id_perfil']) ) ? $data['id_perfil'] : 0;            
            if ( $this->perfil != 0) {
                $qPerf = $this->_objBd->prepare("SELECT * 
                                                 FROM xcatperfiles 
                                                 WHERE id_perfil=:perfil;");
                $qPerf->execute( array(':perfil' => $this->perfil) );
                $datPerf = $qPerf->fetch(PDO::FETCH_ASSOC);
                $this->insert  = $datPerf['insertar'];
                $this->edit    = $datPerf['modificar'];
                $this->delete  = $datPerf['baja'];
                $this->ctrlUsr = $datPerf['usuarios'];
                $this->reportes= $datPerf['reportes'];
            } else {                             
                $this->perfil  = 0;
                $this->insert  = 0;
                $this->edit    = 0;
                $this->delete  = 0;
                $this->ctrlUsr = 0;
                $this->reportes= 0;
            }
        }
        $this->passw = "";
    }//-- Fin del constructor...
		
    public function getPerfil($idMod)
    {         
        $qSql = $this->_objBd->prepare("SELECT p.*
                                        FROM xtblusuarios u 
                                            JOIN xtblacceso a ON u.id_usuario=a.id_usuario 
                                            JOIN xcatperfiles p ON a.id_perfil=p.id_perfil
                                        WHERE u.id_usuario=:id_usuario AND a.id_menu=:id_menu;");
        $qSql->execute( array(':id_usuario' => $this->xCveUsr, ':id_menu' => $idMod) );
        $data = $qSql->fetch(PDO::FETCH_ASSOC);
        $this->xPerfil  = $data['id_perfil'];
        $this->xInsert  = $data['insertar'];
        $this->xEdit    = $data['modificar'];
        $this->xDelete  = $data['eliminar'];
        $this->xCtrlUsr = $data['usuarios'];
        $this->xReportes= $data['reportes'];
                 
    }
    
    public function ultimoAcceso(){
        $idUsr = $_SESSION['xlogin_id_sisp'];
        
        $sql = "UPDATE xtblusuarios
                   SET ultimo_acceso=NOW()
                WHERE id_usuario=:id_usuario;";
        try {
            $qry = $this->_objBd->prepare($sql);
            $qry->execute(array(":id_usuario" => $idUsr));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}
?>