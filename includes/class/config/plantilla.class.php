<?php
/**
 * @author SisA
 * @copyright 2013
 *
 * La clase Plantilla recibe los siguientes par�metros:
 *   -> titulo [Cadena]  = T�tulo de la p�gina actual.
 *   -> sripts [Cadena]  = Son los scripts adicionales que se requieren en la pagina actual.    
 *   -> header [Bool]    = Bandera que determina si se mostrar� o no el encabezado.
 *   -> menu [Bool]      = Bandera que determina si se mostrar� o no el menu principal.
 *   -> menu_lgn [Bool]  = Bandera que determina si se mostrar� o no el menu del login.
 *   -> id_menu[Entero]  = Id del men� al que corresponde el m�dulo actual.
 *   -> txt_mod[Cadena]  = Texto adicional para mostrar en la barra de t�tulo del m�dulo actual.
 *   -> name_mod[Bool]   = Bandera que determina si se mostrar� el t�tulo del m�dulo actual.
 *   -> footer [Bool]    = Bandera que determina si se mostrar� o no el pie de p�gina.
 *   -> usr [Entero]     = El Id del usuario actual(para el control e permisos).
 * 
 */
class Plantilla 
{
    private $_titulo;
    private $_scripts;
    private $_header;
    private $_menuLogin;
    private $_menu;
    private $_idMenu;
    private $_textMod;
    private $_footer;
    private $_idUsr;    
    
    private $_objUsr;
    private $_objSys;
    
    public function __construct($params)
    {
        // Recepci�n de par�metros
        $this->_titulo      = $params['titulo'];        
        $this->_scripts     = ( isset($params['scripts']) )     ? $params['scripts']    : null;        
        $this->_header      = ( isset($params['header']) )      ? $params['header']     : true;
        $this->_menuLogin   = ( isset($params['menuLogin']) )   ? $params['menuLogin']  : false; 
        $this->_menu        = ( isset($params['menu']) )        ? $params['menu']       : true;
        $this->_idMenu      = ( isset($params['idMenu']) )      ? $params['idMenu']     : 0;
        $this->_textMod     = ( isset($params['textMod']) )     ? $params['textMod']    : '';
        $this->_footer      = ( isset($params['footer']) )      ? $params['footer']     : false;
        $this->_idUsr       = $params['usr']; 
               
        // Creaci�n de objetos
        $this->_objUsr = ( $this->_idUsr != 0 ) ? new Usuario($this->_idUsr) : null;       
        $this->_objSys = new System();
    }
    
    // Funciones p�blicas
    public function paginaInicio()
    {
        $this->_htmlBegin();
            $this->_headBegin();
                $this->_titlePage();
                $this->_metas();
                $this->_scripts();
            $this->_headEnd(); 
         
            $this->_BodyBegin();
                if ($this->_header) {
                    $this->_header();
                }
                if ($this->_menu) {
                    $this->_menu();
                }                
                $this->_ContentBegin();
    }
    
    public function paginaFin()
    {
                $this->_contentEnd();
                if ($this->_footer) {
                    $this->_footer();
                }
            $this->_bodyEnd();    
        $this->_htmlEnd();  
    }
    
    public function mostrarNombreModulo()
    {
        $this->_NameModule();
    }
    
    public function accesosDirectos()
    {
        echo '<div id="dvAccesos-Directos" class="dvAccesos-Directos">';
        $accesos = $this->_objSys->getShortcuts($this->_idUsr);
        if (count($accesos) > 0) {
            echo '<table id="tbIconos-Accesos" class="tbIconos-Accesos">';            
            foreach ($accesos As $key => $datos) {
                echo '<tr>';
                    echo '<td>';
                        echo '<a href="' . $datos['url'] . '" title="' . $datos['hint'] . '">';
                            echo '<img src="' . PATH_IMAGES . $datos['icono'] . '" alt="" />';
                            echo $datos['descrip'];
                        echo '</a>';
                    echo '</td>';
                echo '</tr>';
            }
            echo '</table>';
        }
        echo '</div>';
    }
    
    private function _htmlBegin()
    {
        echo '<!DOCTYPE html>
              <html lang="es">';
    }
    
    private function _headBegin()
    {
     	echo '<head>';
    }
    	
    private function _titlePage()
    {
    	echo '<title>'. $this->_titulo .'</title>';
    }
    	
    private function _metas()
    {
		echo '<link rel="icon" type="image/x-icon" href="includes/css/imgs/icons/icono.ico" />';
        // echo '<meta charset="utf-8">';
        echo '<meta http-equiv="Expires" content="0" />';
        echo '<meta http-equiv="Last-Modified" content="0" />'; 
        //echo '<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />'; 
        echo '<meta http-equiv="Pragma" content="no-cache" />';
        echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
        
    }
    	
    private function _scripts()
    {
        echo '<link type="text/css" href="includes/css/styles.css" rel="stylesheet"/>';        
        echo '<link type="text/css" href="includes/js/jquery-ui-1.10.3/css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />';            
        echo '<script type="text/javascript" src="includes/js/jquery-1.8.2.min.js"></script>';
        echo '<script type="text/javascript" src="includes/js/xmenu/js/hoverIntent.js"></script>';
        echo '<script type="text/javascript" src="includes/js/xmenu/js/superfish.min.js"></script>';
        echo '<script type="text/javascript" src="includes/js/jquery.validate.js"></script>';
        echo '<script type="text/javascript" src="includes/js/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js"></script>';
        echo '<script type="text/javascript" src="includes/js/xmain.js"></script>';
        if (count($this->_scripts) > 0) {
            foreach( $this->_scripts as $script )
                echo $script;
        }
    }
    	
    private function _headEnd()
    {
        echo '<!--[if gte IE 9]>
                <style type="text/css">
                    .gradient {
                        filter: none;
                    }
                </style>
              <![endif]-->';
    	echo '</head>';
    }
    	
    private function _bodyBegin()
    {
    	echo '<body><div id="dvBody" class="dvBody">';
    }	
    
    private function _header()
    {
        echo '<div id="dvHeader" class="dvHeader">';
            echo '<div class="dvHeader-dvTitulo">';
                echo '<span>Secretar�a de Seguridad P�blica y Protecci�n Civil</span>';
            echo '</div>';
            if ($this->_menuLogin) {
                echo '<ul class="ulMenuLogin">';
                    echo '<li> <a href="#" id="lnkPswd"><img src="' . PATH_IMAGES . 'icons/user_config20.png" alt="" />�Olvid� su contrase�a?</a> </li>';
                    echo '<li> <a href="#" id="lnkAyuda"><img src="' . PATH_IMAGES . 'icons/help20.png" alt="" />Ayuda</a> </li>';
                    echo '<li> <a href="#" id="lnkAcercade"><img src="' . PATH_IMAGES . 'icons/info20.png" alt="" />Acerca de...</a> </li>';
                echo '</ul>';
            } else { 
                echo '<span class="dvHeader-spnVersion">SISP v. 1.0</span>';
            }
        echo '</div>';
    }
    
    private function _menu()
    {
        echo '<div id="dvMenu-Main" class="dvMenu-Main">';
            $this->_objSys->getMenu($this->_objUsr->idUsr, $this->_objUsr->perfil);
            echo '<div id="dvPanelSesion">';
                echo '<a href="index.php?m=0" id="iconHome" title="Inicio..."></a>';
                echo '<a href="#" id="iconConfigUsr" title="Opciones de usuario..."></a>';
                echo '<a href="index.php?m=-1" id="iconSalir" title="Salir del sistema"></a>';
            echo '</div>';
        echo '</div>';
    }
    
    private function _nameModule()
    {
        $datos = $this->_objSys->nameModule($this->_idMenu, $this->_textMod);
        echo '<span class="spIconTitle" style="background-image: url(' . PATH_IMAGES . $datos['icono'] . ');"></span>';
        echo '<span class="spTextTitle">' . $datos['descrip'] . '</span>';
    }
    
    private function _contentBegin()
    {
        echo '<div id="dvContent-Main" class="dvContent-Main">';            
    }
    
    private function _contentEnd()
    {
        echo '</div></div>';
    }
    
    private function _showFooter()
    {
        $nom_usr = ( $this->_oUsr != NULL ) ? $this->_oUsr->xNomUsr : '--';
        echo '<div id="dvFooter" class="dvFooter"> Usuario: ' . $nom_usr . '</div>';
    }    
    
    private function _bodyEnd()
    {
        echo '</body>';
    }		
    
    private function _htmlEnd()
    {
    	echo '</html>';
    }   
}
?>