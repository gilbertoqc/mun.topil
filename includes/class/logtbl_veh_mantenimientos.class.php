<?php
/**
 *
 */
class LogtblVehMantenimientos
{
    public $id_vehiculo; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_mantenimiento; /** @Tipo: int(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_ini; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_fin; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $detalles; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $nom_resp; /** @Tipo: varchar(75), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $rfc_curp; /** @Tipo: varchar(18), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $domicilio; /** @Tipo: varchar(200), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $razon_social; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $resp_taller; /** @Tipo: varchar(75), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $kilometraje_total; /** @Tipo: mediumint(6) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_tipo_mtto; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_servicio; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehTipoMantenimiento; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehTipoServicio; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogtblVehiculos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_municipios.class.php';
        require_once 'logcat_veh_tipo_mantenimiento.class.php';
        require_once 'logcat_veh_tipo_servicio.class.php';
        require_once 'logtbl_vehiculos.class.php';
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->LogcatVehTipoMantenimiento = new LogcatVehTipoMantenimiento();
        $this->LogcatVehTipoServicio = new LogcatVehTipoServicio();
        $this->LogtblVehiculos = new LogtblVehiculos();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_mantenimiento)
    {
        $sql = "SELECT id_vehiculo, id_mantenimiento, fecha_ini, fecha_fin, detalles, nom_resp, rfc_curp, id_municipio, domicilio, razon_social, resp_taller, kilometraje_total, id_tipo_mtto, id_tipo_servicio
                FROM logtbl_veh_mantenimientos
                WHERE id_mantenimiento=:id_mantenimiento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_mantenimiento' => $id_mantenimiento));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_vehiculo = $data['id_vehiculo'];
            $this->id_mantenimiento = $data['id_mantenimiento'];
            $this->fecha_ini = $data['fecha_ini'];
            $this->fecha_fin = $data['fecha_fin'];
            $this->detalles = $data['detalles'];
            $this->nom_resp = $data['nom_resp'];
            $this->rfc_curp = $data['rfc_curp'];
            $this->id_municipio = $data['id_municipio'];
            $this->domicilio = $data['domicilio'];
            $this->razon_social = $data['razon_social'];
            $this->resp_taller = $data['resp_taller'];
            $this->kilometraje_total = $data['kilometraje_total'];
            $this->id_tipo_mtto = $data['id_tipo_mtto'];
            $this->id_tipo_servicio = $data['id_tipo_servicio'];

            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->LogcatVehTipoMantenimiento->select($this->id_tipo_mtto);
            $this->LogcatVehTipoServicio->select($this->id_tipo_servicio);
            $this->LogtblVehiculos->select($this->id_vehiculo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_vehiculo, a.id_mantenimiento, a.fecha_ini, a.fecha_fin, a.detalles, a.nom_resp, a.rfc_curp, a.id_municipio, a.domicilio, a.razon_social, a.resp_taller, a.kilometraje_total, a.id_tipo_mtto, a.id_tipo_servicio,
                  b.id_municipio, b.municipio, b.cabecera, b.id_entidad, b.id_region,
                  c.id_tipo_mtto, c.tipo_mtto, c.xstat,
                  d.id_tipo_servicio, d.tipo_servicio, d.xstat                  
                FROM logtbl_veh_mantenimientos a 
                 LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio
                 LEFT JOIN logcat_veh_tipo_mantenimiento c ON a.id_tipo_mtto=c.id_tipo_mtto
                 LEFT JOIN logcat_veh_tipo_servicio d ON a.id_tipo_servicio=d.id_tipo_servicio
                 LEFT JOIN logtbl_vehiculos e ON a.id_vehiculo=e.id_vehiculo";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo' => $data['id_vehiculo'],
                               'id_mantenimiento' => $data['id_mantenimiento'],
                               'fecha_ini' => $data['fecha_ini'],
                               'fecha_fin' => $data['fecha_fin'],
                               'detalles' => $data['detalles'],
                               'nom_resp' => $data['nom_resp'],
                               'rfc_curp' => $data['rfc_curp'],
                               'id_municipio' => $data['id_municipio'],
                               'domicilio' => $data['domicilio'],
                               'razon_social' => $data['razon_social'],
                               'resp_taller' => $data['resp_taller'],
                               'kilometraje_total' => $data['kilometraje_total'],
                               'id_tipo_mtto' => $data['id_tipo_mtto'],
                               'id_tipo_servicio' => $data['id_tipo_servicio'],
                               'admcat_municipios_municipio' => $data['municipio'],
                               'admcat_municipios_cabecera' => $data['cabecera'],
                               'admcat_municipios_id_entidad' => $data['id_entidad'],
                               'admcat_municipios_id_region' => $data['id_region'],
                               'logcat_veh_tipo_mantenimiento_tipo_mtto' => $data['tipo_mtto'],
                               'logcat_veh_tipo_mantenimiento_xstat' => $data['xstat'],
                               'logcat_veh_tipo_servicio_tipo_servicio' => $data['tipo_servicio'],
                               'logcat_veh_tipo_servicio_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT COUNT(*) 
                FROM logtbl_veh_mantenimientos a ";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_veh_mantenimientos(id_vehiculo, id_mantenimiento, fecha_ini, fecha_fin, detalles, nom_resp, rfc_curp, id_municipio, domicilio, razon_social, resp_taller, kilometraje_total, id_tipo_mtto, id_tipo_servicio)
                VALUES(:id_vehiculo, :id_mantenimiento, :fecha_ini, :fecha_fin, :detalles, :nom_resp, :rfc_curp, :id_municipio, :domicilio, :razon_social, :resp_taller, :kilometraje_total, :id_tipo_mtto, :id_tipo_servicio);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":id_mantenimiento" => $this->id_mantenimiento, ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":detalles" => $this->detalles, ":nom_resp" => $this->nom_resp, ":rfc_curp" => $this->rfc_curp, ":id_municipio" => $this->id_municipio, ":domicilio" => $this->domicilio, ":razon_social" => $this->razon_social, ":resp_taller" => $this->resp_taller, ":kilometraje_total" => $this->kilometraje_total, ":id_tipo_mtto" => $this->id_tipo_mtto, ":id_tipo_servicio" => $this->id_tipo_servicio));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_veh_mantenimientos
                   SET fecha_ini=:fecha_ini, fecha_fin=:fecha_fin, detalles=:detalles, nom_resp=:nom_resp, rfc_curp=:rfc_curp, id_municipio=:id_municipio, domicilio=:domicilio, razon_social=:razon_social, resp_taller=:resp_taller, kilometraje_total=:kilometraje_total, id_tipo_mtto=:id_tipo_mtto, id_tipo_servicio=:id_tipo_servicio
                WHERE id_mantenimiento=:id_mantenimiento;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_mantenimiento" => $this->id_mantenimiento, ":fecha_ini" => $this->fecha_ini, ":fecha_fin" => $this->fecha_fin, ":detalles" => $this->detalles, ":nom_resp" => $this->nom_resp, ":rfc_curp" => $this->rfc_curp, ":id_municipio" => $this->id_municipio, ":domicilio" => $this->domicilio, ":razon_social" => $this->razon_social, ":resp_taller" => $this->resp_taller, ":kilometraje_total" => $this->kilometraje_total, ":id_tipo_mtto" => $this->id_tipo_mtto, ":id_tipo_servicio" => $this->id_tipo_servicio));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>