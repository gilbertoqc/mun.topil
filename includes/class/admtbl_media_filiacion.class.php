<?php
/**
 *
 */
class AdmtblMediaFiliacion
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_complexion; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_color_piel; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_forma_cara; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_cabello_cantidad; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_cabello_color; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_cabello_forma; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_frente; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_cejas; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_ojos_color; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_ojos_tamanio; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_nariz; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_boca; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_labios; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_menton; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_oreja; /** @Tipo: smallint(4) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $senias; /** @Tipo: varchar(255), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $usa_anteojos; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $estatura; /** @Tipo: double(5,2) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $peso; /** @Tipo: double(5,2) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $observaciones; /** @Tipo: text, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatMfOrejaDer; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfBoca; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfCabelloCantidad; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfCabelloColor; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfCabelloForma; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfCejas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfColorPiel; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfComplexion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfFormaCara; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfFrente; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfLabios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfMenton; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfNariz; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfOjosColor; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMfOjosTamanio; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_mf_oreja_der.class.php';
        require_once 'admcat_mf_boca.class.php';
        require_once 'admcat_mf_cabello_cantidad.class.php';
        require_once 'admcat_mf_cabello_color.class.php';
        require_once 'admcat_mf_cabello_forma.class.php';
        require_once 'admcat_mf_cejas.class.php';
        require_once 'admcat_mf_color_piel.class.php';
        require_once 'admcat_mf_complexion.class.php';
        require_once 'admcat_mf_forma_cara.class.php';
        require_once 'admcat_mf_frente.class.php';
        require_once 'admcat_mf_labios.class.php';
        require_once 'admcat_mf_menton.class.php';
        require_once 'admcat_mf_nariz.class.php';
        require_once 'admcat_mf_ojos_color.class.php';
        require_once 'admcat_mf_ojos_tamanio.class.php';
        require_once 'admtbl_datos_personales.class.php';
        $this->AdmcatMfOrejaDer = new AdmcatMfOrejaDer();
        $this->AdmcatMfBoca = new AdmcatMfBoca();
        $this->AdmcatMfCabelloCantidad = new AdmcatMfCabelloCantidad();
        $this->AdmcatMfCabelloColor = new AdmcatMfCabelloColor();
        $this->AdmcatMfCabelloForma = new AdmcatMfCabelloForma();
        $this->AdmcatMfCejas = new AdmcatMfCejas();
        $this->AdmcatMfColorPiel = new AdmcatMfColorPiel();
        $this->AdmcatMfComplexion = new AdmcatMfComplexion();
        $this->AdmcatMfFormaCara = new AdmcatMfFormaCara();
        $this->AdmcatMfFrente = new AdmcatMfFrente();
        $this->AdmcatMfLabios = new AdmcatMfLabios();
        $this->AdmcatMfMenton = new AdmcatMfMenton();
        $this->AdmcatMfNariz = new AdmcatMfNariz();
        $this->AdmcatMfOjosColor = new AdmcatMfOjosColor();
        $this->AdmcatMfOjosTamanio = new AdmcatMfOjosTamanio();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, id_complexion, id_color_piel, id_forma_cara, id_cabello_cantidad, id_cabello_color, id_cabello_forma, id_frente, id_cejas, id_ojos_color, id_ojos_tamanio, id_nariz, id_boca, id_labios, id_menton, id_oreja, senias, usa_anteojos, estatura, peso, observaciones
                FROM admtbl_media_filiacion
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->id_complexion = $data['id_complexion'];
            $this->id_color_piel = $data['id_color_piel'];
            $this->id_forma_cara = $data['id_forma_cara'];
            $this->id_cabello_cantidad = $data['id_cabello_cantidad'];
            $this->id_cabello_color = $data['id_cabello_color'];
            $this->id_cabello_forma = $data['id_cabello_forma'];
            $this->id_frente = $data['id_frente'];
            $this->id_cejas = $data['id_cejas'];
            $this->id_ojos_color = $data['id_ojos_color'];
            $this->id_ojos_tamanio = $data['id_ojos_tamanio'];
            $this->id_nariz = $data['id_nariz'];
            $this->id_boca = $data['id_boca'];
            $this->id_labios = $data['id_labios'];
            $this->id_menton = $data['id_menton'];
            $this->id_oreja = $data['id_oreja'];
            $this->senias = $data['senias'];
            $this->usa_anteojos = $data['usa_anteojos'];
            $this->estatura = $data['estatura'];
            $this->peso = $data['peso'];
            $this->observaciones = $data['observaciones'];

            $this->AdmcatMfOrejaDer->select($this->id_oreja);
            $this->AdmcatMfBoca->select($this->id_boca);
            $this->AdmcatMfCabelloCantidad->select($this->id_cabello_cantidad);
            $this->AdmcatMfCabelloColor->select($this->id_cabello_color);
            $this->AdmcatMfCabelloForma->select($this->id_cabello_forma);
            $this->AdmcatMfCejas->select($this->id_cejas);
            $this->AdmcatMfColorPiel->select($this->id_color_piel);
            $this->AdmcatMfComplexion->select($this->id_complexion);
            $this->AdmcatMfFormaCara->select($this->id_forma_cara);
            $this->AdmcatMfFrente->select($this->id_frente);
            $this->AdmcatMfLabios->select($this->id_labios);
            $this->AdmcatMfMenton->select($this->id_menton);
            $this->AdmcatMfNariz->select($this->id_nariz);
            $this->AdmcatMfOjosColor->select($this->id_ojos_color);
            $this->AdmcatMfOjosTamanio->select($this->id_ojos_tamanio);
            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.id_complexion, a.id_color_piel, a.id_forma_cara, a.id_cabello_cantidad, a.id_cabello_color, a.id_cabello_forma, a.id_frente, a.id_cejas, a.id_ojos_color, a.id_ojos_tamanio, a.id_nariz, a.id_boca, a.id_labios, a.id_menton, a.id_oreja, a.senias, a.usa_anteojos, a.estatura, a.peso, a.observaciones,
                  b.id_oreja, b.oreja,
                  c.id_boca, c.boca,
                  d.id_cabello_cantidad, d.cabello_cantidad,
                  e.id_cabello_color, e.cabello_color,
                  f.id_cabello_forma, f.cabello_forma,
                  g.id_cejas, g.cejas,
                  h.id_color_piel, h.color_piel,
                  i.id_complexion, i.complexion,
                  j.id_forma_cara, j.forma_cara,
                  k.id_frente, k.frente,
                  l.id_labios, l.labios,
                  .id_menton, .menton,
                  .id_nariz, .nariz,
                  .id_ojos_color, .ojos_color,
                  .id_ojos_tamanio, .ojos_tamanio,
                  .curp, .nombre, .a_paterno, .a_materno, .fecha_nac, .genero, .rfc, .cuip, .folio_ife, .mat_cartilla, .licencia_conducir, .pasaporte, .id_estado_civil, .id_tipo_sangre, .id_nacionalidad, .id_entidad, .id_municipio, .lugar_nac, .id_status
                FROM admtbl_media_filiacion a 
                 LEFT JOIN admcat_mf_oreja_der b ON a.id_oreja=b.id_oreja
                 LEFT JOIN admcat_mf_boca c ON a.id_boca=c.id_boca
                 LEFT JOIN admcat_mf_cabello_cantidad d ON a.id_cabello_cantidad=d.id_cabello_cantidad
                 LEFT JOIN admcat_mf_cabello_color e ON a.id_cabello_color=e.id_cabello_color
                 LEFT JOIN admcat_mf_cabello_forma f ON a.id_cabello_forma=f.id_cabello_forma
                 LEFT JOIN admcat_mf_cejas g ON a.id_cejas=g.id_cejas
                 LEFT JOIN admcat_mf_color_piel h ON a.id_color_piel=h.id_color_piel
                 LEFT JOIN admcat_mf_complexion i ON a.id_complexion=i.id_complexion
                 LEFT JOIN admcat_mf_forma_cara j ON a.id_forma_cara=j.id_forma_cara
                 LEFT JOIN admcat_mf_frente k ON a.id_frente=k.id_frente
                 LEFT JOIN admcat_mf_labios l ON a.id_labios=l.id_labios
                 LEFT JOIN admcat_mf_menton  ON a.id_menton=.id_menton
                 LEFT JOIN admcat_mf_nariz  ON a.id_nariz=.id_nariz
                 LEFT JOIN admcat_mf_ojos_color  ON a.id_ojos_color=.id_ojos_color
                 LEFT JOIN admcat_mf_ojos_tamanio  ON a.id_ojos_tamanio=.id_ojos_tamanio
                 LEFT JOIN admtbl_datos_personales  ON a.curp=.curp";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'id_complexion' => $data['id_complexion'],
                               'id_color_piel' => $data['id_color_piel'],
                               'id_forma_cara' => $data['id_forma_cara'],
                               'id_cabello_cantidad' => $data['id_cabello_cantidad'],
                               'id_cabello_color' => $data['id_cabello_color'],
                               'id_cabello_forma' => $data['id_cabello_forma'],
                               'id_frente' => $data['id_frente'],
                               'id_cejas' => $data['id_cejas'],
                               'id_ojos_color' => $data['id_ojos_color'],
                               'id_ojos_tamanio' => $data['id_ojos_tamanio'],
                               'id_nariz' => $data['id_nariz'],
                               'id_boca' => $data['id_boca'],
                               'id_labios' => $data['id_labios'],
                               'id_menton' => $data['id_menton'],
                               'id_oreja' => $data['id_oreja'],
                               'senias' => $data['senias'],
                               'usa_anteojos' => $data['usa_anteojos'],
                               'estatura' => $data['estatura'],
                               'peso' => $data['peso'],
                               'observaciones' => $data['observaciones'],
                               'admcat_mf_oreja_der_oreja' => $data['oreja'],
                               'admcat_mf_boca_boca' => $data['boca'],
                               'admcat_mf_cabello_cantidad_cabello_cantidad' => $data['cabello_cantidad'],
                               'admcat_mf_cabello_color_cabello_color' => $data['cabello_color'],
                               'admcat_mf_cabello_forma_cabello_forma' => $data['cabello_forma'],
                               'admcat_mf_cejas_cejas' => $data['cejas'],
                               'admcat_mf_color_piel_color_piel' => $data['color_piel'],
                               'admcat_mf_complexion_complexion' => $data['complexion'],
                               'admcat_mf_forma_cara_forma_cara' => $data['forma_cara'],
                               'admcat_mf_frente_frente' => $data['frente'],
                               'admcat_mf_labios_labios' => $data['labios'],
                               'admcat_mf_menton_menton' => $data['menton'],
                               'admcat_mf_nariz_nariz' => $data['nariz'],
                               'admcat_mf_ojos_color_ojos_color' => $data['ojos_color'],
                               'admcat_mf_ojos_tamanio_ojos_tamanio' => $data['ojos_tamanio'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_media_filiacion(curp, id_complexion, id_color_piel, id_forma_cara, id_cabello_cantidad, id_cabello_color, id_cabello_forma, id_frente, id_cejas, id_ojos_color, id_ojos_tamanio, id_nariz, id_boca, id_labios, id_menton, id_oreja, senias, usa_anteojos, estatura, peso, observaciones)
                VALUES(:curp, :id_complexion, :id_color_piel, :id_forma_cara, :id_cabello_cantidad, :id_cabello_color, :id_cabello_forma, :id_frente, :id_cejas, :id_ojos_color, :id_ojos_tamanio, :id_nariz, :id_boca, :id_labios, :id_menton, :id_oreja, :senias, :usa_anteojos, :estatura, :peso, :observaciones);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":id_complexion" => $this->id_complexion, ":id_color_piel" => $this->id_color_piel, ":id_forma_cara" => $this->id_forma_cara, ":id_cabello_cantidad" => $this->id_cabello_cantidad, ":id_cabello_color" => $this->id_cabello_color, ":id_cabello_forma" => $this->id_cabello_forma, ":id_frente" => $this->id_frente, ":id_cejas" => $this->id_cejas, ":id_ojos_color" => $this->id_ojos_color, ":id_ojos_tamanio" => $this->id_ojos_tamanio, ":id_nariz" => $this->id_nariz, ":id_boca" => $this->id_boca, ":id_labios" => $this->id_labios, ":id_menton" => $this->id_menton, ":id_oreja" => $this->id_oreja, ":senias" => $this->senias, ":usa_anteojos" => $this->usa_anteojos, ":estatura" => $this->estatura, ":peso" => $this->peso, ":observaciones" => $this->observaciones));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_media_filiacion
                   SET id_complexion=:id_complexion, id_color_piel=:id_color_piel, id_forma_cara=:id_forma_cara, id_cabello_cantidad=:id_cabello_cantidad, id_cabello_color=:id_cabello_color, id_cabello_forma=:id_cabello_forma, id_frente=:id_frente, id_cejas=:id_cejas, id_ojos_color=:id_ojos_color, id_ojos_tamanio=:id_ojos_tamanio, id_nariz=:id_nariz, id_boca=:id_boca, id_labios=:id_labios, id_menton=:id_menton, id_oreja=:id_oreja, senias=:senias, usa_anteojos=:usa_anteojos, estatura=:estatura, peso=:peso, observaciones=:observaciones
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":id_complexion" => $this->id_complexion, ":id_color_piel" => $this->id_color_piel, ":id_forma_cara" => $this->id_forma_cara, ":id_cabello_cantidad" => $this->id_cabello_cantidad, ":id_cabello_color" => $this->id_cabello_color, ":id_cabello_forma" => $this->id_cabello_forma, ":id_frente" => $this->id_frente, ":id_cejas" => $this->id_cejas, ":id_ojos_color" => $this->id_ojos_color, ":id_ojos_tamanio" => $this->id_ojos_tamanio, ":id_nariz" => $this->id_nariz, ":id_boca" => $this->id_boca, ":id_labios" => $this->id_labios, ":id_menton" => $this->id_menton, ":id_oreja" => $this->id_oreja, ":senias" => $this->senias, ":usa_anteojos" => $this->usa_anteojos, ":estatura" => $this->estatura, ":peso" => $this->peso, ":observaciones" => $this->observaciones));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>