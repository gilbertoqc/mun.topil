<?php
/**
 *
 */
class LogcatArmLoc
{
    public $id_loc; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $loc; /** @Tipo: varchar(5), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de licencias dentro de un combobox.
     * @param int $id, id de licencia seleccionada por deafult     
     * @return array html(options)
     */
    public function shwLicencias($id=0){
        $aryDatos = $this->selectAll('', 'loc Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_loc"] )
                $html .= '<option value="'.$datos["id_loc"].'" selected>'.$datos["loc"].'</option>';
            else
                $html .= '<option value="'.$datos["id_loc"].'" >'.$datos["loc"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_loc)
    {
        $sql = "SELECT id_loc, loc, xstat
                FROM logcat_arm_loc
                WHERE id_loc=:id_loc;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_loc' => $id_loc));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_loc = $data['id_loc'];
            $this->loc = $data['loc'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_loc, a.loc, a.xstat
                FROM logcat_arm_loc a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_loc' => $data['id_loc'],
                               'loc' => $data['loc'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_loc(id_loc, loc, xstat)
                VALUES(:id_loc, :loc, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_loc" => $this->id_loc, ":loc" => $this->loc, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_loc
                   SET loc=:loc, xstat=:xstat
                WHERE id_loc=:id_loc;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_loc" => $this->id_loc, ":loc" => $this->loc, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>