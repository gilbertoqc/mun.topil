<?php
/**
 *
 */
class OpecatMcsTiposConflictos
{
    public $id_tipo_conflicto; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_conflicto; /** @Tipo: varchar(60), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de tipos de conflictos dentro de un combobox.
     * @param int $id, id del tipo de conflicto seleccionado por deafult
     * @return array html(options)
     */
    public function getTiposConflictos($id=0)
    {
        $aryDatos = $this->selectAll('id_tipo_conflicto Asc','');
        $html = '<option value="0">- Seleccione -</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_conflicto"] )
                $html .= '<option value="'.$datos["id_tipo_conflicto"].'" selected>'.$datos["tipo_conflicto"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_conflicto"].'" >'.$datos["tipo_conflicto"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_conflicto)
    {
        $sql = "SELECT id_tipo_conflicto, tipo_conflicto
                FROM opecat_mcs_tipos_conflictos
                WHERE id_tipo_conflicto=:id_tipo_conflicto;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_conflicto' => $id_tipo_conflicto));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_conflicto = $data['id_tipo_conflicto'];
            $this->tipo_conflicto = $data['tipo_conflicto'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_tipo_conflicto, a.tipo_conflicto
                FROM opecat_mcs_tipos_conflictos a ";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_conflicto' => $data['id_tipo_conflicto'],
                               'tipo_conflicto' => $data['tipo_conflicto'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mcs_tipos_conflictos(id_tipo_conflicto, tipo_conflicto)
                VALUES(:id_tipo_conflicto, :tipo_conflicto);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_conflicto" => $this->id_tipo_conflicto, ":tipo_conflicto" => $this->tipo_conflicto));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mcs_tipos_conflictos
                   SET tipo_conflicto=:tipo_conflicto
                WHERE id_tipo_conflicto=:id_tipo_conflicto;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_conflicto" => $this->id_tipo_conflicto, ":tipo_conflicto" => $this->tipo_conflicto));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>