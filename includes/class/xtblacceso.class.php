<?php
/**
 *
 */
class Xtblacceso
{
    public $id_usuario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_perfil; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_menu; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $Xcatperfiles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $Xtblmenu; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $Xtblusuarios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'xcatperfiles.class.php';
        require_once 'xtblmenu.class.php';
        require_once 'xtblusuarios.class.php';
        $this->Xcatperfiles = new Xcatperfiles();
        $this->Xtblmenu = new Xtblmenu();
        $this->Xtblusuarios = new Xtblusuarios();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_usuario, $id_perfil, $id_menu)
    {
        $sql = "SELECT id_usuario, id_perfil, id_menu
                FROM xtblacceso
                WHERE id_usuario=:id_usuario AND id_perfil=:id_perfil AND id_menu=:id_menu;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_usuario' => $id_usuario, ':id_perfil' => $id_perfil, ':id_menu' => $id_menu));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_usuario = $data['id_usuario'];
            $this->id_perfil = $data['id_perfil'];
            $this->id_menu = $data['id_menu'];

            $this->Xcatperfiles->select($this->id_perfil);
            $this->Xtblmenu->select($this->id_menu);
            $this->Xtblusuarios->select($this->id_usuario);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_usuario, a.id_perfil, a.id_menu,
                  b.id_perfil, b.perfil, b.insertar, b.modificar, b.baja, b.usuarios, b.reportes,
                  c.id_menu, c.menu, c.url, c.icono, c.stat, c.id_padre, c.orden,
                  d.id_usuario, d.nom_usr, d.pswd, d.nombre, d.id_perfil, d.fecha_reg, d.fecha_edit, d.stat
                FROM xtblacceso a 
                 LEFT JOIN xcatperfiles b ON a.id_perfil=b.id_perfil
                 LEFT JOIN xtblmenu c ON a.id_menu=c.id_menu
                 LEFT JOIN xtblusuarios d ON a.id_usuario=d.id_usuario";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_usuario' => $data['id_usuario'],
                               'id_perfil' => $data['id_perfil'],
                               'id_menu' => $data['id_menu'],
                               'xcatperfiles_perfil' => $data['perfil'],
                               'xcatperfiles_insertar' => $data['insertar'],
                               'xcatperfiles_modificar' => $data['modificar'],
                               'xcatperfiles_baja' => $data['baja'],
                               'xcatperfiles_usuarios' => $data['usuarios'],
                               'xcatperfiles_reportes' => $data['reportes'],
                               'xtblmenu_menu' => $data['menu'],
                               'xtblmenu_url' => $data['url'],
                               'xtblmenu_icono' => $data['icono'],
                               'xtblmenu_stat' => $data['stat'],
                               'xtblmenu_id_padre' => $data['id_padre'],
                               'xtblmenu_orden' => $data['orden'],
                               'xtblusuarios_nom_usr' => $data['nom_usr'],
                               'xtblusuarios_pswd' => $data['pswd'],
                               'xtblusuarios_nombre' => $data['nombre'],
                               'xtblusuarios_id_perfil' => $data['id_perfil'],
                               'xtblusuarios_fecha_reg' => $data['fecha_reg'],
                               'xtblusuarios_fecha_edit' => $data['fecha_edit'],
                               'xtblusuarios_stat' => $data['stat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xtblacceso(id_usuario, id_perfil, id_menu)
                VALUES(:id_usuario, :id_perfil, :id_menu);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_usuario" => $this->id_usuario, ":id_perfil" => $this->id_perfil, ":id_menu" => $this->id_menu));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xtblacceso
                   SET 
                WHERE id_usuario=:id_usuario AND id_perfil=:id_perfil AND id_menu=:id_menu;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_usuario" => $this->id_usuario, ":id_perfil" => $this->id_perfil, ":id_menu" => $this->id_menu));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>