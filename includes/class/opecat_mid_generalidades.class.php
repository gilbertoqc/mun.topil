<?php
/**
 *
 */
class OpecatMidGeneralidades
{
    public $id_generalidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_tipo_generalidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $generalidad; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos    
    public $OpecatMidTiposGeneralidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();
        
        require_once 'opecat_mid_tipos_generalidades.class.php';
        $this->OpecatMidTiposGeneralidades = new OpecatMidTiposGeneralidades();
    }
    
    /**
    * Funci�n para obtener un el id maximo de la tabla
    * @param  campos que conforman la clave primaria de la tabla
    * @return boolean true, si la consulta se realiz� con �xito
    */
    public function getIdTipoGeneralidad( $id_generalidad ){        
        $sql = "SELECT id_tipo_generalidad as id_tipo
                  FROM opecat_mid_generalidades
                  WHERE id_generalidad=:id_generalidad";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute( array('id_generalidad' => $id_generalidad ) );
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id_tipo"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para mostrar la lista de --- dentro de un combobox.
     * @param int $id, id .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Generalidades($id_tipo=0, $id_generalidad){
        
        if( ( $id_tipo != 0 )  || ( $id_tipo != '') ){
            $aryDatos = $this->selectAll('a.id_tipo_generalidad =' . $id_tipo, ' a.id_generalidad asc ');
        }else{
            $aryDatos = $this->selectAll('', ' a.id_generalidad asc ');
        }      
          
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id_generalidad == $datos["id_generalidad"] )
                $html .= '<option value="'.$datos["id_generalidad"].'" selected>'.$datos["generalidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_generalidad"].'" >'.$datos["generalidad"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_generalidad)
    {
        $sql = "SELECT id_generalidad, id_tipo_generalidad, generalidad
                FROM opecat_mid_generalidades
                WHERE id_generalidad=:id_generalidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_generalidad' => $id_generalidad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_generalidad = $data['id_generalidad'];
            $this->id_tipo_generalidad = $data['id_tipo_generalidad'];
            $this->generalidad = $data['generalidad'];
            
            $this->OpecatMidTiposGeneralidades->select($this->id_tipo_generalidad);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_generalidad, a.id_tipo_generalidad, a.generalidad,                  
                  c.id_tipo_generalidad, c.tipo_generalidad
                FROM opecat_mid_generalidades a                  
                 LEFT JOIN opecat_mid_tipos_generalidades c ON a.id_tipo_generalidad=c.id_tipo_generalidad";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_generalidad' => $data['id_generalidad'],
                               'id_tipo_generalidad' => $data['id_tipo_generalidad'],
                               'generalidad' => $data['generalidad'],                               
                               'opecat_mid_tipos_generalidades_tipo_generalidad' => $data['tipo_generalidad'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_generalidades(id_generalidad, id_tipo_generalidad, generalidad)
                VALUES(:id_generalidad, :id_tipo_generalidad, :generalidad);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_generalidad" => $this->id_generalidad, ":id_tipo_generalidad" => $this->id_tipo_generalidad, ":generalidad" => $this->generalidad));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_generalidades
                   SET id_tipo_generalidad=:id_tipo_generalidad, generalidad=:generalidad
                WHERE id_generalidad=:id_generalidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_generalidad" => $this->id_generalidad, ":id_tipo_generalidad" => $this->id_tipo_generalidad, ":generalidad" => $this->generalidad));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>