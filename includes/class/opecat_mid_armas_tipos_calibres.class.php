<?php
/**
 *
 */
class OpecatMidArmasTiposCalibres
{
    public $id_arma_tipo; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_arma_calibre; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidArmasTipos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidArmasCalibres; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_armas_tipos.class.php';
        require_once 'opecat_mid_armas_calibres.class.php';
        $this->OpecatMidArmasTipos = new OpecatMidArmasTipos();
        $this->OpecatMidArmasCalibres = new OpecatMidArmasCalibres();
    }


    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_arma_tipo, $id_arma_calibre)
    {
        $sql = "SELECT id_arma_tipo, id_arma_calibre
                FROM opecat_mid_armas_tipos_calibres
                WHERE id_arma_tipo=:id_arma_tipo AND id_arma_calibre=:id_arma_calibre;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_arma_tipo' => $id_arma_tipo, ':id_arma_calibre' => $id_arma_calibre));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_arma_tipo = $data['id_arma_tipo'];
            $this->id_arma_calibre = $data['id_arma_calibre'];

            $this->OpecatMidArmasTipos->select($this->id_arma_tipo);
            $this->OpecatMidArmasCalibres->select($this->id_arma_calibre);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_arma_tipo, a.id_arma_calibre,
                  b.id_arma_tipo, b.arma_tipo,
                  c.id_arma_calibre, c.arma_calibre
                FROM opecat_mid_armas_tipos_calibres a
                 LEFT JOIN opecat_mid_armas_tipos b ON a.id_arma_tipo=b.id_arma_tipo
                 LEFT JOIN opecat_mid_armas_calibres c ON a.id_arma_calibre=c.id_arma_calibre";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_arma_tipo' => $data['id_arma_tipo'],
                               'id_arma_calibre' => $data['id_arma_calibre'],
                               'opecat_mid_armas_tipos_arma_tipo' => $data['arma_tipo'],
                               'opecat_mid_armas_calibres_arma_calibre' => $data['arma_calibre'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_armas_tipos_calibres(id_arma_tipo, id_arma_calibre)
                VALUES(:id_arma_tipo, :id_arma_calibre);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_arma_tipo" => $this->id_arma_tipo, ":id_arma_calibre" => $this->id_arma_calibre));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_armas_tipos_calibres
                   SET
                WHERE id_arma_tipo=:id_arma_tipo AND id_arma_calibre=:id_arma_calibre;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_arma_tipo" => $this->id_arma_tipo, ":id_arma_calibre" => $this->id_arma_calibre));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>