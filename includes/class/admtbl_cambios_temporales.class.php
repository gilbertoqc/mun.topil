<?php
/**
 *
 */
class AdmtblCambiosTemporales
{
    public $idcambio_temp; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_reg; /** @Tipo: datetime, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_inicio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_fin; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_tipo_cambio; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_operativo_seg; /** @Tipo: smallint(5), @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_area; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_ubicacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $no_oficio; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_oficio; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $no_soporte; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_soporte; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $firmante_soporte; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $cargo_firmante; /** @Tipo: varchar(80), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $capturo; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatTipoCambio; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatOperativosSeguridad; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatAreas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatUbicaciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_tipo_cambio.class.php';
        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_operativos_seguridad.class.php';
        require_once 'admcat_areas.class.php';
        require_once 'admcat_municipios.class.php';
        require_once 'admcat_ubicaciones.class.php';
        $this->AdmcatTipoCambio = new AdmcatTipoCambio();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatOperativosSeguridad = new AdmcatOperativosSeguridad();
        $this->AdmcatAreas = new AdmcatAreas();
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->AdmcatUbicaciones = new AdmcatUbicaciones();
    }
      
    
    /**
     * Funci�n para seleccionar los datos  de la tabla admtbl_cambios_temporales para el grid
     * 
     */
    public function selectListTramite($sqlWhere='', $sqlValues=array() )
    {
        $sql = "select idcambio_temp, fecha_inicio, fecha_fin, c.id_tipo_cambio, t.tipo_cambio, c.id_area, i.area, no_oficio,fecha_oficio 
                from admtbl_cambios_temporales c
                LEFT JOIN admcat_tipo_cambio t ON t.id_tipo_cambio=c.id_tipo_cambio 
                LEFT JOIN admcat_areas i ON c.id_area=i.id_area
                  ";
        if (!empty($sqlWhere))
            $sql .= " WHERE  $sqlWhere";                
        $sql .= ";";        
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            //echo $sql;
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'idcambio_temp' => $data['idcambio_temp'],
                               'fecha_inicio' => $data['fecha_inicio'],
                               'fecha_fin' => $data['fecha_fin'],
                               'id_tipo_cambio' => $data['id_tipo_cambio'],
                               'tipo_cambio' => $data['tipo_cambio'],
                               'area' => $data['area'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($idcambio_temp)
    {
        $sql = "SELECT idcambio_temp, fecha_reg, fecha_inicio, fecha_fin, id_tipo_cambio, curp, id_operativo_seg, id_area, id_municipio, id_ubicacion, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante, capturo
                FROM admtbl_cambios_temporales
                WHERE idcambio_temp=:idcambio_temp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':idcambio_temp' => $idcambio_temp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->idcambio_temp = $data['idcambio_temp'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->fecha_inicio = $data['fecha_inicio'];
            $this->fecha_fin = $data['fecha_fin'];
            $this->id_tipo_cambio = $data['id_tipo_cambio'];
            $this->curp = $data['curp'];
            $this->id_operativo_seg = $data['id_operativo_seg'];
            $this->id_area = $data['id_area'];
            $this->id_municipio = $data['id_municipio'];
            $this->id_ubicacion = $data['id_ubicacion'];
            $this->no_oficio = $data['no_oficio'];
            $this->fecha_oficio = $data['fecha_oficio'];
            $this->no_soporte = $data['no_soporte'];
            $this->fecha_soporte = $data['fecha_soporte'];
            $this->firmante_soporte = $data['firmante_soporte'];
            $this->cargo_firmante = $data['cargo_firmante'];
            $this->capturo = $data['capturo'];

            $this->AdmcatTipoCambio->select($this->id_tipo_cambio);
            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatOperativosSeguridad->select($this->id_operativo_seg);
            $this->AdmcatAreas->select($this->id_area);
            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->AdmcatUbicaciones->select($this->id_ubicacion);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.idcambio_temp, a.fecha_reg, a.fecha_inicio, a.fecha_fin, a.id_tipo_cambio, a.curp, a.id_operativo_seg, a.id_area, a.id_municipio, a.id_ubicacion, a.no_oficio, a.fecha_oficio, a.no_soporte, a.fecha_soporte, a.firmante_soporte, a.cargo_firmante, a.capturo,
                  b.id_tipo_cambio, b.tipo_cambio, b.modo,
                  c.curp, c.nombre, c.a_paterno, c.a_materno, c.fecha_nac, c.genero, c.rfc, c.cuip, c.folio_ife, c.mat_cartilla, c.licencia_conducir, c.pasaporte, c.id_estado_civil, c.id_tipo_sangre, c.id_nacionalidad, c.id_entidad, c.id_municipio, c.lugar_nac, c.id_status,
                  d.id_operativo_seg, d.nombre_operativo, d.Objetivo, d.Observaciones, d.fecha_inicio, d.fecha_fin,
                  e.id_area, e.area, e.titular, e.tel�fono, e.status, e.id_nivel, e.id_depende, e.orden,
                  f.id_municipio, f.municipio, f.cabecera, f.id_entidad, f.id_region,
                  g.id_ubicacion, g.ubicacion, g.status
                FROM admtbl_cambios_temporales a 
                 LEFT JOIN admcat_tipo_cambio b ON a.id_tipo_cambio=b.id_tipo_cambio
                 LEFT JOIN admtbl_datos_personales c ON a.curp=c.curp
                 LEFT JOIN admcat_operativos_seguridad d ON a.id_operativo_seg=d.id_operativo_seg
                 LEFT JOIN admcat_areas e ON a.id_area=e.id_area
                 LEFT JOIN admcat_municipios f ON a.id_municipio=f.id_municipio
                 LEFT JOIN admcat_ubicaciones g ON a.id_ubicacion=g.id_ubicacion";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'idcambio_temp' => $data['idcambio_temp'],
                               'fecha_reg' => $data['fecha_reg'],
                               'fecha_inicio' => $data['fecha_inicio'],
                               'fecha_fin' => $data['fecha_fin'],
                               'id_tipo_cambio' => $data['id_tipo_cambio'],
                               'curp' => $data['curp'],
                               'id_operativo_seg' => $data['id_operativo_seg'],
                               'id_area' => $data['id_area'],
                               'id_municipio' => $data['id_municipio'],
                               'id_ubicacion' => $data['id_ubicacion'],
                               'no_oficio' => $data['no_oficio'],
                               'fecha_oficio' => $data['fecha_oficio'],
                               'no_soporte' => $data['no_soporte'],
                               'fecha_soporte' => $data['fecha_soporte'],
                               'firmante_soporte' => $data['firmante_soporte'],
                               'cargo_firmante' => $data['cargo_firmante'],
                               'capturo' => $data['capturo'],
                               'admcat_tipo_cambio_tipo_cambio' => $data['tipo_cambio'],
                               'admcat_tipo_cambio_modo' => $data['modo'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               'admcat_operativos_seguridad_nombre_operativo' => $data['nombre_operativo'],
                               'admcat_operativos_seguridad_Objetivo' => $data['Objetivo'],
                               'admcat_operativos_seguridad_Observaciones' => $data['Observaciones'],
                               'admcat_operativos_seguridad_fecha_inicio' => $data['fecha_inicio'],
                               'admcat_operativos_seguridad_fecha_fin' => $data['fecha_fin'],
                               'admcat_areas_area' => $data['area'],
                               'admcat_areas_titular' => $data['titular'],
                               'admcat_areas_tel�fono' => $data['tel�fono'],
                               'admcat_areas_status' => $data['status'],
                               'admcat_areas_id_nivel' => $data['id_nivel'],
                               'admcat_areas_id_depende' => $data['id_depende'],
                               'admcat_areas_orden' => $data['orden'],
                               'admcat_municipios_municipio' => $data['municipio'],
                               'admcat_municipios_cabecera' => $data['cabecera'],
                               'admcat_municipios_id_entidad' => $data['id_entidad'],
                               'admcat_municipios_id_region' => $data['id_region'],
                               'admcat_ubicaciones_ubicacion' => $data['ubicacion'],
                               'admcat_ubicaciones_status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_cambios_temporales(idcambio_temp, fecha_reg, fecha_inicio, fecha_fin, id_tipo_cambio, curp, id_operativo_seg, id_area, id_municipio, id_ubicacion, no_oficio, fecha_oficio, no_soporte, fecha_soporte, firmante_soporte, cargo_firmante, capturo)
                VALUES(:idcambio_temp, :fecha_reg, :fecha_inicio, :fecha_fin, :id_tipo_cambio, :curp, :id_operativo_seg, :id_area, :id_municipio, :id_ubicacion, :no_oficio, :fecha_oficio, :no_soporte, :fecha_soporte, :firmante_soporte, :cargo_firmante, :capturo);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":idcambio_temp" => $this->idcambio_temp, ":fecha_reg" =>date("Y-m-d"), ":fecha_inicio" => $this->fecha_inicio, ":fecha_fin" => $this->fecha_fin, ":id_tipo_cambio" => $this->id_tipo_cambio, ":curp" => $this->curp, ":id_operativo_seg" => $this->id_operativo_seg, ":id_area" => $this->id_area, ":id_municipio" => $this->id_municipio, ":id_ubicacion" => $this->id_ubicacion, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante, ":capturo" => $this->capturo));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_cambios_temporales
                   SET fecha_reg=:fecha_reg, fecha_inicio=:fecha_inicio, fecha_fin=:fecha_fin, id_tipo_cambio=:id_tipo_cambio, curp=:curp, id_operativo_seg=:id_operativo_seg, id_area=:id_area, id_municipio=:id_municipio, id_ubicacion=:id_ubicacion, no_oficio=:no_oficio, fecha_oficio=:fecha_oficio, no_soporte=:no_soporte, fecha_soporte=:fecha_soporte, firmante_soporte=:firmante_soporte, cargo_firmante=:cargo_firmante, capturo=:capturo
                WHERE idcambio_temp=:idcambio_temp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":idcambio_temp" => $this->idcambio_temp, ":fecha_reg" => $this->fecha_reg, ":fecha_inicio" => $this->fecha_inicio, ":fecha_fin" => $this->fecha_fin, ":id_tipo_cambio" => $this->id_tipo_cambio, ":curp" => $this->curp, ":id_operativo_seg" => $this->id_operativo_seg, ":id_area" => $this->id_area, ":id_municipio" => $this->id_municipio, ":id_ubicacion" => $this->id_ubicacion, ":no_oficio" => $this->no_oficio, ":fecha_oficio" => $this->fecha_oficio, ":no_soporte" => $this->no_soporte, ":fecha_soporte" => $this->fecha_soporte, ":firmante_soporte" => $this->firmante_soporte, ":cargo_firmante" => $this->cargo_firmante, ":capturo" => $this->capturo));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {
      $sql = "DELETE FROM admtbl_cambios_temporales
                WHERE idcambio_temp=:idcambio_temp 
                AND curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":idcambio_temp" => $this->idcambio_temp, ":curp" => $this->curp));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }

    }
}


?>