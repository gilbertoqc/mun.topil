<?php
/**
 *
 */
class AdmcatCategorias
{
    public $id_categoria; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $categoria; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de las categor�as dentro de un combobox.
     * @param int $id, id de la categor�a seleccionada por deafult     
     * @return array html(options)
     */
    public function shwCategorias($id=0){
        $aryDatos = $this->selectAll('', 'categoria Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_categoria"] )
                $html .= '<option value="'.$datos["id_categoria"].'" selected>'.$datos["categoria"].'</option>';
            else
                $html .= '<option value="'.$datos["id_categoria"].'" >'.$datos["categoria"].'</option>';
        }
        return $html;
    }
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_categoria)
    {
        $sql = "SELECT id_categoria, categoria, status
                FROM admcat_categorias
                WHERE id_categoria=:id_categoria;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_categoria' => $id_categoria));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_categoria = $data['id_categoria'];
            $this->categoria = $data['categoria'];
            $this->status = $data['status'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_categoria, a.categoria, a.status
                FROM admcat_categorias a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_categoria' => $data['id_categoria'],
                               'categoria' => $data['categoria'],
                               'status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_categorias(id_categoria, categoria, status)
                VALUES(:id_categoria, :categoria, :status);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_categoria" => $this->id_categoria, ":categoria" => $this->categoria, ":status" => $this->status));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_categorias
                   SET categoria=:categoria, status=:status
                WHERE id_categoria=:id_categoria;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_categoria" => $this->id_categoria, ":categoria" => $this->categoria, ":status" => $this->status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>