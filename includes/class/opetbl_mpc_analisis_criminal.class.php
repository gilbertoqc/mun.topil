<?php
/**
 *
 */
class OpetblMpcAnalisisCriminal
{
    public $id_analisis_criminal; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_perfil_criminal; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $fecha_analisis; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $analisis_criminal; /** @Tipo: varchar(400), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_usuario; /** @Tipo: varchar(40), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpetblMpcPerfilesCriminales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opetbl_mpc_perfiles_criminales.class.php';
        $this->OpetblMpcPerfilesCriminales = new OpetblMpcPerfilesCriminales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_analisis_criminal)
    {
        $sql = "SELECT id_analisis_criminal, id_perfil_criminal, fecha_analisis, analisis_criminal, id_usuario
                FROM opetbl_mpc_analisis_criminal
                WHERE id_analisis_criminal=:id_analisis_criminal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_analisis_criminal' => $id_analisis_criminal));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_analisis_criminal = $data['id_analisis_criminal'];
            $this->id_perfil_criminal = $data['id_perfil_criminal'];
            $this->fecha_analisis = $data['fecha_analisis'];
            $this->analisis_criminal = $data['analisis_criminal'];
            $this->id_usuario = $data['id_usuario'];

            $this->OpetblMpcPerfilesCriminales->select($this->id_perfil_criminal);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_analisis_criminal, a.id_perfil_criminal, a.fecha_analisis, a.analisis_criminal, a.id_usuario,
                  b.id_perfil_criminal, b.id_situacion, b.nombre, b.apellido_paterno, b.apellido_materno, b.alias, b.sexo, b.fecha_nacimineto, b.id_nacionalidad, b.lugar_origen, b.id_estado_civil, b.complexion, b.ocupacion, b.domicilio, b.estatura, b.Peso, b.cabello, b.colorPiel, b.ColorOjos
                FROM opetbl_mpc_analisis_criminal a 
                 LEFT JOIN opetbl_mpc_perfiles_criminales b ON a.id_perfil_criminal=b.id_perfil_criminal";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_analisis_criminal' => $data['id_analisis_criminal'],
                               'id_perfil_criminal' => $data['id_perfil_criminal'],
                               'fecha_analisis' => $data['fecha_analisis'],
                               'analisis_criminal' => $data['analisis_criminal'],
                               'id_usuario' => $data['id_usuario'],
                               'opetbl_mpc_perfiles_criminales_id_situacion' => $data['id_situacion'],
                               'opetbl_mpc_perfiles_criminales_nombre' => $data['nombre'],
                               'opetbl_mpc_perfiles_criminales_apellido_paterno' => $data['apellido_paterno'],
                               'opetbl_mpc_perfiles_criminales_apellido_materno' => $data['apellido_materno'],
                               'opetbl_mpc_perfiles_criminales_alias' => $data['alias'],
                               'opetbl_mpc_perfiles_criminales_sexo' => $data['sexo'],
                               'opetbl_mpc_perfiles_criminales_fecha_nacimineto' => $data['fecha_nacimineto'],
                               'opetbl_mpc_perfiles_criminales_id_nacionalidad' => $data['id_nacionalidad'],
                               'opetbl_mpc_perfiles_criminales_lugar_origen' => $data['lugar_origen'],
                               'opetbl_mpc_perfiles_criminales_id_estado_civil' => $data['id_estado_civil'],
                               'opetbl_mpc_perfiles_criminales_complexion' => $data['complexion'],
                               'opetbl_mpc_perfiles_criminales_ocupacion' => $data['ocupacion'],
                               'opetbl_mpc_perfiles_criminales_domicilio' => $data['domicilio'],
                               'opetbl_mpc_perfiles_criminales_estatura' => $data['estatura'],
                               'opetbl_mpc_perfiles_criminales_Peso' => $data['Peso'],
                               'opetbl_mpc_perfiles_criminales_cabello' => $data['cabello'],
                               'opetbl_mpc_perfiles_criminales_colorPiel' => $data['colorPiel'],
                               'opetbl_mpc_perfiles_criminales_ColorOjos' => $data['ColorOjos'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

	 /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT ac.id_analisis_criminal, pc.id_perfil_criminal, pc.nombre, pc.apellido_paterno,
				pc.apellido_materno,ac.fecha_analisis,ac.analisis_criminal, ac.id_usuario
				FROM opetbl_mpc_analisis_criminal AS ac
				LEFT JOIN opetbl_mpc_perfiles_criminales AS pc ON pc.id_perfil_criminal=ac.id_perfil_criminal";

        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_analisis_criminal' =>  $data['id_analisis_criminal'],
								'id_perfil_criminal' =>  $data['id_perfil_criminal'],
								'nombre' =>  $data['nombre'],
								'apellido_paterno' =>  $data['apellido_paterno'],
								'apellido_materno' =>  $data['apellido_materno'],
								'fecha_analisis' =>  $data['fecha_analisis'],
								'analisis_criminal' =>  $data['analisis_criminal'],
								'id_usuario' =>  $data['id_usuario'],
                                );
                                
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
				FROM opetbl_mpc_analisis_criminal AS ac
				LEFT JOIN opetbl_mpc_perfiles_criminales AS pc ON pc.id_perfil_criminal=ac.id_perfil_criminal";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

	
    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mpc_analisis_criminal(id_analisis_criminal, id_perfil_criminal, fecha_analisis, analisis_criminal, id_usuario)
                VALUES(:id_analisis_criminal, :id_perfil_criminal, :fecha_analisis, :analisis_criminal, :id_usuario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_analisis_criminal" => $this->id_analisis_criminal, ":id_perfil_criminal" => $this->id_perfil_criminal, ":fecha_analisis" => $this->fecha_analisis, ":analisis_criminal" => $this->analisis_criminal, ":id_usuario" => $this->id_usuario));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mpc_analisis_criminal
                   SET id_perfil_criminal=:id_perfil_criminal, fecha_analisis=:fecha_analisis, analisis_criminal=:analisis_criminal, id_usuario=:id_usuario
                WHERE id_analisis_criminal=:id_analisis_criminal;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_analisis_criminal" => $this->id_analisis_criminal, ":id_perfil_criminal" => $this->id_perfil_criminal, ":fecha_analisis" => $this->fecha_analisis, ":analisis_criminal" => $this->analisis_criminal, ":id_usuario" => $this->id_usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>