<?php
/**
 *
 */
class AdmcatTipoCambio
{
    public $id_tipo_cambio; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_cambio; /** @Tipo: varchar(45), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $modo; /** @Tipo: tinyint(4), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    /**
     * Funci�n para mostrar la lista de los tipos de cambios de adscripcion dentro de un combobox.
     * @param int $id, id del tipo de cambio seleccionado por deafult     
     * @return array html(options)
     */
    public function shwTipoCambio($id=0,$tipo){
        $aryDatos = $this->selectAll('modo='.$tipo, 'id_tipo_cambio Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_cambio"] )
                $html .= '<option value="'.$datos["id_tipo_cambio"].'" selected>'.$datos["tipo_cambio"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_cambio"].'" >'.$datos["tipo_cambio"].'</option>';
        }
        return $html;
    }
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_cambio)
    {
        $sql = "SELECT id_tipo_cambio, tipo_cambio, modo
                FROM admcat_tipo_cambio
                WHERE id_tipo_cambio=:id_tipo_cambio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_cambio' => $id_tipo_cambio));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_cambio = $data['id_tipo_cambio'];
            $this->tipo_cambio = $data['tipo_cambio'];
            $this->modo = $data['modo'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_tipo_cambio, a.tipo_cambio, a.modo
                FROM admcat_tipo_cambio a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_cambio' => $data['id_tipo_cambio'],
                               'tipo_cambio' => $data['tipo_cambio'],
                               'modo' => $data['modo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_tipo_cambio(id_tipo_cambio, tipo_cambio, modo)
                VALUES(:id_tipo_cambio, :tipo_cambio, :modo);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_cambio" => $this->id_tipo_cambio, ":tipo_cambio" => $this->tipo_cambio, ":modo" => $this->modo));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_tipo_cambio
                   SET tipo_cambio=:tipo_cambio, modo=:modo
                WHERE id_tipo_cambio=:id_tipo_cambio;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_cambio" => $this->id_tipo_cambio, ":tipo_cambio" => $this->tipo_cambio, ":modo" => $this->modo));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>