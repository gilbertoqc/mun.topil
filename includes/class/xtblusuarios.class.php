<?php
/**
 *
 */
class Xtblusuarios
{
    public $id_usuario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nom_usr; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $pswd; /** @Tipo: varchar(254), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $nombre; /** @Tipo: varchar(250), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_perfil; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $filtro_ip; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $fecha_reg; /** @Tipo: datetime, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_edit; /** @Tipo: datetime, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $fecha_expira; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $ultimo_acceso; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $stat; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $Xcatperfiles; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'xcatperfiles.class.php';
        $this->Xcatperfiles = new Xcatperfiles();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_usuario)
    {
        $sql = "SELECT id_usuario, nom_usr, pswd, nombre, id_perfil, filtro_ip, fecha_reg, fecha_edit, fecha_expira, ultimo_acceso, stat
                FROM xtblusuarios
                WHERE id_usuario=:id_usuario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_usuario' => $id_usuario));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_usuario = $data['id_usuario'];
            $this->nom_usr = $data['nom_usr'];
            $this->pswd = $data['pswd'];
            $this->nombre = $data['nombre'];
            $this->id_perfil = $data['id_perfil'];
            $this->filtro_ip = $data['filtro_ip'];
            $this->fecha_reg = $data['fecha_reg'];
            $this->fecha_edit = $data['fecha_edit'];
            $this->fecha_expira = $data['fecha_expira'];
            $this->ultimo_acceso = $data['ultimo_acceso'];
            $this->stat = $data['stat'];

            $this->Xcatperfiles->select($this->id_perfil);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_usuario, a.nom_usr, a.pswd, a.nombre, a.id_perfil, a.filtro_ip, a.fecha_reg, a.fecha_edit, a.fecha_expira, a.ultimo_acceso, a.stat,
                  b.perfil, b.insertar, b.modificar, b.baja, b.usuarios, b.reportes
                FROM xtblusuarios a 
                 LEFT JOIN xcatperfiles b ON a.id_perfil=b.id_perfil";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_usuario' => $data['id_usuario'],
                               'nom_usr' => $data['nom_usr'],
                               'pswd' => $data['pswd'],
                               'nombre' => $data['nombre'],
                               'id_perfil' => $data['id_perfil'],
                               'filtro_ip' => $data['filtro_ip'],
                               'fecha_reg' => $data['fecha_reg'],
                               'fecha_edit' => $data['fecha_edit'],
                               'fecha_expira' => $data['fecha_expira'],
                               'ultimo_acceso' => $data['ultimo_acceso'],
                               'stat' => $data['stat'],
                               'perfil' => $data['perfil'],
                               'insertar' => $data['insertar'],
                               'modificar' => $data['modificar'],
                               'baja' => $data['baja'],
                               'usuarios' => $data['usuarios'],
                               'reportes' => $data['reportes'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }

    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT a.id_usuario, a.nom_usr, a.nombre, a.id_perfil, a.fecha_reg, a.fecha_expira, a.ultimo_acceso, IF(a.stat = 1, 'ACTIVO', 'INACTIVO') stat,
                  b.perfil 
                FROM xtblusuarios a 
                 LEFT JOIN xcatperfiles b ON a.id_perfil=b.id_perfil";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_usuario' => $data['id_usuario'],
                               'nom_usr' => $data['nom_usr'],
                               'nombre' => $data['nombre'],
                               'id_perfil' => $data['id_perfil'],
                               'fecha_reg' => $data['fecha_reg'],                               
                               'fecha_expira' => $data['fecha_expira'],
                               'ultimo_acceso' => $data['ultimo_acceso'],
                               'stat' => $data['stat'],
                               'perfil' => $data['perfil'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT COUNT(*)
                FROM xtblusuarios a 
                 LEFT JOIN xcatperfiles b ON a.id_perfil=b.id_perfil";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xtblusuarios(id_usuario, nom_usr, pswd, nombre, id_perfil, filtro_ip, fecha_reg, fecha_edit, fecha_expira, ultimo_acceso, stat)
                VALUES(:id_usuario, :nom_usr, :pswd, :nombre, :id_perfil, :filtro_ip, NOW(), NULL, DATE_ADD(CURDATE(), INTERVAL 90 DAY), NULL, 1);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_usuario" => $this->id_usuario, ":nom_usr" => $this->nom_usr, ":pswd" => $this->pswd, ":nombre" => $this->nombre, ":id_perfil" => $this->id_perfil, ":filtro_ip" => $this->filtro_ip));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xtblusuarios
                   SET nom_usr=:nom_usr, pswd=:pswd, nombre=:nombre, id_perfil=:id_perfil, filtro_ip=:filtro_ip, /*fecha_reg=:fecha_reg,*/ fecha_edit=CURDATE()/*, fecha_expira=:fecha_expira, stat=:stat*/
                WHERE id_usuario=:id_usuario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_usuario" => $this->id_usuario, ":nom_usr" => $this->nom_usr, ":pswd" => $this->pswd, ":nombre" => $this->nombre, ":id_perfil" => $this->id_perfil, ":filtro_ip" => $this->filtro_ip/*, ":fecha_reg" => $this->fecha_reg, ":fecha_edit" => $this->fecha_edit, ":fecha_expira" => $this->fecha_expira, ":stat" => $this->stat*/));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para dar de baja a un usuario
     * @return boolean true si el proceso es satisfactorio
     */
    public function delete()
    {
        $sql = "UPDATE xtblusuarios
                   SET stat=0 
                WHERE id_usuario=:id_usuario;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_usuario" => $this->id_usuario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
}


?>