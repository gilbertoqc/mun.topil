<?php
/**
 *
 */
class AdmtblDomicilio
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $calle; /** @Tipo: varchar(60), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $n_exterior; /** @Tipo: varchar(6), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $n_interior; /** @Tipo: varchar(6), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $entre_calle1; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $entre_calle2; /** @Tipo: varchar(60), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $colonia; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $ciudad; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $cod_postal; /** @Tipo: int(6) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tel_fijo; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $tel_movil; /** @Tipo: varchar(15), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_entidad; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_municipios.class.php';
        require_once 'admtbl_datos_personales.class.php';
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, calle, n_exterior, n_interior, entre_calle1, entre_calle2, colonia, ciudad, cod_postal, tel_fijo, tel_movil, id_municipio, id_entidad
                FROM admtbl_domicilio
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->calle = $data['calle'];
            $this->n_exterior = $data['n_exterior'];
            $this->n_interior = $data['n_interior'];
            $this->entre_calle1 = $data['entre_calle1'];
            $this->entre_calle2 = $data['entre_calle2'];
            $this->colonia = $data['colonia'];
            $this->ciudad = $data['ciudad'];
            $this->cod_postal = $data['cod_postal'];
            $this->tel_fijo = $data['tel_fijo'];
            $this->tel_movil = $data['tel_movil'];
            $this->id_municipio = $data['id_municipio'];
            $this->id_entidad = $data['id_entidad'];

            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->AdmtblDatosPersonales->select($this->curp);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.calle, a.n_exterior, a.n_interior, a.entre_calle1, a.entre_calle2, a.colonia, a.ciudad, a.cod_postal, a.tel_fijo, a.tel_movil, a.id_municipio, a.id_entidad,
                  b.id_municipio, b.municipio, b.cabecera, b.id_entidad, b.id_region,
                  c.curp, c.nombre, c.a_paterno, c.a_materno, c.fecha_nac, c.genero, c.rfc, c.cuip, c.folio_ife, c.mat_cartilla, c.licencia_conducir, c.pasaporte, c.id_estado_civil, c.id_tipo_sangre, c.id_nacionalidad, c.id_entidad, c.id_municipio, c.lugar_nac, c.id_status
                FROM admtbl_domicilio a 
                 LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio
                 LEFT JOIN admtbl_datos_personales c ON a.curp=c.curp";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'calle' => $data['calle'],
                               'n_exterior' => $data['n_exterior'],
                               'n_interior' => $data['n_interior'],
                               'entre_calle1' => $data['entre_calle1'],
                               'entre_calle2' => $data['entre_calle2'],
                               'colonia' => $data['colonia'],
                               'ciudad' => $data['ciudad'],
                               'cod_postal' => $data['cod_postal'],
                               'tel_fijo' => $data['tel_fijo'],
                               'tel_movil' => $data['tel_movil'],
                               'id_municipio' => $data['id_municipio'],
                               'id_entidad' => $data['id_entidad'],
                               'admcat_municipios_municipio' => $data['municipio'],
                               'admcat_municipios_cabecera' => $data['cabecera'],
                               'admcat_municipios_id_entidad' => $data['id_entidad'],
                               'admcat_municipios_id_region' => $data['id_region'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_domicilio(curp, calle, n_exterior, n_interior, entre_calle1, entre_calle2, colonia, ciudad, cod_postal, tel_fijo, tel_movil, id_municipio, id_entidad)
                VALUES(:curp, :calle, :n_exterior, :n_interior, :entre_calle1, :entre_calle2, :colonia, :ciudad, :cod_postal, :tel_fijo, :tel_movil, :id_municipio, :id_entidad);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":calle" => $this->calle, ":n_exterior" => $this->n_exterior, ":n_interior" => $this->n_interior, ":entre_calle1" => $this->entre_calle1, ":entre_calle2" => $this->entre_calle2, ":colonia" => $this->colonia, ":ciudad" => $this->ciudad, ":cod_postal" => $this->cod_postal, ":tel_fijo" => $this->tel_fijo, ":tel_movil" => $this->tel_movil, ":id_municipio" => $this->id_municipio, ":id_entidad" => $this->id_entidad));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_domicilio
                   SET calle=:calle, n_exterior=:n_exterior, n_interior=:n_interior, entre_calle1=:entre_calle1, entre_calle2=:entre_calle2, colonia=:colonia, ciudad=:ciudad, cod_postal=:cod_postal, tel_fijo=:tel_fijo, tel_movil=:tel_movil, id_municipio=:id_municipio, id_entidad=:id_entidad
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":calle" => $this->calle, ":n_exterior" => $this->n_exterior, ":n_interior" => $this->n_interior, ":entre_calle1" => $this->entre_calle1, ":entre_calle2" => $this->entre_calle2, ":colonia" => $this->colonia, ":ciudad" => $this->ciudad, ":cod_postal" => $this->cod_postal, ":tel_fijo" => $this->tel_fijo, ":tel_movil" => $this->tel_movil, ":id_municipio" => $this->id_municipio, ":id_entidad" => $this->id_entidad));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>