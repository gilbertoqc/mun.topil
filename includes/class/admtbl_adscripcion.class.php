<?php
/**
 *
 */
class AdmtblAdscripcion
{
    public $curp; /** @Tipo: varchar(18), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $fecha_ing; /** @Tipo: date, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $cargo; /** @Tipo: varchar(150), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_categoria; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo_funcion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_especialidad; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_nivel_mando; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_area; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_municipio; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_entidad; /** @Tipo: int(10) unsigned, @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_corporacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_ubicacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_horario; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $id_plaza; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatMunicipios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatAreas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatCategorias; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatCorporaciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblDatosPersonales; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatEspecialidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatHorarios; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatNivelMando; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmtblPlantillaPlazas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatTipoFunciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $AdmcatUbicaciones; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_municipios.class.php';
        require_once 'admcat_areas.class.php';
        require_once 'admcat_categorias.class.php';
        require_once 'admcat_corporaciones.class.php';
        require_once 'admtbl_datos_personales.class.php';
        require_once 'admcat_especialidades.class.php';
        require_once 'admcat_horarios.class.php';
        require_once 'admcat_nivel_mando.class.php';
        //require_once 'admtbl_plantilla_plazas.class.php';
        require_once 'admcat_tipo_funciones.class.php';
        require_once 'admcat_ubicaciones.class.php';
        $this->AdmcatMunicipios = new AdmcatMunicipios();
        $this->AdmcatAreas = new AdmcatAreas();
        $this->AdmcatCategorias = new AdmcatCategorias();
        $this->AdmcatCorporaciones = new AdmcatCorporaciones();
        $this->AdmtblDatosPersonales = new AdmtblDatosPersonales();
        $this->AdmcatEspecialidades = new AdmcatEspecialidades();
        $this->AdmcatHorarios = new AdmcatHorarios();
        $this->AdmcatNivelMando = new AdmcatNivelMando();
        //$this->AdmtblPlantillaPlazas = new AdmtblPlantillaPlazas();
        $this->AdmcatTipoFunciones = new AdmcatTipoFunciones();
        $this->AdmcatUbicaciones = new AdmcatUbicaciones();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($curp)
    {
        $sql = "SELECT curp, fecha_ing, cargo, id_categoria, id_tipo_funcion, id_especialidad, id_nivel_mando, id_area, id_municipio, id_entidad, id_corporacion, id_ubicacion, id_horario
                FROM admtbl_adscripcion
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':curp' => $curp));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->curp = $data['curp'];
            $this->fecha_ing = $data['fecha_ing'];
            $this->cargo = $data['cargo'];
            $this->id_categoria = $data['id_categoria'];
            $this->id_tipo_funcion = $data['id_tipo_funcion'];
            $this->id_especialidad = $data['id_especialidad'];
            $this->id_nivel_mando = $data['id_nivel_mando'];
            $this->id_area = $data['id_area'];
            $this->id_municipio = $data['id_municipio'];
            $this->id_entidad = $data['id_entidad'];
            $this->id_corporacion = $data['id_corporacion'];
            $this->id_ubicacion = $data['id_ubicacion'];
            $this->id_horario = $data['id_horario'];
            //$this->id_plaza = $data['id_plaza'];

            $this->AdmcatMunicipios->select($this->id_municipio);
            $this->AdmcatAreas->select($this->id_area);
            $this->AdmcatCategorias->select($this->id_categoria);
            $this->AdmcatCorporaciones->select($this->id_corporacion);
            $this->AdmtblDatosPersonales->select($this->curp);
            $this->AdmcatEspecialidades->select($this->id_especialidad);
            $this->AdmcatHorarios->select($this->id_horario);
            $this->AdmcatNivelMando->select($this->id_nivel_mando);
            //$this->AdmtblPlantillaPlazas->select($this->id_plaza);
            $this->AdmcatTipoFunciones->select($this->id_tipo_funcion);
            $this->AdmcatUbicaciones->select($this->id_ubicacion);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.curp, a.fecha_ing, a.cargo, a.id_categoria, a.id_tipo_funcion, a.id_especialidad, a.id_nivel_mando, a.id_area, a.id_municipio, a.id_entidad, a.id_corporacion, a.id_ubicacion, a.id_horario, a.id_plaza,
                  b.id_municipio, b.municipio, b.cabecera, b.id_entidad, b.id_region,
                  c.id_area, c.area, c.titular, c.tel�fono, c.status, c.id_nivel, c.id_depende, c.orden,
                  d.id_categoria, d.categoria, d.status,
                  e.id_corporacion, e.corporacion,
                  f.curp, f.nombre, f.a_paterno, f.a_materno, f.fecha_nac, f.genero, f.rfc, f.cuip, f.folio_ife, f.mat_cartilla, f.licencia_conducir, f.pasaporte, f.id_estado_civil, f.id_tipo_sangre, f.id_nacionalidad, f.id_entidad, f.id_municipio, f.lugar_nac, f.id_status,
                  g.id_especialidad, g.especialidad, g.status,
                  h.id_horario, h.horario, h.status,
                  i.id_nivel_mando, i.nivel_mando,                  
                  k.id_tipo_funcion, k.tipo_funcion,
                  l.id_ubicacion, l.ubicacion, l.status
                FROM admtbl_adscripcion a 
                 LEFT JOIN admcat_municipios b ON a.id_municipio=b.id_municipio
                 LEFT JOIN admcat_areas c ON a.id_area=c.id_area
                 LEFT JOIN admcat_categorias d ON a.id_categoria=d.id_categoria
                 LEFT JOIN admcat_corporaciones e ON a.id_corporacion=e.id_corporacion
                 LEFT JOIN admtbl_datos_personales f ON a.curp=f.curp
                 LEFT JOIN admcat_especialidades g ON a.id_especialidad=g.id_especialidad
                 LEFT JOIN admcat_horarios h ON a.id_horario=h.id_horario
                 LEFT JOIN admcat_nivel_mando i ON a.id_nivel_mando=i.id_nivel_mando                 
                 LEFT JOIN admcat_tipo_funciones k ON a.id_tipo_funcion=k.id_tipo_funcion
                 LEFT JOIN admcat_ubicaciones l ON a.id_ubicacion=l.id_ubicacion";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'curp' => $data['curp'],
                               'fecha_ing' => $data['fecha_ing'],
                               'cargo' => $data['cargo'],
                               'id_categoria' => $data['id_categoria'],
                               'id_tipo_funcion' => $data['id_tipo_funcion'],
                               'id_especialidad' => $data['id_especialidad'],
                               'id_nivel_mando' => $data['id_nivel_mando'],
                               'id_area' => $data['id_area'],
                               'id_municipio' => $data['id_municipio'],
                               'id_entidad' => $data['id_entidad'],
                               'id_corporacion' => $data['id_corporacion'],
                               'id_ubicacion' => $data['id_ubicacion'],
                               'id_horario' => $data['id_horario'],
                               'admcat_municipios_municipio' => $data['municipio'],
                               'admcat_municipios_cabecera' => $data['cabecera'],
                               'admcat_municipios_id_entidad' => $data['id_entidad'],
                               'admcat_municipios_id_region' => $data['id_region'],
                               'admcat_areas_area' => $data['area'],
                               'admcat_areas_titular' => $data['titular'],
                               'admcat_areas_tel�fono' => $data['tel�fono'],
                               'admcat_areas_status' => $data['status'],
                               'admcat_areas_id_nivel' => $data['id_nivel'],
                               'admcat_areas_id_depende' => $data['id_depende'],
                               'admcat_areas_orden' => $data['orden'],
                               'admcat_categorias_categoria' => $data['categoria'],
                               'admcat_categorias_status' => $data['status'],
                               'admcat_corporaciones_corporacion' => $data['corporacion'],
                               'admtbl_datos_personales_nombre' => $data['nombre'],
                               'admtbl_datos_personales_a_paterno' => $data['a_paterno'],
                               'admtbl_datos_personales_a_materno' => $data['a_materno'],
                               'admtbl_datos_personales_fecha_nac' => $data['fecha_nac'],
                               'admtbl_datos_personales_genero' => $data['genero'],
                               'admtbl_datos_personales_rfc' => $data['rfc'],
                               'admtbl_datos_personales_cuip' => $data['cuip'],
                               'admtbl_datos_personales_folio_ife' => $data['folio_ife'],
                               'admtbl_datos_personales_mat_cartilla' => $data['mat_cartilla'],
                               'admtbl_datos_personales_licencia_conducir' => $data['licencia_conducir'],
                               'admtbl_datos_personales_pasaporte' => $data['pasaporte'],
                               'admtbl_datos_personales_id_estado_civil' => $data['id_estado_civil'],
                               'admtbl_datos_personales_id_tipo_sangre' => $data['id_tipo_sangre'],
                               'admtbl_datos_personales_id_nacionalidad' => $data['id_nacionalidad'],
                               'admtbl_datos_personales_id_entidad' => $data['id_entidad'],
                               'admtbl_datos_personales_id_municipio' => $data['id_municipio'],
                               'admtbl_datos_personales_lugar_nac' => $data['lugar_nac'],
                               'admtbl_datos_personales_id_status' => $data['id_status'],
                               'admcat_especialidades_especialidad' => $data['especialidad'],
                               'admcat_especialidades_status' => $data['status'],
                               'admcat_horarios_horario' => $data['horario'],
                               'admcat_horarios_status' => $data['status'],
                               'admcat_nivel_mando_nivel_mando' => $data['nivel_mando'],
                               'admtbl_plantilla_plazas_id_categoria' => $data['id_categoria'],
                               'admtbl_plantilla_plazas_id_area' => $data['id_area'],
                               'admtbl_plantilla_plazas_observaciones' => $data['observaciones'],
                               'admcat_tipo_funciones_tipo_funcion' => $data['tipo_funcion'],
                               'admcat_ubicaciones_ubicacion' => $data['ubicacion'],
                               'admcat_ubicaciones_status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admtbl_adscripcion(curp, fecha_ing, cargo, id_categoria, id_tipo_funcion, id_especialidad, id_nivel_mando, id_area, id_municipio, id_entidad, id_corporacion, id_ubicacion, id_horario)
                VALUES(:curp, :fecha_ing, :cargo, :id_categoria, :id_tipo_funcion, :id_especialidad, :id_nivel_mando, :id_area, :id_municipio, :id_entidad, :id_corporacion, :id_ubicacion, :id_horario);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":fecha_ing" => $this->fecha_ing, ":cargo" => $this->cargo, ":id_categoria" => $this->id_categoria, ":id_tipo_funcion" => $this->id_tipo_funcion, ":id_especialidad" => $this->id_especialidad, ":id_nivel_mando" => $this->id_nivel_mando, ":id_area" => $this->id_area, ":id_municipio" => $this->id_municipio, ":id_entidad" => $this->id_entidad, ":id_corporacion" => $this->id_corporacion, ":id_ubicacion" => $this->id_ubicacion, ":id_horario" => $this->id_horario));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admtbl_adscripcion
                   SET fecha_ing=:fecha_ing, cargo=:cargo, id_categoria=:id_categoria, id_tipo_funcion=:id_tipo_funcion, id_especialidad=:id_especialidad, id_nivel_mando=:id_nivel_mando, id_area=:id_area, id_municipio=:id_municipio, id_entidad=:id_entidad, id_corporacion=:id_corporacion, id_ubicacion=:id_ubicacion, id_horario=:id_horario
                WHERE curp=:curp;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":curp" => $this->curp, ":fecha_ing" => $this->fecha_ing, ":cargo" => $this->cargo, ":id_categoria" => $this->id_categoria, ":id_tipo_funcion" => $this->id_tipo_funcion, ":id_especialidad" => $this->id_especialidad, ":id_nivel_mando" => $this->id_nivel_mando, ":id_area" => $this->id_area, ":id_municipio" => $this->id_municipio, ":id_entidad" => $this->id_entidad, ":id_corporacion" => $this->id_corporacion, ":id_ubicacion" => $this->id_ubicacion, ":id_horario" => $this->id_horario));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>