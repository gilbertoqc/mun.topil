<?php
/**
 *
 */
class LogcatArmOperacion
{
    public $id_operacion; /** @Tipo: tinyint(2), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $descripcion; /** @Tipo: varchar(10), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_operacion)
    {
        $sql = "SELECT id_operacion, descripcion
                FROM logcat_arm_operacion
                WHERE id_operacion=:id_operacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_operacion' => $id_operacion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_operacion = $data['id_operacion'];
            $this->descripcion = $data['descripcion'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_operacion, a.descripcion
                FROM logcat_arm_operacion a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_operacion' => $data['id_operacion'],
                               'descripcion' => $data['descripcion'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_operacion(id_operacion, descripcion)
                VALUES(:id_operacion, :descripcion);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_operacion" => $this->id_operacion, ":descripcion" => $this->descripcion));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_operacion
                   SET descripcion=:descripcion
                WHERE id_operacion=:id_operacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_operacion" => $this->id_operacion, ":descripcion" => $this->descripcion));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>