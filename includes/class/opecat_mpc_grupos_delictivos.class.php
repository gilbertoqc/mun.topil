<?php
/**
 *
 */
class OpecatMpcGruposDelictivos
{
    public $id_grupo_delictivo; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $grupo_delictivo; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

/**
     * Funci�n para mostrar la lista de Grupos Delictivos dentro de un combobox.
     * @param int $id, id del grupo_delictivo seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_GruposDelictivos($id=0){
        $aryDatos = $this->selectAll('grupo_delictivo Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_grupo_delictivo"] )
                $html .= '<option value="'.$datos["id_grupo_delictivo"].'" selected>'.$datos["grupo_delictivo"].'</option>';
            else
                $html .= '<option value="'.$datos["id_grupo_delictivo"].'" >'.$datos["grupo_delictivo"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_grupo_delictivo)
    {
        $sql = "SELECT id_grupo_delictivo, grupo_delictivo
                FROM opecat_mpc_grupos_delictivos
                WHERE id_grupo_delictivo=:id_grupo_delictivo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_grupo_delictivo' => $id_grupo_delictivo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_grupo_delictivo = $data['id_grupo_delictivo'];
            $this->grupo_delictivo = $data['grupo_delictivo'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_grupo_delictivo, a.grupo_delictivo
                FROM opecat_mpc_grupos_delictivos a ";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_grupo_delictivo' => $data['id_grupo_delictivo'],
                               'grupo_delictivo' => $data['grupo_delictivo'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mpc_grupos_delictivos(id_grupo_delictivo, grupo_delictivo)
                VALUES(:id_grupo_delictivo, :grupo_delictivo);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_grupo_delictivo" => $this->id_grupo_delictivo, ":grupo_delictivo" => $this->grupo_delictivo));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mpc_grupos_delictivos
                   SET grupo_delictivo=:grupo_delictivo
                WHERE id_grupo_delictivo=:id_grupo_delictivo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_grupo_delictivo" => $this->id_grupo_delictivo, ":grupo_delictivo" => $this->grupo_delictivo));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>