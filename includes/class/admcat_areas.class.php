<?php
/**
 *
 */
class AdmcatAreas
{
    public $id_area; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $area; /** @Tipo: varchar(254), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $titular; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $tel�fono; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    public $id_nivel; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_depende; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: YES, @Llave: MUL, @Default: NULL */
    public $orden; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos    

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();        
    }
    /**
     * 
     * 
     */ 
    public function shwAreas($tamMax=50, $id=0)
    {
        try{
            $qSql = $this->_conexBD->prepare("SELECT ca.id_area, ca.area 
                                              FROM admcat_areas ca 
                                                JOIN admcat_nivel_area cna ON ca.id_nivel=cna.id_nivel 
                                              WHERE status=1 AND id_depende IS NULL 
                                              ORDER BY cna.gerarquia, ca.orden;");
            $qSql->execute();                
            while ($f = $qSql->fetch(PDO::FETCH_ASSOC)) {
                if (strlen($f['area']) > $tamMax ) {
                    $descrip = substr($f['area'], 0, $tamMax - 3) . "...";
                    $xhint = 'title="' . $f['area'] . '"';
                }
                else {
                    $descrip = $f['area'];
                    $xhint = '';
                }
                $html .= '<option value="' . $f['id_area'] . '" ' . $xhint . '>' . $descrip . '</option>';
                $html .= $this->_subAreas($f['id_area'], 1, $id, $tamMax);
            }
            
            return $html;
        }
        catch (PDOException $e) {
            $this->msjError = $e.getMessage();
            return false;
        }       
    }
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_area)
    {
        $sql = "SELECT id_area, area, titular, tel�fono, status, id_nivel, id_depende, orden
                FROM admcat_areas
                WHERE id_area=:id_area;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_area' => $id_area));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_area = $data['id_area'];
            $this->area = $data['area'];
            $this->titular = $data['titular'];
            $this->tel�fono = $data['tel�fono'];
            $this->status = $data['status'];
            $this->id_nivel = $data['id_nivel'];
            $this->id_depende = $data['id_depende'];
            $this->orden = $data['orden'];
            
            //$this->AdmcatNivelArea->select($this->id_nivel);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_area, a.area, a.titular, a.tel�fono, a.status, a.id_nivel, a.id_depende, a.orden,
                  b.id_area, b.area, b.titular, b.tel�fono, b.status, b.id_nivel, b.id_depende, b.orden,
                  c.id_nivel, c.nivel, c.gerarquia
                FROM admcat_areas a 
                 LEFT JOIN admcat_areas b ON a.id_depende=b.id_area
                 LEFT JOIN admcat_nivel_area c ON a.id_nivel=c.id_nivel";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_area' => $data['id_area'],
                               'area' => $data['area'],
                               'titular' => $data['titular'],
                               'tel�fono' => $data['tel�fono'],
                               'status' => $data['status'],
                               'id_nivel' => $data['id_nivel'],
                               'id_depende' => $data['id_depende'],
                               'orden' => $data['orden'],
                               'admcat_areas_id_area' => $data['id_area'],
                               'admcat_areas_area' => $data['area'],
                               'admcat_areas_titular' => $data['titular'],
                               'admcat_areas_tel�fono' => $data['tel�fono'],
                               'admcat_areas_status' => $data['status'],
                               'admcat_areas_id_nivel' => $data['id_nivel'],
                               'admcat_areas_orden' => $data['orden'],
                               'admcat_nivel_area_nivel' => $data['nivel'],
                               'admcat_nivel_area_gerarquia' => $data['gerarquia'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_areas(id_area, area, titular, tel�fono, status, id_nivel, id_depende, orden)
                VALUES(:id_area, :area, :titular, :tel�fono, :status, :id_nivel, :id_depende, :orden);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_area" => $this->id_area, ":area" => $this->area, ":titular" => $this->titular, ":tel�fono" => $this->tel�fono, ":status" => $this->status, ":id_nivel" => $this->id_nivel, ":id_depende" => $this->id_depende, ":orden" => $this->orden));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_areas
                   SET area=:area, titular=:titular, tel�fono=:tel�fono, status=:status, id_nivel=:id_nivel, id_depende=:id_depende, orden=:orden
                WHERE id_area=:id_area;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_area" => $this->id_area, ":area" => $this->area, ":titular" => $this->titular, ":tel�fono" => $this->tel�fono, ":status" => $this->status, ":id_nivel" => $this->id_nivel, ":id_depende" => $this->id_depende, ":orden" => $this->orden));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
    
    private function _subAreas($idArea, $nivel, $id, $tamMax)
    {
        $qCount = $this->_conexBD->prepare("SELECT COUNT(*) 
                                            FROM admcat_areas ca 
                                            WHERE ca.status=1 AND ca.id_depende=:id_area;");
        $qCount->execute( array(":id_area" => $idArea) );
        if ($qCount->fetchColumn() > 0) {
            $qSql = $this->_conexBD->prepare("SELECT ca.id_area, ca.area, cna.gerarquia 
                                              FROM admcat_areas ca 
                                                JOIN admcat_nivel_area cna ON ca.id_nivel=cna.id_nivel 
                                              WHERE ca.status=1 AND ca.id_depende=:id_area 
                                              ORDER BY cna.gerarquia, ca.orden;");
            $qSql->execute( array(":id_area" => $idArea) );
            
            $nivel++;
            $html = '';
            $qSql->setFetchMode(PDO::FETCH_ASSOC);
            while ($f1 = $qSql->fetch()) {
                //--- control de la tabulacion...
                $xtab = $this->_setSangria($f1["gerarquia"]);               
                //--- Tama�o del texto a mostrar en el option...
                $xtam_maximo = $tamMax - (($nivel-1) * 5);
                $tam_subcadena = ($xtam_maximo - 3) + $nivel;
                if ((strlen($f1['area']) > $xtam_maximo) && (strlen($f1['area']) > $tam_subcadena)) {
                    $descrip = substr($f1['area'], 0, $tam_subcadena)."...";
                    $xhint = 'title="' . $f1['area'] . '"';
                }            
                else {
                    $descrip = $f1['area'];
                    $xhint = '';
                }
                
                $xSelected = "";
                if ( $id == $f1['id_area'])
                    $xSelected = "selected";
                $html .= '<option value="' . $f1['id_area'] . '" ' . $xhint . ' ' . $xSelected . '>' . $xtab . $descrip . '</option>';
                
                $html .= $this->_subAreas($f1['id_area'], $nivel, $id, $tamMax);
            }
        }
        
        return $html;
    }
    private function _setSangria($nivel)
    {
        $total = (int)($nivel - 1) * 4;                        
        $tab = "";
        for ($idx=0; $idx<$total; $idx++)
            $tab .= "&nbsp;";
        return $tab;
    }
}


?>