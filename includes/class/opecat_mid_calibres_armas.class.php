<?php
/**
 *
 */
class OpecatMidCalibresArmas
{
    public $id_calibre_arma; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $calibre_arma; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_calibre_arma)
    {
        $sql = "SELECT id_calibre_arma, calibre_arma
                FROM opecat_mid_calibres_armas
                WHERE id_calibre_arma=:id_calibre_arma;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_calibre_arma' => $id_calibre_arma));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_calibre_arma = $data['id_calibre_arma'];
            $this->calibre_arma = $data['calibre_arma'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_calibre_arma, a.calibre_arma
                FROM opecat_mid_calibres_armas a ";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_calibre_arma' => $data['id_calibre_arma'],
                               'calibre_arma' => $data['calibre_arma'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_calibres_armas(id_calibre_arma, calibre_arma)
                VALUES(:id_calibre_arma, :calibre_arma);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_calibre_arma" => $this->id_calibre_arma, ":calibre_arma" => $this->calibre_arma));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_calibres_armas
                   SET calibre_arma=:calibre_arma
                WHERE id_calibre_arma=:id_calibre_arma;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_calibre_arma" => $this->id_calibre_arma, ":calibre_arma" => $this->calibre_arma));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>