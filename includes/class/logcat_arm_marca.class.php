<?php
/**
 *
 */
class LogcatArmMarca
{
    public $id_marca; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $marca; /** @Tipo: varchar(70), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_clase; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_modelo; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatArmClase; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatArmModelo; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_arm_clase.class.php';
        require_once 'logcat_arm_modelo.class.php';
        $this->LogcatArmClase = new LogcatArmClase();
        $this->LogcatArmModelo = new LogcatArmModelo();
    }
    
    /**
     * Funci�n para mostrar la lista de marcas dentro de un combobox.
     * @param int $id, id de la marca seleccionado por deafult  
     * @param int $id_clase, id de la clase para el filtro de la clase
     * @param int $id_modelo, id del modelo para el filtro del modelo
     * @return array html(options)
     */
    public function shwMarcas($id, $id_clase, $id_modelo){
        if( $id_clase != 0 ){
            $query .= ' a.id_clase=' . $id_clase;
            if( $id_modelo != 0 ){
                $query .= ' and a.id_modelo=' . $id_modelo;
            }else{
                
            }
        }else{
            $query = '';
        }        
                            
        $aryDatos = $this->selectAll($query, 'b.clase, c.modelo Asc');
        
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_marca"] )
                $html .= '<option value="'.$datos["id_marca"].'" selected>'.$datos["marca"].'</option>';
            else
                $html .= '<option value="'.$datos["id_marca"].'" >'.$datos["marca"].'</option>';
        }
        return $html;
    }
    
    /**
     * Funci�n para obtener la marca del arma atravez de la tabla de marcas
     * @param  $int id_marca, se recibe el parametro id_marca para hacer el filtro
     * @return la marca del arma
     */
    public function getMarca( $id_marca ){
	
		$sql = "SELECT m.id_marca, m.marca, m.xstat
                FROM logtbl_arm_armamento as a
                INNER JOIN logcat_arm_marca as m On m.id_marca=a.id_marca  
				WHERE a.id_marca=:id_marca
				AND m.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['marca'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}
    
    /**
     * Funci�n para obtener el id de la clase del arma atravez de la tabla de marcas
     * @param  $int id_marca, se recibe el parametro id_marca para hacer el filtro
     * @return el id de la clase
     */
    public function getIdClase( $id_marca ){
	
		$sql = "SELECT c.id_clase
                FROM logcat_arm_clase as c
				INNER JOIN logcat_arm_marca as m On m.id_clase=c.id_clase
				WHERE m.id_marca=:id_marca
				AND m.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['id_clase'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}
    
    /**
     * Funci�n para obtener el id del modelo del arma atravez de la tabla de marcas
     * @param  se recibe el parametro id_marca para hacer el filtro
     * @return el id del modelo
     */
    public function getIdModelo( $id_marca ){
	
		$sql = "SELECT c.id_modelo
                FROM logcat_arm_marca as a
				INNER JOIN logcat_arm_modelo as c On c.id_modelo=a.id_modelo
				WHERE a.id_marca=:id_marca
				AND c.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['id_modelo'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_marca)
    {
        $sql = "SELECT id_marca, marca, id_clase, id_modelo, xstat
                FROM logcat_arm_marca
                WHERE id_marca=:id_marca;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_marca' => $id_marca));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_marca = $data['id_marca'];
            $this->marca = $data['marca'];
            $this->id_clase = $data['id_clase'];
            $this->id_modelo = $data['id_modelo'];
            $this->xstat = $data['xstat'];

            $this->LogcatArmClase->select($this->id_clase);
            $this->LogcatArmModelo->select($this->id_modelo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_marca, a.marca, a.id_clase, a.id_modelo, a.xstat,
                  b.id_clase, b.clase, b.id_tipo, b.xstat,
                  c.id_modelo, c.modelo, c.id_calibre, c.xstat
                FROM logcat_arm_marca a 
                 LEFT JOIN logcat_arm_clase b ON a.id_clase=b.id_clase
                 LEFT JOIN logcat_arm_modelo c ON a.id_modelo=c.id_modelo";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_marca' => $data['id_marca'],
                               'marca' => $data['marca'],
                               'id_clase' => $data['id_clase'],
                               'id_modelo' => $data['id_modelo'],
                               'xstat' => $data['xstat'],
                               'logcat_arm_clase_clase' => $data['clase'],
                               'logcat_arm_clase_id_tipo' => $data['id_tipo'],
                               'logcat_arm_clase_xstat' => $data['xstat'],
                               'logcat_arm_modelo_modelo' => $data['modelo'],
                               'logcat_arm_modelo_id_calibre' => $data['id_calibre'],
                               'logcat_arm_modelo_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_marca(id_marca, marca, id_clase, id_modelo, xstat)
                VALUES(:id_marca, :marca, :id_clase, :id_modelo, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_marca" => $this->id_marca, ":marca" => $this->marca, ":id_clase" => $this->id_clase, ":id_modelo" => $this->id_modelo, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_marca
                   SET marca=:marca, id_clase=:id_clase, id_modelo=:id_modelo, xstat=:xstat
                WHERE id_marca=:id_marca;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_marca" => $this->id_marca, ":marca" => $this->marca, ":id_clase" => $this->id_clase, ":id_modelo" => $this->id_modelo, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>