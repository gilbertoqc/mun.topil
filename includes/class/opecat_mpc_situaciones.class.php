<?php
/**
 *
 */
class OpecatMpcSituaciones
{
    public $id_situacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $situacion; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

     /**
     * Funci�n para mostrar la lista de Situaciones dentro de un combobox.
     * @param int $id, id de la situacion seleccionada por deafult
     * @return array html(options)
     */
    public function getCat_Situaciones($id=0){
        $aryDatos = $this->selectAll('id_situacion asc',"");
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_situacion"] )
                $html .= '<option value="'.$datos["id_situacion"].'" selected>'.$datos["situacion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_situacion"].'" >'.$datos["situacion"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_situacion)
    {
        $sql = "SELECT id_situacion, situacion
                FROM opecat_mpc_situaciones
                WHERE id_situacion=:id_situacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_situacion' => $id_situacion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_situacion = $data['id_situacion'];
            $this->situacion = $data['situacion'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_situacion, a.situacion
                FROM opecat_mpc_situaciones a ";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_situacion' => $data['id_situacion'],
                               'situacion' => $data['situacion'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mpc_situaciones(id_situacion, situacion)
                VALUES(:id_situacion, :situacion);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_situacion" => $this->id_situacion, ":situacion" => $this->situacion));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mpc_situaciones
                   SET situacion=:situacion
                WHERE id_situacion=:id_situacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_situacion" => $this->id_situacion, ":situacion" => $this->situacion));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>