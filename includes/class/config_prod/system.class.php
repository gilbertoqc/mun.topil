<?php
/**
 * @author SisA
 * @copyright 2013
 *  
 * La clase System. Para el control de par�metros de configuraci�n de todo el sistema 
 * 
 */
 include_once 'config.cfg.php';
class System
{
    public $msjError;
    private $_oBd;
    private $_server_name;      
    private $_subdomain;
    private $_pswd_main;
    public $encrypt;
    
    public function __construct()
    {
        require_once('mysql.class.php');
        $this->_objBd = new MySqlPdo();
        
        $this->server_name  = SERVER_NAME;        
        $this->subdomain    = SUBDOMAIN;
        
        // Pswd para encriptaci�n de la informaci�n en la BD
        $this->pswd_main    = '520x1000109010511100500x480x490x510x360x731x670x800x770x';
        // Define si se encriptar� la informaci�n en la BD
        $this->encrypt      = false;
    }
    
    /** 
     * Se obtiene el nombre de dominio y subdominio del sistema
     * 
     */ 
    public function getServer()
    {
        return $this->server_name . $this->subdomain;
    }
    
    /** 
     * Se obtiene el pswd de excriptaci�n de la inf. en la BD
     * 
     */ 
    public function getPswd()
    {
       return $this->Decrypt($this->_pswd_main); 
    }
    
    /** 
     * Genera el HTML del men� principal de acuerdo a los permisos del usuario
     * 
     */ 
    public function getMenu($id_usr, $perfil)
    {        
        $host = $this->getServer();
        
        $menu = '<ul id="ulMenu-Main">';        
        // Recorre el resto de los menus
        $qMenu = $this->_objBd->prepare("SELECT id_menu, menu, url, icono, id_padre 
                                         FROM xtblmenu 
                                         WHERE id_padre IS NULL AND stat=1 
                                         ORDER BY orden;");
        $qMenu->execute();
        $qMenu->setFetchMode(PDO::FETCH_ASSOC);
        while ($fMenu = $qMenu->fetch()) {
            // Crea el men� definido en la tabla
            $menu_temp = "";           
            // Genera el link de acceso al m�dulo correspondiente del menu
            $url = '#';
            if (!empty($fMenu['url']) && $fMenu['url'] != '#') {
                $url = $host . 'index.php?m=' . $fMenu['id_menu'];
            }
            // Verifica si existen opciones y/o submenus dependientes del men� actual
            $temp = $this->_getSubmenu($fMenu['id_menu'], 1, $id_usr, $perfil);
            // Apertura de la opcion del men� de nivel 1...
            if (!empty($temp)) {
                $menu_temp = '<li>';
                $menu_temp .= '<a href="' . $url . '" style="text-align: center;">';
                $menu_temp .= '<img src="' . PATH_IMAGES . $fMenu['icono'] . '" alt="" />'; 
                $menu_temp .= '<span style="position: relative;">' . $fMenu['menu'] . '</span> <span class="sty_xopmenu"></span>';
                $menu_temp .= '</a>';
            } else {
                $menu_temp = '<li>';
                $menu_temp .= '<a href="' . $url . '" style="text-align: center;">';
                $menu_temp .= '<img src="' . PATH_IMAGES . $fMenu['icono'] . '" alt="" />'; 
                $menu_temp .= '<span style="position: relative;">' . $fMenu['menu'] . '</span>';
                $menu_temp .= '</a>';               
            }
            // Agrega los submen�s si estos existen
            $menu_temp .= $temp;
            // Cierra la opci�n del men� de nivel 1
            $menu_temp .= '</li>'; 
            // Valida si ser� creado(visualmente para el usuario) el men� actual
            if ($temp != '' || $this->_getAcceso($fMenu['id_menu'], $id_usr) || $perfil != 0) {
                $menu .= $menu_temp;
            }
        }        
        $menu .= '</ul>';
        
        echo $menu;
    }
    /** 
     * Obtiene el contenido de los submen�s del men� principal
     * (Funci�n Recursiva)
     * 
     */ 
    private function _getSubmenu($id_menu, $nivel, $id_usr, $perfil)
    {
        $host = $this->getServer();
        
        $qSql = $this->_objBd->prepare("SELECT COUNT(*) 
                                        FROM xtblmenu 
                                        WHERE id_padre=:id_menu AND stat=1;");                  
        $qSql->execute( array(':id_menu' => $id_menu) );
        if ($qSql->fetchColumn() > 0) {
            $qSbm = $this->_objBd->prepare("SELECT id_menu, menu, url 
                                            FROM xtblmenu 
                                            WHERE id_padre=:id_menu AND stat=1
                                            ORDER BY orden;");                  
            $qSbm->execute( array(':id_menu' => $id_menu) );
            $qSbm->setFetchMode(PDO::FETCH_ASSOC);
            // Despliega las opciones del men� actual
            if ($nivel == 1){
                $menu_temp = '<ul class="ulMenu-Main-Submenu">';
            } else {
                $menu_temp = '<ul class="ulMenu-Main-Subsubmenu">';
            }
            $band = 0;            
            while ($fSbm = $qSbm->fetch()) {
                $temp = '';
                $temp1 = '';
                $url = '#';
                if (!empty($fSbm['url']) && $fSbm['url'] != '#' ) {
                    $url = $host . 'index.php?m=' . $fSbm['id_menu'];
                }
                if ($fSbm['url'] == '#') {
                    $temp1 .= $this->_getSubmenu($fSbm['id_menu'], $nivel+1, $id_usr, $perfil);
                }
                if ($nivel >= 1 && !empty($temp1)) {
                    $temp .= '<li><a href="' . $url . '">' . $fSbm['menu'] . ' <span class="sty_xsubm"></span></a>';
                } else {
                    $temp .= '<li><a href="' . $url . '">' . $fSbm['menu'] . '</a>';
                }
                $temp .= $temp1 . '</li>';
                if ($perfil != 0 || ($fSbm['url'] != '#' && $this->_getAcceso($fSbm['id_menu'], $id_usr))) {
                    $menu_temp .= $temp;
                    $band = 1;
                } else if ($fSbm['url'] == '#' && $temp1 != '') {
                    $menu_temp .= $temp;
                    $band = 1;
                }
            }
            $menu_temp .= '</ul>';
            if ($band == 0) {
                $menu_temp = '';
            }
        } else
            $menu_temp .= '';       
         
        return $menu_temp;
    }
    /** 
     * Valida el permiso de acceso de un usuario al men� especificado para controlar si se le muestra o no dicho men�
     *  
     */ 
    private function _getAcceso($id_menu, $id_usr)
    {
        $qSql = $this->_objBd->prepare("SELECT COUNT(*) 
                                        FROM xtblacceso 
                                        WHERE id_usuario=:id_usr AND id_menu=:id_menu;");
        $qSql->execute( array(':id_usr' => $id_usr, ':id_menu' => $id_menu) );
        if ($qSql->fetchColumn() > 0){
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Obtiene la url del menu actual para redireccionar la petici�n del usuario en el index principal
     * 
     * 
     */ 
    public function getUrlMenu($id_menu)
    {
        $qSql = $this->_objBd->prepare("SELECT url 
                                        FROM xtblmenu 
                                        WHERE id_menu=:id_menu;");
        $qSql->execute( array(':id_menu' => $id_menu) );
        return $qSql->fetchColumn();
    }
    
    /**
     * Obtiene la ruta navegaci�n del m�dulo actual conforme a la estructura del men� principal
     * 
     * 
     */ 
    public function nameModule($id_menu, $txt_extra='')
    {
        if ( $id_menu > 0) {
            $datos_name_mod = $this->_getNameModule($id_menu);
            $descrip_menu = $datos_name_mod['descrip'];
            $qSql = $this->_objBd->prepare("SELECT menu 
                                            FROM xtblmenu 
                                            WHERE id_menu=:id_menu;");
            $qSql->execute( array(':id_menu' => $id_menu) );
            $data = $qSql->fetch(PDO::FETCH_ASSOC);
            $descrip_menu .= $data['menu'];
            $datos['icono'] = $datos_name_mod['icono'];
        } else {
            $descrip_menu = 'Inicio';
            $datos['icono'] = null;
        }
        $descrip_menu .= ( !empty($txt_extra) ) ? ' : ' . $txt_extra : '';
        
        $datos['descrip']   = $descrip_menu;        
        
        return $datos;
    }
    /**
     * Funci�n que genera el t�tulo del subm�dulo para mostrarse en el sistema
     * @param int $id_menu, id del men� al cual acces� el usuario
     * @return array, que contiene la descripci�n y el �cono del m�dulo actual
     */ 
    private function _getNameModule($id_menu)
    {
        $xtemp_descrip = "";
        $qCount = $this->_objBd->prepare("SELECT COUNT(*) 
                                          FROM xtblmenu x 
                                          WHERE id_menu=:id_menu;");
        $qSql = $this->_objBd->prepare("SELECT x.id_padre, icono, (SELECT menu FROM xtblmenu m WHERE m.id_menu=x.id_padre) xdescrip
                                        FROM xtblmenu x 
                                        WHERE id_menu=:id_menu;");
        $datos = array();
        $icono = null;
        do{
            $qCount->execute( array(':id_menu' => $id_menu) );
            $qSql->execute( array(':id_menu' => $id_menu) );
            $data = $qSql->fetch(PDO::FETCH_ASSOC);
            $id_menu = $data['id_padre'];
            if (!empty($id_menu))
                $xtemp_descrip = $data['xdescrip'] . ' :: ' . $xtemp_descrip;
            if (!empty($data['icono']))
                $icono = $data['icono'];
        } while ($qCount->fetchColumn() > 0 && !empty($id_menu));
        
        $datos['descrip']   = $xtemp_descrip;
        $datos['icono']     = $icono;
        
        return $datos;
    }
    /**
     * Funci�n que obtiene la lista de accesos directos disponibles para el usuario logueado
     * @param int $id_usr, id del usuario logueado
     * @return array o boolean, devuelve el array con la lista de accesos directos o falso si ocurre un problema
     */ 
    public function getShortcuts($id_usr)
    {
        $qSql = $this->_objBd->prepare("SELECT ca.id_menu, ca.descripcion, ca.icono, ca.hint  
                                        FROM xcatatajos ca
                                            LEFT JOIN xtblatajos a ON ca.id_menu=a.id_menu 
                                        WHERE a.id_usuario=:id_usr 
                                        ORDER BY a.orden;");   
        if ($qSql->execute( array(':id_usr' => $id_usr) )) {
            $qSql->setFetchMode(PDO::FETCH_ASSOC);
            $datos = array();
            $host = $this->getServer();
            while ($f = $qSql->fetch()) {
                $datos[] = array( 'url'     => $host . 'index.php?m=' . $f['id_menu'],
                                  'descrip' => $f['descripcion'],
                                  'icono'   => $f['icono'],
                                  'hint'    => $f['hint'] );
            }
            return $datos;
        } else
            return false;
    }
    
    /*
    //-- Genera el contenido estructurado del combo con los m�dulos del men� principal...
    public function get_combo_modulos($tamMax, $xOptSelected=0)
    {
        $qSql = $this->_oBd->prepare("SELECT * 
                                            FROM xtbmenu 
                                            WHERE stat=1 AND id_padre IS NULL 
                                            ORDER BY orden;");
        if( $qSql->execute() ){
            $qSql->setFetchMode(PDO::FETCH_ASSOC);
            while( $f = $qSql->fetch() ){
                if( strlen($f['menu']) > $tamMax ){
                    $descrip = substr($f['menu'], 0, $tamMax - 3) . "...";
                    $xhint = 'title="' . $f['menu'] . '"';
                }
                else{
                    $descrip = $f['menu'];
                    $xhint = '';
                } 
                $html .= '<option value="' . $f['id_menu'] . '" ' . $xhint . '>' . $descrip . '</option>';
                $html .= $this->_opcion($f['id_menu'], 1, $xOptSelected, $tamMax);
            }        
        }
        else{
            $aryError = $qSql->errorInfo();
            $this->xERROR = $aryError[2];
            return false;
        }
        
        return $html;
    }
    private function _opcion($id_menu, $xNivel, $xOptSelected, $tamMax)
    {
        $qCount = $this->_oBd->prepare("SELECT COUNT(*) 
                                            FROM xtbmenu cm 
                                            WHERE cm.stat=1 AND cm.id_padre=:xid_menu;");
        $qCount->execute( array(":xid_menu" => $id_menu) );
        if( $qCount->fetchColumn() > 0 ){
            $qSql = $this->_oBd->prepare("SELECT cm.id_menu, cm.menu            
                                                FROM xtbmenu cm 
                                                WHERE cm.stat=1 AND cm.id_padre=:xid_menu 
                                                ORDER BY cm.orden;");
            $qSql->execute( array(":xid_menu" => $id_menu) );
        
            $xNivel++;
            $html = '';
            $qSql->setFetchMode(PDO::FETCH_ASSOC);
            while( $f1 = $qSql->fetch() ){
                //--- control de la tabulacion...
                $xtab = $this->_xtabulador($xNivel);               
                //--- Tama�o del texto a mostrar en el option...
                $xtam_maximo = $tamMax - (($xNivel-1) * 5);
                $tam_subcadena = ($xtam_maximo - 3) + $xNivel;
                if( (strlen($f1['menu']) > $xtam_maximo) && (strlen($f1['menu']) > $tam_subcadena) ){
                    $descrip = substr($f1['menu'], 0, $tam_subcadena)."...";
                    $xhint = 'title="' . $f1['menu'] . '"';
                }            
                else{
                    $descrip = $f1['menu'];
                    $xhint = '';
                }
                
                $xSelected = "";
                if( $xOptSelected == $f1['id_menu'] )
                    $xSelected = "selected";
                $html .= '<option value="' . $f1['id_menu'] . '" ' . $xhint . ' ' . $xSelected . '>' . $xtab . $descrip . '</option>';
                
                $html .= $this->_opcion($f1['id_menu'], $xNivel, $xOptSelected, $tamMax);
            }
        }
        
        return $html;
    }
    private function _xtabulador($nivel)
    {
        $total = (int)($nivel - 1) * 5;                        
        $tab = "";
        for( $idx=0; $idx<$total; $idx++ )
            $tab .= "&nbsp;";
        return $tab;
    }
    */
    
    /**
     * Funci�n para realizar el registro de la auditor�a
     * @param int $usr, id del usuario que realiz� el cambio
     * @param string $tabla, nombre de la tabla afectada
     * @param string $id_reg, identificador del registro afectado
     * @param string $oper, tipo de operaci�n realizada: Ins = Inserci�n, Edt = Modificaci�n, Dlt = Eliminaci�n o Baja 
     */ 
    public function registroLog($usr, $tabla, $id_reg, $oper)
    {  
        try{ 	
            $qSql = $this->_objBd->prepare("CALL xproLog(:usr, :tabla, :id_reg, :oper, :ip);");
            $qSql->execute( array(':usr' => $usr, ':tabla' => $tabla, ':id_reg' => $id_reg, ':oper' => $oper, ':ip' => $this->_GetIP()) );
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
        }
    }
    /**
     * Funci�n que obtiene la direcci�n IP del cliente
     */ 
    private function _GetIP()
    {
        return $_SERVER['REMOTE_ADDR']; 
   	}  
        
    /**
     * Funci�n que genera la descripci�n corta de una fecha
     * @param string $fecha, en formato: dd-mm-yyyy
     * @return string, la descripci�n corta de la fecha generada
     */  
    public function fechaCorta($fecha){
        $f = split('-', $fecha);
        $sfecha = $f[0] . '-' . $this->nombreMesCorto($f[1]) . '-' . $f[2];
        return $sfecha;
    }
    /**
     * Funci�n que genera la descripci�n completa de una fecha
     * @param string $fecha, en formato: dd-mm-yyyy
     * @return string, la descripci�n de la fecha generada
     */   
    public function getFechaLarga($fecha){
        //Obtiene el numero correspondiente al dia de 0 a 6 comenzando por Domingo... 
        $f_aux = date('w-m-Y', strtotime($fecha));
        $d = split('-', $f_aux);
        //Obtiene el dia, mes y a�o por separado...
        $f = split('-', $fecha);
        $sfecha = $this->nombreDiaLargo($d[0]) . ', ' . $f[0] . ' de ' . $this->nombreMesLargo($f[1]) . ' de ' . $f[2];        
        return $sfecha;
    }
    /**
     * Funci�n que obtiene el nombre corto del d�a espeficicado
     * @param int $dia, el n�mero del d�a del cual desea saber su nombre corto (1 - 31)
     * @return string, el nombre corto del d�a obtenido
     */ 
    public function nombreDiaCorto($dia){
        if ($dia == 0) 		
            $sdia = 'Dom';
        else if ($dia == 1)	
            $sdia = 'Lun';
        else if ($dia == 2)	
            $sdia = 'Mar';
        else if ($dia == 3)	
            $sdia = 'Mie';
        else if ($dia == 4)	
            $sdia = 'Jue';
        else if ($dia == 5)	
            $sdia = 'Vie';
        else if ($dia == 6)	
            $sdia = 'Sab';
        else 
            $sdia = $dia;
        
        return $sdia;
    }
    /**
     * Funci�n que obtiene el nombre largo del d�a espeficicado
     * @param int $dia, el n�mero del d�a del cual desea saber su nombre largo (1 - 31)
     * @return string, el nombre largo del d�a obtenido
     */ 
    public function nombreDiaLargo($dia){
        if ($dia == 0) 		
            $sdia = 'Domingo';
        else if ($dia == 1)	
            $sdia = 'Lunes';
        else if ($dia == 2)	
            $sdia = 'Martes';
        else if ($dia == 3)	
            $sdia = 'Mi�rcoles';
        else if ($dia == 4)	
            $sdia = 'Jueves';
        else if ($dia == 5)	
            $sdia = 'Viernes';
        else if ($dia == 6)	
            $sdia = 'S�bado';
        else 
            $sdia = $dia;
        
        return $sdia;
    }
    /**
     * Funci�n que obtiene el nombre corto del mes espeficicado
     * @param int $mes, el n�mero del mes del cual desea saber su nombre corto (1 - 12)
     * @return string, el nombre corto del mes obtenido
     */ 
    public function nombreMesCorto($mes)
    {
        if ($mes == 1)          
            $smes = 'Ene';
        else if ($mes == 2)	    
            $smes = 'Feb';
        else if ($mes == 3)	    
            $smes = 'Mar';
        else if ($mes == 4)	    
            $smes = 'Abr';
        else if ($mes == 5)	    
            $smes = 'May';
        else if ($mes == 6)	    
            $smes = 'Jun';
        else if ($mes == 7)	    
            $smes = 'Jul';		
        else if ($mes == 8)	    
            $smes = 'Ago';
        else if ($mes == 9)	    
            $smes = 'Sep';
        else if ($mes == 10)	
            $smes = 'Oct';
        else if ($mes == 11)	
            $smes = 'Nov';
        else if ($mes == 12)	
            $smes = 'Dic';
        
        return $smes;
    }
    /**
     * Funci�n que obtiene el nombre largo del mes espeficicado
     * @param int $mes, el n�mero del mes del cual desea saber su nombre largo (1 - 12)
     * @return string, el nombre largo del mes obtenido
     */ 
    public function nombreMesLargo($mes)
    {
        if ($mes == 1 )        
            $smes = 'Enero';
        else if ($mes == 2)   
            $smes = 'Febrero';
        else if ($mes == 3)	
            $smes = 'Marzo';
        else if ($mes == 4)	
            $smes = 'Abril';
        else if ($mes == 5)	
            $smes = 'Mayo';
        else if ($mes == 6)	
            $smes = 'Junio';
        else if ($mes == 7)	
            $smes = 'Julio';		
        else if ($mes == 8)	
            $smes = 'Agosto';
        else if ($mes == 9)   
            $smes = 'Septiembre';
        else if ($mes == 10)	
            $smes = 'Octubre';
        else if ($mes == 11)	
            $smes = 'Noviembre';
        else if ($mes == 12)	
            $smes = 'Diciembre';
        
        return $smes;
    }
    /**
     * Funci�n para convertir una fecha a un formato especificado
     * @param string $fecha, la fecha a convertir, ejemplo: 21-Dic-2010, 21/12/2010
     * @param string $formato, el formato al cual desea convertir la fecha especificada
     * @return string de la fecha convertida
     */ 
    public function convertirFecha($fecha, $formato='dd-mm-yyyy')
    {
        $xf = array();
        $xf = (strpos($fecha, '-') === true) ? explode('-', $fecha) : explode('/', $fecha);         
        if ($xf[1] == 'Ene')      
            $xmes = '01';
        else if ($xf[1] == 'Feb') 
            $xmes = '02';
        else if ($xf[1] == 'Mar') 
            $xmes = '03';
        else if ($xf[1] == 'Abr') 
            $xmes = '04';
        else if ($xf[1] == 'May') 
            $xmes = '05';
        else if ($xf[1] == 'Jun') 
            $xmes = '06';
        else if ($xf[1] == 'Jul') 
            $xmes = '07';
        else if ($xf[1] == 'Ago') 
            $xmes = '08';
        else if ($xf[1] == 'Sep') 
            $xmes = '09';
        else if ($xf[1] == 'Oct') 
            $xmes = '10';
        else if ($xf[1] == 'Nov') 
            $xmes = '11';
        else if ($xf[1] == 'Dic') 
            $xmes = '12';
        else
            $xmes = $xf[1];         

        $nva_fecha = $xf[2] . '-' . $xmes . '-' . $xf[0];
        if ($formato == 'dd-mm-yyyy')      
            $nva_fecha = date('d-m-Y', strtotime($nva_fecha));
        else if ($formato == 'mm-dd-yyyy') 
            $nva_fecha = date('m-d-Y', strtotime($nva_fecha));
        else if ($formato == 'yyyy-mm-dd') 
            $nva_fecha = date('Y-m-d', strtotime($nva_fecha));
        else if ($formato == 'yyyy-dd-mm') 
            $nva_fecha = date('Y-d-m', strtotime($nva_fecha));      
        
        return $nva_fecha;
    }
    
    /**
     * Funciones de encriptaci�n
     */
    public function encrypt($string, $key='')
    {
        return $this->_encryptSys($string, $this->pswd_main);
    }
    public function decrypt($string, $key='')
    {
        return $this->_decryptSys($string, $this->pswd_main);
    }
    private function _encryptSys($string, $key)
    {
        $xmax = array(65, 69, 73, 79, 85, 97, 101, 105, 111, 117);
     	$_cad = '';
    	for ($i=0; $i<StrLen($string); $i++) {
    	 	unset($_chr);
    		$_chr = ord(SubStr($string, $i, 1));
    		for ($ix=0; $ix<Count($xmax); $ix++)
    			if ($xmax[$ix] == $_chr)
    				break;
    		($ix < (Count($xmax) - 1)) ? ($_chr .= '1') : ($_chr .= '0');
    		$_cad .= str_pad($_chr, 4, 'x', STR_PAD_RIGHT);
    	}
    			
    	return $_cad;
    }
    private function _decryptSys($string, $key)
    {
        $_cad = '';
		for ($i=0; $i<StrLen($string); $i+=4) {
			$_xcad = SubStr($string, $i, 4);
			$_chr = '';
			for ($ix=0; $ix<StrLen($_xcad); $ix++)
				if (ord(SubStr($_xcad, $ix, 1)) != 120) 
                    $_chr .= SubStr($_xcad, $ix, 1); else break;
			$_cad .= chr(SubStr($_chr, 0, StrLen($_chr)-1));			
		}
		
		return $_cad;
    }
}
?>