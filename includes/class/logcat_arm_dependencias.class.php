<?php
/**
 *
 */
class LogcatArmDependencias
{
    public $id_dependencia; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $dependencia; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $id_tipo_dependencia; /** @Tipo: tinyint(4), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de dependencias dentro de un combobox.
     * @param int $id, id de dependencia seleccionado por deafult     
     * @return array html(options)
     */
    public function shwDependencias( $id=0, $id_tipodep ){
        if( $id_tipodep != 0 )
            $aryDatos = $this->selectAll('td.id_tipo_dependencia=' . $id_tipodep, 'td.id_tipo_dependencia Asc');
        else
            $aryDatos = $this->selectAll('a.xstat=1', 'a.dependencia Asc');
            
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_dependencia"] )
                $html .= '<option value="'.$datos["id_dependencia"].'" selected>'.$datos["dependencia"].'</option>';
            else
                $html .= '<option value="'.$datos["id_dependencia"].'" >'.$datos["dependencia"].'</option>';
        }
        return $html;
    }
    
    /**
     * Funci�n para obtener el id del tipo de dependencia atravez de la tabla de marcas
     * @param  $int id_marca, se recibe el parametro id_tipodependencia para hacer el filtro
     * @return el id de la clase
     */
    public function getIdTipoDependencia( $id_dep ){
	
		$sql = "SELECT d.id_tipo_dependencia
                FROM logcat_arm_tipo_dependencias as td
				INNER JOIN logcat_arm_dependencias as d On td.id_tipo_dependencia=d.id_tipo_dependencia
				WHERE td.id_dependencia=:id_dependencia
				AND td.xstat=:xstat";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_dependencia' => $id_dep, 'xstat' => '1'));
            $data = $qry->fetch(PDO::FETCH_ASSOC);            
            return $data['id_tipo_dependencia'];

        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return 0;
        }	
		
	}

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_dependencia)
    {
        $sql = "SELECT id_dependencia, dependencia, id_tipo_dependencia, xstat
                FROM logcat_arm_dependencias
                WHERE id_dependencia=:id_dependencia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_dependencia' => $id_dependencia));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_dependencia = $data['id_dependencia'];
            $this->dependencia = $data['dependencia'];
            $this->id_tipo_dependencia = $data['id_tipo_dependencia'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }      

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_dependencia, a.dependencia, a.id_tipo_dependencia, a.xstat
                FROM logcat_arm_dependencias a 
                    LEFT JOIN logcat_arm_tipodependencia as td On td.id_tipo_dependencia=a.id_tipo_dependencia ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_dependencia' => $data['id_dependencia'],
                               'dependencia' => $data['dependencia'],
                               'id_tipo_dependencia' => $data['id_tipo_dependencia'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_arm_dependencias(id_dependencia, dependencia, id_tipo_dependencia, xstat)
                VALUES(:id_dependencia, :dependencia, :id_tipo_dependencia, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_dependencia" => $this->id_dependencia, ":dependencia" => $this->dependencia, ":id_tipo_dependencia" => $this->id_tipo_dependencia, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_arm_dependencias
                   SET dependencia=:dependencia, id_tipo_dependencia=:id_tipo_dependencia, xstat=:xstat
                WHERE id_dependencia=:id_dependencia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_dependencia" => $this->id_dependencia, ":dependencia" => $this->dependencia, ":id_tipo_dependencia" => $this->id_tipo_dependencia, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>