<?php
/**
 *
 */
class OpecatMidGeneralidadesRoles
{
    public $id_generalidad_rol; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $generalidad_rol; /** @Tipo: varchar(100), @Acepta Nulos: NO, @Llave: UNI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once '.class.php';
        $this-> = new ();
    }

    /**
     * Funci�n para mostrar la lista de .... dentro de un combobox.
     * @param int $id, id del .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Generalidades_Roles($id_generalidad_rol=0){
        $aryDatos = $this->selectAll('','id_generalidad_rol Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id_generalidad_rol == $datos["id_generalidad_rol"] )
                $html .= '<option value="'.$datos["id_generalidad_rol"].'" selected>'.$datos["generalidad_rol"].'</option>';
            else
                $html .= '<option value="'.$datos["id_generalidad_rol"].'" >'.$datos["generalidad_rol"].'</option>';
        }
        return $html;
    }


    /**
     * Funci�n para obtener un registro espec�fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_generalidad_rol)
    {
        $sql = "SELECT id_generalidad_rol, generalidad_rol
                FROM opecat_mid_generalidades_roles
                WHERE id_generalidad_rol=:id_generalidad_rol;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_generalidad_rol' => $id_generalidad_rol));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_generalidad_rol = $data['id_generalidad_rol'];
            $this->generalidad_rol = $data['generalidad_rol'];

            $this->->select($this->generalidad_rol);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_generalidad_rol, a.generalidad_rol,
                  b.id_generalidad_rol, b.generalidad_rol
                FROM opecat_mid_generalidades_roles a
                 LEFT JOIN  b ON a.generalidad_rol=b.";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_generalidad_rol' => $data['id_generalidad_rol'],
                               'generalidad_rol' => $data['generalidad_rol'],
                               '_id_generalidad_rol' => $data['id_generalidad_rol'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_generalidades_roles(id_generalidad_rol, generalidad_rol)
                VALUES(:id_generalidad_rol, :generalidad_rol);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_generalidad_rol" => $this->id_generalidad_rol, ":generalidad_rol" => $this->generalidad_rol));
            if ($qry)
                return $qry->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_generalidades_roles
                   SET generalidad_rol=:generalidad_rol
                WHERE id_generalidad_rol=:id_generalidad_rol;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_generalidad_rol" => $this->id_generalidad_rol, ":generalidad_rol" => $this->generalidad_rol));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>