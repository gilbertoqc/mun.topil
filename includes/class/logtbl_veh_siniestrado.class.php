<?php
/**
 *
 */
class LogtblVehSiniestrado
{
    public $id_siniestrado; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_vehiculo; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $num_siniestro; /** @Tipo: varchar(15), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $fecha_siniestro; /** @Tipo: date, @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogtblVehiculos; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logtbl_vehiculos.class.php';
        $this->LogtblVehiculos = new LogtblVehiculos();
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_siniestrado)
    {
        $sql = "SELECT id_siniestrado, id_vehiculo, num_siniestro, fecha_siniestro
                FROM logtbl_veh_siniestrado
                WHERE id_siniestrado=:id_siniestrado;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_siniestrado' => $id_siniestrado));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_siniestrado = $data['id_siniestrado'];
            $this->id_vehiculo = $data['id_vehiculo'];
            $this->num_siniestro = $data['num_siniestro'];
            $this->fecha_siniestro = $data['fecha_siniestro'];

            $this->LogtblVehiculos->select($this->id_vehiculo);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_siniestrado, a.id_vehiculo, a.num_siniestro, a.fecha_siniestro,
                  b.id_vehiculo, b.num_serie, b.num_economico, b.num_motor, b.poliza, b.inciso, b.placas, b.num_placas, b.tarjeta_circulacion, b.reg_fed_veh, b.num_puertas, b.num_cilindros, b.foto_frente, b.foto_lat_der, b.foto_lat_izq, b.foto_posterior, b.foto_interior, b.id_estado_fisico, b.id_situacion, b.id_modelo, b.id_transmision, b.id_color, b.id_tipo, b.id_estado
                FROM logtbl_veh_siniestrado a 
                 LEFT JOIN logtbl_vehiculos b ON a.id_vehiculo=b.id_vehiculo";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_siniestrado' => $data['id_siniestrado'],
                               'id_vehiculo' => $data['id_vehiculo'],
                               'num_siniestro' => $data['num_siniestro'],
                               'fecha_siniestro' => $data['fecha_siniestro'],
                               'logtbl_vehiculos_num_serie' => $data['num_serie'],
                               'logtbl_vehiculos_num_economico' => $data['num_economico'],
                               'logtbl_vehiculos_num_motor' => $data['num_motor'],
                               'logtbl_vehiculos_poliza' => $data['poliza'],
                               'logtbl_vehiculos_inciso' => $data['inciso'],
                               'logtbl_vehiculos_placas' => $data['placas'],
                               'logtbl_vehiculos_num_placas' => $data['num_placas'],
                               'logtbl_vehiculos_tarjeta_circulacion' => $data['tarjeta_circulacion'],
                               'logtbl_vehiculos_reg_fed_veh' => $data['reg_fed_veh'],
                               'logtbl_vehiculos_num_puertas' => $data['num_puertas'],
                               'logtbl_vehiculos_num_cilindros' => $data['num_cilindros'],
                               'logtbl_vehiculos_foto_frente' => $data['foto_frente'],
                               'logtbl_vehiculos_foto_lat_der' => $data['foto_lat_der'],
                               'logtbl_vehiculos_foto_lat_izq' => $data['foto_lat_izq'],
                               'logtbl_vehiculos_foto_posterior' => $data['foto_posterior'],
                               'logtbl_vehiculos_foto_interior' => $data['foto_interior'],
                               'logtbl_vehiculos_id_estado_fisico' => $data['id_estado_fisico'],
                               'logtbl_vehiculos_id_situacion' => $data['id_situacion'],
                               'logtbl_vehiculos_id_modelo' => $data['id_modelo'],
                               'logtbl_vehiculos_id_transmision' => $data['id_transmision'],
                               'logtbl_vehiculos_id_color' => $data['id_color'],
                               'logtbl_vehiculos_id_tipo' => $data['id_tipo'],
                               'logtbl_vehiculos_id_estado' => $data['id_estado'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
       /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlValues, cadena que contiene el parametro de busqueda.
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos     
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
     /*
create view viewvehlstsiniestrado as
SELECT v.id_vehiculo,v.num_serie,v.num_economico,v.num_motor,v.poliza,v.inciso,v.placas,
				v.num_placas,tarjeta_circulacion,reg_fed_veh,num_puertas,v.num_cilindros,v.foto_frente,
				v.foto_lat_der,v.foto_lat_izq,v.foto_posterior,v.foto_interior,est.estado_fisico,
				sit.situacion,mdl.modelo,tra.transmision,col.color,cla.clasificacion,mar.marca,tip.tipo,es.estado,
        sin.num_siniestro, sin.fecha_siniestro
FROM logtbl_vehiculos v
        LEFT JOIN logcat_veh_estado_fisico as est ON est.id_estado_fisico=v.id_estado_fisico
        LEFT JOIN logcat_veh_situacion as sit ON sit.id_situacion=v.id_situacion
        LEFT JOIN logcat_veh_modelo as mdl ON mdl.id_modelo=v.id_modelo
        LEFT JOIN logcat_veh_transmision as tra ON tra.id_transmision=v.id_transmision
        LEFT JOIN logcat_veh_color as col ON col.id_color=v.id_color
        LEFT JOIN logcat_veh_tipo as tip ON tip.id_tipo=v.id_tipo
        LEFT JOIN logcat_veh_estado as es ON es.id_estado=v.id_estado
        LEFT JOIN logcat_veh_marca as mar ON mar.id_marca=tip.id_marca
        LEFT JOIN logcat_veh_clasificacion as cla ON cla.id_clasificacion=tip.id_clasificacion
        LEFT JOIN logtbl_veh_siniestrado as sin ON sin.id_vehiculo = v.id_vehiculo
       WHERE v.id_estado = 3
     */
    public function selectLstSin($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "select num_serie, num_economico, marca, tipo, num_siniestro, fecha_siniestro from viewvehlstsiniestrado ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";        
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);                
            } else {
                $qry->execute();                
            }
            $datos = array();            
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(                               
                               'id_vehiculo' => $data['id_vehiculo'],
                               'num_serie' => $data['num_serie'],
                               'num_economico' => $data['num_economico'],
                               'marca' => $data['marca'],
                               'tipo' => $data['tipo'],
                               'num_siniestro' => $data['num_siniestro'],
                               'fecha_siniestro' => $data['fecha_siniestro'],                              
                               );
            }
            //echo count( $datos );
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_veh_siniestrado(id_siniestrado, id_vehiculo, num_siniestro, fecha_siniestro)
                VALUES(:id_siniestrado, :id_vehiculo, :num_siniestro, :fecha_siniestro);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_siniestrado" => $this->id_siniestrado, ":id_vehiculo" => $this->id_vehiculo, ":num_siniestro" => $this->num_siniestro, ":fecha_siniestro" => $this->fecha_siniestro));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_veh_siniestrado
                   SET id_vehiculo=:id_vehiculo, num_siniestro=:num_siniestro, fecha_siniestro=:fecha_siniestro
                WHERE id_siniestrado=:id_siniestrado;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_siniestrado" => $this->id_siniestrado, ":id_vehiculo" => $this->id_vehiculo, ":num_siniestro" => $this->num_siniestro, ":fecha_siniestro" => $this->fecha_siniestro));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar datos de la tabla de logtbl_vehiculos siniestrados
     * @return boolean true si el proceso es satisfactorio
     */
    public function update_sin()
    {
        $sql = "UPDATE logtbl_vehiculos
                   SET id_estado=:id_estado 
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":id_estado" => 3 ));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>