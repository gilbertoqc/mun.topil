<?php
/**
 *
 */
class Xcatperfiles
{
    public $id_perfil; /** @Tipo: tinyint(3) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $perfil; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $insertar; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $modificar; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $baja; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $usuarios; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $reportes; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de perfiles de usuarios dentro de un combobox.
     * @param int $id, id del perfil seleccionado por deafult 
     * @return array html(options)
     */
    public function shwPerfiles($id=0){
        $aryDatos = $this->selectAll('', 'perfil Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_perfil"] )
                $html .= '<option value="'.$datos["id_perfil"].'" selected>'.$datos["perfil"].'</option>';
            else
                $html .= '<option value="'.$datos["id_perfil"].'" >'.$datos["perfil"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_perfil)
    {
        $sql = "SELECT id_perfil, perfil, insertar, modificar, baja, usuarios, reportes
                FROM xcatperfiles
                WHERE id_perfil=:id_perfil;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_perfil' => $id_perfil));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_perfil = $data['id_perfil'];
            $this->perfil = $data['perfil'];
            $this->insertar = $data['insertar'];
            $this->modificar = $data['modificar'];
            $this->baja = $data['baja'];
            $this->usuarios = $data['usuarios'];
            $this->reportes = $data['reportes'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_perfil, a.perfil, a.insertar, a.modificar, a.baja, a.usuarios, a.reportes
                FROM xcatperfiles a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_perfil' => $data['id_perfil'],
                               'perfil' => $data['perfil'],
                               'insertar' => $data['insertar'],
                               'modificar' => $data['modificar'],
                               'baja' => $data['baja'],
                               'usuarios' => $data['usuarios'],
                               'reportes' => $data['reportes'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO xcatperfiles(id_perfil, perfil, insertar, modificar, baja, usuarios, reportes)
                VALUES(:id_perfil, :perfil, :insertar, :modificar, :baja, :usuarios, :reportes);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_perfil" => $this->id_perfil, ":perfil" => $this->perfil, ":insertar" => $this->insertar, ":modificar" => $this->modificar, ":baja" => $this->baja, ":usuarios" => $this->usuarios, ":reportes" => $this->reportes));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE xcatperfiles
                   SET perfil=:perfil, insertar=:insertar, modificar=:modificar, baja=:baja, usuarios=:usuarios, reportes=:reportes
                WHERE id_perfil=:id_perfil;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_perfil" => $this->id_perfil, ":perfil" => $this->perfil, ":insertar" => $this->insertar, ":modificar" => $this->modificar, ":baja" => $this->baja, ":usuarios" => $this->usuarios, ":reportes" => $this->reportes));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>