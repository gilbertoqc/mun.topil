<?php
/**
 *
 */
class LogcatVehColor
{
    public $id_color; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $color; /** @Tipo: varchar(75), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $color_hex; /** @Tipo: varchar(7), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

	/**
     * Funci�n para mostrar la lista de colores dentro de un combobox.
     * @param int $id, id del color seleccionado por deafult     
     * @return array html(options)
     */
    public function shwColor($id=0){
        $aryDatos = $this->selectAll('xstat=1', 'color Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_color"] )
                $html .= '<option value="'.$datos["id_color"].'" selected>'.$datos["color"].'</option>';
            else
                $html .= '<option value="'.$datos["id_color"].'" >'.$datos["color"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_color)
    {
        $sql = "SELECT id_color, color, color_hex, xstat
                FROM logcat_veh_color
                WHERE id_color=:id_color;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_color' => $id_color));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_color = $data['id_color'];
            $this->color = $data['color'];
            $this->color_hex = $data['color_hex'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_color, a.color, a.color_hex, a.xstat
                FROM logcat_veh_color a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_color' => $data['id_color'],
                               'color' => $data['color'],
                               'color_hex' => $data['color_hex'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_color(id_color, color, color_hex, xstat)
                VALUES(:id_color, :color, :color_hex, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_color" => $this->id_color, ":color" => $this->color, ":color_hex" => $this->color_hex, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_color
                   SET color=:color, color_hex=:color_hex, xstat=:xstat
                WHERE id_color=:id_color;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_color" => $this->id_color, ":color" => $this->color, ":color_hex" => $this->color_hex, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>