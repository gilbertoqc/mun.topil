<?php
/**
 *
 */
class LogtblVehiculos
{
    public $id_vehiculo; /** @Tipo: int(5), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $num_serie; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $num_economico; /** @Tipo: varchar(5), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $num_motor; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $poliza; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $inciso; /** @Tipo: varchar(25), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $placas; /** @Tipo: varchar(8), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $num_placas; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $tarjeta_circulacion; /** @Tipo: varchar(12), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $reg_fed_veh; /** @Tipo: varchar(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $num_puertas; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $num_cilindros; /** @Tipo: tinyint(1), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $foto_frente; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $foto_lat_der; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $foto_lat_izq; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $foto_posterior; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $foto_interior; /** @Tipo: varchar(50), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $id_estado_fisico; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_situacion; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_modelo; /** @Tipo: tinyint(2), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_transmision; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_color; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_tipo; /** @Tipo: smallint(5), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_estado; /** @Tipo: tinyint(2), @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */    

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $LogcatVehColor; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehEstadoFisico; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehModelo; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehSituacion; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehTipo; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehEstado; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $LogcatVehTransmision; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'logcat_veh_color.class.php';
        require_once 'logcat_veh_estado_fisico.class.php';
        require_once 'logcat_veh_modelo.class.php';
        require_once 'logcat_veh_situacion.class.php';
        require_once 'logcat_veh_tipo.class.php';
        require_once 'logcat_veh_transmision.class.php';
        $this->LogcatVehColor = new LogcatVehColor();
        $this->LogcatVehEstadoFisico = new LogcatVehEstadoFisico();
        $this->LogcatVehModelo = new LogcatVehModelo();
        $this->LogcatVehSituacion = new LogcatVehSituacion();
        $this->LogcatVehTipo = new LogcatVehTipo();
        $this->LogcatVehTransmision = new LogcatVehTransmision();
    }

         /**
        * Funci�n para obtener un el id maximo de la tabla
        * @param  campos que conforman la clave primaria de la tabla
        * @return boolean true, si la consulta se realiz� con �xito
        */
        public function getIdUltimoVehiculo(){        
            $sql = "SELECT MAX(id_vehiculo) as id
                      FROM logtbl_vehiculos;";
            try{
                $qry = $this->_conexBD->prepare($sql);
                $qry->execute();
                $data = $qry->fetch(PDO::FETCH_ASSOC);
                
                return $data["id"];
                
            }catch (PDOException $e) {
                $this->msjError = $e->getMessage();
                return false;
            }
        }

    /**
     * Funci�n que controla la obtenci�n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);        
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }
        
        return $datos;
    }
    
    /**
     * Funci�n para obtener los registros m�nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {    
        $sql = "SELECT v.id_vehiculo,v.num_serie,v.num_economico,v.num_motor,v.poliza,v.inciso,v.placas,									
				v.num_placas,tarjeta_circulacion,reg_fed_veh,num_puertas,v.num_cilindros,v.foto_frente,
				v.foto_lat_der,v.foto_lat_izq,v.foto_posterior,v.foto_interior,est.estado_fisico,
				sit.situacion,mdl.modelo,tra.transmision,col.color,cla.clasificacion,mar.marca,tip.tipo,es.estado
                FROM logtbl_vehiculos v
                    LEFT JOIN logcat_veh_estado_fisico as est ON est.id_estado_fisico=v.id_estado_fisico
                    LEFT JOIN logcat_veh_situacion as sit ON sit.id_situacion=v.id_situacion
                    LEFT JOIN logcat_veh_modelo as mdl ON mdl.id_modelo=v.id_modelo
                    LEFT JOIN logcat_veh_transmision as tra ON tra.id_transmision=v.id_transmision
					LEFT JOIN logcat_veh_color as col ON col.id_color=v.id_color
					LEFT JOIN logcat_veh_tipo as tip ON tip.id_tipo=v.id_tipo
                    LEFT JOIN logcat_veh_estado as es ON es.id_estado=v.id_estado                    
          			LEFT JOIN logcat_veh_marca as mar ON mar.id_marca=tip.id_marca
					LEFT JOIN logcat_veh_clasificacion as cla ON cla.id_clasificacion=tip.id_clasificacion
					WHERE v.id_estado <> 2 AND v.id_estado <> 3 ";          
        if (!empty($sqlWhere)) {
            $sql .= "\nAND $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo' => $data['id_vehiculo'],
                               'num_serie' => $data['num_serie'],
                               'num_economico' => $data['num_economico'],
                               'num_motor' => $data['num_motor'],
                               'poliza' => $data['poliza'],
                               'inciso' => $data['inciso'],
                               'placas' => $data['placas'],
							   'tarjeta_circulacion' => $data['tarjeta_circulacion'],
							   'reg_fed_veh' => $data['reg_fed_veh'],
							   'num_puertas' => $data['num_puertas'],
                               'num_cilindros' => $data['num_cilindros'],
                               'foto_frente' => $data['foto_frente'],                               
                               'foto_lat_der' => $data['foto_lat_der'],
                               'foto_lat_izq' => $data['foto_lat_izq'],                               
                               'foto_posterior' => $data['foto_posterior'],
                               'foto_interior' => $data['foto_interior'],
                               'estado_fisico' => $data['estado_fisico'],
                               'situacion' => $data['situacion'],
                               'modelo' => $data['modelo'],
                               'transmision' => $data['transmision'],                               
                               'color' => $data['color'],                               
                               'tipo' => $data['tipo'],
                               'estado' => $data['estado'],
                               'marca' => $data['marca'],
                               'clasificacion' => $data['clasificacion']
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici�n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {    
        $sql = "SELECT COUNT(*) 
                FROM logtbl_vehiculos v
                    LEFT JOIN logcat_veh_estado_fisico as est ON est.id_estado_fisico=v.id_estado_fisico
                    LEFT JOIN logcat_veh_situacion as sit ON sit.id_situacion=v.id_situacion
                    LEFT JOIN logcat_veh_modelo as mdl ON mdl.id_modelo=v.id_modelo
                    LEFT JOIN logcat_veh_transmision as tra ON tra.id_transmision=v.id_transmision
					LEFT JOIN logcat_veh_color as col ON col.id_color=v.id_color
					LEFT JOIN logcat_veh_tipo as tip ON tip.id_tipo=v.id_tipo
                    LEFT JOIN logcat_veh_estado as es ON es.id_estado=v.id_estado 
          			LEFT JOIN logcat_veh_marca as mar ON mar.id_marca=tip.id_marca
					LEFT JOIN logcat_veh_clasificacion as cla ON cla.id_clasificacion=tip.id_clasificacion
					WHERE v.id_estado <> 2 AND v.id_estado <> 3 ";
        if (!empty($sqlWhere)) {
            $sql .= "\nAND $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }            
            $total_reg = $qry->fetchColumn();
            
            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_vehiculo)
    {
        $sql = "SELECT id_vehiculo, num_serie, num_economico, num_motor, poliza, inciso, placas, num_placas, tarjeta_circulacion, reg_fed_veh, num_puertas, num_cilindros, foto_frente, foto_lat_der, foto_lat_izq, foto_posterior, foto_interior, id_estado_fisico, id_situacion, id_modelo, id_transmision, id_color, id_tipo, id_estado
                FROM logtbl_vehiculos
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_vehiculo' => $id_vehiculo));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_vehiculo = $data['id_vehiculo'];
            $this->num_serie = $data['num_serie'];
            $this->num_economico = $data['num_economico'];
            $this->num_motor = $data['num_motor'];
            $this->poliza = $data['poliza'];
            $this->inciso = $data['inciso'];
            $this->placas = $data['placas'];
            $this->num_placas = $data['num_placas'];
            $this->tarjeta_circulacion = $data['tarjeta_circulacion'];
            $this->reg_fed_veh = $data['reg_fed_veh'];
            $this->num_puertas = $data['num_puertas'];
            $this->num_cilindros = $data['num_cilindros'];
            $this->foto_frente = $data['foto_frente'];
            $this->foto_lat_der = $data['foto_lat_der'];
            $this->foto_lat_izq = $data['foto_lat_izq'];
            $this->foto_posterior = $data['foto_posterior'];
            $this->foto_interior = $data['foto_interior'];
            $this->id_estado_fisico = $data['id_estado_fisico'];
            $this->id_situacion = $data['id_situacion'];
            $this->id_modelo = $data['id_modelo'];
            $this->id_transmision = $data['id_transmision'];
            $this->id_color = $data['id_color'];
            $this->id_tipo = $data['id_tipo'];
            $this->id_estado = $data['id_estado'];

            $this->LogcatVehColor->select($this->id_color);
            $this->LogcatVehEstadoFisico->select($this->id_estado_fisico);
            $this->LogcatVehModelo->select($this->id_modelo);
            $this->LogcatVehSituacion->select($this->id_situacion);
            $this->LogcatVehTipo->select($this->id_tipo);
            $this->LogcatVehTransmision->select($this->id_transmision);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_vehiculo, a.num_serie, a.num_economico, a.num_motor, a.poliza, a.inciso, a.placas, a.num_placas, a.tarjeta_circulacion, a.reg_fed_veh, a.num_puertas, a.num_cilindros, a.foto_frente, a.foto_lat_der, a.foto_lat_izq, a.foto_posterior, a.foto_interior, a.id_estado_fisico, a.id_situacion, a.id_modelo, a.id_transmision, a.id_color, a.id_tipo,
                  b.id_color, b.color, b.color_hex, b.xstat,
                  c.id_estado_fisico, c.estado_fisico, c.id_estado_vehiculo_sn, c.xstat,
                  d.id_modelo, d.modelo, d.xstat,
                  e.id_situacion, e.situacion, e.xstat,
                  f.id_tipo, f.tipo, f.id_tipo_sn, f.xstat, f.id_marca, f.id_clasificacion,
                  es.id_estado, es.estado, es.xstat
                  g.id_transmision, g.transmision, g.xstat
                FROM logtbl_vehiculos a 
                 LEFT JOIN logcat_veh_color b ON a.id_color=b.id_color
                 LEFT JOIN logcat_veh_estado_fisico c ON a.id_estado_fisico=c.id_estado_fisico
                 LEFT JOIN logcat_veh_modelo d ON a.id_modelo=d.id_modelo
                 LEFT JOIN logcat_veh_situacion e ON a.id_situacion=e.id_situacion
                 LEFT JOIN logcat_veh_tipo f ON a.id_tipo=f.id_tipo
                 LEFT JOIN logcat_veh_estado as es ON es.id_estado=v.id_estado 
                 LEFT JOIN logcat_veh_transmision g ON a.id_transmision=g.id_transmision";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_vehiculo' => $data['id_vehiculo'],
                               'num_serie' => $data['num_serie'],
                               'num_economico' => $data['num_economico'],
                               'num_motor' => $data['num_motor'],
                               'poliza' => $data['poliza'],
                               'inciso' => $data['inciso'],
                               'placas' => $data['placas'],
                               'num_placas' => $data['num_placas'],
                               'tarjeta_circulacion' => $data['tarjeta_circulacion'],
                               'reg_fed_veh' => $data['reg_fed_veh'],
                               'num_puertas' => $data['num_puertas'],
                               'num_cilindros' => $data['num_cilindros'],
                               'foto_frente' => $data['foto_frente'],
                               'foto_lat_der' => $data['foto_lat_der'],
                               'foto_lat_izq' => $data['foto_lat_izq'],
                               'foto_posterior' => $data['foto_posterior'],
                               'foto_interior' => $data['foto_interior'],
                               'id_estado_fisico' => $data['id_estado_fisico'],
                               'id_situacion' => $data['id_situacion'],
                               'id_modelo' => $data['id_modelo'],
                               'id_transmision' => $data['id_transmision'],
                               'id_color' => $data['id_color'],
                               'id_tipo' => $data['id_tipo'],
                               'id_estado' => $data['id_estado'],
                               'logcat_veh_color_color' => $data['color'],
                               'logcat_veh_color_color_hex' => $data['color_hex'],
                               'logcat_veh_color_xstat' => $data['xstat'],
                               'logcat_veh_estado_fisico_estado_fisico' => $data['estado_fisico'],
                               'logcat_veh_estado_fisico_id_estado_vehiculo_sn' => $data['id_estado_vehiculo_sn'],
                               'logcat_veh_estado_fisico_xstat' => $data['xstat'],
                               'logcat_veh_modelo_modelo' => $data['modelo'],
                               'logcat_veh_modelo_xstat' => $data['xstat'],
                               'logcat_veh_situacion_situacion' => $data['situacion'],
                               'logcat_veh_situacion_xstat' => $data['xstat'],
                               'logcat_veh_tipo_tipo' => $data['tipo'],
                               'logcat_veh_estado_estado' => $data['estado'],
                               'logcat_veh_tipo_id_tipo_sn' => $data['id_tipo_sn'],
                               'logcat_veh_tipo_xstat' => $data['xstat'],
                               'logcat_veh_tipo_id_marca' => $data['id_marca'],
                               'logcat_veh_tipo_id_clasificacion' => $data['id_clasificacion'],
                               'logcat_veh_transmision_transmision' => $data['transmision'],
                               'logcat_veh_transmision_xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logtbl_vehiculos(id_vehiculo, num_serie, num_economico, num_motor, poliza, inciso, placas, num_placas, tarjeta_circulacion, reg_fed_veh, num_puertas, num_cilindros, foto_frente, foto_lat_der, foto_lat_izq, foto_posterior, foto_interior, id_estado_fisico, id_situacion, id_modelo, id_transmision, id_color, id_tipo)
                VALUES(:id_vehiculo, :num_serie, :num_economico, :num_motor, :poliza, :inciso, :placas, :num_placas, :tarjeta_circulacion, :reg_fed_veh, :num_puertas, :num_cilindros, :foto_frente, :foto_lat_der, :foto_lat_izq, :foto_posterior, :foto_interior, :id_estado_fisico, :id_situacion, :id_modelo, :id_transmision, :id_color, :id_tipo);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, ":num_serie" => $this->num_serie, ":num_economico" => $this->num_economico, ":num_motor" => $this->num_motor, ":poliza" => $this->poliza, ":inciso" => $this->inciso, ":placas" => $this->placas, ":num_placas" => $this->num_placas, ":tarjeta_circulacion" => $this->tarjeta_circulacion, ":reg_fed_veh" => $this->reg_fed_veh, ":num_puertas" => $this->num_puertas, ":num_cilindros" => $this->num_cilindros, ":foto_frente" => $this->foto_frente, ":foto_lat_der" => $this->foto_lat_der, ":foto_lat_izq" => $this->foto_lat_izq, ":foto_posterior" => $this->foto_posterior, ":foto_interior" => $this->foto_interior, ":id_estado_fisico" => $this->id_estado_fisico, ":id_situacion" => $this->id_situacion, ":id_modelo" => $this->id_modelo, ":id_transmision" => $this->id_transmision, ":id_color" => $this->id_color, ":id_tipo" => $this->id_tipo));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logtbl_vehiculos
                   SET num_serie=:num_serie, num_economico=:num_economico, num_motor=:num_motor, poliza=:poliza, 
                   inciso=:inciso, placas=:placas, num_placas=:num_placas, tarjeta_circulacion=:tarjeta_circulacion, 
                   reg_fed_veh=:reg_fed_veh, num_puertas=:num_puertas, num_cilindros=:num_cilindros, 
                   foto_frente=:foto_frente, foto_lat_der=:foto_lat_der, foto_lat_izq=:foto_lat_izq, 
                   foto_posterior=:foto_posterior, foto_interior=:foto_interior, id_estado_fisico=:id_estado_fisico, 
                   id_situacion=:id_situacion, id_modelo=:id_modelo, id_transmision=:id_transmision, id_color=:id_color, 
                   id_tipo=:id_tipo
                WHERE id_vehiculo=:id_vehiculo;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_vehiculo" => $this->id_vehiculo, 
                                ":num_serie" => $this->num_serie, 
                                ":num_economico" => $this->num_economico, 
                                ":num_motor" => $this->num_motor, 
                                ":poliza" => $this->poliza, 
                                ":inciso" => $this->inciso, 
                                ":placas" => $this->placas, 
                                ":num_placas" => $this->num_placas, 
                                ":tarjeta_circulacion" => $this->tarjeta_circulacion, 
                                ":reg_fed_veh" => $this->reg_fed_veh, 
                                ":num_puertas" => $this->num_puertas, 
                                ":num_cilindros" => $this->num_cilindros, 
                                ":foto_frente" => $this->foto_frente, 
                                ":foto_lat_der" => $this->foto_lat_der, 
                                ":foto_lat_izq" => $this->foto_lat_izq, 
                                ":foto_posterior" => $this->foto_posterior, 
                                ":foto_interior" => $this->foto_interior, 
                                ":id_estado_fisico" => $this->id_estado_fisico, 
                                ":id_situacion" => $this->id_situacion, 
                                ":id_modelo" => $this->id_modelo, 
                                ":id_transmision" => $this->id_transmision, 
                                ":id_color" => $this->id_color, 
                                ":id_tipo" => $this->id_tipo));			
            if ($qry){
                return true;
            }else
                return false;
        } catch(PDOException $e) {        
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>