<?php
/**
 *
 */
class AdmcatMotivoSancion
{
    public $id_motivo_sancion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $motivo_sancion; /** @Tipo: varchar(100), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de los motivos de sanciones dentro de un combobox.
     * @param int $id, id del motivo seleccionado por deafult     
     * @return array html(options)
     */
    public function shwMotivoSancion($id=0){
        $aryDatos = $this->selectAll('', 'motivo_sancion Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_motivo_sancion"] )
                $html .= '<option value="'.$datos["id_motivo_sancion"].'" selected>'.$datos["motivo_sancion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_motivo_sancion"].'" >'.$datos["motivo_sancion"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_motivo_sancion)
    {
        $sql = "SELECT id_motivo_sancion, motivo_sancion, status
                FROM admcat_motivo_sancion
                WHERE id_motivo_sancion=:id_motivo_sancion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_motivo_sancion' => $id_motivo_sancion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_motivo_sancion = $data['id_motivo_sancion'];
            $this->motivo_sancion = $data['motivo_sancion'];
            $this->status = $data['status'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_motivo_sancion, a.motivo_sancion, a.status
                FROM admcat_motivo_sancion a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_motivo_sancion' => $data['id_motivo_sancion'],
                               'motivo_sancion' => $data['motivo_sancion'],
                               'status' => $data['status'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_motivo_sancion(id_motivo_sancion, motivo_sancion, status)
                VALUES(:id_motivo_sancion, :motivo_sancion, :status);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_motivo_sancion" => $this->id_motivo_sancion, ":motivo_sancion" => $this->motivo_sancion, ":status" => $this->status));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_motivo_sancion
                   SET motivo_sancion=:motivo_sancion, status=:status
                WHERE id_motivo_sancion=:id_motivo_sancion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_motivo_sancion" => $this->id_motivo_sancion, ":motivo_sancion" => $this->motivo_sancion, ":status" => $this->status));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>