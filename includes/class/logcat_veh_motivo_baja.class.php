<?php
/**
 *
 */
class LogcatVehMotivoBaja
{
    public $id_motivo_baja; /** @Tipo: int(11), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $motivo_baja; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $xstat; /** @Tipo: tinyint(1), @Acepta Nulos: NO, @Llave: --, @Default: 1 */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

	/**
     * Funci�n para mostrar la lista de motivos de baja dentro de un combobox.
     * @param int $id, id del motivo de baja seleccionado por deafult     
     * @return array html(options)
     */
    public function shwMotivoBaja($id=0){
        $aryDatos = $this->selectAll('xstat=1', 'motivo_baja Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_motivo_baja"] )
                $html .= '<option value="'.$datos["id_motivo_baja"].'" selected>'.$datos["motivo_baja"].'</option>';
            else
                $html .= '<option value="'.$datos["id_motivo_baja"].'" >'.$datos["motivo_baja"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_motivo_baja)
    {
        $sql = "SELECT id_motivo_baja, motivo_baja, xstat
                FROM logcat_veh_motivo_baja
                WHERE id_motivo_baja=:id_motivo_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_motivo_baja' => $id_motivo_baja));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_motivo_baja = $data['id_motivo_baja'];
            $this->motivo_baja = $data['motivo_baja'];
            $this->xstat = $data['xstat'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_motivo_baja, a.motivo_baja, a.xstat
                FROM logcat_veh_motivo_baja a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_motivo_baja' => $data['id_motivo_baja'],
                               'motivo_baja' => $data['motivo_baja'],
                               'xstat' => $data['xstat'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO logcat_veh_motivo_baja(id_motivo_baja, motivo_baja, xstat)
                VALUES(:id_motivo_baja, :motivo_baja, :xstat);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_motivo_baja" => $this->id_motivo_baja, ":motivo_baja" => $this->motivo_baja, ":xstat" => $this->xstat));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE logcat_veh_motivo_baja
                   SET motivo_baja=:motivo_baja, xstat=:xstat
                WHERE id_motivo_baja=:id_motivo_baja;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_motivo_baja" => $this->id_motivo_baja, ":motivo_baja" => $this->motivo_baja, ":xstat" => $this->xstat));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>