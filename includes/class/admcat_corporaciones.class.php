<?php
/**
 *
 */
class AdmcatCorporaciones
{
    public $id_corporacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $corporacion; /** @Tipo: char(20), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_corporacion)
    {
        $sql = "SELECT id_corporacion, corporacion
                FROM admcat_corporaciones
                WHERE id_corporacion=:id_corporacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_corporacion' => $id_corporacion));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_corporacion = $data['id_corporacion'];
            $this->corporacion = $data['corporacion'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_corporacion, a.corporacion
                FROM admcat_corporaciones a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_corporacion' => $data['id_corporacion'],
                               'corporacion' => $data['corporacion'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_corporaciones(id_corporacion, corporacion)
                VALUES(:id_corporacion, :corporacion);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_corporacion" => $this->id_corporacion, ":corporacion" => $this->corporacion));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_corporaciones
                   SET corporacion=:corporacion
                WHERE id_corporacion=:id_corporacion;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_corporacion" => $this->id_corporacion, ":corporacion" => $this->corporacion));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>