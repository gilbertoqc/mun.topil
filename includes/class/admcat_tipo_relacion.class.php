<?php
/**
 *
 */
class AdmcatTipoRelacion
{
    public $id_tipo_relacion; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $tipo_relacion; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $status; /** @Tipo: tinyint(1) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: 1 */
    public $id_tipo_referencia; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $AdmcatTipoReferencia; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'admcat_tipo_referencia.class.php';
        $this->AdmcatTipoReferencia = new AdmcatTipoReferencia();
    }
    
    /**
     * Funci�n para mostrar la lista de los tipo de relaciones dentro de un combobox.
     * @param int $id, id del tipo de funci�n seleccionado por deafult     
     * @return array html(options)
     */
    public function shwTipoRelaciones($id=0, $id_tipo_referencia=0){
        $aryDatos = $this->selectAll('a.id_tipo_referencia=' . $id_tipo_referencia, 'tipo_relacion Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_tipo_relacion"] )
                $html .= '<option value="'.$datos["id_tipo_relacion"].'" selected>'.$datos["tipo_relacion"].'</option>';
            else
                $html .= '<option value="'.$datos["id_tipo_relacion"].'" >'.$datos["tipo_relacion"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_tipo_relacion, $id_tipo_referencia)
    {
        $sql = "SELECT id_tipo_relacion, tipo_relacion, status, id_tipo_referencia
                FROM admcat_tipo_relacion
                WHERE id_tipo_relacion=:id_tipo_relacion AND id_tipo_referencia=:id_tipo_referencia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_tipo_relacion' => $id_tipo_relacion, ':id_tipo_referencia' => $id_tipo_referencia));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_tipo_relacion = $data['id_tipo_relacion'];
            $this->tipo_relacion = $data['tipo_relacion'];
            $this->status = $data['status'];
            $this->id_tipo_referencia = $data['id_tipo_referencia'];

            $this->AdmcatTipoReferencia->select($this->id_tipo_referencia);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_tipo_relacion, a.tipo_relacion, a.status, a.id_tipo_referencia,
                    b.tipo_referencia 
                FROM admcat_tipo_relacion a 
                    LEFT JOIN admcat_tipo_referencia b ON a.id_tipo_referencia=b.id_tipo_referencia";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_tipo_relacion' => $data['id_tipo_relacion'],
                               'tipo_relacion' => $data['tipo_relacion'],
                               'status' => $data['status'],
                               'id_tipo_referencia' => $data['id_tipo_referencia'],
                               'tipo_referencia' => $data['tipo_referencia']
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_tipo_relacion(id_tipo_relacion, tipo_relacion, status, id_tipo_referencia)
                VALUES(:id_tipo_relacion, :tipo_relacion, :status, :id_tipo_referencia);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_relacion" => $this->id_tipo_relacion, ":tipo_relacion" => $this->tipo_relacion, ":status" => $this->status, ":id_tipo_referencia" => $this->id_tipo_referencia));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_tipo_relacion
                   SET tipo_relacion=:tipo_relacion, status=:status
                WHERE id_tipo_relacion=:id_tipo_relacion AND id_tipo_referencia=:id_tipo_referencia;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_tipo_relacion" => $this->id_tipo_relacion, ":tipo_relacion" => $this->tipo_relacion, ":status" => $this->status, ":id_tipo_referencia" => $this->id_tipo_referencia));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>