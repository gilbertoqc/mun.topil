<?php
/**
 *
 */
class OpetblMidIncidentesPersonas
{
    public $id_persona; /** @Tipo: int(10) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_folio_incidente; /** @Tipo: int(10) unsigned zerofill, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_persona_rol; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: MUL, @Default: NULL */
    public $id_persona_causa; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $nombre; /** @Tipo: varchar(35), @Acepta Nulos: NO, @Llave: --, @Default: NULL */
    public $apellido_paterno; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $apellido_materno; /** @Tipo: varchar(30), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $edad; /** @Tipo: smallint(6), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $sexo; /** @Tipo: tinyint(4), @Acepta Nulos: YES, @Llave: --, @Default: NULL */
    public $domicilio; /** @Tipo: varchar(250), @Acepta Nulos: YES, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si ?ste ocurre
    private $_conexBD; // objeto de conexi?n a la base de datos
    public $OpetblMidIncidentes; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidPersonasRolesCausas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    //public $OpecatMidPersonasRolesCausas; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opetbl_mid_incidentes.class.php';
        require_once 'opecat_mid_personas_roles_causas.class.php';
        //require_once 'opecat_mid_personas_roles_causas.class.php';
        $this->OpetblMidIncidentes = new OpetblMidIncidentes();
        $this->OpecatMidPersonasRolesCausas = new OpecatMidPersonasRolesCausas();
        //$this->OpecatMidPersonasRolesCausas = new OpecatMidPersonasRolesCausas();
    }

    /**
    * Funci�n para obtener un registro espec�fico de la tabla
    * @param  campos que conforman la clave primaria de la tabla
    * @return boolean true, si la consulta se realiz� con �xito
    */
    public function getIdUltimoPersona(){        
        $sql = "SELECT MAX(id_persona) as id
                  FROM opetbl_mid_incidentes_personas;";
        try{
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            
            return $data["id"];
            
        }catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return x;
        }
    }

    /**
     * Funci?n para obtener un registro espec?fico de la tabla
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz? con ?xito
     */
    public function select($id_persona)
    {
        $sql = "SELECT id_persona, id_folio_incidente, id_persona_rol, id_persona_causa, nombre, apellido_paterno, apellido_materno, edad, sexo, domicilio
                FROM opetbl_mid_incidentes_personas
                WHERE id_persona=:id_persona;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_persona' => $id_persona));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_persona = $data['id_persona'];
            $this->id_folio_incidente = $data['id_folio_incidente'];
            $this->id_persona_rol = $data['id_persona_rol'];
            $this->id_persona_causa = $data['id_persona_causa'];
            $this->nombre = $data['nombre'];
            $this->apellido_paterno = $data['apellido_paterno'];
            $this->apellido_materno = $data['apellido_materno'];
            $this->edad = $data['edad'];
            $this->sexo = $data['sexo'];
            $this->domicilio = $data['domicilio'];

            $this->OpetblMidIncidentes->select($this->id_folio_incidente);
            $this->OpecatMidPersonasRolesCausas->select($this->id_persona_rol);
            //$this->OpecatMidPersonasRolesCausas->select($this->id_persona_causa);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlOrder="", $sqlLimit="")
    {
        $sql = "SELECT a.id_persona, a.id_folio_incidente, a.id_persona_rol, a.id_persona_causa, a.nombre, a.apellido_paterno, a.apellido_materno, a.edad, a.sexo, a.domicilio,
                  b.id_folio_incidente, b.prioridad, b.fecha_hora_captura, b.fecha_incidente, b.dia, b.mes, b.annio, b.hora_incidente, b.id_estado, b.id_region, b.id_cuartel, b.id_municipio, b.direccion, b.localidad, b.colonia, b.calle, b.latitud, b.longitud, b.id_asunto, b.id_fuente, b.hechos_html, b.hechos_text, b.ip_usuario, b.id_usuario,
                  c.id_persona_rol, c.id_persona_causa,
                  d.id_persona_rol, d.id_persona_causa
                FROM opetbl_mid_incidentes_personas a
                 LEFT JOIN opetbl_mid_incidentes b ON a.id_folio_incidente=b.id_folio_incidente
                 LEFT JOIN opecat_mid_personas_roles_causas c ON a.id_persona_rol=c.id_persona_rol
                 LEFT JOIN opecat_mid_personas_roles_causas d ON a.id_persona_causa=d.id_persona_causa";
        if (!empty($sql_order))
            $sql .= " ORDER BY $sql_order";
        if (!empty($sql_limit))
            $sql .= " LIMIT $sql_limit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_persona' => $data['id_persona'],
                               'id_folio_incidente' => $data['id_folio_incidente'],
                               'id_persona_rol' => $data['id_persona_rol'],
                               'id_persona_causa' => $data['id_persona_causa'],
                               'nombre' => $data['nombre'],
                               'apellido_paterno' => $data['apellido_paterno'],
                               'apellido_materno' => $data['apellido_materno'],
                               'edad' => $data['edad'],
                               'sexo' => $data['sexo'],
                               'domicilio' => $data['domicilio'],
                               'opetbl_mid_incidentes_prioridad' => $data['prioridad'],
                               'opetbl_mid_incidentes_fecha_hora_captura' => $data['fecha_hora_captura'],
                               'opetbl_mid_incidentes_fecha_incidente' => $data['fecha_incidente'],
                               'opetbl_mid_incidentes_dia' => $data['dia'],
                               'opetbl_mid_incidentes_mes' => $data['mes'],
                               'opetbl_mid_incidentes_annio' => $data['annio'],
                               'opetbl_mid_incidentes_hora_incidente' => $data['hora_incidente'],
                               'opetbl_mid_incidentes_id_estado' => $data['id_estado'],
                               'opetbl_mid_incidentes_id_region' => $data['id_region'],
                               'opetbl_mid_incidentes_id_cuartel' => $data['id_cuartel'],
                               'opetbl_mid_incidentes_id_municipio' => $data['id_municipio'],
                               'opetbl_mid_incidentes_direccion' => $data['direccion'],
                               'opetbl_mid_incidentes_localidad' => $data['localidad'],
                               'opetbl_mid_incidentes_colonia' => $data['colonia'],
                               'opetbl_mid_incidentes_calle' => $data['calle'],
                               'opetbl_mid_incidentes_latitud' => $data['latitud'],
                               'opetbl_mid_incidentes_longitud' => $data['longitud'],
                               'opetbl_mid_incidentes_id_asunto' => $data['id_asunto'],
                               'opetbl_mid_incidentes_id_fuente' => $data['id_fuente'],
                               'opetbl_mid_incidentes_hechos_html' => $data['hechos_html'],
                               'opetbl_mid_incidentes_hechos_text' => $data['hechos_text'],
                               'opetbl_mid_incidentes_ip_usuario' => $data['ip_usuario'],
                               'opetbl_mid_incidentes_id_usuario' => $data['id_usuario'],
                               'opecat_mid_personas_roles_causas_id_persona_causa' => $data['id_persona_causa'],
                               'opecat_mid_personas_roles_causas_id_persona_rol' => $data['id_persona_rol'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }


	/**
     * Funci?n que controla la obtenci?n de los registros de la tabla para mostrarse en el grid
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllGrid($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit=''){
        $datos = array();
        // Se obtiene la cantidad de resgistros
        $total_reg = $this->selectAllCount($sqlWhere, $sqlValues, $sqlOrder);
        if ($total_reg > 0) {
            $datos['total'] = $total_reg;
            // Se obtienen los datos de los registros de la tabla
            $registros = $this->selectAllMin($sqlWhere, $sqlValues, $sqlOrder, $sqlLimit);
            // Se asignan los registros de la tabla al array principal
            $datos['datos'] = array_values($registros);
        } else {
            $datos['total'] = 0;
            $datos['datos'] = null;
        }

        return $datos;
    }

 /**
     * Funci?n para obtener los registros m?nimos requeridos de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllMin($sqlWhere='', $sqlValues=array(), $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT p.id_persona,p.id_folio_incidente,r.persona_rol, c.persona_causa,
				p.nombre,p.apellido_paterno,p.apellido_materno, p.edad,p.sexo, p.domicilio
				FROM opetbl_mid_incidentes_personas p
				LEFT JOIN opecat_mid_personas_roles r ON p.id_persona_rol=r.id_persona_rol
				LEFT JOIN opecat_mid_personas_causas c ON p.id_persona_causa=c.id_persona_causa";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }
        if (!empty($sqlLimit)) {
            $sql .= "\nLIMIT $sqlLimit";
        }
        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                                'id_persona'         =>  $data['id_persona'],
                                'id_folio_incidente' =>  $data['id_folio_incidente'],
                                'persona_rol'        =>  $data['persona_rol'],
                                'persona_causa'      =>  $data['persona_causa'],
                                'nombre'             =>  $data['nombre'],
                                'apellido_paterno'   =>  $data['apellido_paterno'],
                                'apellido_materno'   =>  $data['apellido_materno'],
                                'edad'               =>  $data['edad'],
                                'sexo'               =>  $data['sexo'],
                                'domicilio'          =>  $data['domicilio'],
                                );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci?n para obtener el total de registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci?n de datos
     * @param array $sqlValues, arreglo que contiene las claves y los valores de los campos contemplados en la condici?n Where
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAllCount($sqlWhere='', $sqlValues=array(), $sqlOrder='')
    {
        $sql = "SELECT count(*)
                FROM opetbl_mid_incidentes_personas p
				LEFT JOIN opecat_mid_personas_roles r ON p.id_persona_rol=r.id_persona_rol
				LEFT JOIN opecat_mid_personas_causas c ON p.id_persona_causa=c.id_persona_causa";
        if (!empty($sqlWhere)) {
            $sql .= "\nWHERE $sqlWhere";
        }
        if (!empty($sqlOrder)) {
            $sql .= "\nORDER BY $sqlOrder";
        }

        $sql .= ";";
        //echo $sql;
        try {
            $qry = $this->_conexBD->prepare($sql);
            if (count($sqlValues) > 0) {
                $qry->execute($sqlValues);
            } else {
                $qry->execute();
            }
            $total_reg = $qry->fetchColumn();

            return $total_reg;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }


    /**
     * Funci?n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el ?ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opetbl_mid_incidentes_personas(id_folio_incidente, id_persona_rol, id_persona_causa, nombre, 
                apellido_paterno, apellido_materno, edad, sexo, domicilio)
                VALUES(:id_folio_incidente, :id_persona_rol, :id_persona_causa, :nombre, :apellido_paterno, :apellido_materno, 
                :edad, :sexo, :domicilio);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_folio_incidente" => $this->id_folio_incidente, 
                                ":id_persona_rol" => $this->id_persona_rol, ":id_persona_causa" => $this->id_persona_causa, 
                                ":nombre" => $this->nombre, ":apellido_paterno" => $this->apellido_paterno, 
                                ":apellido_materno" => $this->apellido_materno, ":edad" => $this->edad, ":sexo" => $this->sexo, 
                                ":domicilio" => $this->domicilio));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            //$this->msjError = $sql;
            return false;
        }
    }

    /**
     * Funci?n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opetbl_mid_incidentes_personas
                   SET id_folio_incidente=:id_folio_incidente, id_persona_rol=:id_persona_rol, id_persona_causa=:id_persona_causa, nombre=:nombre, apellido_paterno=:apellido_paterno, apellido_materno=:apellido_materno, edad=:edad, sexo=:sexo, domicilio=:domicilio
                WHERE id_persona=:id_persona;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_persona" => $this->id_persona, ":id_folio_incidente" => $this->id_folio_incidente, ":id_persona_rol" => $this->id_persona_rol, ":id_persona_causa" => $this->id_persona_causa, ":nombre" => $this->nombre, ":apellido_paterno" => $this->apellido_paterno, ":apellido_materno" => $this->apellido_materno, ":edad" => $this->edad, ":sexo" => $this->sexo, ":domicilio" => $this->domicilio));
            if ($qry)
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>