<?php
/**
 *
 */
class AdmcatNivelEstudios
{
    public $id_nivel_estudios; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $nivel_estudios; /** @Tipo: varchar(50), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }

    /**
     * Funci�n para mostrar la lista de los niveles de estudio dentro de un combobox.
     * @param int $id, id del nivel de estudio seleccionado por deafult     
     * @return array html(options)
     */
    public function shwNivelEstudios($id=0){
        $aryDatos = $this->selectAll('', 'id_nivel_estudios Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_nivel_estudios"] )
                $html .= '<option value="'.$datos["id_nivel_estudios"].'" selected>'.$datos["nivel_estudios"].'</option>';
            else
                $html .= '<option value="'.$datos["id_nivel_estudios"].'" >'.$datos["nivel_estudios"].'</option>';
        }
        return $html;
    }
    
    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_nivel_estudios)
    {
        $sql = "SELECT id_nivel_estudios, nivel_estudios
                FROM admcat_nivel_estudios
                WHERE id_nivel_estudios=:id_nivel_estudios;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_nivel_estudios' => $id_nivel_estudios));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_nivel_estudios = $data['id_nivel_estudios'];
            $this->nivel_estudios = $data['nivel_estudios'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_nivel_estudios, a.nivel_estudios
                FROM admcat_nivel_estudios a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_nivel_estudios' => $data['id_nivel_estudios'],
                               'nivel_estudios' => $data['nivel_estudios'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_nivel_estudios(id_nivel_estudios, nivel_estudios)
                VALUES(:id_nivel_estudios, :nivel_estudios);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_nivel_estudios" => $this->id_nivel_estudios, ":nivel_estudios" => $this->nivel_estudios));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_nivel_estudios
                   SET nivel_estudios=:nivel_estudios
                WHERE id_nivel_estudios=:id_nivel_estudios;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_nivel_estudios" => $this->id_nivel_estudios, ":nivel_estudios" => $this->nivel_estudios));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>