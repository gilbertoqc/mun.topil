<?php
/**
 *
 */
class OpecatMidGeneralidadesUnidades
{
    public $id_generalidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $id_unidad; /** @Tipo: smallint(6), @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos
    public $OpecatMidGeneralidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */
    public $OpecatMidUnidadesGeneralidades; /** Objeto para acceder a las propiedades de la clase del mismo nombre */

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

        require_once 'opecat_mid_generalidades.class.php';
        require_once 'opecat_mid_unidades_generalidades.class.php';
        $this->OpecatMidGeneralidades = new OpecatMidGeneralidades();
        $this->OpecatMidUnidadesGeneralidades = new OpecatMidUnidadesGeneralidades();
    }
    
    /**
     * Funci�n para mostrar la lista de --- dentro de un combobox.
     * @param int $id, id .... seleccionado por deafult
     * @return array html(options)
     */
    public function getCat_mid_Generalidades_Unidades($id_generalidad=0){
        $aryDatos = $this->selectAll('id_generalidad Asc','');
        $html = '<option value="0">-- SELECCIONE --</option>';
        foreach( $aryDatos as $datos ){
            if( $id_generalidad == $datos["id_generalidad"] )
                $html .= '<option value="'.$datos["id_generalidad"].'" selected>'.$datos["id_generalidad"].'</option>';
            else
                $html .= '<option value="'.$datos["id_generalidad"].'" >'.$datos["id_generalidad"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_generalidad, $id_unidad)
    {
        $sql = "SELECT id_generalidad, id_unidad
                FROM opecat_mid_generalidades_unidades
                WHERE id_generalidad=:id_generalidad AND id_unidad=:id_unidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_generalidad' => $id_generalidad, ':id_unidad' => $id_unidad));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_generalidad = $data['id_generalidad'];
            $this->id_unidad = $data['id_unidad'];

            $this->OpecatMidGeneralidades->select($this->id_generalidad);
            $this->OpecatMidUnidadesGeneralidades->select($this->id_unidad);
            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_generalidad, a.id_unidad,
                  b.id_generalidad, b.id_tipo_generalidad, b.generalidad,
                  c.id_unidad, c.unidad
                FROM opecat_mid_generalidades_unidades a 
                 LEFT JOIN opecat_mid_generalidades b ON a.id_generalidad=b.id_generalidad
                 LEFT JOIN opecat_mid_unidades_generalidades c ON a.id_unidad=c.id_unidad";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_generalidad' => $data['id_generalidad'],
                               'id_unidad' => $data['id_unidad'],
                               'opecat_mid_generalidades_id_tipo_generalidad' => $data['id_tipo_generalidad'],
                               'opecat_mid_generalidades_generalidad' => $data['generalidad'],
                               'opecat_mid_unidades_generalidades_unidad' => $data['unidad'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO opecat_mid_generalidades_unidades(id_generalidad, id_unidad)
                VALUES(:id_generalidad, :id_unidad);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_generalidad" => $this->id_generalidad, ":id_unidad" => $this->id_unidad));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE opecat_mid_generalidades_unidades
                   SET 
                WHERE id_generalidad=:id_generalidad AND id_unidad=:id_unidad;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_generalidad" => $this->id_generalidad, ":id_unidad" => $this->id_unidad));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>