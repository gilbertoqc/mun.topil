<?php
/**
 *
 */
class AdmcatStatusPrestacion
{
    public $id_status_prest; /** @Tipo: smallint(5) unsigned, @Acepta Nulos: NO, @Llave: PRI, @Default: NULL */
    public $status_prest; /** @Tipo: varchar(30), @Acepta Nulos: NO, @Llave: --, @Default: NULL */

    public $msjError; // almacena el mensaje de error si �ste ocurre
    private $_conexBD; // objeto de conexi�n a la base de datos

    public function __construct()
    {
        require_once 'config/mysql.class.php';
        $this->_conexBD = new MySqlPdo();

    }
    
    /**
     * Funci�n para mostrar la lista de status de prestaciones dentro de un combobox.
     * @param int $id, id de la categor�a seleccionada por deafult     
     * @return array html(options)
     */
    public function shwStatusPrest($id=-1){
        $aryDatos = $this->selectAll('', 'id_status_prest Asc');
        $html = '';
        foreach( $aryDatos as $datos ){
            if( $id == $datos["id_status_prest"] )
                $html .= '<option value="'.$datos["id_status_prest"].'" selected>'.$datos["status_prest"].'</option>';
            else
                $html .= '<option value="'.$datos["id_status_prest"].'" >'.$datos["status_prest"].'</option>';
        }
        return $html;
    }

    /**
     * Funci�n para obtener un registro espec�fico de la tabla 
     * @param  campos que conforman la clave primaria de la tabla
     * @return boolean true, si la consulta se realiz� con �xito
     */
    public function select($id_status_prest)
    {
        $sql = "SELECT id_status_prest, status_prest
                FROM admcat_status_prestacion
                WHERE id_status_prest=:id_status_prest;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(':id_status_prest' => $id_status_prest));
            $data = $qry->fetch(PDO::FETCH_ASSOC);
            $this->id_status_prest = $data['id_status_prest'];
            $this->status_prest = $data['status_prest'];

            return true;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para obtener los registros de la tabla de acuerdo con las condiciones especificadas
     * @param string $sqlWhere, cadena que contiene la sentencia SQL para condicionar la selecci�n de datos
     * @param string $sqlOrder, cadena que contiene la sentencia SQL para ordenar los datos
     * @param string $sqlLimit, cadena que contiene la sentencia SQL para limitar la cantidad de registros a mostrar
     * @return array $datos, arreglo que contiene los datos obtenidos en la consulta
     */
    public function selectAll($sqlWhere='', $sqlOrder='', $sqlLimit='')
    {
        $sql = "SELECT a.id_status_prest, a.status_prest
                FROM admcat_status_prestacion a ";
        if (!empty($sqlWhere))
            $sql .= " WHERE $sqlWhere";
        if (!empty($sqlOrder))
            $sql .= " ORDER BY $sqlOrder";
        if (!empty($sqlLimit))
            $sql .= " LIMIT $sqlLimit";
        $sql .= ";";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute();
            $datos = array();
            while ($data = $qry->fetch(PDO::FETCH_ASSOC)) {
                $datos[] = array(
                               'id_status_prest' => $data['id_status_prest'],
                               'status_prest' => $data['status_prest'],
                               );
            }
            return $datos;
        } catch (PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para agregar un nuevo registro a la tabla
     * @return int, si el proceso es satisfactorio devuelve el �ltimo id insertado
     * @return boolean falso si el proceso falla
     */
    public function insert()
    {
        $sql = "INSERT INTO admcat_status_prestacion(id_status_prest, status_prest)
                VALUES(:id_status_prest, :status_prest);";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_status_prest" => $this->id_status_prest, ":status_prest" => $this->status_prest));
            if ($qry)
                return $this->_conexBD->lastInsertId();
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    /**
     * Funci�n para actualizar el registro seleccionado de la tabla
     * @return boolean true si el proceso es satisfactorio
     */
    public function update()
    {
        $sql = "UPDATE admcat_status_prestacion
                   SET status_prest=:status_prest
                WHERE id_status_prest=:id_status_prest;";
        try {
            $qry = $this->_conexBD->prepare($sql);
            $qry->execute(array(":id_status_prest" => $this->id_status_prest, ":status_prest" => $this->status_prest));
            if ($qry) 
                return true;
            else
                return false;
        } catch(PDOException $e) {
            $this->msjError = $e->getMessage();
            return false;
        }
    }

    public function delete()
    {

    }
}


?>