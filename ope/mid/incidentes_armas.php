<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Incidencia Delictiva
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi?n de las clases...
//-----------------------------------------------------------------//
/*
include  'includes/class/opecat_mid_personas_roles.class.php';
include  'includes/class/opecat_mid_armas_roles.class.php';
include  'includes/class/opecat_mid_armas_marcas.class.php';
include  'includes/class/opecat_mid_armas_tipos.class.php';
include  'includes/class/opecat_mid_armas_calibres.class.php';
*/
/*
$objArmasRoles =new OpecatMidArmasRoles;
$objArmasMarcas =new OpecatMidArmasMarcas;
$objArmasTipos =new OpecatMidArmasTipos;
$objArmasCalibres =new OpecatMidArmasCalibres;
*/

//include  'includes/class/opetbl_mid_incidentes_armas.class.php';
//$objDataGridArmas = new OpetblMidIncidentesArmas();

//se reciben parametros
//id_folio_incidente
$id_folio_incidente = $objSys->decrypt( $_GET["id_folio_incidente"] );

//-----------------------------------------------------------------//
//-- Bloque de definici?n de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Incidentes->Armas',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mid/_css/mid.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mid/_js/incidentes_armas.js"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din?mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"]. '&mod=' . $objSys->encrypt('incidentes_panel') . '&id_folio_incidente=' . $_GET["id_folio_incidente"];

?>
  <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                    <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Regresar al panel principal...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                    <a href="#" id="btnAgregar" class="Tool-Bar-Btn gradient" style="width: 80px;" title="Agregar arma involucrada ...">
                      <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
                    </a>
                </td>
            </tr>
        </table>
     </div>

<div id="dvForm-Perfil" class="dvForm-Data" style="border: none; height: 520px; margin: auto auto; margin-top: 10px; width: auto;">
    <span class="dvForm-Data-pTitle">
        <img src="<?php echo PATH_IMAGES;?>icons/armas24.png" class="icono"/>
         Armas
    </span>

    <!--****** INICIO TABLA DE DATOS  *****-->
    <div id="dvGridArmas" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
          <table class="xGrid-tbSearch">
              <tr>
                  <td>Buscar: <input type="text" name="txtBuscar" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
              </tr>
          </table>
          <table class="xGrid-tbCols">
              <tr>
                  <th style="width: 1%; text-align: center;">&nbsp;</th>
                  <th style="width: 3%;" class="xGrid-tbCols-ColSortable">ID</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable">ROL</th>
                  <th style="width: 15%;" class="xGrid-tbCols-ColSortable">MARCA</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable">TIPO</th>
                  <th style="width: 5%;" class="xGrid-tbCols-ColSortable">CALIBRE</th>
                  <th style="width: 10%;" class="xGrid-tbCols-ColSortable">MODELO</th>
                  <th style="width: 15%;" class="xGrid-tbCols-ColSortable">MATRICULA</th>
                  <th style="width: 15%;" class="xGrid-tbCols-ColSortable">NUMERO SERIE</th>

                  <th style="width: 7%;" class="xGrid-thNo-Class"></th>
              </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
          <table class="xGrid-tbBody">
              <?php

              ?>
          </table>
        </div>
    </div>
    <!--****** FIN TABLA DE DATOS  ******-->

</div>

<!-- INICIO FORMULARIO DE DIALOGO -->
<div id="dvFormIncidentesArmas" style="display: none;">
    <div id="dvIncArmCont" class="dvForm-Data" style="border-bottom: 2px double #488ac7; min-height: 300px; min-width: 500px; overflow-y: hidden;"></div>
</div>

<input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('ope/mid/_ajx/grid_incidentes_armas.php');?>" />
<input type="hidden" id="hdnUrlSave" name="hdnUrlSave" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_reg_armas.php'); ?>" />
<input type="hidden" id="hdnUrlArm" name="hdnUrlArm" value="<?php echo $objSys->encrypt('ope/mid/_ajx/ajx_frm_armas.php'); ?>" />
<input type="hidden" id="id_folio_incidente" name="id_folio_incidente" value="<?php echo $_GET["id_folio_incidente"]; ?>" />
<input type="hidden" id="id_arma" value="" />


<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>
