<?php
/**
 * Ficha de Informaci�n Personal
 */
session_start();
if( isset($_SESSION["admitted_xsisp"]) ){
     header('content-type: text/html; charset=UTF-8');

    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/admtbl_datos_personales.class.php';
    include $path . 'includes/class/admtbl_adscripcion.class.php';
    include $path . 'includes/class/admtbl_domicilio.class.php';
    include $path . 'includes/class/admtbl_nivel_estudios.class.php';
    include $path . 'includes/class/admtbl_doctos_ident.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes.class.php';
    include $path . 'includes/lib/fpdf16/pdf.php';

    $objSys = new System();
    $objInc             =   new OpetblMidIncidentes;
    $objDatPer = new AdmtblDatosPersonales();
    $objAdscrip = new AdmtblAdscripcion();
    $objDomi = new AdmtblDomicilio();
    $objNivelEst = new AdmtblNivelEstudios();
    $objDocIdent = new AdmtblDoctosIdent();



    $pdf = new PDF("P", "mm", "letter");

    $curp = $_SESSION["xCurpFicha"];
    $folio= $objSys->decrypt($_GET["F"]);
    $objInc->select($folio);

   // echo($objInc->OpecatMidRegionesOperativas->region);


    $pdf->xInFooter = true;
    $pdf->xPageNo = true;

    $pdf->txt_footer_l = "Reporte de Incidente";
    $pdf->StartPageGroup();
    $pdf->AddPage();
    $pdf->SetMargins(10, 5, 10);
    //$pdf->SetAutoPageBreak(auuto,5);

    // Encabezado
    $pdf->Image($path . "includes/css/imgs/plantilla/logo_gob_gro.jpg", 10, 5, 35, 27);
    //$pdf->Image($path . "includes/css/imgs/plantilla/logo_secretriado_consejo.jpg", 80, 5,120, 27);
    $pdf->SetFont("Arial", "B", 12);
    $pdf->SetXY(55, 10);
    $pdf->Cell(160, 0, "SECRETAR�A DE SEGURIDAD P�BLICA Y PROTECCI�N CIVIL");

    $pdf->SetFillColor(150);
    $pdf->Rect(12, 41, 195, 7, "F");
    $pdf->SetFillColor(255);
    $pdf->SetY(40);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(0, 7, "DETALLES DEL INCIDENTE", 1, 0, "C", true);

    $y = 55;
    /*
    *--------------------------------------------------------------------------------------*
    * DETALLES DEL INCIDENTE
    *--------------------------------------------------------------------------------------*
    */
    // $objDatPer->select($curp);
    // // Marco del bloque: Datos personales
    // $pdf->SetFillColor(190);
    // $pdf->Rect(11, $y-1, 196, 50, "F");
    // $pdf->SetFillColor(255);
    // $pdf->Rect(10, $y-2, 196, 50, "FD");

    // Foto
    $objDocIdent->select($curp);
    $foto_fte = ( !empty($objDocIdent->foto_frente) ) ? $path . 'mapa.jpg' . $objDocIdent->foto_frente : '';
    if( empty($foto_fte) )
        $foto_fte = ( $objDatPer->genero == 1 ) ? $path . 'mapa.jpg' : 'mapa.jpg';
    $pdf->SetFillColor(255);
    //$pdf->Rect(105, $y-6,  100, 64, "FD");
   // $pdf->Image($foto_fte, 110, $y-5.5, 96, 63);

    // Folio
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Folio:");
    $pdf->SetTextColor(194,8,8);
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $folio);
    // Prioridad
    $y += 6;
    $pdf->SetTextColor(0);
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Prioridad:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(95, 5, ( !empty($objDatPer->cuip) ) ? $objDatPer->cuip : "ALTA");
    // Fecha y Hora
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Fecha y Hora:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->nombre . " " . $objDatPer->a_paterno . " " . $objDatPer->a_materno);
    // Region
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Region:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, date("d/m/Y", strtotime($objDatPer->fecha_nac)));
    // Municipio
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Municipio:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, ($objDatPer->genero == 1) ? "MASCULINO" : "FEMENINO");
    // Localidad
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Localidad:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, ( !empty($objDatPer->mat_cartilla) ) ? $objDatPer->mat_cartilla : "- - -");
    // Direcci�n
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Direcci�n:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->AdmcatEstadoCivil->estado_civil);
    // Asunto
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Asunto:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->AdmcatTipoSangre->tipo_sangre);
    // Fuente
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Fuente:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->AdmcatTipoSangre->tipo_sangre);
    // Hechos
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    $pdf->Cell(40, 5, "Relato de Hechos:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->Cell(90, 5, $objDatPer->AdmcatTipoSangre->tipo_sangre);

    // Hechos
    $y += 6;
    $pdf->SetFont("Arial", "B", 10);
    $pdf->SetXY(20, $y);
    //$pdf->Cell(40, 5, "Relato de Hechos:");
    $pdf->SetFont("Arial", "", 10);
    $pdf->MultiCell(188, 5, html_entity_decode(preg_replace('/\s\s+/', ' ',strip_tags($objInc->hechos_text))));


    $nl=$pdf->GetMultiCellHeight(188,5,$hecho, $border=null, $align='J');

    /*
    *--------------------------------------------------------------------------------------*
    * PERSONAS RELACIONADAS
    *--------------------------------------------------------------------------------------*
    */
    $objDomi->select($curp);
    $y += 5+$nl;
    // Marco del bloque: Domicilio
    $pdf->SetFillColor(190);
    // $pdf->Rect(10, $y, 196, 47);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "PERSONAS RELACIONADAS", 1, 0, "C", true);

    /*
    *--------------------------------------------------------------------------------------*
    * VEHICULOS RELACIONADOS
    *--------------------------------------------------------------------------------------*
    */
    $objNivelEst->select($curp);
    $y += 15;
    // Marco del bloque: Nivel de estudios
    $pdf->SetFillColor(190);
    //$pdf->Rect(10, $y, 196, 30);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "VEHICULOS RELACIONADOS:", 1, 0, "C", true);

    $pdf->AddPage();
    /*
    *--------------------------------------------------------------------------------------*
    * ARMAS RELACIONADAS
    *--------------------------------------------------------------------------------------*
    */
    $objAdscrip->select($curp);
    $y = 15;
    // Marco del bloque: Adscripci�n
    $pdf->SetFillColor(190);
    //$pdf->Rect(10, $y, 196, 65);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "ARMAS RELACIONADAS", 1, 0, "C", true);

    /*
    *--------------------------------------------------------------------------------------*
    * GENERALIDADES RELACIONADAS
    *--------------------------------------------------------------------------------------*
    */
    $objAdscrip->select($curp);
    $y += 15;
    // Marco del bloque: Adscripci�n
    $pdf->SetFillColor(190);
    //$pdf->Rect(10, $y, 196, 65);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "GENERALIDADES RELACIONADAS", 1, 0, "C", true);

     /*
    *--------------------------------------------------------------------------------------*
    * AMULRIMEDIA RELACIONADA
    *--------------------------------------------------------------------------------------*
    */
    $objAdscrip->select($curp);
    $y += 15;
    // Marco del bloque: Adscripci�n
    $pdf->SetFillColor(190);
    //$pdf->Rect(10, $y, 196, 65);
    $pdf->SetY($y);
    $pdf->SetFont("Arial", "B", 11);
    $pdf->Cell(196, 5, "MULTIMEDIA RELACIONADA", 1, 0, "C", true);

    /*
    *--------------------------------------------------------------------------------------*
    * Reguardo de armamento
    *--------------------------------------------------------------------------------------*
    */
    // if( $objAdscrip->id_tipo_funcion == 1 ){
    //     // Marco del bloque: Armamento
    //     $pdf->SetFillColor(190);
    //     $pdf->Rect(10, $y, 196, 40);
    //     $pdf->SetY($y);
    //     $pdf->SetFont("Arial", "B", 11);
    //     $pdf->Cell(196, 5, "GENERALIDADES RELACIONADAS", 1, 0, "C", true);
    //     $y += 5;

    //     include $path . 'includes/class/logtbl_arm_asignacion.class.php';
    //     $objAsigArma = new LogtblArmAsignacion();
    //     $datArmas = $objAsigArma->selectLstAsignadas("curp='" . $objDatPer->curp . "'");

    //     $pdf->SetFont("Arial", "B", 9);
    //     $pdf->SetFillColor(220);
    //     $pdf->SetY($y);
    //     $pdf->Cell(36, 5, "MATRICULA", 1, 0, "C", "true");
    //     $pdf->Cell(25, 5, "TIPO", 1, 0, "C", "true");
    //     $pdf->Cell(45, 5, "MARCA", 1, 0, "C", "true");
    //     $pdf->Cell(30, 5, "MODELO", 1, 0, "C", "true");
    //     $pdf->Cell(30, 5, "CLASE", 1, 0, "C", "true");
    //     $pdf->Cell(30, 5, "CALIBRE", 1, 0, "C", "true");
    //     $y += 5;
    //     $pdf->SetFont("Arial", "", 9);
    //     foreach( $datArmas As $reg => $arma ){
    //         $pdf->SetY($y);
    //         $pdf->CellFitScale(36, 5, $arma["matricula"], 1, 0, "C");
    //         $pdf->CellFitScale(25, 5, $arma["tipo"], 1, 0, "C");
    //         $pdf->CellFitScale(45, 5, $arma["marca"], 1, 0, "C");
    //         $pdf->CellFitScale(30, 5, $arma["modelo"], 1, 0, "C");
    //         $pdf->CellFitScale(30, 5, $arma["clase"], 1, 0, "C");
    //         $pdf->CellFitScale(30, 5, $arma["calibre"], 1, 0, "C");
    //         $y += 5;
    //     }

    //     $y += 30;
    // }

    /*
    *--------------------------------------------------------------------------------------*
    * Resguardo de veh�culo
    *--------------------------------------------------------------------------------------*
    */
    // Marco del bloque: Veh�culos
    // $pdf->SetFillColor(190);
    // $pdf->Rect(10, $y, 196, 30);
    // $pdf->SetY($y);
    // $pdf->SetFont("Arial", "B", 11);
    // $pdf->Cell(196, 5, "RESGUARDO DE VEH�CULO:", 1, 0, "C", true);
    // $y += 5;

    // include $path . 'includes/class/logtbl_veh_datos.class.php';
    // $objVehic = new LogtblVehDatos();

    // $datVehic = $objVehic->selectAll("a.curp='" . $objDatPer->curp . "'");
    // $pdf->SetFont("Arial", "B", 9);
    // $pdf->SetFillColor(220);
    // $pdf->SetY($y);
    // $pdf->Cell(50, 5, "No. SERIE", 1, 0, "C", "true");
    // $pdf->Cell(35, 5, "PLACAS", 1, 0, "C", "true");
    // $pdf->Cell(50, 5, "MARCA", 1, 0, "C", "true");
    // $pdf->Cell(40, 5, "TIPO", 1, 0, "C", "true");
    // $pdf->Cell(21, 5, "MODELO", 1, 0, "C", "true");
    // $y += 5;
    // $pdf->SetFont("Arial", "", 9);
    // foreach( $datVehic As $reg => $veh ){
    //     $pdf->SetY($y);
    //     $objVehic->LogtblVehiculos->select($veh["id_vehiculo"]);

    //     $pdf->CellFitScale(50, 5, $objVehic->LogtblVehiculos->num_serie, 1, 0, "C");
    //     $placas = ( !empty($objVehic->LogtblVehiculos->placas) ) ? $objVehic->LogtblVehiculos->placas : "- - -";
    //     $pdf->CellFitScale(35, 5, $placas, 1, 0, "C");
    //     $pdf->CellFitScale(50, 5, $objVehic->LogtblVehiculos->LogcatVehTipo->LogcatVehMarca->marca, 1, 0, "C");
    //     $pdf->CellFitScale(40, 5, $objVehic->LogtblVehiculos->LogcatVehTipo->tipo, 1, 0, "C");
    //     $pdf->CellFitScale(21, 5, $objVehic->LogtblVehiculos->LogcatVehModelo->modelo, 1, 0, "C");

    //     $y += 5;
    // }
    /*
    *--------------------------------------------------------------------------------------*
    *
    *--------------------------------------------------------------------------------------*
    */

    $pdf->Output();
}
?>