<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    
    include $path . 'includes/class/opetbl_mid_incidentes_vehiculos.class.php';   
    include $path . 'includes/class/config/system.class.php';
    $objVeh = new OpetblMidIncidentesVehiculos();
    $objSys = new System();  
    
    //se reciben parametros
    //id_folio_incidente
    $id_folio_incidente = $objSys->decrypt( $_POST["id_folio_incidente"] );
    //id_persona
    if( !empty( $_POST["id_vehiculo"] ) ){
        $id_vehiculo = $_POST["id_vehiculo"];
        $objVeh->select( $id_vehiculo );
        $oper = 1;
    }else{
        $id_vehiculo = $objVeh->getIdUltimoVehiculo() + 1;
        $oper = 2;
    }
    
    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
    //$html .= '<p>hola</p>';  
    $html='';  
                  
    $html .= '<form id="frmIncidentesVehiculos" name="vehiculos" method="post" enctype="multipart/form-data">';
        $html .= ' <fieldset id="fsetopetbl_mid_incidentes_vehiculos" class="fsetForm-Data">';    
            $html .= '<table id="tbfrmopetbl_mid_incidentes_vehiculos" class="tbForm-Data">';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtIdVehiculo">Vehiculo:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtIdVehiculo" id="txtIdVehiculo" value="' . $id_vehiculo . '" style="width: 100px;" maxlength="" readonly="readonly"/>';
                    $html .= '  <span class="pRequerido">*</span>';
                    $html .= '  </td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="cbxIdRolVehiculo">Rol del vehiculo:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <select name="cbxIdRolVehiculo" id="cbxIdRolVehiculo" style="width: 250px;">';
                    $html .= $objVeh->OpecatMidVehiculosRoles->getCat_mid_Vehiculos_Roles( $objVeh->id_vehiculo_rol );
                    $html .= '  </select>';
                    $html .= '  <span class="pRequerido">*</span>';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="cbxIdMarca">Marca:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <select name="cbxIdMarca" id="cbxIdMarca" style="width: 250px;">';
                    $html .= $objVeh->OpecatMidVehiculosMarcas->getCat_mid_Vehiculos_Marcas( $objVeh->id_vehiculo_marca );
                    $html .= '  </select>';
                    $html .= '  <span class="pRequerido">*</span>';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="cbxIdCategoria">Categoria:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <select name="cbxIdCategoria" id="cbxIdCategoria" style="width: 250px;">';
                    $html .= $objVeh->OpecatMidVehiculosCategorias->getCat_mid_Vehiculos_Categorias( $objVeh->id_vehiculo_categoria);
                    $html .= '  </select>';
                    $html .= '  <span class="pRequerido">*</span>';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtModelo">Modelo:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtModelo" id="txtModelo" style="width: 300px;" maxlength="4" class="" value="' . $objVeh->modelo . '"/>';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td><label for="txtColor">Color:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtColor" id="txtColor" style="width: 300px;" maxlength="30" class="" value="' . $objVeh->color . '" />';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtTipo">Tipo:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtTipo" id="txtTipo" style="width: 300px;" maxlength="30" class="" value="' . $objVeh->tipo . '" />';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtNumeroSerie">Numero de Serie:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtNumeroSerie" id="txtNumeroSerie" style="width: 300px;" maxlength="30" class="" value="' . $objVeh->numero_serie . '" />';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtNumeroMotor">Numero de Motor:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtNumeroMotor" id="txtNumeroMotor" style="width: 300px;" maxlength="30" class="" value="' . $objVeh->numero_motor . '" />';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '  <td class="descripcion"><label>Es extranjero:</label></td>';
                    if( $objVeh->es_extranjero == 1){
                        $radio1="checked";
                        $radio2="";
                    }elseif( $objVeh->es_extranjero == 2 ){
                        $radio1="";
                        $radio2="checked";
                    }else{
                        $radio1="checked";
                    }
                    
                    $html .= '  <td class="validation">';
                    $html .= '  <div class="dvRadioGroup">';
                    $html .= '   <label class="label-Radio" style="margin-right: 10px;">';
                    $html .= '<input type="radio" name="rbnExranjero" id="rbnExranjero1" value="1" ' . $radio1 . ' />NO</label>';
                    $html .= '   <label class="label-Radio">';
                    $html .= '<input type="radio" name="rbnExranjero" id="rbnExranjero2" value="2" ' . $radio2 . ' />SI</label>';
                    $html .= '  </div>';
                    $html .= '  </td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtOrigenPlacas">Origen de Placas:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtOrigenPlacas" id="txtOrigenPlacas" style="width: 300px;" maxlength="30" class="" value="' . $objVeh->origen_placas . '" />';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtNumeroPlacas">Numero de Placas:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtNumeroPlacas" id="txtNumeroPlacas" style="width: 300px;" maxlength="30" class="" value="' . $objVeh->numero_placas . '" />';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="cbxIdEmpresa">Empresa:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <select name="cbxIdEmpresa" id="cbxIdEmpresa" style="width: 300px;">';
                    $html .= $objVeh->OpecatMidVehiculosEmpresas->getCat_mid_Vehiculos_Empresas( $objVeh->id_vehiculo_empresa );
                    $html .= '  </select>';
                    $html .= '  <span class="pRequerido">*</span>';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="cbxIdTipoServicio">Tipo de Servicio:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <select name="cbxIdTipoServicio" id="cbxIdTipoServicio" style="width: 300px;">';
                    $html .= $objVeh->OpecatMidVehiculosTiposServicios->getCat_mid_Vehiculos_Tipos_Servicios( $objVeh->id_vehiculo_tipo_servicio );
                    $html .= '  </select>';
                    $html .= '  <span class="pRequerido">*</span>';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtTipoCarga">Tipo de Carga:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <input type="text" name="txtTipoCarga" id="txtTipoCarga" style="width: 300px;" maxlength="100" class="" value="' . $objVeh->tipo_carga . '" />';
                    $html .= '</td>';
                $html .= '   </tr>';
                $html .= '   <tr>';
                    $html .= '   <td class="descripcion"><label for="txtAreaObservaciones">Observaciones:</label></td>';
                    $html .= '   <td class="validation">';
                    $html .= '  <textarea name="txtAreaObservaciones" id="txtAreaObservaciones" cols="45" rows="2">' . $objVeh->observaciones . '</textarea>';
                    $html .= '</td>';
                $html .= '   </tr>';
            $html .= '</table>';
        $html .= '</fieldset>';
        
        $html .= '<p class="infoRequerida"><img alt="" src="' . PATH_IMAGES . 'icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podra continuar hasta que los complete.</p>';
                    
        $html .= '<input type="hidden" id="txtFolioIncidente" name="txtFolioIncidente" value="' . $id_folio_incidente . '" />';
        $html .= '<input type="hidden" id="oper" name="oper" value="' . $oper . '" />';
        
    $html .= '</form>';
  

    // Formatea los datos y los envia al grid...
    $ajx_datos["html"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>