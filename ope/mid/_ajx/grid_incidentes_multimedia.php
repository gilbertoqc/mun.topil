<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de parámetros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una búsqueda.
 * @param int colSort, contiene el indice de la columna por la cual se ordenarón los datos.
 * @param string typeSort, define el tipo de ordenaci?n de los datos: ASC o DESC.
 * @param int rowStart, especifica el número de fila por la cual inicia la paginación del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrarón en cada p?gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_multimedia.class.php';
    $objSys = new System();
    $objDataGridMultimedia = new OpetblMidIncidentesMultimedia();

    //--------------------- Recepción de parámetros --------------------------//
    // búsqueda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {

        $sql_where = 'm.id_multimedia LIKE ? '
                    . 'OR mt.multimedia_tipo LIKE ? '
                    . 'OR m.descripcion LIKE ? '
                    . 'OR m.archivo LIKE ? ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            );
   }


    // Ordenaci?n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'm.id_multimedia ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
         $sql_order = 'mt.multimedia_tipo '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'm.descripcion ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'm.archivo ' . $_GET['typeSort'];
    }

    // paginación...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objDataGridMultimedia->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $dato["id_multimedia"]; //$objSys->encrypt($dato["id_multimedia"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_multimedia"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 1%;">';
                // $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 5%; font-size:13px; color:#FF0000;"><strong>'.$dato["id_multimedia"].'</strong></td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["multimedia_tipo"] . '</td>';
                $html .= '<td style="text-align: left; width: 20%;">' . $dato["archivo"] . '</td>';
                $html .= '<td style="text-align: left; width: 30%;">' . $dato["descripcion"] . '</td>';

                $url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("incidentes_panel") . "&id=" . $id_crypt;
                $url_del = "ope/mid/_reportes/reporte_incidente.pdf";
                $html .= '<td style="text-align: left; width: 15%;">';
                $html .= '  <a href="' . $url_edit . '" class="lnkBtnOpcionGrid" title="Editar Incidente ..." ><img src="' . PATH_IMAGES . 'icons/edit24.png" alt="modificar" /></a>';
                $html .= '  <a href="' . $url_del . '" target="_blank" class="lnkBtnOpcionGrid" title="Imprimir Incidente ..." ><img src="' . PATH_IMAGES . 'icons/pdf24.png" alt="baja" /></a>';
                $html .= '  <a href="' . $url_del . '" target="_blank" class="lnkBtnOpcionGrid" title="Descargar Multimedia ..." ><img src="' . PATH_IMAGES . 'icons/Download24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//... '.$dato["hechos_text"] .'
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; font-size:18px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros de incidentes ...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi?n...";
}
?>