<?php
/**
 * Complemento ajax para obtener los datos de una persona involucrada en el incidente actual. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/opetbl_mid_incidentes_personas.class.php';
    $objIncidPer = new OpetblMidIncidentesPersonas();
    
    $objIncidPer->select($_GET["id"]);
    if ($objIncidPer->id_persona > 0) {
        $ajx_datos['rslt']  = true;
        $ajx_datos["id_persona_rol"] = $objIncidPer->id_persona_rol;
        $ajx_datos["id_persona_causa"] = $objIncidPer->id_persona_causa;
        $ajx_datos["nombre"] = utf8_encode($objIncidPer->nombre);
        $ajx_datos["apellido_paterno"] = utf8_encode($objIncidPer->apellido_paterno);
        $ajx_datos["apellido_materno"] = utf8_encode($objIncidPer->apellido_materno);
        $ajx_datos["edad"] = $objIncidPer->edad;
        $ajx_datos["sexo"] = $objIncidPer->sexo;
        $ajx_datos["domicilio"] = utf8_encode($objIncidPer->domicilio);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objIncidPer->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>