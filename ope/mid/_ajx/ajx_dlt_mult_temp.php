<?php
/**
 * Complemento del llamado ajax para eliminar los archivos en una carpeta temporal del servidor.
 * Lista de parámetros recibidos por POST 
 * @param String file, contiene el nombre del archivo a eliminar en el servidor.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    $uploaddir = '../_upload/';	        	
    $pos_ext = strrpos($_POST["file"], '.');
    $ext = substr($_POST["file"], ($pos_ext + 1));
    $ext = strtolower($ext);
    
    $archivo_normal = $_POST["file"];
    unlink($uploaddir . $archivo_normal);
    $pos = strpos("jpg,jpeg,png,gif,bmp", $ext);
    if( $pos !== false ){
        $archivo_mini = substr($_POST["file"], 0, $pos_ext) . '-thb.' . $ext;  
        unlink($uploaddir . $archivo_mini);
    }
}
?>