<?php
/**
 * Complemento ajax para guardar los datos del los vehiculos involucrados en incidentes
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/mysql.class.php';     
    include $path . 'includes/class/opetbl_mid_incidentes_vehiculos.class.php';    
    include $path . 'includes/class/config/system.class.php';
    
    $conexBD = new MySQLPDO();
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objVeh  = new OpetblMidIncidentesVehiculos();           
    
    // Datos    
    $objVeh->id_vehiculo                = $_POST["txtIdVehiculo"];                
    $objVeh->id_folio_incidente         = $_POST["txtFolioIncidente"];
    $objVeh->id_vehiculo_rol            = $_POST["cbxIdRolVehiculo"];
    $objVeh->id_vehiculo_categoria      = $_POST["cbxIdCategoria"];
    $objVeh->id_vehiculo_marca          = $_POST["cbxIdMarca"];
    $objVeh->tipo                       = strtoupper( $_POST["txtTipo"] ); 
    $objVeh->modelo                     = strtoupper( $_POST["txtModelo"] );
    $objVeh->color                      = strtoupper( $_POST["txtColor"] );
    $objVeh->numero_serie               = strtoupper( $_POST["txtNumeroSerie"] );
    $objVeh->numero_motor               = strtoupper( $_POST["txtNumeroMotor"] );
    $objVeh->numero_placas              = strtoupper( $_POST["txtNumeroPlacas"] );
    $objVeh->es_extranjero              = $_POST["rbnExranjero"];
    $objVeh->origen_placas              = strtoupper( $_POST["txtOrigenPlacas"] );
    $objVeh->id_vehiculo_tipo_servicio  = $_POST["cbxIdTipoServicio"];
    $objVeh->id_vehiculo_empresa        = $_POST["cbxIdEmpresa"];
    $objVeh->tipo_carga                 = strtoupper( $_POST["txtTipoCarga"] );
    $objVeh->observaciones              = strtoupper( $_POST["txtAreaObservaciones"] );
    
    
    //se ejecuta la operacion de ingreso o actualizacion
    $conexBD->beginTransaction();   
    if( $_POST["oper"] == 1 ){
        $result = $objVeh->update() ;  
        $id = $objVeh->id_vehiculo;
        $oper = "Upd";
    }elseif( $_POST["oper"] == 2 ){
        $result = $objVeh->insert();  
        $id = $result;
        $oper = "Ins";        
    } 
    //se verifica si hubo exito en la consulta de insercion o actualizacion del registro
    if( $result ){
        $objSys->registroLog($objUsr->idUsr, 'opetbl_mid_incidentes_vehiculos', $objVeh->id_vehiculo , $oper);                   
        $ajx_datos['rslt']  = true;                
        $ajx_datos['error'] = "";        
        $conexBD->commit();
    }else{
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] =$objVeh->msjError;         
        $conexBD->rollBack();             
    }

    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;    
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>