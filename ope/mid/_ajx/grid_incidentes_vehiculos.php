<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el indice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci?n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p?gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_vehiculos.class.php';

    $objSys = new System();
    $objDataGridVehiculos = new OpetblMidIncidentesVehiculos();

    //--------------------- Recepcion de parametros --------------------------//
    // b�squeda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {

        $sql_where = ' v.id_folio_incidente = ? '
                    . 'OR v.id_vehiculo LIKE ? '
                    . 'OR vr.vehiculo_rol LIKE ? '
                    . 'OR vc.vehiculo_categoria LIKE ? '
                    . 'OR vm.vehiculo_marca LIKE ? '
                    . 'OR v.tipo LIKE ? '
                    . 'OR v.color LIKE ? '
                    . 'OR v.numero_serie LIKE ? '
                    . 'OR v.numero_motor LIKE ? '
                    . 'OR v.numero_placas LIKE ? ';
        $sql_values = array($objSys->decrypt( $_GET["id_folio_incidente"] ),
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            );
   }else{
        $sql_where = ' v.id_folio_incidente = ? ';
        $sql_values = array($objSys->decrypt( $_GET["id_folio_incidente"] ) );
   }
    // Ordenaci?n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'v.id_vehiculo ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
         $sql_order = 'vr.vehiculo_rol '. $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'vc.vehiculo_categoria ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'vm.vehiculo_marca ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'v.tipo ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 6) {
        $sql_order = 'v.color ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 7) {
        $sql_order = 'v.numero_serie ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 8) {
        $sql_order = 'v.numero_motor ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 9) {
        $sql_order = 'v.numero_placas ' . $_GET['typeSort'];
    }
    // paginaci�n...
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];

    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];
    //------------------------------------------------------------------------//

    $datos = $objDataGridVehiculos->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];

    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $dato["id_vehiculo"]; //$objSys->encrypt($dato["id_vehiculo"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_vehiculo"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 3%;">';
                 $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 3%; font-size:13px; color:#FF0000;"><strong>'.$dato["id_vehiculo"].'</strong></td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["vehiculo_rol"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["vehiculo_marca"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["modelo"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["numero_placas"] . '</td>';
                $html .= '<td style="text-align: center; width: 13%;">' . $dato["numero_serie"] . '</td>';
                $html .= '<td style="text-align: center; width: 13%;">' . $dato["numero_motor"] . '</td>';
                    $url_del = "ope/mid/_reportes/reporte_incidente.pdf";
                $html .= '<td style="text-align: center; width: 9%;">';
                $html .= '  <a href="#" id="veh-' . $dato["id_vehiculo"] . '" class="lnkBtnOpcionGrid classModificar" title="Editar Incidente ..." ><img src="' . PATH_IMAGES . 'icons/edit24.png" alt="modificar" /></a>';
                $html .= '  <a href="' . $url_del . '" target="_blank" class="lnkBtnOpcionGrid" title="Imprimir Incidente ..." ><img src="' . PATH_IMAGES . 'icons/pdf24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//... '.$dato["hechos_text"] .'
       		$html .= '</tr>';
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; font-size:18px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron registros de incidentes ...';
        $html .= '</span>';
    } else {
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }

    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>