<?php
/**
 * Complemento ajax para obtener los datos de una persona involucrada en el incidente actual. 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/opetbl_mid_incidentes_personas.class.php';
    $objUsr  = new Usuario(); 
    $objSys  = new System();
    $objIncidPer = new OpetblMidIncidentesPersonas();
    
    if ($objIncidPer->delete($_GET["id"]) > 0) {
        $objSys->registroLog($objUsr->idUsr, "opetbl_mid_incidentes_personas", $_GET["id"], 'Dlt');
        $ajx_datos['rslt']  = true;
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['error'] = $objIncidPer->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>