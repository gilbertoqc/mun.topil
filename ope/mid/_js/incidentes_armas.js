var xGrid;
$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/

    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
        var wWindow = $(this).width();
        if( $(this).scrollTop() > toolbar_offset.top ){
            $('#dvHeader').addClass('fixedHead');
            toolbar.addClass('fixedToolBar');
        }
        else{
            $('#dvHeader').removeClass('fixedHead');
            toolbar.removeClass('fixedToolBar');
        }
    });


/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
/*** Ajusta el tama�o del contenedor del grid... ***/

    $('#dvGridArmas').css('height', ($(window).height() - 300) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGridArmas').xGrid({
        xColSort: 1,
        xTypeSort: 'Desc',
        xLoadDataStart:1,
        xRowsDisplay: 10,
        xTypeSelect: 0,
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xAjaxParam : "id_folio_incidente="+$('#id_folio_incidente').val(),
        xTypeDataAjax: 'json',
    });

/********************************************************************/
/** Convierte a may�sculas el contenido de los textbox y textarea. **/
/********************************************************************/
$('#txtNumeroSerie,#txtModelo,#txtMatricula').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });


/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/
/***** Aplicaci�n de las reglas de validaci�n de los campos del formulario *****/

  $('#frmIncidentesArmas').validate({
      rules: {
      // INICIO de Reglas de validacion para el formulario
        txtIdArma:{
            required:true,digits:true,
        },
        cbxIdRolArma:{
            required:true,min: 1,
        },
        cbxIdMarcaArma:{
            required:true,min: 1,
        },
        cbxIdTipoArma:{
            required:true,
            min: 1,
        },
        cbxIdCalibreArma:{
            required:true,min: 1,
        },
        txtNumeroSerie:{
            required:true,
        },
        txtModelo:{
            required:true,
        },
        txtMatricula:{
            required:true,
        },

      //***** FIN de Reglas de validacion para el formulario
     },
     messages:{
     //***** INICIO de Mensajes de validacion para el formulario
        txtIdArma:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            digits: '<span class="ValidateError" title="Este campo s�lo acepta digitos"></span>',
       },
        cbxIdRolArma:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        cbxIdMarcaArma:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        cbxIdTipoArma:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        cbxIdCalibreArma:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        txtNumeroSerie:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        txtModelo:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },
        txtMatricula:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
       },

     // FIN de Mensajes de validacion para el formulario
     },
      errorClass: "help-inline",
      errorElement: "span",
      highlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('success').addClass('error');
      },
        unhighlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('error').addClass('success');
      }
  //***** Fin de validacion
  });


/********************************************************************/
/***** Control del formulario para agregar/modifiar un arma *****/
/********************************************************************/

    $('#dvFormIncidentesArmas').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        modal: true,
        resizable: false,
        draggable: false,
        width: 700,
        buttons: {
            'Guardar Arma': function(){
                guardarArma();
            },
            'Cancelar': function(){
                $(this).dialog('close');
            }
        }
    });

    //mostrar el formulario de alta de armas
    $("#btnAgregar, .classModificar").live("click", function(e){

        if( $(this).attr('id') != 'btnAgregar' ){
            $("#id_arma").val( $(this).attr('id').substring(4) );
        }else{
            $("#id_arma").val('');
        }
        $.ajax({
            url: xDcrypt( $('#hdnUrlArm').val() ),
            dataType: 'json',
            data: { 'id_arma' : $("#id_arma").val(), 'id_folio_incidente' : $("#id_folio_incidente").val() },
            type: 'POST',
            async: true,
            cache: false,
            success: function ( xdata ) {
                $("#dvIncArmCont").html( xdata.html );
                $('#dvFormIncidentesArmas').dialog('option','title',"Datos del Arma");
                $('#dvFormIncidentesArmas').dialog('open');
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });

    });

    //-- Se asigna el estilo intercalado de cada fila...
    $('#dvGridArmas table.xGrid-tbBody tr:even').addClass('xGrid-tbBody-trWhite');
    $('#dvGridArmas table.xGrid-tbBody tr:odd').addClass('xGrid-tbBody-trGray');

    // Control para la carga din�mica de los calibres del arma
    $('#cbxIdTipoArma').live("change", function(e){
        obtenerCalibres( $("#cbxIdTipoArma").val() );
    });


/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/

/***** Fin del document.ready *****/
});

//funcion para guardar los datos de las armas
function guardarArma(){
    var valida = $('#frmIncidentesArmas').validate().form();
    if (valida) {
        var frmPreg = $(getHTMLMensaje('� Esta seguro de guardar los datos actuales ?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 380,
            buttons:{
                'Aceptar': function(){
                    $.ajax({
                        url: xDcrypt( $('#hdnUrlSave').val() ),
                        data: $('#frmIncidentesArmas').serialize(),
                        type: 'post',
                        dataType: 'json',
                        async: false,
                        cache: false,
                        success: function (xdata) {
                            // Verifica el resultado del proceso
                            if (xdata.rslt) {
                                frmPreg.dialog('close');
                                xGrid.refreshGrid();
                                $('#dvFormIncidentesArmas').dialog('close');
                            } else {
                                shwError('Error: ' + xdata.error);
                                frmPreg.dialog('close');
                            }
                        },
                        error: function(objeto, detalle, otroobj){
                            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
                        }
                    });
                },
                'Cancelar': function(){
                    frmPreg.dialog('close');
                }
            }
        });
    }
}

//funcion que obtiene los diferentes calibres
function obtenerCalibres( id_arma_tipo ){
    $.ajax({
        url: xDcrypt( $('#hdnUrlCalibres').val() ),
        data: {'id_arma_tipo': id_arma_tipo},
        dataType: 'json',
        type: 'post',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#cbxIdCalibreArma').html('<option>Cargando calibres...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#cbxIdCalibreArma').html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}