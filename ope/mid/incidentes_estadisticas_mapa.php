<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Incidencia Delictiva
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi?n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici?n de parámetros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Estadisticas Mapa',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<link type="text/css" href="ope/mid/_css/mid.css" rel="stylesheet"/>',
                                   ),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din?mico...
//-----------------------------------------------------------------//
  $urlRegresar = "index.php?m=" . $_SESSION["xIdMenu"];
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('-----');
  $urlMapa = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('incidentes_estadistica_mapa');

?>
  <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                    <a href="#" id="btnAnterior" class="Tool-Bar-Btn gradient" style="display: none; width: 60px;" title="Validar y continuar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back241.png" alt="" style="border: none;" /><br />Anterior
                    </a>
                    <a href="#" id="btnSiguiente" class="Tool-Bar-Btn gradient" style="display: none; width: 60px;" title="Estadisticas en Mapa...">
                        <img src="<?php echo PATH_IMAGES;?>icons/police_maker24.png" alt="" style="border: none;" /><br />Mapa
                    </a>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="display: none; width: 110px;" title="Guardar los datos del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlRegresar?>" id="btnRegresar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back24.png" alt="" style="border: none;" /><br />Regresar
                    </a>
                </td>
            </tr>
        </table>
     </div>

  <form id="frmPerfilCriminalAgregar" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Perfil" class="dvForm-Data">
            <span class="dvForm-Data-pTitle">
                <img src="<?php echo PATH_IMAGES;?>icons/pin_24.png" class="icono"/>
                Incidencia Delictiva
            </span>

    <!-- Contenido del Formulario -->
      <div id="mapaEstadisticas" class="mapaIncidente" style="height: 600px;">
      <!-- Aqui mapa para Georeferenciar Incidentes
        Agregar varios puntos con iconos diferentes para visualizar
        la incidencia delictiva en una demarcacion
      -->
      </div>

<!-- Fin del Contenido del Formulario -->
      </div>
</form>


<!-- Cargamos las librerias al final para obtimizar la caraga del modulo -->
  <script type="text/javascript" src="includes/js/locationpicker/locationpicker.jquery.js"></script>
  <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&libraries=places"></script>
  <script type="text/javascript" src="ope/mid/_js/incidentes_estadisticas_mapa.js"></script>

 <!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=es"></script>
  <script type="text/javascript" src="js/map.js"></script> -->

<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>