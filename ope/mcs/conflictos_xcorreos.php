<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo conflictos Sociales
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - ',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mcs/_css/mcs.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/"></script>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
  $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('');
?>

  <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                    <a href="#" id="btnAnterior" class="Tool-Bar-Btn gradient" style="display: none; width: 60px;" title="Validar y continuar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/back241.png" alt="" style="border: none;" /><br />Anterior
                    </a>
                    <a href="#" id="btnSiguiente" class="Tool-Bar-Btn gradient" style=" width: 60px;" title="Validar y continuar...">
                        <img src="<?php echo PATH_IMAGES;?>icons/next24.png" alt="" style="border: none;" /><br />Siguiente
                    </a>
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="display: none; width: 60px;" title="Guardar los datos del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                    </a>
                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 60px;" title="Cancelar la alta del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
    </div>

<form id="frmxcorreos" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
    <div id="dvForm-xcorreos" class="dvForm-Data">
      <span class="dvForm-Data-pTitle">
          <img src="<?php echo PATH_IMAGES;?>icons/add.png" class="icono"/>
          Agenda de Correos
      </span>

    <!-- Contenido del Formulario -->
    <fieldset id="fsetopetbl_mcs_conflictos_xcorreos" class="fsetForm-Data">
        <!--<legend>opetbl_mcs_xcorreos</legend>-->
       <table id="tbfrmopetbl_mcs_conflictos_xcorreos" class="tbForm-Data">
         <tr>
             <td class="descripcion"><label for="txtIdCorreo">IdCorreo:</label></td>
             <td class="validation">
                <input type="text" name="txtIdCorreo" id="txtIdCorreo" value="" maxlength="" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtCorreo">Correo:</label></td>
             <td class="validation">
                <input type="text" name="txtCorreo" id="txtCorreo" value="" maxlength="60" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtNombre">Nombre:</label></td>
             <td class="validation">
                <input type="text" name="txtNombre" id="txtNombre" value="" maxlength="50" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtApellidoPaterno">ApellidoPaterno:</label></td>
             <td class="validation">
                <input type="text" name="txtApellidoPaterno" id="txtApellidoPaterno" value="" maxlength="45" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
         <tr>
             <td class="descripcion"><label for="txtApellidoMaterno">ApellidoMaterno:</label></td>
             <td class="validation">
                <input type="text" name="txtApellidoMaterno" id="txtApellidoMaterno" value="" maxlength="45" class="" />
                <span class="pRequerido">*</span>
              </td>
         </tr>
       </table>
    </fieldset>

    <!-- Fin del Contenido del Formulario -->
    <p class="infoRequerida"><img alt="" src="<?php echo PATH_IMAGES;?>icons/Warning24.png" /> Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios y no podr� continuar hasta que los complete.</p>
  </div>
</form>
<?php
  //-----------------------------------------------------------------//
  //-- Bloque de cerrado de la plantilla...
  //-----------------------------------------------------------------//
$plantilla->paginaFin();
?>
