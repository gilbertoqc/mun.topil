<?php
/**
 * Complemento del llamado ajax para listar los municipios dentro de un combobox. 
 * @param int id_entidad, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/opecat_mcs_municipios.class.php';
    $objMunicipio = new OpecatMcsMunicipios();
    
    $datos = $objMunicipio->getMunicipios(0, 0, $_GET['id_region']);
    
    if (empty($objMunicipio->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objMunicipio->msjError;        
    }
    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>