<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Conflictos Sociales
 */
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/opetbl_mcs_conflictos.class.php';
$objConflictos = new OpetblMcsConflictos();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - ',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array( '<link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" media="all" rel="stylesheet">',
    			'<link href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" media="all" rel="stylesheet">',
                	'<link type="text/css" href="includes/js/timeline/css/styles.css" rel="stylesheet"/>',
                					'<script src="includes/js/timeline/js/main.js"></script>',
                					'<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.1/modernizr.min.js"></script>',
    '								<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">',
                                   '<link type="text/css" href="ope/mcs/_css/mcs.css" rel="stylesheet"/>'),
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
  $urlCancel = "index.php?m=" . $_SESSION["xIdMenu"];
  $urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('');
?>

  <div id="dvTool-Bar" class="dvTool-Bar">
        <table>
            <tr>
                <td class="tdNombreModulo">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td class="tdBotonesAccion">
                    <a href="#" id="btnAnterior" class="Tool-Bar-Btn gradient" style="display:none;width: 60px;" title="Agregar Nuevo Evento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Agregar
                    </a>

                    <a href="<?php echo $urlCancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 60px;" title="Cancelar la alta del nuevo elemento...">
                        <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                    </a>
                </td>
            </tr>
        </table>
  </div>



<div id="dvForm-TimeLine" class="dvForm-Data">
  <span class="dvForm-Data-pTitle">
      <img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" class="icono"/>
       Linea de Tiempo de Conflictos Sociales
  </span>

  <div class="timeline animated">
     <?php

     $conflictos=$objConflictos->selectFechas('2014-05-01','2014-05-16');

     var_dump($conflictos);


     for ($i=0; $i <10 ; $i++) {
     	echo ' <div class="timeline-row">
        <div class="timeline-time">
          Fecha: '.date("d-m-Y").'  Hora: '.date("H:m:s").'
        </div>
        <div class="timeline-icon">
          <div class="bg-primary">
            <i class="fa fa-pencil"></i>
          </div>
        </div>
        <div class="panel timeline-content">
          <div class="panel-body">
            <h2>
              This is a title for this timeline post
            </h2>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
            </p>
          </div>
        </div>
      </div>';
       }


     ?>

      <div class="timeline-row">
        <div class="timeline-time">
          <small>Oct 25</small>6:14 PM
        </div>
        <div class="timeline-icon">
          <div class="bg-warning">
            <i class="fa fa-quote-right"></i>
          </div>
        </div>
        <div class="panel timeline-content">
          <div class="panel-body">
            <blockquote>
              <p>
                Lorem ipsum velit ullamco anim pariatur proident eu deserunt laborum. Lorem ipsum ad in nostrud adipisicing cupidatat anim officia ad id cupidatat veniam quis elit ullamco.
              </p>
              <small>John Smith</small></blockquote>
          </div>
        </div>
      </div>
      <div class="timeline-row">
        <div class="timeline-time">
          <small>Oct 16</small>9:00 AM
        </div>
        <div class="timeline-icon">
          <div class="bg-info">
            <i class="fa fa-camera"></i>
          </div>
        </div>
        <div class="panel timeline-content">
          <div class="panel-body">
            <h2>
              This is an image posted on my timeline
            </h2>
            <img class="img-responsive" src="https://lorempixel.com/output/nature-q-c-640-480-10.jpg" />
            <p>
              Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
            </p>
          </div>
        </div>
      </div>
      <div class="timeline-row">
        <div class="timeline-time">
          <small>Oct 12</small>1:14 PM
        </div>
        <div class="timeline-icon">
          <div class="bg-primary">
            <i class="fa fa-video-camera"></i>
          </div>
        </div>
        <div class="panel timeline-content">
          <div class="panel-body">
            <h2>
              This is a title for this timeline post
            </h2>
            <div class="video-container">
              <iframe allowfullscreen="" frameborder="0" mozallowfullscreen="" src="https://player.vimeo.com/video/16202331?title=0&amp;byline=0&amp;portrait=0" webkitallowfullscreen=""></iframe>
            </div>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
            </p>
          </div>
        </div>
      </div>
      <div class="timeline-row">
        <div class="timeline-time">
          <small>Oct 10</small>4:53 PM
        </div>
        <div class="timeline-icon">
          <div class="bg-primary">
            <i class="fa fa-pencil"></i>
          </div>
        </div>
        <div class="panel timeline-content">
          <div class="panel-body">
            <h2>
              This is a title for this timeline post
            </h2>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
            </p>
          </div>
        </div>
      </div>
      <div class="timeline-row">
        <div class="timeline-time">
          <small>Oct 9</small>6:14 PM
        </div>
        <div class="timeline-icon">
          <div class="bg-warning">
            <i class="fa fa-quote-right"></i>
          </div>
        </div>
        <div class="panel timeline-content">
          <div class="panel-body">
            <blockquote>
              <p>
                Lorem ipsum velit ullamco anim pariatur proident eu deserunt laborum. Lorem ipsum ad in nostrud adipisicing cupidatat anim officia ad id cupidatat veniam quis elit ullamco.
              </p>
              <small>John Smith</small></blockquote>
          </div>
        </div>
      </div>
      <div class="timeline-row">
        <div class="timeline-time">
          <small>Oct 6</small>9:00 AM
        </div>
        <div class="timeline-icon">
          <div class="bg-info">
            <i class="fa fa-camera"></i>
          </div>
        </div>
        <div class="panel timeline-content">
          <div class="panel-body">
            <h2>
              This is an image posted on my timeline
            </h2>
            <img class="img-responsive" src="https://lorempixel.com/output/nature-q-c-640-480-10.jpg" />
            <p>
              Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
            </p>
          </div>
        </div>
      </div>
      <div class="timeline-row">
        <div class="timeline-time">
          <small>Oct 2</small>1:14 PM
        </div>
        <div class="timeline-icon">
          <div class="bg-primary">
            <i class="fa fa-video-camera"></i>
          </div>
        </div>
        <div class="panel timeline-content">
          <div class="panel-body">
            <h2>
              This is a title for this timeline post
            </h2>
            <div class="video-container">
              <iframe allowfullscreen="" frameborder="0" mozallowfullscreen="" src="https://player.vimeo.com/video/16202331?title=0&amp;byline=0&amp;portrait=0" webkitallowfullscreen=""></iframe>
            </div>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.
            </p>
          </div>
        </div>
      </div>
    </div>

</div>

<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>