<?php
/**
 * @author Markino
 * @copyright 2014
 * Modulo Conflictos Sociales
 */

//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Operativo - Conflictos Sociales',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<link type="text/css" href="ope/mcs/_css/mcs.css" rel="stylesheet"/>',
                                   '<script type="text/javascript" src="ope/mcs/_js/index.js"></script>'),
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>

<?php
    //-------------------------------- Barra de opciones ------------------------------------//
?>

    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 50%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 50%;">
                    <!-- Botones de opci�n... -->
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('conflictos_sociales_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Agregar Conflicto Social...">
                        <img src="includes/css/imgs/icons/add.png" alt="" style="border: none;" /><br />Agregar
                    </a>
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('conflictos_timeline');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Agenda Conflicto Social...">
                        <img src="includes/css/imgs/icons/edit_dat24.png" alt="" style="border: none;" /><br />Agenda
                    </a>
                    <!--
                    <a href="index.php?m=<?php echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('conflictos_timeline');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Agenda ...">
                        <img src="includes/css/imgs/icons/print_select.png" alt="" style="border: none;" /><br />Timeline
                    </a>
                    -->
                </td>
            </tr>
        </table>
    </div>

    <div id="dvGridConflictosSociales" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtBuscar" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" style="width: 250px;" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>
                    <th style="width: 3%; text-align: center;">&nbsp;</th>
                    <th style="width: 6%; text-align: center;" class="xGrid-tbCols-ColSortable">FOLIO</th>
                    <th style="width: 10%; text-align: center;"  class="xGrid-tbCols-ColSortable">REGION</th>
                    <th style="width: 10%; text-align: center;" class="xGrid-tbCols-ColSortable">MUNICIPIO</th>
                    <th style="width: 10%; text-align: center;" class="xGrid-tbCols-ColSortable">LOCALIDAD</th>
                    <th style="width: 6%; text-align: center;" class="xGrid-tbCols-ColSortable">FECHA</th>
                    <th style="width: 5%; text-align: center;" class="xGrid-tbCols-ColSortable">HORA INICIO</th>
                    <th style="width: 5%; text-align: center;" class="xGrid-tbCols-ColSortable">HORA FIN</th>
                    <th style="width: 10%; text-align: center;" class="xGrid-tbCols-ColSortable">MOVIMIENTO</th>
                    <th style="width: 10%; text-align: center;" class="xGrid-tbCols-ColSortable">ORGANIZACION</th>
                    <th style="width: 15%;" class="xGrid-thNo-Class">HECHOS</th>
                    <th style="width: 10%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
            <table class="xGrid-tbBody">

            </table>
        </div>
    </div>

    <input type="hidden" id="hdnUrlDatos" value="<?php echo $objSys->encrypt('ope/mcs/_ajx/grid_conflictos_listado.php');?>" />

<?php
    //-----------------------------------------------------------------//
    //-- Bloque de cerrado de la plantilla...
    //-----------------------------------------------------------------//
    $plantilla->PaginaFin();
?>