<?php
/**
 * @author Markinho
 * @date(01/04/2014)
 * Clase creada para agregar Funciones generales para los modulos Operativos
 * La intencion es no modificar los archivos de las clases generales
 * posteriomente reorganizar en un solo archivo todas las funciones
 */
class funciones
{
/**
 * Agregar: L=Left, R=Right
 * Funcion que permite obtener una cadena de una longitud definida agregando caracteres a la izquierda
 * dise�ada para los numeros de folios. Los parametros son:
 * $cadena=La cadena que se va a procesar generalmente numeros
 * $longitud=Longitud de la cadena a obtener por defecto 6
 * $caracter=Caracter que se agregara para completar la longitud requerida por defceto 0
 */
public function fillCad($cadena,$longitud=6,$caracter="0",$side="L"){
    $fs="";
	$l=strlen(trim($cadena));
        if($l>0 && $l<$longitud){
	       for($i=1;$i<=$longitud-$l;$i++){
           $fs.=$caracter;
	    }
           if($side=="L"){
              $fs.=$cadena;
           }else{
              $fs=$cadena.=$fs;
           }
        }
    return $fs;
}

public function calculaEdad($fechaNacimiento){
  // obtengo la marca de tiempo de la fecha de nacimiento
  $fecha=date_parse($fechaNacimiento);
  $dia=
  $mes=
  $annio=
  $edad=0;
  $fecha_nacimiento = mktime(0, 0, 0, $mes, $dia, $annio);

  // obtengo la marca de tiempo de la fecha actual
  $hoy = mktime();
  // obtengo la diferencia entre fecha de nacimiento y hoy
  $diferencia = $hoy - $fecha_nacimiento;
  //obtengo la edad
  $edad = $diferencia / (365 * 24 * 60 * 60); # a�os que pasaron entre 2 fechas

  return (int)$edad;
}


public function calculaFechaNacimiento($edad){
  // probable a�o de nacimiento de luciana
  $annio = (int)date('Y') - $edad;
  // probable fecha de nacimiento m�s antigua
  $probable_mas_antigua = date('d/m/') . ($annio-1);
  // probable fecha de nacimiento m�s reciente
  $probable_mas_reciente = date('d/m/') . $anio;
}





/**
*
*
*
*/
public function guardarArchivo($directorio,$archivo){
  /*
   * $resultado puede retornar los siguientes valores:
   * 0= Se ha guardado corerctamente el archivo
   * 1= No existe el directorio
   * 2= No puede guardarse el archivo
   */
  $resultado=0;

 //comparamos la existencia del directorio
  if(file_exists($directorio)) {
      if(file_exists($archivo)) {

      } else {
        $resultado=2;
      }
  } else {
    $resultado= 1;
  }

  return $resultado;
}



/**
*
*
*
*/

}//fin de la clase

?>