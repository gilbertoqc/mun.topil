<?php
/**
 * Instrucciones para guardar la iformacion enl atabla Perfiles Criminales
 * Este mismo archivo sera utilizado para Guerdar un nuevo Perfil y para editar un Perfil Existente
 *
 * Si el txtIdPerfil=0 se agregara un nuevo perfil
 * Si el txtIdPerfil<>0 se estara editando ese perfil
 * @author Markinho
 * @
 */
 //-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
$path = '';
include 'includes/class/opetbl_mpc_perfiles_criminales.class.php';
$objPerfilCriminal= new OpetblMpcPerfilesCriminales();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Perfil Criminal',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="ope/mpc/_js/perfil_criminal_rg.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Registro');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
<?php
    // Se asignan los valores a los campos
    // datos del incidente
    //unset($objPerfilCriminal);
    $objPerfilCriminal->id_perfil_criminal = $_POST["txtIdPerfilCriminal"];
    $objPerfilCriminal->id_situacion = $_POST["cbxIdSituacion"];
	$objPerfilCriminal->nombre = $_POST["txtNombre"];
	$objPerfilCriminal->apellido_paterno = $_POST["txtApellidoPaterno"];
	$objPerfilCriminal->apellido_materno = $_POST["txtApellidoMaterno"];
	$objPerfilCriminal->alias = $_POST["txtAlias"];
	$objPerfilCriminal->sexo = $xsexo=($_POST["rbnSexo"]==1)?"MASCULINO":"FEMENINO";
	$objPerfilCriminal->fecha_nacimineto = $objSys->convertirFecha($_POST["txtFechaNacimineto"], 'yyyy-mm-dd');
    $objPerfilCriminal->id_nacionalidad = $_POST["cbxNacionalidad"];
	$objPerfilCriminal->lugar_origen = $_POST["txtLugarOrigen"];
    $objPerfilCriminal->id_estado_civil = $_POST["cbxEstadoCivil"];
	$objPerfilCriminal->complexion = $_POST["cbxComplexion"];
	$objPerfilCriminal->ocupacion = $_POST["txtOcupacion"];
    $objPerfilCriminal->domicilio = $_POST["txtDomicilio"];
    $objPerfilCriminal->estatura = $_POST["txtEstatura"];
	$objPerfilCriminal->Peso = $_POST["txtPeso"];
	$objPerfilCriminal->cabello = $_POST["txtCabello"];
	$objPerfilCriminal->colorPiel = $_POST["txtColorPiel"];
	$objPerfilCriminal->ColorOjos = $_POST["txtColorOjos"];
    $objPerfilCriminal->edad = "00";
    
    /*Instruccione para mover la fotografia de uploads->fotografias*/
    if($_POST["hdnImgName"]!=''){
        if (rename ("ope/mpc/uploads/".$_POST["hdnImgName"],"ope/mpc/fotografias/".$_POST["txtNombre"]."_".$_POST["txtApellidoPaterno"]."_".$_POST["hdnImgName"])) {
            $objPerfilCriminal->fotografia_frente = $_POST["txtNombre"]."_".$_POST["txtApellidoPaterno"]."_".$_POST["hdnImgName"];
        } else {
            $objPerfilCriminal->fotografia_frente = "sin_fotografia.png";
        }      
    }else{
        $objPerfilCriminal->fotografia_frente = "sin_fotografia.png";
    }
    
    echo  $objPerfilCriminal->fotografia_frente . "<br>";
    echo "ID: " . $objPerfilCriminal->id_perfil_criminal;
    

  // Inicia la transacci�n
    if($_POST["oper"] == 1){
        $result = $objPerfilCriminal->update();
        $des_oper = "Upd";
        echo $result;
        //$url_atras = $objSys->encrypt("incidentes_panel");           
    }else{
        $result = $objPerfilCriminal->insert();
        $des_oper = "Ins";  
        //$url_atras = $objSys->encrypt("index");                
    } 
        
    //-------------------------------------------------------------------//            
    if ( $result ){
        $objSys->registroLog($objUsr->idUsr, 'opetbl_mpc_perfiles_criminales', $objPerfilCriminal->id_folio_incidente , $des_oper);
        $error = '';
    }else {
        $error = (!empty($objPerfilCriminal->msjError)) ? $objPerfilCriminal->msjError : 'Error al guardar el incidente.';        
    }
    
    $mod = (empty($error)) ? $url_atras : $objSys->encrypt("incidentes"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"];?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>