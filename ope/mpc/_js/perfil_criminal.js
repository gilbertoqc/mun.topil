$(document).ready(function(){
/********************************************************************/
/* Controla el scroll para seguir mostrando el header y el toolbar. */
/********************************************************************/
    var toolbar = $('#dvTool-Bar');
    var toolbar_offset = toolbar.offset();
    $(window).on('scroll', function(){
         var wWindow = $(this).width();
         if( $(this).scrollTop() > toolbar_offset.top ){
             $('#dvHeader').addClass('fixedHead');
             toolbar.addClass('fixedToolBar');
         }
         else{
             $('#dvHeader').removeClass('fixedHead');
             toolbar.removeClass('fixedToolBar');
         }
     });

/********************************************************************/
/************* Asignacion de funcionalidad a controles **************/
/********************************************************************/
 //Aplica la propiedad de ToolTip a todos los elementos que contengan la propiedad title
    $(document).tooltip({
          tooltipClass: "toolTipDetails",
          content: function () {
              return $(this).prop('title');
          },
          show:{
              effect: "clip",
              delay: 250
          },
          position: { my: "left center", at: "right center" }
      }).addClass("efecto-over");


    $('#cbxIdSituacion').focus();

    $('#cbxIdSituacion').change(function() {
       $('#txtNombre').focus();
    });

    $('#txtFechaNacimineto').datepicker({
        yearRange: 'c-80:c-10',
    });

    $('#txtFechaNacimineto').change(function() {
       $('#cbxEstadoCivil').focus();
    });

    // $('#txtFechaNacimineto').mask("99/99/9999",{placeholder:"_"})

/*
    $('#txtEstatura').mask("999.999",{placeholder:"_"});
    $('#txtPeso').mask("99.999",{placeholder:"_"});
*/


/*** Asignacion de la funcionalidad ajax para el div que contendra la fotografia ***/
    var url_img = xDcrypt($('#hdnUrlImg').val());
    //-- Script para cargar la Fotograf�a de Perfil Izquierdo...
    new AjaxUpload('#fotografia', {
        action: url_img + 'upload.php',
        onSubmit : function(file, ext){
            if (! (ext && /^(jpg)$/.test(ext))){
               // extensiones permitidas
               alert('Error: Solo se permiten imagenes con extension (.jpg)');
               // cancela upload
               return false;
            } else {
               $("#fotografia").html('<span style="display: block; font-size: 8pt; margin: auto auto; padding: 10px 1px 1px 1px;">Cargando imagen...</span>');
            }
        },
        onComplete: function(file, response){
            if( response === "upload_error" ){
                alert("Error in upload");
            } else{
                var imgHtml = '<img id="imgup" src="' + url_img + response + '" class="fotografia" />';
                $("#hdnImgName").val(response);
                //alert(response);
                $("#fotografia").empty();
                $("#fotografia").append(imgHtml);
            }
        }
    });


   // Inicializaci�n de los componentes Fecha
       // Control para la carga din�mica de municipios
    $('#cbxEntNacimiento').change(function(){
        obtenerMunicipios('cbxMpioNacimiento', $(this).val());
    });

    $('#cbxEntDomicilio').change(function(){
        obtenerMunicipios('cbxMpioDomicilio', $(this).val());
    });

    $('#cbxRegion').change(function(){
        obtenerMunicipios('cbxMpioAdscripcion', $(this).val());
    });

/********************************************************************/
/** Convierte a may�sculas el contenido de los textbox y textarea. **/
/********************************************************************/
    $('#txtNombre,#txtApellidoPaterno,#txtApellidoMaterno,#txtAlias,#txtLugarOrigen').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    $('#txtComplexion,#txtOcupacion,#txtDomicilio,#txtEstatura,#txtPeso,#txtCabello,#txtColorPiel,#txtColorOjos,#txtSenasPart').blur(function(){ $(this).val(xTrim( $(this).val().toUpperCase() )) });
    

/********************************************************************/
/********* Asignacion de validacion para los controles **************/
/********************************************************************/
$('#frmPerfilCriminal').validate({
    rules: {
    /***** INICIO de Reglas de validacion para el formulario de xxxxx *****/
        // txtIdPerfilCriminal:{
        //     required:true,
        // },
        cbxIdSituacion:{
            required:true,
            min:1,
        },
        txtNombre:{
            required:true,
        },
        txtApellidoPaterno:{
            required:true,
        },
        // txtApellidoMaterno:{
        //     required:true,
        // },
        txtAlias:{
            required:true,
        },
        rbnSexo:{
            required:true,
            min: 1,
        },
        // txtFechaNacimineto:{
        //     required:true,
        // },
        cbxNacionalidad:{
            required: true,
            min: 1,
        },
        // txtLugarOrigen:{
        //     required:true,
        // },
        cbxEstadoCivil:{
            required: true,
            min: 1,
        },
        // txtComplexion:{
        //     required:true,
        // },
        txtOcupacion:{
            required:true,
        },
        // txtDomicilio:{
        //     required:true,
        // },
         txtEstatura:{
             //required:true,
             number: true,
         },
         txtPeso:{
             //required:true,
             number: true,
         },
        // txtCabello:{
        //     required:true,
        // },
        // txtColorPiel:{
        //     required:true,
        // },
        // txtColorOjos:{
        //     required:true,
        // },
    /***** FIN de Reglas de validacion para el formulario de xxxxx *****/
    },
    messages:{
    /***** INICIO de Mensajes de validacion para el formulario de xxxxx *****/
        txtIdPerfilCriminal:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>'
        },
        cbxIdSituacion:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtNombre:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtApellidoPaterno:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtApellidoMaterno:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtAlias:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        rbnSexo:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtFechaNacimineto:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxNacionalidad:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError"title="Este campo es obligatorio"></span>',
        },
        txtLugarOrigen:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        cbxEstadoCivil:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtComplexion:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtOcupacion:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtDomicilio:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtEstatura:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            number: '<span class="ValidateError" title="Este campo s�lo acepta numeros"></span>',
        },
        txtPeso:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            number: '<span class="ValidateError" title="Este campo s�lo acepta numeros"></span>',
        },
        txtCabello:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
            min: '<span class="ValidateError"title="Este campo es obligatorio"></span>',
           
       }  ,
        txtColorPiel:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        txtColorOjos:{
            required: '<span class="ValidateError" title="Este campo es obligatorio"></span>',
        },
        
    /***** FIN de Mensajes de validacion para el formulario de xxxxx *****/
    },
    errorClass: "help-inline",
    errorElement: "span",
    highlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('success').addClass('error');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).parents('.validation').removeClass('error').addClass('success');
    }
});



/********************************************************************/
/****** Asignacion de funcionalidad de los Botones de Acction  ******/
/********************************************************************/
// Acci�n del bot�n Guardar
$('#btnGuardar').click(function(){
    var valida = $('#frmPerfilCriminal').validate().form();

    if (valida) {
        var frmPreg = $(getHTMLMensaje('�Est� seguro de guardar los cambios realizados a Datos Criminales?', 2));
        frmPreg.dialog({
            autoOpen: true,
            modal: true,
            resizable: false,
            width: 350,
            buttons:{
                'Aceptar': function(){
                    $('#frmPerfilCriminal').submit();
                },
                'Cancelar': function(){
                    $(this).dialog('close');
                }
            }
        });
    }
});


/***** Fin del document.ready *****/
});



function obtenerMunicipios(contenedor, id){
    var tp_filtro = ( contenedor != 'cbxMpioAdscripcion' ) ? 1 : 2;
    $.ajax({
        url: xDcrypt($('#hdnUrlMpio').val()),
        data: {'id_entidad': id, 'id_region': id, 'filtro': tp_filtro},
        dataType: 'json',
        async: true,
        cache: false,
        beforeSend: function () {
            $('#' + contenedor).html('<option>Cargando municipios...</option>');
        },
        success: function (xdata) {
            if (xdata.rslt)
                $('#' + contenedor).html(xdata.html);
            else
                shwError(xdata.error);
        },
        error: function(objeto, detalle, otroobj){
            shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
        }
    });
}