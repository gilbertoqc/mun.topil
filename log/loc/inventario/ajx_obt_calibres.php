<?php
/**
 * Complemento del llamado ajax para listar los calibres dentro de un combobox. 
 * @param int id_marca, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/logcat_arm_calibre.class.php';
    $objCal = new LogcatArmCalibre();
    
    $datos = '<option value="0"></option>';
    if ($_GET["id_modelo"] != 0){
        $datos .= $objCal->shwCalibres($_GET["id_calibre"], $_GET['id_modelo']);
    }
    if (empty($objCal->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objCal->msjError;        
    }
    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>