<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla de armamento en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_arm_armamento.class.php';
    $objSys = new System();
    $objArm = new LogtblArmArmamento();

    //--------------------- Recepci�n de par�metros --------------------------//
    // B�squeda...
    //$sql_where = ' a.matricula NOT IN ( select matricula from logtbl_arm_baja)';
    //$sql_where = ' a.id_situacion = 1 OR a.id_situacion = 3 ';
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        $sql_where .= '( a.matricula LIKE ? '
                   . ' OR a.serie LIKE ? '
                   . ' OR m.marca LIKE ? '
                   . ' OR mo.modelo LIKE ? '
                   . ' OR cal.calibre LIKE ? '
                   . ' OR fd.foliod LIKE ? '
                   . ' OR l.loc LIKE ? ) ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%');
    }
    // Ordenaci�n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'a.matricula ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 2) {
        $sql_order = 'a.serie ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 3) {
        $sql_order = 'm.marca ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 4) {
        $sql_order = 'mo.modelo ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 5) {
        $sql_order = 'cal.calibre ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 6) {
        $sql_order = 'fd.foliod ' . $_GET['typeSort'];
    }else if ($ind_campo_ord == 7) {
        $sql_order = 'l.loc ' . $_GET['typeSort'];
    }   
    // Paginaci�n...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
       
    $datos = $objArm->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $objSys->encrypt($dato["matricula"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["matricula"] . '">';
       			//--------------------- Impresion de datos ----------------------//
                $html .= '<td style="text-align: center; width: 3%;">';
                   $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                $html .= '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["matricula"] . '</td>';               
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["serie"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["marca"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["modelo"] . '</td>';
                $html .= '<td style="text-align: center; width: 16%;">' . $dato["calibre"] . '</td>';
                $html .= '<td style="text-align: center; width:  8%;">' . $dato["foliod"] . '</td>';
				$html .= '<td style="text-align: center; width:  8%;">' . $dato["loc"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">';
                $html .= '  <a href="#" class="lnkBtnOpcionGrid classModificar" id="Mod-' . $dato["matricula"] .  '" title="Actualizar los datos..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';                                
                $html .= '  <a href="#" class="lnkBtnOpcionGrid classBaja" id="Baj-' . $dato["matricula"] .  '" title="Dar de baja..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';  		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		$html .= 'No se encontraron matriculas en esta consulta...';
        $html .= '</span>';
    } else {                
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>