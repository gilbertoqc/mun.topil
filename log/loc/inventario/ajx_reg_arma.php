<?php
/**
 * Complemento ajax para guardar los datos del armamento 
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/logtbl_arm_armamento.class.php';
    include $path . 'includes/class/logtbl_arm_alta.class.php';
    include $path . 'includes/class/config/system.class.php';
    $objSys  = new System();  
    $objUsr  = new Usuario();  
    $objArm  = new LogtblArmArmamento();
    $objAlta = new LogtblArmAlta();        
    
    // Datos del armamento
    $objArm->matricula = strtoupper( $_POST["txtMatricula"] );
    $objArm->serie = strtoupper( $_POST["txtSerie"] );
    $objArm->id_loc = $_POST["cbxLicencia"];
    //$objArm->id_marca = $_POST["cbxTipo"];
    //$objArm->id_clase = $_POST["cbxClase"];
    //$objArm->id_marca = $_POST["cbxMarca"];
    //$objArm->id_modelo = $_POST["cbxModelo"];
    //$objArm->id_calibre = $_POST["cbxCalibre"];
    $objArm->id_foliod = $_POST["cbxFolioc"];
    //$objArm-> = $_POST["cbxFoliod"];
    $objArm->id_situacion = $_POST["cbxSituacion"];
    $objArm->id_propietario = $_POST["cbxPropietario"];
    //$objArm->id_tipo_dependencia = $_POST["cbxTipoDep"];
    //$objArm->id_dependencia = $_POST["cbxDep"];
    //$objArm->id_instituciones = $_POST["cbxInst"];       
    $objArm->id_estado = $_POST["cbxEstado"];  
    $objArm->id_estatus = $_POST["cbxEstatus"];
    $objArm->id_motivo_movimiento = $_POST["cbxMotivoMov"];
    $objArm->id_marca = $_POST["cbxMarca"];      
    //$objArm->id_ubicacion = $_POST["cbxUbicacion"];
    
    $result = ($_POST["dtTypeOper"] == 1) ? $objArm->update() : $objArm->insert();
    
    $datos = $objArm->selectAllGrid('a.matricula NOT IN ( select matricula from logtbl_arm_baja)', $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    if ($result) {
        $objSys->registroLog($objUsr->idUsr, 'logtbl_arm_armamento', $objArm->matricula, "Ins");
        $objAlta->matricula = $objArm->matricula;
        $objAlta->usuario   = $objUsr->idUsr;
        $objAlta->insert();
        $ajx_datos['rslt']  = true;
        
        // Se obtienen las ultimas altas de armamento para el grid principal de resultados.
        $datos = $objArm->selectAll("a.matricula='" . strtoupper( $_POST["txtMatricula"] ) . "'", "a.matricula Asc");
        $html = '<table class="xGrid-tbBody" id="dvGrid-Armrs-tbBody">';
        foreach( $datos As $reg => $dato ){            
                
                $html .= '<tr id="' . $nombreGrid . '-' . $dato["matricula"] . '">';
           			//--------------------- Impresion de datos ----------------------//
                    $html .= '<td style="text-align: center; width: 3%;">';
                       $html .= '<input type="radio" name="' . $nombreGrid . '-rbId" />';               
                    $html .= '</td>';
                    $html .= '<td style="text-align: center; width: 17%;">' . $dato["matricula"] . '</td>';               
                    $html .= '<td style="text-align: center; width: 15%;">' . $dato["serie"] . '</td>';
                    $html .= '<td style="text-align: center; width: 20%;">' . $dato["marca"] . '</td>';
                    $html .= '<td style="text-align: center; width: 15%;">' . $dato["foliod"] . '</td>';
    				$html .= '<td style="text-align: center; width: 15%;">' . $dato["loc"] . '</td>';
    //$url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("_edit") . "&id=" . $id_crypt;
    //$url_baja = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("vehiculo_baja") . "&id=" . $id_crypt;
                    $html .= '<td style="text-align: center; width: 15%;">';
                    $html .= '  <a href="#" class="lnkBtnOpcionGrid" id="btnModifica" title="Modificar caracteristicas del arma..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';                                
                    $html .= '  <a href="#" class="lnkBtnOpcionGrid" id="btnBaja" title="Dar de baja el arma..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                    $html .= '</td>';
                 	//---------------------------------------------------------------//
           		$html .= '</tr>'; 
        }        
        $html .= '</table>';
        
        $ajx_datos['html']  = utf8_encode($html);
        $ajx_datos["total"] = $totalReg;
        //$ajx_datos['campos']= $objArm->matricula . "-" . $objArm->id_serie . "-" . $objArm->id_loc . "-" . $objArm->id_foliod . "-" .  $objArm->id_situacion . "-" . $objArm->id_propietario . "-" . $objArm->id_estado . "-" . $objArm->id_estatus . "-" . $objArm->id_motivo_movimiento . "-" . $objArm->id_marca . "-->" . $_POST["dtTypeOper"];
        $ajx_datos['error'] = '';
            
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos["total"] = 0;
        $ajx_datos['error'] = $objArm->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>