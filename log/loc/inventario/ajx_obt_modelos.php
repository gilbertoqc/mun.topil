<?php
/**
 * Complemento del llamado ajax para listar los modelos dentro de un combobox. 
 * @param int id_calibre, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/logcat_arm_modelo.class.php';
    $objMod = new LogcatArmModelo();
    
    $datos = '<option value="0"></option>';
    if ($_GET["id_calibre"] != 0){
        $datos .= $objMod->shwModelos( 0, $_GET["id_calibre"] );
    }
    if (empty($objMod->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    } else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objMod->msjError;        
    }
    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>