<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    include $path . 'includes/class/logtbl_arm_armamento.class.php';
    include $path . 'includes/class/logcat_arm_tipo.class.php';
    include $path . 'includes/class/logcat_arm_clase.class.php';
    include $path . 'includes/class/logcat_arm_modelo.class.php';
    include $path . 'includes/class/logcat_arm_folioc.class.php';    
    
    $objArm             = new LogtblArmArmamento();
    $objTipo            = new LogcatArmTipo();
    $objClase           = new LogcatArmClase();
    $objModelo          = new LogcatArmModelo();
    $objFolioc          = new LogcatArmFolioc();    
    
    //variables recibidas
    $oper = $_POST["oper"]; 
    if( $oper == 1 ){   
        $datos = $objArm->select( $_POST["matricula"] );        
    }

    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
                  
    $html .='<form id="frmAlta" method="post" enctype="multipart/form-data">';    
            $html .='<div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 350px; width: auto;">';                         
                //registro de armamento
                $html .='<table id="tbForm-Alta" class="tbForm-Data" style="width: 600px;">'; 
                    $html .='<tr>';
                        $html .='<td><label for="txtMatricula">Matricula:</label></td>';
                        $html .='<td class="validation" style="width: 400px;">';
                            $html .='<input type="text" name="txtMatricula" id="txtMatricula" value="' . $objArm->matricula . '" maxlength="12" title="..." style="padding: 5px; width: 210px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                                 
                    $html .='<tr>';
                        $html .='<td><label for="txtSerie">Serie:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<input type="text" name="txtSerie" id="txtSerie" value="' . $objArm->serie . '" maxlength="20" title="..." style="width: 200px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td><label for="cbxLicencia">Licencia:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxLicencia" id="cbxLicencia" title="">';
                                $html .='<option value="0">&nbsp;</option>';                                
                                $html .= $objArm->LogcatArmLoc->shwLicencias( $objArm->id_loc );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td><label for="cbxTipo">Tipo:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxTipo" id="cbxTipo" title="">';
                                $html .='<option value="0">&nbsp;</option>';
$html .= $objTipo->shwTipos( $objClase->getIdTipo( $objArm->LogcatArmMarca->getIdClase( $objArm->id_marca ) ) );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                                        
                    $html .='<tr>';
                        $html .='<td><label for="cbxClase">Clase:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxClase" id="cbxClase" title="">';
                                $html .='<option value="0">&nbsp;</option>';                                
$html .= $objClase->shwClases( $objArm->LogcatArmMarca->getIdClase( $objArm->id_marca ), $objClase->getIdTipo( $objArm->LogcatArmMarca->getIdClase( $objArm->id_marca ) ) );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                    
                    $html .='<tr>';
                        $html .='<td><label for="cbxCalibre">Calibre:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxCalibre" id="cbxCalibre" title="">';
                                $html .='<option value="0">&nbsp;</option>';
$html .= $objModelo->LogcatArmCalibre->shwCalibres( $objModelo->getIdCalibre( $objArm->LogcatArmMarca->getIdModelo( $objArm->id_marca ) ) );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                    
                    $html .='<tr>';
                        $html .='<td><label for="cbxModelo">Modelo:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxModelo" id="cbxModelo" title="">';
                                $html .='<option value="0">&nbsp;</option>';                                
$html .= $objArm->LogcatArmMarca->LogcatArmModelo->shwModelos( $objArm->LogcatArmMarca->getIdModelo($objArm->id_marca), $objModelo->getIdCalibre( $objArm->LogcatArmMarca->getIdModelo( $objArm->id_marca ) ) );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td><label for="cbxMarca">Marca:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxMarca" id="cbxMarca" title="">';
                                $html .='<option value="0">&nbsp;</option>';
$html .= $objArm->LogcatArmMarca->shwMarcas( $objArm->id_marca, $objArm->LogcatArmMarca->getIdClase( $objArm->id_marca ), $objArm->LogcatArmMarca->getIdModelo( $objArm->id_marca ) );                            
                                $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                                                             
                    $html .='<tr>';
                        $html .='<td><label for="cbxFolioc">Folio C:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxFolioc" id="cbxFolioc" title="">';
                                $html .='<option value="0">&nbsp;</option>';
$html .= $objFolioc->shwFolioc( $objArm->LogcatArmFoliod->getIdFolioc( $objArm->id_foliod ) );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                     $html .='<tr>';
                        $html .='<td><label for="cbxFoliod">Folio D:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxFoliod" id="cbxFoliod" title="">';
                                $html .='<option value="0">&nbsp;</option>';                                
$html .= $objArm->LogcatArmFoliod->shwFoliod($objArm->id_foliod, $objArm->LogcatArmFoliod->getIdFolioc( $objArm->id_foliod )  );
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                                        
                    $html .='<tr>';
                        $html .='<td><label for="cbxPropietario">Propietario:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxPropietario" id="cbxPropietario" title="">';
                                $html .='<option value="0">&nbsp;</option>';                            
                                $html .= $objArm->LogcatArmPropietario->shwPropietario( $objArm->id_propietario );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';  
                    $html .='<tr>';
                        $html .='<td><label for="cbxSituacion">Situacion:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxSituacion" id="cbxSituacion" title="">';
                                //$html .='<option value="0">&nbsp;</option>';                            
                                //$html .= $objArm->LogcatArmSituacion->shwSituacion( $objArm->id_situacion );
                                //VALOR POR DEFECTO AL PONER DE ALTA
                                $html .='<option value="3" selected="true">DEPOSITO</option>';                                                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">POR DEFECTO</span>';
                        $html .='</td>';
                    $html .='</tr>';                  
                    $html .='<tr>';
                        $html .='<td><label for="cbxEstado">Estado:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxEstado" id="cbxEstado" title="">';
                                //$html .='<option value="0">&nbsp;</option>';                                
                                //$html .= $objArm->LogcatArmEstado->shwEstado( $objArm->id_estado );
                                //VALOR POR DEFECTO AL PONER DE ALTA
                                 $html .='<option value="3" selected="true">UTIL</option>';                                                               
                            $html .='</select>';
                            $html .='<span class="pRequerido">POR DEFECTO</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td><label for="cbxEstatus">Estatus:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxEstatus" id="cbxEstatus" title="">';
                                //$html .='<option value="0">&nbsp;</option>';                                
                                //$html .= $objArm->LogcatArmEstatus->shwEstatus( $objArm->id_estatus );
                                $html .='<option value="4" selected="true">REGULARIZADA</option>';                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">POR DEFECTO</span>';
                        $html .='</td>';
                    $html .='</tr>';
                     $html .='<tr>';
                        $html .='<td><label for="cbxMotivoMov">Motivo movimiento:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxMotivoMov" id="cbxMotivoMov" title="">';
                                //$html .='<option value="0">&nbsp;</option>';                                
                                //$html .= $objArm->LogcatArmMotivoMovimiento->shwMotivoMov( $objArm->id_motivo_movimiento );
                                $html .='<option value="1">ARMA NUEVA</option>';                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">POR DEFECTO</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='</tr>';
                $html .='</table>';                                                  
            $html .='</div>';
            $html .='<p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>';                        
        $html .= '<input type="hidden" id="dtTypeOper" name="dtTypeOper" value="' . $oper . '" />';
    $html .='</form>';
  

    // Formatea los datos y los envia al grid...
    $ajx_datos["html"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>