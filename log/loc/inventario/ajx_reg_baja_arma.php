<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    require_once $path . 'includes/class/config/mysql.class.php';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_arm_baja.class.php';
    
    $conexBD = new MySQLPDO();
    $objSys  = new System();
    $objUsr  = new Usuario();
    //$objArm  = new LogtblArmArmamento();    
    $objBaja = new LogtblArmBaja();
    
    // Datos del armamento
    $objBaja->matricula = strtoupper( $_POST["txtMatricula"] );
    $objBaja->oficio = strtoupper( $_POST["txtOficio"] );
    $objBaja->fecha_oficio = $objSys->convertirFecha( $_POST["txtFechaOficio"], "yyyy-mm-dd" );
    $objBaja->id_motivo= $_POST["cbxMotivo"];
    //$objBaja->fecha_baja = $objSys->convertirFecha( $_POST["txtFechaBaja"], "yyyy-mm-dd" );
    $objBaja->id_usuario = $objUsr->idUsr;
        
    // Inicia la transacci�n    
    $conexBD->beginTransaction();                 
    
    if( $objBaja->insert() > 0 ){
        //la funcion $objBaja->update_baja() actualiza datos en la tabla de armamento a la hora de la baja.
        if ( $objBaja->update_baja() ){        
            $objSys->registroLog($objUsr->idUsr, 'logtbl_arm_baja', $objBaja->matricula, "Ins");
            $conexBD->commit();
            $ajx_datos['rslt']  = true;            
            $ajx_datos['html']  = utf8_encode($html);            
            $ajx_datos['error'] = '';
        }else{
            $conexBD->rollBack();
            $error = (!empty($objDet->msjError)) ? $objDet->msjError : 'Error al guardar los datos.';
            $ajx_datos['rslt']  = false;
            $ajx_datos['html']  = '';
        }
            
    } else {
        $conexBD->rollBack();
        $error = (!empty($objDet->msjError)) ? $objDet->msjError : 'Error al guardar los datos.';
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';  
        //$ajx_datos['campos']= $objBaja->matricula . "-" .$objBaja->oficio. "-" .$objBaja->fecha_oficio . "-" .$objBaja->id_motivo. "-" .$objBaja->fecha_baja . "-" .$objBaja->id_usuario;
        $ajx_datos['error'] = $objBaja->msjError;
    }
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>