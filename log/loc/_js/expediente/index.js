var xGrid;
$(document).ready(function(){
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Personal').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Personal').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });    
    
    //-- Definici�n del dialog como formulario para portacion de armas....
    $('#dvExp').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 800,
        height: 540,
        modal: true,
        resizable: false,
        draggable:false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Controla el bot�n para visualizar el formulario de portacion de armas...
    $(".classExp").live("click", function(e){
        var ID = $(this).attr("id");
        $.ajax({
            url: xDcrypt( $('#hdnUrlExp').val() ),  
            data: { 'curp': ID.substring(4) },          
            dataType: 'json',
            type: 'POST',
            async: true,
            cache: false,            
            success: function (xdata) {                    
                    $('#dvExpCont').html(xdata.html);                    
                    $('#dvExp').dialog('option','title',xdata.titulo);
                    $('#dvExp').dialog('open');                    
            },
            error: function(objeto, detalle, otroobj){
                shwError(detalle.toUpperCase() + ': ' + otroobj, 450);
            }
        });
    }); 

    $('#xReportes').click(function(){
        $('#dvForm-Registro').dialog('open'); 
    });

});