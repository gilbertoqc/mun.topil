<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/admtbl_adscripcion.class.php';
$objAds = new AdmtblAdscripcion();

//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Control de Armamento',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/xgrid.js"></script>',
                                   '<script type="text/javascript" src="log/loc/_js/asignacion/index.js"></script>'),                
                'header' => true,
                'menu' => true,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
    //se seleccionan los datos personales
    $objAds->AdmtblDatosPersonales->select( $curp );
    //se seleccionan los datos de la adscripcion
    $datosAds = $objAds->selectAll( "a.curp='" . $curp . "'", '', '' );
    $objPer = $objAds->AdmtblDatosPersonales;   

    //-------------------------------- Barra de opciones ------------------------------------//
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 60%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 40%;">
                    <!-- Botones de opci�n... 
                    <a href="index.php?m=<?php //echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('persona_add');?>" id="xRegistrar" class="Tool-Bar-Btn" style="" title="Ver la portaci&oacute;n de armamento...">
                        <img src="<?php //echo PATH_IMAGES;?>icons/add.png" alt="" style="border: none;" /><br />Registrar
                    </a>
                    -->
                    <!--
                    <a href="index.php?m=<?php //echo $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('form_ficha_crim');?>" id="xReportes" class="Tool-Bar-Btn" style="" title="Ir al �ndice de reportes...">
                        <img src="<?php //echo PATH_IMAGES;?>icons/report24.png" alt="" style="border: none;" /><br />Reportes
                    </a>
                    -->
                </td>
            </tr>
        </table>
    </div>

    <div id="dvGrid-Personal" style="border: none; height: 350px; margin: auto auto; margin-top: 10px; width: auto;">
        <div class="xGrid-dvHeader gradient">
            <table class="xGrid-tbSearch">
                <tr>
                    <td>Buscar: <input type="text" name="txtSearch" size="25" value="<?php if( isset($_SESSION['xBuscarGrid']) ) echo $_SESSION['xBuscarGrid'];?>" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                </tr>
            </table>
            <table class="xGrid-tbCols">
                <tr>  
                    <th style="width:  3%; text-align: center;">&nbsp;</th>
                    <th style="width: 20%; text-align: center;" class="xGrid-tbCols-ColSortable">NOMBRE COMPLETO</th>  
                    <th style="width: 10%; text-align: center;" class="xGrid-tbCols-ColSortable">C.U.R.P.</th>
                    <th style="width: 20%; text-align: center;" class="xGrid-tbCols-ColSortable">ESPECIALIDAD</th>
                    <th style="width: 17%; text-align: center;" class="xGrid-tbCols-ColSortable">CATEGOR�A</th>
                    <th style="width: 25%; text-align: center;" class="xGrid-tbCols-ColSortable">ADSCRIPCI�N</th>
                    <th style="width:  5%;" class="xGrid-thNo-Class">&nbsp;</th>
                </tr>
            </table>
        </div>
        <div class="xGrid-dvBody">
        
        </div>        
    </div>
    
    <!-- CONTENEDOR PARA LA EL FORMULARIO DE ARMAS ASIGNADAS ------------- -->
    <div id="dvArmas" style="display: none;" title="Portaci�n de Armamento">    
        
        <!-- FICHA DEL ELEMENTO CON FOTOGRAFIA MINIATURA -->
        <div id="dvDatos" style="margin: auto auto; margin-top: 10px; min-height: 100px; width: auto;"></div>
            
        <!-- div para el boton de asignaciones -->
        <div id="dvAgregar" style="margin: auto auto; margin-top: 10px; min-height: 15px; width: auto; position: relative;">
            <table style="width: 100%; border: 0px;">
                <tr>
                <td style="width: 95%;">&nbsp;</td>
                <td style="text-align: center; width:  5%;">  
                    <a href="#" class="lnkBtnOpcionGrid classBusca" title="Asignar armamento..."> 
                    <img src="<?php echo PATH_IMAGES; ?>icons/add.png" alt="Asignar armamento." />Asignar</a>
                </td>
                </tr>
            </table>
        </div>
            
        <!-- PORTACION DE ARMAMENTO DEL ELEMENTO EN CUESTION -->
        <div id="dvGrid-Portacion" style="border: none; height: 240px; margin: auto auto; margin-top: 10px; width: auto;">
            <div class="xGrid-dvHeader gradient">
                <table class="xGrid-tbCols">
                    <tr>
                        <th style="width:  4%; text-align: center;" >No.</th>
                        <th style="width: 12%;" >MATRICULA</th>
                        <th style="width:  7%;" >TIPO</th>  
                        <th style="width: 15%;" >MARCA</th>
                        <th style="width: 19%;" >MODELO</th>
                        <th style="width: 19%;" >CLASE</th>                                                        
                        <th style="width: 19%;" >CALIBRE</th>
                        <th style="width:  5%;" >&nbsp;</th>                                                         
                    </tr>
                </table>
                
            </div>
            
            <div class="xGrid-dvBody">
            <!-- aqui van datos de las armas -->
            </div>
        </div>                                 
            
        <!-- CONTENEDOR PARA EL FORMULARIO PARA ASIGNAR UN ARMA NUEVA ------------- -->
        <div id="dvBuscar" style="display: none;" title="Portaci�n de Armamento">    
            <div id="dvBuscarCont" class="dvForm-Data" style="border-bottom: 2px double #488ac7; min-height: 280px;  min-width: 650px; width: 910px; overflow: hidden;">
            
                <!-- PORTACION DE ARMAMENTO DEL ELEMENTO EN CUESTION. -->                                                                               
                <div class="xGrid-dvHeader gradient">
                    <table class="xGrid-tbSearch">
                        <tr>
                            <td>Buscar: <input type="text" name="txtSearch" size="25" value="" /> <a href="#" title="Buscar..." class="xGrid-tbSearch-btnSearch"></a></td>
                        </tr>
                    </table>
                
                    <table class="xGrid-tbCols">
                        <tr>                                                      
                            <th style="width: 10%; text-align: center;">MATRICULA</th>  
                            <th style="width:  6%; text-align: center;">TIPO</th>                        
                            <th style="width: 15%; text-align: center;">MARCA</th>                                                                                
                            <th style="width: 20%; text-align: center;">MODELO</th>
                            <th style="width: 19%; text-align: center;">CLASE</th>
                            <th style="width: 20%; text-align: center;">CALIBRE</th>
                            <th style="width:  5%; text-align: center;">&nbsp;</th>                                                                            
                        </tr>
                    </table>
                </div>
                <div class="xGrid-dvBody" id="divBusca">                                       
                    <!--  aqui de despliegan las armas buscadas -->
                </div> 
            
            </div>
        </div>
                     
        <!-- datos necesarios para pasarlos por formulario                                                    
        $html.='<input type="hidden" id="hdnUrlCurp"        value="' . $objSys->encrypt( $curp ) . '" /> -->
        <input type="hidden" id="hdnUrlCurp" />
        <input type="hidden" id="hdnUrlObtArmas"    value="<?php echo $objSys->encrypt('log/loc/asignacion/ajx_obt_armas.php'); ?>" />
        <input type="hidden" id="hdnUrlObtArmAsig"  value="<?php echo $objSys->encrypt('log/loc/asignacion/ajx_obt_arm_asig.php'); ?>" />
        <input type="hidden" id="hdnUrlOperArma"    value="<?php echo $objSys->encrypt('log/loc/asignacion/ajx_oper_arma.php'); ?>" />
        <input type="hidden" id="hdnUrlBuscar"      value="<?php echo $objSys->encrypt('log/loc/asignacion/ajx_frm_buscar.php'); ?>" />
        <input type="hidden" id="hdnUrlDatos"       value="<?php echo $objSys->encrypt('log/loc/asignacion/ajx_obt_datos_grid.php');?>" />
        <input type="hidden" id="hdnUrlObtDatosPer" value="<?php echo $objSys->encrypt('log/loc/asignacion/ajx_obt_datos_per.php');?>" />              
    </div>            
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>