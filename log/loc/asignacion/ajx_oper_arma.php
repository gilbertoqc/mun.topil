<?php
/**
 * Complemento ajax para guardar los datos del armamento  en la tabla de bajas
 * 
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/users.class.php';
    include $path . 'includes/class/config/system.class.php';
    require_once $path . 'includes/class/config/mysql.class.php';
    include $path . 'includes/class/logtbl_arm_asignacion.class.php';
    include $path . 'includes/class/logtbl_arm_historial_asigna_descarga.class.php';
    
    $objSys  = new System();
    $objUsr  = new Usuario(); 
    $conexBD = new MySQLPDO();       
    $objAsig = new LogtblArmAsignacion();
    $objHist = new LogtblArmHistorialAsignaDescarga();

    // Datos del armamento
    $objAsig->matricula = $_POST["matricula"];
    $objAsig->curp      = $_POST["curp"];                 

    //se lleva acabo la operacion deseada de insercion o borrado del registro segun se requiera 
    //usando el parametro de oper.
    // Inicia la transacci�n
    $conexBD->beginTransaction();
    $result = $_POST["oper"] == '1' ? $objAsig->insert() : $objAsig->delete();
    //$result = $_POST["oper"] == 1 ? $objAsig->insert() : true;
    $oper = $_POST["oper"] == '1' ? 'Ins' : 'Del';

    if ( $result ) {
        $objSys->registroLog($objUsr->idUsr, 'logtbl_arm_asignacion', $objAsig->matricula, $oper);
        //datos para el historial
        $objHist->matricula     = $_POST["matricula"];
        $objHist->curp          = $_POST["curp"];
        $objHist->id_operacion  = $_POST["oper"];
        //$objHist->oficio      = '';
        //$objHist->fecha       = '';
        $objHist->usuario       = $objUsr->idUsr;
        
        //se termina la transaccion
        if( $objHist->insert() ){
            $res = 'Exito';
            $ajx_datos['rslt']  = true; 
           $conexBD->commit();
        }else{
            $res = 'Fail!';
            $ajx_datos['rslt']  = false; 
            $conexBD->rollBack();
        }
        //$ajx_datos['res']  = $res;
        //$ajx_datos['rslt']  = true;   
        //$ajx_datos['rslt']  = $_POST["matricula"] . " " . $_POST["curp"] . " " . $objUsr->idUsr;                          
        $ajx_datos['error'] = '';
            
    } else {
        //$conexBD->rollBack();
        //$ajx_datos['res']   = 'Tremendo Fail!';
        //$ajx_datos['res']   = $objAsig->matricula . " " . $objAsig->curp;
        //$ajx_datos['res']  = $objAsig->matricula . " " . $objAsig->curp . " " . $objUsr->idUsr . " " . $oper;
        $ajx_datos['rslt']  = false;                     
        $ajx_datos['error'] = $objAsig->msjError;
    }
    
    //$ajx_datos['res']   = $objAsig->delete();
    
    echo json_encode($ajx_datos);
} else {
    $ajx_datos["rslt"] = false;
    //$ajx_datos['rslt']  = $_POST["matricula"] . $_POST["curp"]; 
    $ajx_datos["error"] = "Error de Sesi�n...";
    echo json_encode($ajx_datos);
}
?>