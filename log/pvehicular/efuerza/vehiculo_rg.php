<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
session_start();
if (isset($_SESSION['admitted_xsisp'])) { 
$path = '';
require_once $path . 'includes/class/config/mysql.class.php';
include_once $path . 'includes/class/logtbl_vehiculos.class.php';
include_once $path . 'includes/class/logtbl_veh_datos.class.php';
include_once $path . 'includes/class/logtbl_veh_detalles.class.php';


$conexBD        = new MySQLPDO();
$objVeh         = new LogtblVehiculos();
$objDat         = new LogtblVehDatos();
$objDet         = new LogtblVehDetalles();


//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo_rg.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Registro');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    // caracteristicas del vehiculo
    $objVeh->id_vehiculo        = 0;
    $objVeh->num_serie          = $_POST["txtNumSerie"];
    $objVeh->num_economico      = $_POST["txtNumEco"];
    $objVeh->num_motor          = $_POST["txtNumMotor"];
    $objVeh->poliza             = $_POST["txtPoliza"];
    $objVeh->inciso             = $_POST["txtInciso"];
    $objVeh->placas             = $_POST["txtPlacas"];
    $objVeh->tarjeta_circulacion= $_POST["txtTarjetaCir"];
    $objVeh->reg_fed_veh        = $_POST["txtRegFedVeh"];
    $objVeh->num_puertas        = $_POST["txtNumPuertas"];
    $objVeh->num_placas         = $_POST["rbnNumPlacas"];
    $objVeh->num_cilindros      = $_POST["txtNumCilindros"];
    $objVeh->foto_frente        = !empty ( $_POST["hdnFotoFrente"] ) ? ( $objVeh->getIdUltimoVehiculo() + 1 ) . "_frente.jpg" : '';
    $objVeh->foto_lat_der       = !empty ( $_POST["hdnFotoDer"] ) ? ( $objVeh->getIdUltimoVehiculo() + 1 ) . "_derecha.jpg" : '';
    $objVeh->foto_lat_izq       = !empty ( $_POST["hdnFotoIzq"] ) ? ( $objVeh->getIdUltimoVehiculo() + 1 ) . "_izquierda.jpg" : '';
    $objVeh->foto_posterior     = !empty ( $_POST["hdnFotoPost"] ) ? ( $objVeh->getIdUltimoVehiculo() + 1 ) . "_posterior.jpg" : ''; 
    $objVeh->foto_interior      = !empty ( $_POST["hdnFotoInt"] ) ? ( $objVeh->getIdUltimoVehiculo() + 1 ) . "_interior.jpg" : '';
    $objVeh->id_estado_fisico   = $_POST["cbxEdoFisico"];
    $objVeh->id_situacion       = $_POST["cbxSituacion"];
    $objVeh->id_modelo          = $_POST["cbxModelo"];
	$objVeh->id_transmision     = $_POST["cbxTransmision"];
	$objVeh->id_color           = $_POST["cbxColor"];
	//$objVeh->id_categoria       = $_POST["cbxCategoria"];
	//$objVeh->id_marca           = $_POST["cbxMarca"];
	$objVeh->id_tipo            = $_POST["cbxTipo"];
    
    // Inicia la transacci�n    
    $conexBD->beginTransaction();    
    //-------------------------------------------------------------------//            
    if ( $id_vehiculo = $objVeh->insert() ){
        $objDat->id_vehiculo = $id_vehiculo;
        if ( $objDat->insert() ) {
            $objDet->id_vehiculo = $id_vehiculo;
            if ( $objDet->insert() ) { 
                //fotos del vehiculo
                $path_upload = 'log/_uploadfiles/';
                // Se pasan los archivos de im�genes a la carpeta correspondiente
                $path_fotos = 'log/pvehicular/fotos/';
                if( !empty($objVeh->foto_frente) ){
                    copy($path_upload . $_POST["hdnFotoFrente"], $path_fotos . $objVeh->foto_frente);
                    unlink( $path_upload . $_POST["hdnFotoFrente"] );                    
                }
                if( !empty($objVeh->foto_lat_der) ){
                    copy($path_upload . $_POST["hdnFotoDer"], $path_fotos . $objVeh->foto_lat_der);
                    unlink($path_upload . $_POST["hdnFotoDer"]);
                }
                if( !empty($objVeh->foto_lat_izq) ){
                    copy($path_upload . $_POST["hdnFotoIzq"], $path_fotos . $objVeh->foto_lat_izq);
                    unlink($path_upload . $_POST["hdnFotoIzq"]);
                }
                if( !empty($objVeh->foto_posterior) ){
                    copy($path_upload . $_POST["hdnFotoPost"], $path_fotos . $objVeh->foto_posterior);
                    unlink($path_upload . $_POST["hdnFotoPost"]);
                }
                if( !empty($objVeh->foto_interior) ){
                    copy($path_upload . $_POST["hdnFotoInt"], $path_fotos . $objVeh->foto_interior);
                    unlink($path_upload . $_POST["hdnFotoInt"]);
                }
                $objSys->registroLog($objUsr->idUsr, 'logtbl_vehiculos', $id_vehiculo, "Ins");
                $objSys->registroLog($objUsr->idUsr, 'logtbl_veh_datos', $id_vehiculo, "Ins");
                $objSys->registroLog($objUsr->idUsr, 'logtbl_veh_detalles', $id_vehiculo, "Ins");                    
                $conexBD->commit();
                $error = '';
            }
            else {
                $conexBD->rollBack();
                $error = (!empty($objDet->msjError)) ? $objDet->msjError : 'Error al guardar los detalles.';
            }
        }
        else {
            $conexBD->rollBack();
            $error = (!empty($objDat->msjError)) ? $objDat->msjError : 'Error al guardar los datos.';
        }
    }
    else {
        $conexBD->rollBack();
        $error = (!empty($objVeh->msjError)) ? $objVeh->msjError : 'Error al guardar las caracteristicas.';        
    }
    
    $mod = (empty($error)) ? $objSys->encrypt("index") : $objSys->encrypt("vehiculo_add"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
}
?>