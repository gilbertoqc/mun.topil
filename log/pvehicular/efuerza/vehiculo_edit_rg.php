<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
$path = '';
require_once $path . 'includes/class/config/mysql.class.php';
include_once $path . 'includes/class/logtbl_vehiculos.class.php';
include_once $path . 'includes/class/logtbl_veh_datos.class.php';
include_once $path . 'includes/class/logtbl_veh_detalles.class.php';


$conexBD        = new MySQLPDO();
$objVeh         = new LogtblVehiculos();
$objDat         = new LogtblVehDatos();
$objDet         = new LogtblVehDetalles();


//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo_edit_rg.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Registro');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    // caracteristicas del vehiculo--------------------------------
    $objVeh->id_vehiculo        = $objSys->decrypt( $_POST["hdnIdVehiculo"] );    
    $objVeh->num_serie          = $_POST["txtNumSerie"];
    $objVeh->num_economico      = $_POST["txtNumEco"];
    $objVeh->num_motor          = $_POST["txtNumMotor"];
    $objVeh->poliza             = $_POST["txtPoliza"];
    $objVeh->inciso             = $_POST["txtInciso"];
    $objVeh->placas             = $_POST["txtPlacas"];
    $objVeh->num_placas         = empty( $_POST["rbnNumPlacas"] ) ? null :  $_POST["rbnNumPlacas"];
    $objVeh->tarjeta_circulacion= $_POST["txtTarjetaCir"];
    $objVeh->reg_fed_veh        = $_POST["txtRegFedVeh"];
    $objVeh->num_puertas        = empty( $_POST["txtNumPuertas"] ) ? null :  $_POST["txtNumPuertas"];
    $objVeh->num_cilindros      = empty( $_POST["txtNumCilindros"] ) ? null :  $_POST["txtNumCilindros"];
    $objVeh->foto_frente        = !empty ( $_POST["hdnFotoFrente"] ) ? $objVeh->id_vehiculo . "_frente.jpg" : '';
    $objVeh->foto_lat_der       = !empty ( $_POST["hdnFotoDer"] ) ? $objVeh->id_vehiculo . "_derecha.jpg" : '';
    $objVeh->foto_lat_izq       = !empty ( $_POST["hdnFotoIzq"] ) ? $objVeh->id_vehiculo . "_izquierda.jpg" : '';
    $objVeh->foto_posterior     = !empty ( $_POST["hdnFotoPost"] ) ? $objVeh->id_vehiculo . "_posterior.jpg" : ''; 
    $objVeh->foto_interior      = !empty ( $_POST["hdnFotoInt"] ) ? $objVeh->id_vehiculo . "_interior.jpg" : '';
    $objVeh->id_estado_fisico   = empty( $_POST["cbxEdoFisico"] ) ? null :  $_POST["cbxEdoFisico"];
    $objVeh->id_situacion       = empty( $_POST["cbxSituacion"] ) ? null :  $_POST["cbxSituacion"];
    $objVeh->id_modelo          = empty( $_POST["cbxModelo"] ) ? null :  $_POST["cbxModelo"];
	$objVeh->id_transmision     = empty( $_POST["cbxTransmision"] ) ? null :  $_POST["cbxTransmision"];
	$objVeh->id_color           = empty( $_POST["cbxColor"] ) ? null :  $_POST["cbxColor"];
	//$objVeh->id_categoria       = $_POST["cbxCategoria"];
	//$objVeh->id_marca           = $_POST["cbxMarca"];
	$objVeh->id_tipo            = $_POST["cbxTipo"];
    //datos particulares (resguardo)--------------------------------
    $objDat->fecha_res          = $objSys->convertirFecha( $_POST["txtFechaRes"], "yyyy-mm-dd" );
    //$objDat->fecha_res        = $_POST["txtFechaReg"];
    $objDat->id_municipio       = empty( $_POST["cbxMunicipio"] ) ? null :  $_POST["cbxMunicipio"];
    $objDat->curp               = empty( $_POST["hdnCurp"] ) ? null :  $_POST["hdnCurp"];
    $objDat->id_operatividad    = empty( $_POST["cbxOperatividad"] ) ? null :  $_POST["cbxOperatividad"];
    $objDat->id_ubicacion       = empty( $_POST["cbxUbicacion"] ) ? null :  $_POST["cbxUbicacion"];
    //detalles del vehiculo     ------------------------------------
    $objDet->kilometraje           = empty( $_POST["txtKm"] ) ? null :  $_POST["txtKm"];
    $objDet->espejo_der            = $_POST["chkEspejoDer"];
    $objDet->espejo_izq            = $_POST["chkEspejoIzq"];
    $objDet->espejo_estado         = $_POST["rbnEspejosEst"];
    $objDet->cuarto_der            = $_POST["chkCuartoDer"];
    $objDet->cuarto_izq            = $_POST["chkCuartoIzq"];
    $objDet->cuarto_estado         = $_POST["rbnCuartosEst"];
    $objDet->fanal_der             = $_POST["chkFanalDer"];
    $objDet->fanal_izq             = $_POST["chkFanalIzq"];
    $objDet->fanal_estado          = $_POST["rbnFanalesEst"];
    $objDet->calavera_der          = $_POST["chkCalaveraDer"];
    $objDet->calavera_izq          = $_POST["chkCalaveraIzq"];
    $objDet->calavera_estado       = $_POST["rbnCalaverasEst"];
    $objDet->moldura_der           = $_POST["chkMolduraDer"];
    $objDet->moldura_izq           = $_POST["chkMolduraIzq"];
    $objDet->moldura_del           = $_POST["chkMolduraDel"];
    $objDet->moldura_tras          = $_POST["chkMolduraTras"];
    $objDet->emblema_der           = $_POST["chkEmblemaDer"];
    $objDet->emblema_izq           = $_POST["chkEmblemaIzq"];
    $objDet->emblema_del           = $_POST["chkEmblemaDel"];
    $objDet->emblema_tras          = $_POST["chkEmblemaTras"];
    $objDet->pintura               = $_POST["rbnPinturaEst"];
    $objDet->faros_antiniebla      = $_POST["rbnFarosAntN"];
    $objDet->limpiadores           = $_POST["rbnLimpiadores"];
    $objDet->limpiadores_estado    = $_POST["rbnLimpiadorEst"];
    $objDet->espejo_interior       = $_POST["rbnEspejoInt"];
    $objDet->espejo_interior_estado= $_POST["rbnEspejoIntEst"];
    $objDet->encendedor            = $_POST["rbnEncendedor"];
    $objDet->cinturon              = $_POST["rbnCinturon"];
    $objDet->tapetes               = $_POST["rbnTapetes"];
    $objDet->disco_compacto        = $_POST["rbnCD"];
    $objDet->antena                = $_POST["rbnAntena"];
    $objDet->manijas               = $_POST["rbnManijas"];
    $objDet->cenicero              = $_POST["rbnCenicero"];
    $objDet->claxon                = $_POST["rbnClaxon"];
    $objDet->radio_amfm            = $_POST["rbnRadio"];
    $objDet->vestiduras            = $_POST["rbnVestiduras"];
    $objDet->otros                 = $_POST["txtOtros"];
    //herramientas
    $objDet->gato                  = $_POST["rbnGato"];
    $objDet->maneral               = $_POST["rbnManeral"];
    $objDet->reflejantes           = $_POST["rbnReflejantes"];
    $objDet->cable_pc              = $_POST["rbnCable"];
    $objDet->llave_espanola        = $_POST["rbnLlavees"];
    $objDet->desarmador_plano      = $_POST["rbnDesPlano"];
    $objDet->extintor              = $_POST["rbnExtintor"];
    $objDet->llave_cruz            = $_POST["rbnLlaveCruz"];
    $objDet->llave_l               = $_POST["rbnLlavel"];
    $objDet->pinzas                = $_POST["rbnPinzas"];
    $objDet->llave_allen           = $_POST["rbnLlaveAllen"];
    $objDet->desarmador_cruz       = $_POST["rbnDesCruz"];
    $objDet->llave_bujia           = $_POST["rbnLlaveBujia"];
    $objDet->tapones               = $_POST["rbnTapones"];
    $objDet->rines_acero           = $_POST["rbnRinesAC"];
    $objDet->torreta               = $_POST["rbnTorreta"];
    $objDet->torreta_estado        = $_POST["rbnTorretaEST"];
    $objDet->radio_movil           = $_POST["rbnRadioM"];
    $objDet->radio_movil_estado    = $_POST["rbnRadioMEST"];
    $objDet->alto_parlante         = $_POST["rbnParlante"];
    $objDet->alto_parlante_estado  = $_POST["rbnParlanteEST"];
    $objDet->protector_frontal     = $_POST["rbnProtFrontal"];
    $objDet->lona                  = $_POST["rbnLona"];
    $objDet->rin_aluminio          = $_POST["rbnRinesAL"];
    $objDet->aa                    = $_POST["rbnAA"];
    $objDet->roll_bar              = $_POST["rbnRollbar"];
    $objDet->redilas               = $_POST["rbnRedilas"];
    //datos de llantas
    $objDet->llantas_cantidad      = empty( $_POST["txtLlantasCant"] ) ? null :  $_POST["txtLlantasCant"];
    $objDet->id_medida_llantas     = empty( $_POST["cbxMedidaLlantas"] ) ? null :  $_POST["cbxMedidaLlantas"];
    $objDet->id_marca_llantas      = empty( $_POST["cbxMarcaLlantas"] ) ? null :  $_POST["cbxMarcaLlantas"];
    $objDet->llantas_vida_util     = $_POST["txtLlantasVU"];
    
    
    
    // Inicia la transacci�n    
    $conexBD->beginTransaction();  
    //-------------------------------------------------------------------//            
    if ( $objVeh->update() ){
        $objDat->id_vehiculo = $objVeh->id_vehiculo;
        if ( $objDat->update() ) {
            $objDet->id_vehiculo = $objVeh->id_vehiculo;
            if ( $objDet->update() ) {   
                //fotos del vehiculo
                $path_upload = 'log/_uploadfiles/';
                // Se pasan los archivos de im�genes a la carpeta correspondiente
                $path_fotos = 'log/pvehicular/fotos/';
                if( !empty($objVeh->foto_frente) ){
                    copy($path_upload . $_POST["hdnFotoFrente"], $path_fotos . $objVeh->foto_frente);
                    unlink( $path_upload . $_POST["hdnFotoFrente"] );                    
                }
                if( !empty($objVeh->foto_lat_der) ){
                    copy($path_upload . $_POST["hdnFotoDer"], $path_fotos . $objVeh->foto_lat_der);
                    unlink($path_upload . $_POST["hdnFotoDer"]);
                }
                if( !empty($objVeh->foto_lat_izq) ){
                    copy($path_upload . $_POST["hdnFotoIzq"], $path_fotos . $objVeh->foto_lat_izq);
                    unlink($path_upload . $_POST["hdnFotoIzq"]);
                }
                if( !empty($objVeh->foto_posterior) ){
                    copy($path_upload . $_POST["hdnFotoPost"], $path_fotos . $objVeh->foto_posterior);
                    unlink($path_upload . $_POST["hdnFotoPost"]);
                }
                if( !empty($objVeh->foto_interior) ){
                    copy($path_upload . $_POST["hdnFotoInt"], $path_fotos . $objVeh->foto_interior);
                    unlink($path_upload . $_POST["hdnFotoInt"]);
                }
                $objSys->registroLog($objUsr->idUsr, 'logtbl_vehiculos', $id, "Upd");
                $objSys->registroLog($objUsr->idUsr, 'logtbl_veh_datos', $id, "Upd");
                $objSys->registroLog($objUsr->idUsr, 'logtbl_veh_detalles', $id, "Upd");                    
                $conexBD->commit();
                $error = '';
            }
            else {
                $conexBD->rollBack();
                $error = (!empty($objDet->msjError)) ? $objDet->msjError : 'Error al guardar los detalles.';
            }
        }
        else {
            $conexBD->rollBack();
            $error = (!empty($objDat->msjError)) ? $objDat->msjError : 'Error al guardar los datos.';
        }
    }
    else {
        $conexBD->rollBack();
        $error = (!empty($objVeh->msjError)) ? $objVeh->msjError : 'Error al guardar las caracteristicas.';        
    }
    
    $mod = (empty($error)) ? $objSys->encrypt("index") : $objSys->encrypt("vehiculo_edit_rg"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>