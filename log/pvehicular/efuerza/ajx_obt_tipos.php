<?php
/**
 * Complemento del llamado ajax para listar los tipos de vehiculo dentro de un combobox. 
 * @param int id_clasificacion e int id_marca, recibido por el m�todo GET.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    
    $path = '../../../';
    include $path . 'includes/class/logcat_veh_tipo.class.php';
    $objTipo = new LogcatVehTipo();
    
    $datos = '<option value="0"></option>';
    $datos .= $objTipo->shwTipos(0, $_GET['id_clasificacion']);
    if (empty($objTipo->msjError)) {
        $ajx_datos['rslt']  = true;
        $ajx_datos['html']  = utf8_encode($datos);
        $ajx_datos['error'] = '';
    }    
    else {
        $ajx_datos['rslt']  = false;
        $ajx_datos['html']  = '';
        $ajx_datos['error'] = $objTipo->msjError;        
    }
    
    echo json_encode($ajx_datos);
}
else {
    echo "Error de Sesi�n...";
}
?>