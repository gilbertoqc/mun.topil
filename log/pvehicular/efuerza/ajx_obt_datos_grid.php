<?php
/**
 * Complemento del llamado ajax para listar los registros de la tabla datos personales en el grid.
 * Lista de par�metros recibidos por GET 
 * @param string txtSearch, contiene el texto especificado por el usuario para una b�squeda.
 * @param int colSort, contiene el �ndice de la columna por la cual se ordenar�n los datos.
 * @param string typeSort, define el tipo de ordenaci�n de los datos: ASC o DESC.
 * @param int rowStart, especifica el n�mero de fila por la cual inicia la paginaci�n del grid.
 * @param int rowsDisplay, especifica la cantidad de filas que se mostrar�n en cada p�gina del grid.
 */
session_start();
if (isset($_SESSION['admitted_xsisp'])) {
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    include $path . 'includes/class/config/config.cfg.php';
    include $path . 'includes/class/config/system.class.php';
    include $path . 'includes/class/logtbl_vehiculos.class.php';
    $objSys = new System();
    $objDatVeh = new LogtblVehiculos();

    //--------------------- Recepci�n de par�metros --------------------------//
    // B�squeda...
    $sql_where = '';
    $sql_values = null;
    if (!empty($_GET["txtSearch"])) {
        $sql_where = ' ( cla.clasificacion LIKE ? '
                   . ' OR mar.marca LIKE ? '
                   . ' OR tip.tipo LIKE ? '
                   . ' OR v.placas LIKE ? '
                   . ' OR v.num_serie LIKE ? ) ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%');
    }
    
    /*
    if (!empty($_GET["txtSearch"])) {
        $sql_where = ' ( cla.clasificacion LIKE ? '
                   . ' OR mar.marca LIKE ? '
                   . ' OR tip.tipo LIKE ? '
                   . ' OR v.placas LIKE ? '
                   . ' OR v.num_serie LIKE ? ) ';
        $sql_values = array('%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%',
                            '%' . $_GET['txtSearch'] .'%');
    }
    */
    
    // Ordenaci�n...
    $sql_order = '';
    $ind_campo_ord = $_GET['colSort'];
    if ($ind_campo_ord == 1) {
        $sql_order = 'cla.clasificacion ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 2) {
        $sql_order = 'mar.marca ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 3) {
        $sql_order = 'tip.tipo ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 4) {
        $sql_order = 'v.placas ' . $_GET['typeSort'];
    } else if ($ind_campo_ord == 5) {
        $sql_order = 'v.num_serie ' . $_GET['typeSort'];
    }   
    // Paginaci�n...    
    $sql_limit = $_GET["rowStart"] . ', ' . $_GET["rowsDisplay"];    
    
    // Nombre del grid...
    $nombreGrid = $_GET["IdGrid"];    
    //------------------------------------------------------------------------//
       
    $datos = $objDatVeh->selectAllGrid($sql_where, $sql_values, $sql_order, $sql_limit);
    $totalReg = $datos["total"];
    
    $html = '';
    if ($totalReg > 0) {
        $html = '<table class="xGrid-tbBody" id="' . $nombreGrid . '-tbBody">';
        foreach ($datos["datos"] As $reg => $dato) {
            $id_crypt = $objSys->encrypt($dato["id_vehiculo"]);
            $html .= '<tr id="' . $nombreGrid . '-' . $dato["id_vehiculo"] . '">';
       			//--------------------- Impresion de datos ----------------------//                
                $html .= '<td style="text-align: center; width: 13%;">' . $dato["num_serie"] . '</td>';
                $html .= '<td style="text-align: center; width: 10%;">' . $dato["placas"] . '</td>';  
                $html .= '<td style="text-align: center; width: 14%;">' . $dato["num_economico"] . '</td>';              
                $html .= '<td style="text-align: center; width: 18%;">' . $dato["clasificacion"] . '</td>';               
                $html .= '<td style="text-align: center; width: 18%;">' . $dato["marca"] . '</td>';
                $html .= '<td style="text-align: center; width: 15%;">' . $dato["tipo"] . '</td>';				
$url_edit = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("vehiculo_edit") . "&id=" . $id_crypt;
$url_sin  = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("vehiculo_sin") . "&id=" . $id_crypt;
$url_baja = "index.php?m=" . $_SESSION["xIdMenu"] . "&mod=" . $objSys->encrypt("vehiculo_baja") . "&id=" . $id_crypt;
                $html .= '<td style="text-align: center; width: 12%;">';
                $html .= '  <a href="' . $url_edit . '" class="lnkBtnOpcionGrid" title="Actualizar los datos..." ><img src="' . PATH_IMAGES . 'icons/edit_dat24.png" alt="modificar" /></a>';
                $html .= '  <a href="' . $url_sin . '" class="lnkBtnOpcionGrid" title="Vehiculo siniestrado..." ><img src="' . PATH_IMAGES . 'icons/sin24.png" alt="siniestrado" /></a>';
                $html .= '  <a href="' . $url_baja . '" class="lnkBtnOpcionGrid" title="Dar de baja..." ><img src="' . PATH_IMAGES . 'icons/delete24.png" alt="baja" /></a>';
                $html .= '</td>';
             	//---------------------------------------------------------------//
       		$html .= '</tr>';  		
         }
         $html .= '</table>';
   	} else if ($totalReg == 0) {
        $html = '<span style="color: #ff0000; display: block; padding: 5px; text-align: center; width: 100%;">';
       		//$html .= 'No se encontraron vehiculos en esta consulta...' . $_GET['txtSearch'] . " " . $datos["total"];
            $html .= 'No se encontraron vehiculos en esta consulta...';
        $html .= '</span>';
    } else {        
        $html = '<p>ERROR: ' . $datos["error"] . '</p>';
    }
    
    // Formatea los datos y los envia al grid...
    $ajx_datos["total"] = $totalReg;
    $ajx_datos["html_dat"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
} else {
    echo "Error de Sesi�n...";
}
?>