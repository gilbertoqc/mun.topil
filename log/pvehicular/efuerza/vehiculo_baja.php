<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
include 'includes/class/logtbl_vehiculos.class.php';
include 'includes/class/logtbl_veh_baja.class.php';
include 'includes/class/logcat_veh_motivo_baja.class.php';

$objVeh			= new LogtblVehiculos();
$objBaja		= new LogtblVehBaja();
$objMBaja		= new LogcatVehMotivoBaja();

//se obtiene el id del vehiculo enviado por get encryptado
$id	= $objSys->decrypt( $_GET["id"] );
//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="includes/js/maskedinput-1.3.min.js"></script>',
                                   '<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo_baja.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => '');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla...
$plantilla->paginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
$url_cancel = "index.php?m=" . $_SESSION["xIdMenu"];
$urlSave = "index.php?m=" . $_SESSION['xIdMenu'] . '&mod=' . $objSys->encrypt('vehiculo_baja_rg');
    ?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: left; width: 70%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
                <td style="text-align: right; width: 30%;">
                    <a href="#" id="btnGuardar" class="Tool-Bar-Btn gradient" style="width: 110px;" title="Guardar los datos del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/ok24.png" alt="" style="border: none;" /><br />Guardar
                </a>
                <a href="<?php echo $url_cancel?>" id="btnCancelar" class="Tool-Bar-Btn gradient" style="margin-left: 20px; width: 90px;" title="Cancelar la alta del nuevo elemento...">
                    <img src="<?php echo PATH_IMAGES;?>icons/cancel24.png" alt="" style="border: none;" /><br />Cancelar
                </a>
                </td>
            </tr>
        </table>
    </div>
    <form id="frmRegistro" method="post" action="<?php echo $urlSave;?>" enctype="multipart/form-data">
        <div id="dvForm-Persona" class="dvForm-Data" style="margin-top: 10px; margin-bottom: 60px; text-align: left; width: 850px;">
            <span class="dvForm-Data-pTitle"><img src="<?php echo PATH_IMAGES;?>icons/circle_black.png" style="border: none; margin-right: 7px; vertical-align: middle;" />Datos del vehiculo como baja</span>                                
            <div id="tabsForm" style="
            margin: auto auto; margin-top: 10px; min-height: 150px; width: auto;">                  
                       
                <!-- caracteristicas -->                                                                   
                <table id="tbForm-DatPer" class="tbForm-Data" style="width: 600px;">             
                    <tr>
                        <td><label for="txtNumBaja">N&uacute;mero de lote:</label></td>
                        <td class="validation">
                            <input type="text" name="txtNumLote" id="txtNumLote" value="" maxlength="15" title="..." style="width: 125px;" />
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="cbxMotivoBaja">Motivo de baja:</label></td>
                        <td class="validation">
                            <select name="cbxMotivoBaja" id="cbxMotivoBaja">
                                <option value="0">&nbsp;</option>
                                <?php
                                echo $objMBaja->shwMotivoBaja();
                                ?>
                            </select>
                            <span class="pRequerido">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtFechaBaja">Fecha de baja:</label></td>
                        <td class="validation">
                            <input type="text" name="txtFechaBaja" id="txtFechaBaja" value="" readonly="true" title="dd/mm/aaaa" placeholder="dd/mm/aaaa" style="text-align: center; width: 120px;" />
                            <span class="pRequerido">*</span>
                        </td>
                        
                    </tr>      
                </table>
                                                  
            <p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>
        </div>        
  <input type="hidden" name="dtTypeOper" value="1" />
  <input type="hidden" name="hdnIdVehiculo" value="<?php echo $id; ?>" />
  <input type="hidden" name="dtTypeOper" value="<?php echo $e; ?>" />
    </form>
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->paginaFin();
?>