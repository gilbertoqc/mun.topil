<?php
//-----------------------------------------------------------------//
//-- Bloque de inclusi�n de las clases...
//-----------------------------------------------------------------//
$path = '';
require_once $path . 'includes/class/config/mysql.class.php';
include_once $path . 'includes/class/logtbl_veh_mantenimientos.class.php';


$conexBD    = new MySQLPDO();
$objMtto    = new LogtblVehMantenimientos();


//-----------------------------------------------------------------//
//-- Bloque de definici�n de par�metros para la plantilla...
//-----------------------------------------------------------------//
$params = array('titulo' => 'SISP :: Parque Vehicular',
                'usr' => $_SESSION['xlogin_id_sisp'],
                'scripts' => array('<script type="text/javascript" src="log/pvehicular/_js/efuerza/vehiculo_rg.js"></script>'),                
                'header' => true,
                'menu' => false,
                'idMenu' => $_SESSION['xIdMenu'],
                'textMod' => 'Registro');
//-- Se crea la clase de la plantilla...
$plantilla = new Plantilla($params);
//-- Se genera genera y muestra la estructura de la plantilla....
$plantilla->PaginaInicio();

//-----------------------------------------------------------------//
//-- Bloque de contenido din�mico...
//-----------------------------------------------------------------//
?>
    <div id="dvTool-Bar" class="dvTool-Bar" style="margin: auto auto; margin-top: 1px; width: auto;">
        <table style="width: 100%;">
            <tr>               
                <td style="text-align: left; width: 100%;">
                    <?php $plantilla->mostrarNombreModulo();?>
                </td>
            </tr>
        </table>
    </div>
    <?php
    // Se asignan los valores a los campos
    // caracteristicas del vehiculo
    $objMtto->id_vehiculo        = $objSys->decrypt( $_POST["hdnIdVehiculo"] );
    $objMtto->fecha_ini          = $objSys->convertirFecha( $_POST["txtFechaIni"], "yyyy-mm-dd" );
    $objMtto->fecha_fin          = $objSys->convertirFecha( $_POST["txtFechaFin"], "yyyy-mm-dd" );
    $objMtto->detalles           = $_POST["txtDetalles"];
    $objMtto->nom_resp           = $_POST["txtNomResp"];
    $objMtto->rfc_curp           = $_POST["txtRfcCurp"];
    $objMtto->id_municipio       = empty( $_POST["cbxMunicipio"] ) ? null : $_POST["cbxMunicipio"];
    $objMtto->domicilio          = $_POST["txtDomicilio"];
    $objMtto->razon_social       = $_POST["txtRazonSocial"];
    $objMtto->resp_taller        = $_POST["txtRespTaller"];
    $objMtto->kilometraje_total  = $_POST["txtKmTotal"];
    $objMtto->id_tipo_mtto       = empty( $_POST["cbxTipoMtto"] ) ? null : $_POST["cbxTipoMtto"];
    $objMtto->id_tipo_servicio   = empty( $_POST["cbxTipoServicio"] ) ? null : $_POST["cbxTipoServicio"];
    echo $objMtto->id_vehiculo;
        
    // Inicia la transacci�n    
    $conexBD->beginTransaction();    
    //-------------------------------------------------------------------//            

    if ( $objMtto->insert() ) { 
        $objSys->registroLog($objUsr->idUsr, 'logtbl_veh_mantenimientos', $id_mtto, "Ins");                    
        $conexBD->commit();
        $error = '';
    }else{
        $conexBD->rollBack();
        $error = (!empty($objMtto->msjError)) ? $objMtto->msjError : 'Error al guardar los datos del mantenimiento.';        
    }
    
    $mod = (empty($error)) ? $objSys->encrypt("index") : $objSys->encrypt("mtto_add"); 
    ?>
    <input type="hidden" id="hdnError" value="<?php echo $error;?>" />
    <input type="hidden" id="hdnUrl" value="<?php echo "m=" . $_SESSION["xIdMenu"] . "&mod=" . $mod;?>" />
<?php
//-----------------------------------------------------------------//
//-- Bloque de cerrado de la plantilla...
//-----------------------------------------------------------------//
$plantilla->PaginaFin();
?>