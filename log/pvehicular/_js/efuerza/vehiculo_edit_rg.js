$(document).ready(function(){ 
    // Inicializaci�n de los tabs
    $('#tabsForm').tabs({
        disabled: [1,2,3,4]
    });            
	    
	if ($('#hdnError').val() == '') {
        //-- Mensaje...        
        var msj = $(getHTMLMensaje('En hora buena !!! los datos del vehiculo se actualizaron correctamente.', 1));
        msj.dialog({
            autoOpen: true,                        
            minWidth: 400,
            resizable: false,
            modal: true,
            buttons:{
                'Aceptar': function(){
                    location.href = "index.php";
                }
            }
        });
    }
    else {
        //-- Error...
        var msj = $(getHTMLMensaje('Ohh, al parecer ha ocurrido un error !!!... Detalles:<br />' + $('#hdnError').val(), 3));
        msj.dialog({
            autoOpen: true,                        
            minWidth: 450,
            resizable: false,
            modal: true,
            buttons:{
                'Volver a intentarlo': function(){
                    location.href = "index.php?" + $('#hdnUrl').val();
                },
                'Cancelar el proceso': function(){
                    location.href = "index.php";
                }
            }
        });
    }
		
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida)
            $('#frmRegistro').submit();
    });  
	  
});
