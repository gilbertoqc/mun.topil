var xGrid;
$(document).ready(function(){
    //-- Ajusta el tama�o del contenedor del grid...
    $('#dvGrid-Vehiculos').css('height', ($(window).height() - 250) + 'px');
    //-- Crea e inicializa el control del grid principal...
    xGrid = $('#dvGrid-Vehiculos').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlDatos').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });
    //-- Crea e inicializa el control del grid para bajas de vehiculos...
    xGrid2 = $('#dvGrid-LstBajas').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlObtLstBajas').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });
    //-- Crea e inicializa el control del gridpara vehiculos vehiculos...
    xGrid3 = $('#dvGrid-LstSin').xGrid({
        xColSort: 1,
        xTypeSort: 'Asc',
        xUrlData: xDcrypt($('#hdnUrlObtLstSin').val()),
        xLoadDataStart: 1,
        xTypeDataAjax: 'json'
    });
    
    //-- Definici�n del dialog para visualizar la lista de bajas de vehiculos....
    $('#dvLstBajas').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 930,
        height: 500,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Definici�n del dialog para visualizar la lista de vehiculos siniestrados....
    $('#dvLstSin').dialog({
        autoOpen: false,
        closeOnEscape: false,
        closeText: 'Cerrar',
        width: 930,
        height: 500,
        modal: true,
        resizable: false,
        buttons:{            
            '-- Cerrar --': function(){                
                $(this).dialog('close');
            }
        }
    });
    
    //-- Controla el bot�n para visualizar la lista de bajas de equipamento...
    $("#btnLstBajas").live("click", function(e){
        xGrid2.refreshGrid(); 
        $('#dvLstBajas').dialog('open'); 
    }); 
    //-- Controla el bot�n para visualizar la lista de vehiculos siniestrados...
    $("#btnLstSin").live("click", function(e){
        xGrid3.refreshGrid(); 
        $('#dvLstSin').dialog('open'); 
    }); 
    
    //-- Definici�n del dialog como formulario de registro general...
    $('#dvForm-Registro').dialog({
        autoOpen: false,
        width: 600,
        modal: true,
        resizable: false,
        buttons:{
            'Aceptar': function(){                
                if( setRegistro() ){
                    $(this).dialog('close');
                    //-- Mensaje...
                    var msj = $(getHTMLMensaje('En hora buena !!! el proceso se realiz� correctamente.', 1));
                    msj.dialog({
                        autoOpen: true,                        
                        minWidth: 350,
                        resizable: false,
                        modal: true,
                        buttons:{
                            'Aceptar': function(){
                                $(this).dialog('close');
                                location.reload();
                            }
                        }
                    });
                }
            },
            'Cancelar': function(){                
                $(this).dialog('close');
            }
        },
        close: function(evt){
            clearFormRegistro();
        }
    });

});