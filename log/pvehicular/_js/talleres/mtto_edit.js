$(document).ready(function(){ 
    // Barra de opciones flotante
    var barraOp = $('#dvBarraOpciones');
    var topBar = $(window).height() - (barraOp.height() + 7);
    var leftBar = (($(window).width() / 2) - (barraOp.width() / 2));
    barraOp.css({
        top: topBar,
        left: leftBar,
    });
    $(window).resize(function(){
        var topBar = $(this).height() - 60;
        var leftBar = (($(this).width() / 2) - (barraOp.width() / 2));
        barraOp.css({
            top: topBar,
            left: leftBar,
        });
    });
    
	//codigo para la fecha de siniestro
	$('#txtFechaIni').datepicker({
        yearRange: '2010:2030',
    });
	//codigo para la fecha de siniestro
	$('#txtFechaFin').datepicker({
        yearRange: '2010:2030',
    });
       
    // Acci�n del bot�n Guardar
    $('#btnGuardar').click(function(){
        var valida = $('#frmRegistro').validate().form();
        if (valida)
            $('#frmRegistro').submit();
    });    
});
