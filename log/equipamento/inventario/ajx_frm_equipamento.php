<?php
session_start();
if (isset($_SESSION['admitted_xsisp'])) {    
    header('content-type: text/html; charset=iso-8859-1');
    $path = '../../../';
    //-----------------------------------------------------------------//
    //-- Bloque de inclusi�n de las clases...
    //-----------------------------------------------------------------//
    include $path . 'includes/class/logtbl_equipamento.class.php';            
    $objEq = new LogtblEquipamento(); 
    
    //variables recibidas
    $oper = $_POST["oper"]; 
    if( $oper == 1 ){   
        $datos = $objEq->select( $_POST["id_equipamento"] );        
    }

    //-----------------------------------------------------------------//
    //-- Bloque de contenido din�mico...
    //-----------------------------------------------------------------//      
                  
    $html .='<form id="frmAlta" method="post" enctype="multipart/form-data">';    
            $html .='<div id="tabsForm" style="margin: auto auto; margin-top: 10px; min-height: 310px; width: auto;">';                         
                //registro de armamento
                $html .='<table id="tbForm-Alta" class="tbForm-Data" style="width: 680px;">';                     
                    $html .='<tr>';
                        $html .='<td><label for="cbxMarca">Marca:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxMarca" id="cbxMarca" title="">';
                                $html .='<option value="0">&nbsp;</option>';                                
                                $html .= $objEq->LogcatEqMarca->shwMarcas( $objEq->id_marca );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td><label for="cbxTipo">Tipo:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxTipo" id="cbxTipo" title="">';
                                $html .='<option value="0">&nbsp;</option>';
                                $html .= $objEq->LogcatEqTipo->shwTipos( $objEq->id_tipo );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                                        
                    $html .='<tr>';
                        $html .='<td><label for="cbxEntidad">Entidades:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxEntidad" id="cbxEntidad" title="">';
                                $html .='<option value="0">&nbsp;</option>';                                
                                $html .= $objEq->AdmcatEntidades->shwEntidades( $objEq->id_entidad, 1 );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';  
                    $html .='<tr>';
                        $html .='<td><label for="cbxDependencia">Dependencias:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<select name="cbxDependencia" id="cbxDependencia" title="">';
                                $html .='<option value="0">&nbsp;</option>';                                
                                $html .= $objEq->LogcatEqDependencia->shwDependencias( $objEq->id_dependencia );                                
                            $html .='</select>';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                   
                    $html .='<tr>';
                        $html .='<td><label for="txtInventario">Inventario:</label></td>';
                        $html .='<td class="validation" style="width: 400px;">';
                            $html .='<input type="text" name="txtInventario" id="txtInventario" value="' . $objEq->inventario . '" maxlength="12" title="..." style="padding: 5px; width: 210px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                                 
                    $html .='<tr>';
                        $html .='<td><label for="txtSerie">Serie:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<input type="text" name="txtSerie" id="txtSerie" value="' . $objEq->serie . '" maxlength="20" title="..." style="width: 200px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';
                    $html .='<tr>';
                        $html .='<td><label for="txtModelo">Modelo:</label></td>';
                        $html .='<td class="validation">';
                            $html .='<input type="text" name="txtModelo" id="txtModelo" value="' . $objEq->modelo . '" maxlength="20" title="..." style="width: 200px;" />';
                            $html .='<span class="pRequerido">*</span>';
                        $html .='</td>';
                    $html .='</tr>';                    
                $html .='</table>';                                                  
            $html .='</div>';
            $html .='<p style="margin: 15px 1px 5px 20px;">Los campos marcados con "<span class="pRequerido">*</span>" son obligatorios, y no podr� continuar hasta que los complete.</p>';                        
        $html .= '<input type="hidden" id="dtTypeOper" name="dtTypeOper" value="' . $oper . '" />';
        $html .= '<input type="hidden" id="id_equipamento" name="id_equipamento" value="' . $objEq->id_equipamento . '" />';
    $html .='</form>';
  

    // Formatea los datos y los envia al grid...
    $ajx_datos["html"] = utf8_encode($html);    
    echo json_encode($ajx_datos);
}else
    echo "Error de Sesi�n...";
?>