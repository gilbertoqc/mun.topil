<?php
include 'includes/class/config/config.cfg.php';

session_start();
if ($_SESSION["xlogin_id_sisp"] > 0) {    
    include 'includes/class/config/system.class.php';
    include 'includes/class/config/users.class.php';
    include 'includes/class/config/plantilla.class.php';
    $objSys = new System();
    $objUsr = new Usuario();
    
    $_SESSION['xIdMenu'] = ( isset($_GET['m']) ) ? $_GET['m'] : $_SESSION['xIdMenu'];
    switch( $_SESSION['xIdMenu'] ){
        //-- Inicio...
        case 0:            
            include 'start.php';
        break;
        //-- Salir...
        case -1:
            include 'logout.php';
        break;
        default:
            $url = $objSys->getUrlMenu($_SESSION['xIdMenu']);
            if( !isset($_GET['mod']) ){
                if( file_exists($url . 'index.php') )
                    include $url . 'index.php';
                else
                    include 'start.php';
            }
            else
                include $url . $objSys->decrypt($_GET['mod']) . '.php';
        break;
    }
} else {
    header('Location: ' . SERVER_NAME . SUBDOMAIN . 'login.php');
}
?>